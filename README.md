# GENERAL COMMENTS:

1.  These are paths where data is generated, and where data is taken 
    as input.
    Directories like `dir_src*` are inputs, and `dir_dst` and for output.
    Some scripts take as input the output directory of another script.
    These are ordered from top to bottom, from the first script to the 
    last one, which produces the final files used to generate the same 
    plots as in the ICRC 2015 proceeding.
    These are listed in the fashion:

        path/to/script.ext
        dir_input
        dir_output

    and sometimes in parenthesis the actual variable of the directory 
    name in script.ext.
    IMPORTANT: In `path/to/script.ext` you must change the directory names 
    adapted to your needs.


2.  The scripts like `test_2006.py` can be easily systematized to run for 
    several years; and this "massive" version was not necessary for the validations.


3.  The variables `this_dir` in the scripts refer to the repository path.


4.  In order to run the python script, is recommended to use the 
    "last" Anaconda package (version 2.3.0):
    https://www.continuum.io/downloads
    which does not need sudo privileges, and does not replace the 
    system python binary.


5.  the directories for Auger raw data in ccage.in2p3.fr:
    * `$AUGER_DATA/Raid/data/Sd`    # .root for SD histograms
    * `$AUGER_DATA/Raid/monit/Sd`   # mc...root for monitoring data


6.  to sync with the data-repository at ccage.in2p3.fr (replace `USER` conveniently):
    ```bash
    AUGER_DATA=/sps/pauger/Malargue
    # This brings all monitoring files (SD-status flags, and AoP values):
    $ rsync -rvthil --progress <USER>@ccage.in2p3.fr:${AUGER_DATA}/Raid/monit/Sd/2007/*/mc*.root .
    # This brings charge-histograms .root files:
    $ rsync -rvthil --progress <USER>@ccage.in2p3.fr:${AUGER_DATA}/Raid/data/Sd/2015/* .
    ```

7.  In order to compile the code in `histos/src` (need CDAS installed), you can:
    ```bash
    $ cd [repository dir]/histos/cmt
    $ make clean
    $ cmt config
    $ make
    ```
    This is supposed to generate (among several things) the binary `readmc-scaler3.exe` inside the directory `$HOME/CDAS/MoIO/v5r6/Linux-x86_64/` (or in `$AUGER_REPO/histos/Linux-x86_64/`).
    Such binary will be used by [this](build_mc/test_old.sh) script; which is called in early processing stages (see Sections "Chain of calls for ..." below).


8.  weather data from CLF is included in `build_pressure.corr/data_original`.

    * Local atmospheric pressure (see [details](build_weather/README.md)): 

        http://auger.uis.edu.co/data/privatev2.html#files

        http://fisica.cab.cnea.gov.ar/particulas/html/auger/monit/  # not working anymore

        (contact asoreyh@cab.cnea.gov.ar for updated data).
    
    * NCEP (sattelite data) for geopotential height data:

        http://rda.ucar.edu/datasets/ds090.0/index.html#sfol-wl-/data/ds090.0?g=8


9.  Very often, we use the environment variable `AUGER_REPO` inside 
    the Python/Bash scripts. 
    So it's recommended to set it in the `~/.bashrc` as:
    ```bash
    export AUGER_REPO=[absolute path to this repository]
    ```

10. In the following, we refer to the Scalers and Histograms, to the corrected versions of the raw data for scalers and charge-histogram counts rates.
    This is to be consistent with the associated papers.


---
# TODO:

- [x] issues after 18/aug/2013 (no data processing after this date):
      this is related to an upgrade in the `sd..root` files, so
      need to correct the "3pmt ok" flag in the c++ codes for
      data after this date (histos/src/clases.h, in 
      operate_event() routine, in 'fpmt' variable... I think). 

- [ ] reimplement this with the python-C++ wrapper (another repository in 
      development) in order to avoid so many intermediate 
      files (this version generates about 1.5TB of intermediate files, e.g. 
      for data in jan/2006-dec/2013)


<!--- ----------------------------------------
----------- CHARGE HISTOGRAMS ----------------
------------------------------------------ -->

---
# Chain of calls to build the Histograms:

Pre-process the primitive/original `sd*.root` files from Lyon.
First, the charge histograms:

    build_Histos/massive.sh
    dir_src1
    dir_dst1

Now the `mc*.root` file to grab the monitoring (i.e. callibration) data:

    build_mc/gral.sh
    dir_src2
    dir_dst2

Script to:
* merge the charge-histograms and monit data (also perform the necessary time syncronization before merge).
* make ``multiplet'' corrections.
* filter those charge-histograms with bad PMT-flag (using monit/callibration data values), and AoP outliers.
* make partial integrals of the histogram counts, in a total of 20 bins of deposited energy.
```
build_ReducedHistos/gral_mpi.py
dir_src3a (dirs_all.dir_src) = dir_dst1
dir_src3b (dirs_all.dir_mon) = dir_dst2
dir_dst3  (dirs_all.dir_dst)
```

Calculate average values (and its associated errors and medians) over the SD array.
For this, it makes quality cuts based on:
* the status of the 3 PMTs on every SD detector.
* the position of the 1st peak in the charge histogram of each detector, at each time.
* number of tanks entering in the array average.
* AoP array-average values.
* among other stuff (see ICRC-2015 proceeding)
```
build_array.avrs/test_2006.py
dir_src4 = dir_dst3
dir_dst4 
```

Build running time-averages (of the counts in charge histogram, for 
every energy channel) over the whole time-period. 
This values are needed as normalization factors in the subsequent 
stages of processing.

    build_avr_histos/test_2006.py
    dir_src5 = dir_dst4/15min
    fname_out5
    #dir_dst5 (overriden)


Build 2D histograms of a scatter plot that relates the individual values 
of the AoP and charge-histograms rates ("individual" refering to values 
of single SD tanks).

    code_hist2d/test_build.H2d.txt.py
    dir_src6  (dir_src)  = dir_dst4/15min
    fname_inp            = fname_out5
    dir_dst6  (overriden)

Fit the 2D histograms, and save the fitted values in already existing files (so by "save" we mean "append").

    code_hist2d/make_fits.py
    dir_src7 = dir_dst6
    fname_out7

Generate AoP-corrected data.

    code_aop.correction/test.py
    dir_src8a (dir_src1) = dir_dst4/15min
    fname_inp8a          = fname_out5
    fname_inp8b          = fname_out7
    #dir_src8b (dir_src2) = dir_dst5 (overriden)
    dir_dst8

Build a cleaner ASCII file of pressure data from the original `wclf_l1.dat` (downloaded from [auger.uis.edu.co](http://auger.uis.edu.co/data/privatev2.html#files) server; see comments above).

    build_weather/test.py
    fname_inp9 = ${AUGER_REPO}/build_weather/data_original/wclf_CLF.dat
    fname_out9

Merge the data from charge-histograms and pressure:

    build_aop.corr_phys/test.py
    dir_src10a (dir_src)    = dir_dst8
    fname_inp10 (fname_inp) = fname_out9
    dir_dst10

This generates 2d histograms of the correlation between charge 
histogram counts (AoP-corrected) and atmospheric pressure.

    build_hist2d_correl.pressure/test.py
    dir_src11a (dir_src)  = dir_dst10
    fname_inp11           = fname_out5
    dir_dst11

Make fits of the former 2D-histograms.

    build_hist2d_correl.pressure/test_fits.py
    dir_src12   = dir_dst11
    fname_inp12 = fname_out5
    fname_out12

Generates pressure-corrected charge histogram counts as a function of time,
for the whole time period of analysis (specified by the arguments of script).

    # NOTE: final stage
    build_pressure.corr/test.py
    dir_src13a   (dir_src)    = dir_dst10
    fname_inp13a (fname_inp1) = fname_out5
    fname_inp13b (fname_inp2) = fname_out12
    dir_dst13

* pre-process NCEP data. 
  NOTE: This data is not used for scalers (the geopotential height was found not to be correct for scalers).
  ```
  build_weather/build_ncep_gh.py
  dir_src14                (--dir_src argument)
  dir_dst14                (--dir_out argument)
  ```

<!--- 
here, we are supposed to finish generating more
corrected data. 
So no processing from now one.
Just read data only for visualization.
-->
__The following is just visualization stuff__

This reads the final data to produce averages in wider Ed channels (see [details](post/README.md)).

    post/ch_Eds_smooth.py

```bash
# this generates figures of data above (time 
# profiles of the ICRC 2015 proceeding)
post/bands_icrc2.py
dir_src15 = dir_dst14
dir_dst15 
```


<!--- ----------------------------------------
 ---------------- SCALERS --------------------
 ----------------------------------------- -->

---
# Chain of calls to build the Scalers:

See the `README.md` files in each directory of these scripts for more description.

* reduction (in time resolution) of the original scalers data
  ```
  scl.build_scals/massive_generation_scalers.py
  dir_src1
  dir_dst1
  ```

* pre-processing && reduction of the original monitor data
  ```
  build_mc/test.sh
  dir_src2
  dir_dst2
  ```

* build 2D-histogramas of the relation between scaler rates and AoP
  ```
  scl.build_AoPcorr/make_AoPHist2d.py
  dir_src3a = dir_dst1    (--inp_scals argument)
  dir_src3b = dir_dst2    (--inp_monit argument)
  f_dst2                  (-o argument)
  ```

* perform weighted linear fit of the 2D-histogram above
  ```
  scl.build_AoPcorr/fit_hist2d.py
  f_src4a   = f_dst3      (--h5input argument)
  f_dst4    = f_src4a     (--h5input argument)
  ```

* make AoP-correction to scaler rates
  ```
  scl.build_AoPcorr/massive_AoP_correc_mpi.py
  dir_src4a = dir_dst1    (--dir_scl argument)
  dir_src4b = dir_dst2    (--dir_mon argument)
  f_src4    = f_dst4      (--h5input argument)
  f_dst4                  (-o argument)
  ```

* pre-processing of the original weather data. Basically obtains a version with uniform time resolution. This step might takes ~8hrs.
  ```
  build_weather/test.py
  f_src5    = (original weather data)
  f_dst5
  ```

* convert the above processed-weather-data to a binary format. We might merge these two last steps but we need consistency with charge-histogram processing chain too.
  ```
  build_weather/build_h5_weather.py
  f_src6 (fname_weather) = f_dst5
  f_dst6 (fname_out)
  ```

* pre-process NCEP data. This data is not used for scalers (the geopotentia height was found not to be correct for scalers) so we should uncouple and remove this step.
  ```
  build_weather/build_ncep_gh.py
  dir_src7                (--dir_src argument)
  dir_dst7                (--dir_out argument)
  ```

* data rejection (of scaler rates and AoP values)
  ```
  scl.build_final/rejection_and_PressCorr.py
  f_src8    = f_dst4      (--inp_h5 argument)
  dir_src8  = f_dst6      (--inp_weather argument)
  f_dst8                  (--fname_out argument)
  ```

* build 2D-histograms of relations between scaler rates and pressure (and geopotential height)
  ```bash
  scl.build_final/build_hist2d.py
  f_src9    = f_dst8      (--fname_inp)
  dir_src9  = dir_dst7    (--dir_ncep)
  f_dst9                  (--fname_out)
  ```

* perform linear corrections for pressure and GH effects
  ```bash
  scl.build_final/build_corr.py
  f_src10a  = f_dst8      (-data argument)
  f_src10b  = f_ds9       (-fit argument)
  f_dst10                 (-o argument)
  ```




<!--- 
    RELEVANT SCRIPTS:
    -----------------
    Related to important figures, extraction of data in initial, 
    intermediate or final stages.
-->
---
# Relevant Scripts

All environment variables below are defined in this [makefile.in](./meta_builds/makefile.in).

---
### Figures:

* [Correlation of Scalers w/ AoP](./scl.build_AoPcorr/fit_hist2d.py): it fits the 2D-histogram of the correlation with AoP. Run:
    ```bash
    ./fit_hist2d.py --pdb -- -fio ${sfname_out3} -fig ~/auger_repo/paper/correl__Scalers_vs_AoP.png -cbmax 1e7 -ylim -30 40. -fm paper
    ```

* [Correlation of Histograms w/ AoP](./code_hist2d/make_fits.py): same as above, but for Histograms, and the correlation fits are made for each energy channel. Run:
    ```bash
    ./make_fits.py -- -src ${hdir_dst6} -fo ${AUGER_REPO}/paper/correl__Histos_vs_AoP/fit.txt -plot ${AUGER_REPO}/paper/correl__Histos_vs_AoP -m shape.ok_and_3pmt.ok -cbmax 6e6 -fm paper
    ```

* [Correlation of Scalers w/ pressure](./scl.build_final/build_hist2d.py): same as above, but for pressure. Run:
    ```bash
    ./build_hist2d.py -- -inp ${sfname_out8} -ncep ${wdir_dst14} -o $AUGER_REPO/paper/correl__Scals_vs_Press.h5 -ffitpres $AUGER_REPO/paper/correl__Scals_vs_Press.png -period 01/01/2006 31/12/2015
    ```

* [Correlation of Histogrmas w/ pressure](./build_hist2d_correl.pressure/test_fits.py): same as above, but for pressure. Run:
    ```bash
    ./test_fits.py -- -src ${hdir_dst11} -fo ${AUGER_REPO}/paper/correl__Histos_vs_Press/fit.txt -plot ${AUGER_REPO}/paper/correl__Histos_vs_Press -m shape.ok_and_3pmt.ok -cbmax 6e4 -fm paper
    ```

* [Fit slopes for AoP and Pressure](...): builds a figure for the values of the correlation-fitted slopes between Histograms and AoP values, as a function of the deposited energy. In the inset, the corresponding slopes for Scalers is also shown.
    ```bash
    ./corr.slopes_vs_Ed.py -- \
        -fmaop ${AUGER_REPO}/paper/correl__Histos_vs_AoP/fit.txt ${sfname_out3} \
        -fmpress ${AUGER_REPO}/paper/correl__Histos_vs_Press/fit.txt ${AUGER_REPO}/paper/correl__Scals_vs_Press.h5 \
        -fig ${AUGER_REPO}/paper/corr.slopes_vs_Ed.png
    ```


---
### Extract numerical arrays

Retrieve some numeric arrays of the data, built at the different stages:

* [](): 


---
### Checkpoints

These are stages where we ussualy want to test/check if the data makes some sense in physical terms.




 



<!--- FINAL (relevant) REMARKS -->
---
## Final remarks

The final data, corrected by AoP, atmospheric pressure, and geopotential height are in:
- [Histograms](build_temp.corr/README.md):
 `$AUGER_REPO/build_temp.corr/final_histos.h5`
- [Scalers](scl.build_final/README.md):
 `$AUGER_REPO/scl.build_final/final_scalers.h5`

Other post-processed data (with earlier processing stages than above):
* histograms corrected by AoP and atmos. pressure. See [build_pressure.corr/README.md](build_pressure.corr/README.md).
* weather data (pressure and geopotential height). See [build_weather/README.md](build_weather/README.md).




<!--- REPRODUCTION -->
---
## Remarks related to reproduction of results

To minimize the chances of trouble when reproducing these results, you can build the same tree of dependencies that we were working with.
For that, you can use any of these requirement files:
```bash
etc/requirements/export_work.yml
etc/requirements/list_work.txt
```
Assuming that your Python 2.7 is from Anaconda distribution, you can build the dependencies with:
```bash
# using the .yml file:
conda env create --name work_auger -f $AUGER_REPO/etc/requirements/export_work.yml

# or using the .txt file (in this case we need to be explicit about the 
# channels we'l use):
conda create --name work_auger -y \
    -c anaconda -c nmoraitakis -c yoavram -c trung \
    -c astropy -c auto -c defaults -c pysysops \
    --file $AUGER_REPO/etc/requirements/list_work.txt

# later, you can switch to the "work_auger" environment, where these
# dependencies were built:
source activate work_auger
```
We prefer to use environments within Anaconda framework because it avoids any conflict with your daily 
work in other projects (which may run under other environments or under your system itself).



> *"Most coders think debugging software is about fixing a mistake.
> But that's bullshit.
> Debugging is actually all about finding the bug, about understanding why the bug was there to begin with.
> About knowing that its existence was no accident.
> It came to you, to deliver a message, like an unconscious bubble floating to the surface. popping with the revelation you've secretly known all along.
> The bug forces the software to adapt, evolving into something new because of it.
> Work around it or work through it.
> No matter what, it changes. 
> It becomes something new.
> The next version. The inevitable upgrade."*
> -Mr Robot.


---
 author: Jimmy J. Masías Meza

 email:  jimmy.ilws@gmail.com


<!--- EOF -->
