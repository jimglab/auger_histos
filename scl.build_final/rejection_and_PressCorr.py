#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
import numpy as np
from h5py import File as h5
from os.path import isfile, isdir
import sys
from funcs import Rejection_And_JoinPress_ii
import argparse
import shared_funcs.funcs as sf


#--- retrieve args
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    description="""
    Data rejection on Scalers.
    """
)
parser.add_argument(
'-ih5', '--inp_h5',
type=str,
default='../out/out.scl.build_final/scals_wAoPcorrection_wrejec_iii.h5',
help='input of AoP-corrected scalers.',
)
parser.add_argument(
'-iw', '--inp_weather',
type=str,
default='../out/out.build_weather/weather_5min_2006-2015.h5',
help='input of .h5 weather data.',
)
parser.add_argument(
'-fo', '--fname_out',
type=str,
default='../out/out.scl.build_final/scals_aop_corr.h5',
help='filename of .h5 output',
)
parser.add_argument(
'-dates', '--date_lim', 
nargs=2,
metavar=('DateIni', 'DateEnd'),
action=sf.arg_to_datetime,
default=['01/01/2006', '31/12/2015'],
help='years for period of data-processing (yearini/Jan/01-yearend/Dec/31)',
)
pa = parser.parse_args()


Rejection_And_JoinPress_ii(
    pa.inp_h5, 
    pa.inp_weather, 
    pa.date_lim, 
    pa.fname_out
)

print '\n ---> we generated: %s\n'%pa.fname_out
#EOF
