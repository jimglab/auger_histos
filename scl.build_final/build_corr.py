#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
"""
build scalers with pressure & gh correction
Run:
./build_corr.py -- --dir_src ../out/out.scl.build_final --period 01/01/2006 31/12/2015 --fname_out ./test.h5
"""
import numpy as np
from pylab import find, pause
import h5py
import argparse
from datetime import datetime, timedelta
import funcs as ff
from shared_funcs.funcs import (
    My2DArray, date2gps, gps2date, rebine, 
    ncep_gh, arg_to_datetime
)

#--- retrieve args
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
'-idata', '--fname_inp_data',
type=str,
default='../out/out.scl.build_final/scals_aop_corr.h5',
help='directory where .h5 data file is stored'
)
parser.add_argument(
'-ifit', '--fname_inp_fit',
type=str,
default='../out/out.scl.build_final/hist2d_scal.vs.press_ii.h5',
help='input filename where fit-parameters are stored.',
)
parser.add_argument(
'-ncep', '--dir_ncep',
type=str,
default='../out/out.build_weather/ncep',
help='pre-processed NCEP data (HDF5 files) directly from the original ones.',
)
parser.add_argument(
'-o', '--fname_out',
type=str,
default='test.h5',
help='filename of .h5 output',
)
parser.add_argument(
'-period', '--period',
type=str,
nargs=2,
action=arg_to_datetime,
default=['01/01/2006', '31/12/2015'],
help='period to analyze.',
metavar=('INI-DATE','END-DATE'),
)
pa = parser.parse_args()

#----------------------
fname_inp_fit  = pa.fname_inp_fit #'%s/hist2d_scal.vs.press.h5' % dir_dst
fname_out      = pa.fname_out

finp_f = h5py.File(fname_inp_fit, 'r')

date_ini, date_end = pa.period

print '\n [*] current keys in %s:\n %r\n' % (finp_f.filename, finp_f.keys())
# values from correlation w/ pressure
avr_pres  = finp_f['H2D_w.press/meta/avr_x_pres'].value # <scals w/ AoP-corr>
avr_scal1 = finp_f['H2D_w.press/meta/avr_y_scal'].value
mpress    = finp_f['H2D_w.press/fit/m'].value # [1] press-correction-slope
# values from correlation w/ geop height
avr_gh    = finp_f['H2D_w.gh/meta/avr_x_GH'].value
avr_scal2 = finp_f['H2D_w.gh/meta/avr_y_scal_wPrs_wGH'].value
mgh       = finp_f['H2D_w.gh/fit/m'].value

# average of uncorrected scalers
avr_uncorr_scals = ff.average_from_scals(
    pa.fname_inp_data, 
    date_ini, 
    date_end, 
    vname='scls_uncorr'
    )

finp_d = h5py.File(pa.fname_inp_data, 'r')  # scaler input-data
fo     = h5py.File(fname_out, 'w')          # output
for yyyy in range(date_ini.year, date_end.year+1):
    print yyyy
    for mm in range(date_ini.month, date_end.month+1):
        dpath = 'data_wrejec/%04d/%02d'%(yyyy,mm)
        # get column-numbers and variable-names
        col = {}
        for vname, _col in finp_d[dpath].attrs.iteritems():
            col[vname] = _col

        scl_wAoP_  = finp_d[dpath][:,col['scls_AoPcorr']]/avr_scal1 - 1.
        scl_woAoP_ = finp_d[dpath][:,col['scls_uncorr']]
        press_     = finp_d[dpath][:,col['pressure']]/avr_pres - 1.
        #--- pressure correction
        scl_wAoP_wPrs_ = scl_wAoP_ - mpress*press_
        tscl = finp_d[dpath][:,col['gps']]+315964800 # utc-sec
        gh_ = ncep_gh(yyyy,mm,dir_src=pa.dir_ncep,sync=True,tsync=tscl)/avr_gh-1. #[1] norm dev
        #--- geopotential-height correction
        scl_wAoP_wPrs_wGh_ = scl_wAoP_wPrs_ - mgh*gh_
        #cc    = ~np.isnan(scl_wAoP_wPrs) # flag clean data
        # TODO: correction needed here. The normalization factor that
        # recovers the TRUE absolute values of 'scl_wAoP_wPrs' should
        # the total time-average between 'date_ini' and 'date_end'.
        scl_wAoP_wPrs = (scl_wAoP_wPrs_+1.0)*avr_scal1
        scl_wAoP_wPrs_wGh = (scl_wAoP_wPrs_wGh_+1.)*avr_scal2
        
        ol = {
        't_utc' : tscl,                  # [utc sec]
        'wAoP'          : (scl_wAoP_+1.)*avr_scal1,
        'woAoP'         : scl_woAoP_,    # uncorrected Scalers
        'wAoP_wPrs'     : scl_wAoP_wPrs,
        'wAoP_wPrs_wGh' : scl_wAoP_wPrs_wGh,
        #'wAoP': scl_wAoP_, # normaliz deviations
        #'wAoP_wPrs': scl_wAoP_wPrs_, # (bis)
        #'wAoP_wPrs_wGh': scl_wAoP_wPrs_wGh_, # (bis)
        'press'         : finp_d[dpath][:,col['pressure']][...],
        'aop'           : finp_d[dpath][:,col['aop']][...],
        'ntanks'        : finp_d[dpath][:,col['ntanks']][...],
        }
        for onm, ovar in ol.iteritems():
            opath = '/%s/%04d/%02d'%(onm,yyyy,mm)
            fo[opath] = ovar

#--- useful/complementary values
fo['etc/avr__woAoP']    = avr_uncorr_scals  # < uncorr Scalers >
fo['etc/avr__wAoP']     = avr_scal1         # < AoP-corr Scalers >
#fo['etc/avr__wAoP_wG']
fo['etc/avr__press']    = avr_pres          # < pressure >


print " --> we generated: "+fo.filename
fo.close()

"""
f = h5('test.h5','w')
f['aa'] = np.random.random((10,20))
regref = f['aa'].regionref[:4,:5]
"""
#EOF
