#!/usr/bin/env ipython
# -*- coding: utf-8 -*-

import numpy as np
from h5py import File as h5
from os.path import isfile, ispath
import sys

year_ini,year_end = 2006,2014 # period:(Jan/year_ini - Dec/year_end)
fname_inp   = '../data/scalers_wAoPcorrection_2006-2013_allarray_3pmtsON_wdatarejec_q2.5_.h5'
fname_out	= '../data/scalers_wAoPcorrection_2006-2013_allarray_3pmtsON_q2.5_w2datarejecs.h5';


aop_low	= 3.3;
aop_upp	= 3.8;

DATA = second_rejection(fname_inp, [aop_low,aop_upp], [year_ini, year_end], fname_out)

#DATA	= unique(DATA, 'rows');
#dlmwrite(fname_out, DATA, 'delimiter', ' ', 'precision', 13)
%
fprintf('\n hemos generado: %s!\n\n', fname_out)
