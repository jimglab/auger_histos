#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
"""
- build 2d-histog of correlation scalers-vs-pressure
- fits the correlation, and appends fit-info to .h5 
"""
from pylab import figure, savefig, close
import numpy as np
from datetime import datetime, timedelta
from h5py import File as h5
import os, sys
import funcs as ff
import argparse
from shared_funcs.funcs import arg_to_datetime

#--- retrieve args
parser = argparse.ArgumentParser(
formatter_class = argparse.ArgumentDefaultsHelpFormatter,
description = """
Build a 2D-histogram (correlation of Scalers w/ pressure) and 
linear fit (fit parameters are appended in the <FNAME_OUT> file)
""",
)
parser.add_argument(
'-inp', '--fname_inp',
type=str,
default='../out/out.scl.build_final/scals_aop_corr.h5',
help='input file with AoP-corrected-scalers, synced with pressure data.',
)
parser.add_argument(
'-ncep', '--dir_ncep',
type=str,
default='../out/out.build_weather/ncep',
help='pre-processed NCEP data (HDF5 files)',
)
parser.add_argument(
'-o', '--fname_out',
type=str,
default='../out/out.scl.build_final/hist2d_scal.vs.press.h5',
help='output .h5 file containing 2D-histograms of correlations between: 1) scalers vs pressure; 2) scalers vs geopotential height, and 3) associated linear-fit-parameters and average values.',
)
parser.add_argument(
'-cbmax', '--cbmax', 
type=float, 
default=1e2,
help='maximum value for colorbar'
)
parser.add_argument(
'-period', '--period',
type=str,
nargs=2,
action=arg_to_datetime,
default=['01/01/2006', '31/12/2015'],
help='period to analyze.',
metavar=('INI-DATE','END-DATE'),
)
parser.add_argument(
'-ffitpres', '--fig_fit_press',
type=str,
default='./test_press.png',
help='figure for fit of correlation with pressure',
)
parser.add_argument(
'-fm', '--figmode',
type=str,
default='verb',
help='figure mode: "verb" (verbose info in figure) or "paper" (pretty stuff to look good)',
)
parser.add_argument(
'-plim', '--plim',
type=float,
nargs=4,
default=[-1.9, 2.1, -10., +10.],
metavar=('XMIN','XMAX','YMIN','YMAX'),
help='Plot limits for both axis.',
)
pa = parser.parse_args()

finp = h5(pa.fname_inp, 'r')

date_ini = pa.period[0] #datetime(2006, 1, 1)
date_end = pa.period[1] #datetime(2015,12,31)


#----------------------
#
# PRESSURE
#
# build 2d-hist of correlation w/ pressure
hmgr = ff.hist2d_mgr(
    pa.fname_inp, 
    dates=[date_ini,date_end],
    fname_out=pa.fname_out
    )

hmgr.build_H2D_and_save2file( # generate .h5
    xlim=[-.019, .021], 
    ylim=[-.1, .1],
    nbin=200, #128,
    h5suffix='/H2D_w.press'
    )

# linear-fit of correlation w/ pressure
# (*WARNING*: we append fit-info to 'fname_out')
fp = ff.Fit_Hist2D(fname_inp=pa.fname_out)
xlabel = '$\Delta$ pressure [%]'
ylabel = '$\Delta \langle S_{id}^{corr/AoP} \\rangle_{\\forall id}$ [%]' \
    if pa.figmode=='paper' else '$\Delta$scalers [%]'
fp.fit_and_plot(
    fname_fig=pa.fig_fit_press,
    xylabel=(xlabel, ylabel), 
    h5key={
        'x':'xaxis_pres',
        'y':'yaxis_scal',
        'H':'hist2d',
        'suffix':hmgr.h5suffix, # root path of H2d-pressure data
        'm':'fit/m', 
        'b':'fit_correl.w.press/b'}, # group-paths where-to append
    xlabsize = 13,
    ylabsize = 16,
    xlim = pa.plim[0:2],
    ylim = pa.plim[2:4],
    cblim=(1,pa.cbmax),
    figsize = (6,3) if pa.figmode=='paper' else (6,4),
) 


#----------------------
#
# Geopotential Height
#
# build 2d-hist of correlation between
# the AoP-Press-corr-scaler & geop-height
hgb = ff.hist2d_GeoH_mgr(
    fname_inp_data=pa.fname_inp,
    fname_inp_hist=pa.fname_out,
    dir_ncep=pa.dir_ncep,
    )
hgb.build_H2D_and_save2file(
    nbin=200,
    xlim=(-.02,.02),ylim=(-.04,.04),
    h5suffix_inp=hmgr.h5suffix,
    h5suffix_out='/H2D_w.gh',   # root path where-to append
    )

# linear-fit of correlation w/ geop-height
# (*WARNING*: we append fit-info to 'fname_out')
fp = ff.Fit_Hist2D(fname_inp=pa.fname_out)
fp.fit_and_plot(
    fname_fig='test_hg.png',
    xylabel=('$\Delta$Hg [%]','$\Delta$scalers [%]'), 
    h5key={
        'x':'xaxis_GH',
        'y':'yaxis_scal_wPrs_wGH',
        'H':'H2D_GH.vs.Scals',
        'suffix':hgb.h5suffix, # root path of H2d-GeoH data
        'm':'fit/m', 'b':'fit_correl.w.press/b', # paths where-to append
        },
    cblim=(1,pa.cbmax),
    figsize = (6,3.5) if pa.figmode=='paper' else (6,4),
) 

print " --> we generated: " + pa.fname_out
#EOF
