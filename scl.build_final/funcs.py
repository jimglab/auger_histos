#!/usr/bin/env ipython
# -*- coding: utf-8 -*
import numpy as np
from numpy import (
    nanmean, nanstd, nanmedian, sqrt, square, isnan,
    nanmin, nanmax
)
from os.path import isfile, isdir
import h5py
from shared_funcs.funcs import (
    My2DArray, date2gps, gps2date, rebine, ncep_gh,
    date2utc, utc2date,
)
from shared_funcs.fhist2d import (
    contour_2d, LinearFit_Hist2D
)
from datetime import datetime, timedelta
#--- graphics
from matplotlib import cm
#from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib.colors import LogNorm
import os
from pylab import pause, find
if 'DISPLAY' in os.environ:
    from pylab import figure, close
    from pylab import plot, show, grid, scatter, close # for debugging

#--- globals
GPS2UTC = 315964800


def average_from_scals(fname_inp, date_ini, date_end, vname):
    finp_d = h5py.File(fname_inp, 'r')
    var = []
    check = 1
    for yyyy in range(date_ini.year, date_end.year+1):
        print yyyy
        for mm in range(date_ini.month, date_end.month+1):
            dpath = 'data_wrejec/%04d/%02d'%(yyyy,mm)
            # get column-numbers and variable-names
            col = {}
            for vname, _col in finp_d[dpath].attrs.iteritems():
                col[vname] = _col
         
            # check that 'vname' is a valid key
            if check==1:
                check = 0
                assert vname in col.keys(), '\n [-] %s is not included ' % vname+\
                ' in the attributes:\n %r\n' % finp_d[dpath].attrs.keys()

            # append
            var += finp_d[dpath][:,col['scls_uncorr']][...].tolist()

    var = np.array(var, dtype=np.float64)
    avr = np.nanmean(var) # total average
    assert not(np.isnan(avr)), '\n [-] something\'s wrong!\n'
    finp_d.close()
    return avr


class hist2d_mgr(object):
    def __init__(self, fname_inp, dates, fname_out):
        self.fname_inp = fname_inp
        self.date_ini, self.date_end = dates
        self.fname_out = fname_out

        # get the correspondence between the variable names and
        # the "column" numbers
        self.col = {}
        finp = h5py.File(fname_inp, 'r')
        _k1 = finp['data_wrejec'].keys()[0] # take one subkey
        _k2 = finp['data_wrejec/'+_k1].keys()[0] # take one sub-subkey
        # take the variable keys/names
        for _colname, _colno in finp['data_wrejec/'+_k1+'/'+_k2].attrs.iteritems():
            self.col[_colname] = _colno
        finp.close()

    def build_H2D_and_save2file(self, xlim, ylim, nbin=90, h5suffix='/'):
        self.h5suffix = h5suffix
        RANGE = [xlim,ylim]

        # let's build the 2D-histogram of correlation
        ob = self.build_hist2d(nbin,RANGE)
        H  = ob['H2D']

        # save to file
        fo = h5py.File(self.fname_out, 'w').create_group(h5suffix)
        fo['meta/avr_x_pres'] = ob['avr_pres']
        fo['meta/avr_y_scal'] = ob['avr_scal']
        fo['meta/ini_yyyy-mm-dd'] = 'ini_{y:04d}-{m:02d}-{d:02d}'.format(
            y=self.date_ini.year,m=self.date_ini.month,d=self.date_ini.day)
        fo['meta/end_yyyy-mm-dd'] = 'end_{y:04d}-{m:02d}-{d:02d}'.format(
            y=self.date_end.year,m=self.date_end.month,d=self.date_end.day)
        fo['nbin'] = nbin
        dx = (xlim[1]-xlim[0])/(1.*nbin)
        dy = (ylim[1]-ylim[0])/(1.*nbin)
        fo['xaxis_pres'] = np.arange(xlim[0]+.5*dx,xlim[1],dx)
        fo['yaxis_scal'] = np.arange(ylim[0]+.5*dy,ylim[1],dy)
        fo['hist2d'] = H.T
        fo.file.close()
        print " ---> we generated: " + self.fname_out 

    def build_hist2d(self, nbin, RANGE):
        finp = h5py.File(self.fname_inp, 'r')
        avr_scal, avr_pres = self.calc_averages()
        date_ini, date_end = self.date_ini, self.date_end
        #--- histos
        H = np.zeros((nbin,nbin))#, dtype=np.int64)
        for yyyy in range(date_ini.year, date_end.year+1):
            for mm in range(date_ini.month, date_end.month+1):
                dpath    = 'data_wrejec/%04d/%02d'%(yyyy,mm)
                utc      = finp[dpath][:,self.col['gps']] + GPS2UTC
                # filter the data in time, at the second resolution
                cc       = (utc > date2utc(date_ini)) & (utc < date2utc(date_end))
                scl_wAoP = finp[dpath][cc,self.col['scls_AoPcorr']]/avr_scal - 1.
                press    = finp[dpath][cc,self.col['pressure']]/avr_pres - 1.
                #print yyyy, mm, np.nanmin(press), np.nanmax(press)
                H += np.histogram2d(
                       press, scl_wAoP, bins=(nbin,nbin), 
                       range=RANGE, normed=False
                     )[0]
        o = {
        'H2D'      : H,
        'avr_scal' : avr_scal,
        'avr_pres' : avr_pres,
        }
        return o
        
    def calc_averages(self):
        finp = h5py.File(self.fname_inp, 'r')
        date_ini, date_end = self.date_ini, self.date_end
        #--- calculate averages (for normalization later)
        n_scal, n_press    = 0 , 0
        avr_scal, avr_pres = 0., 0.
        for yyyy in range(date_ini.year, date_end.year+1):
            for mm in range(date_ini.month, date_end.month+1):
                print yyyy, mm
                dpath = 'data_wrejec/%04d/%02d'%(yyyy,mm)
                utc   = finp[dpath][:,self.col['gps']] + GPS2UTC
                # filter the data in time, at the second resolution
                cc    = (utc > date2utc(date_ini)) & (utc < date2utc(date_end))
                scl   = finp[dpath][cc,self.col['scls_AoPcorr']]
                press = finp[dpath][cc,self.col['pressure']]
                # also filter individual NaNs
                cc_s  = cc & (~np.isnan(scl))
                cc_p  = cc & (~np.isnan(press))

                avr_scal += scl[cc_s].sum()
                avr_pres += press[cc_p].sum()
                n_scal   += scl[cc_s].size
                n_press  += press[cc_p].size

        avr_scal /= 1.*n_scal
        avr_pres /= 1.*n_press
        # any average should be NaN:
        assert not(np.isnan(avr_scal) or np.isnan(avr_pres))
        print "avrs: ", avr_scal, avr_pres
        finp.close()
        return avr_scal, avr_pres


class hist2d_GeoH_mgr(object):
    """ builds 2d-histogram of the scatter plot
    between the AoP-corrected-scalers and geopotential
    height.
    input:
    - fname_inp: file w/ AoP-corrected scalers && w/ synced pressure
    - fname_out: filename of output .h5
    - dir_ncep : input NCEP data
    """
    def __init__(self, fname_inp_data, fname_inp_hist, dir_ncep):
        """
        fname_inp_data: filename of .h5 where scalers are (un/corrected versions)
        fname_inp_hist: filename of .h5 with stats info.
        """
        self.fname_inp_data = fname_inp_data
        self.fname_inp_hist = fname_inp_hist
        self.dir_ncep       = dir_ncep

    def build_H2D_and_save2file(self,nbin=90,xlim=None,ylim=None,h5suffix_inp='/',h5suffix_out='/'):
        """
        input:
        nbin        : number of bins for the (square) 2D-histogram.
        h5suffix_inp: root path (inside .h5) where to read the data from.
        h5suffix_out: root path (inside .h5) where to append data to (average
                      values, fit params, etc).
        """
        self.h5suffix = h5suffix_out
        RANGE = [xlim,ylim]
        # let's build the 2D-histogram of correlation
        ob = self.build_hist2d(nbin,RANGE,h5suffix_inp)
        H  = ob['H2D']
        # save to file
        fo = h5py.File(self.fname_inp_hist,'r+').create_group(h5suffix_out)
        fo['meta/avr_y_scal_wPrs_wGH'] = ob['avr_scal']
        fo['meta/avr_x_GH']            = ob['avr_gh']
        fo['nbin_GH'] = nbin
        dx = (xlim[1]-xlim[0])/(1.*nbin)
        dy = (ylim[1]-ylim[0])/(1.*nbin)
        fo['xaxis_GH'] = np.arange(xlim[0]+.5*dx,xlim[1],dx)
        fo['yaxis_scal_wPrs_wGH'] = np.arange(ylim[0]+.5*dy,ylim[1],dy)
        fo['H2D_GH.vs.Scals'] = H.T
        print " ---> we appended data to: " + fo.file.filename
        fo.file.close()

    def build_hist2d(self, nbin, RANGE, h5suffix_inp):
        finp_h = h5py.File(self.fname_inp_hist,'r')[h5suffix_inp] # stats info
        finp_d = h5py.File(self.fname_inp_data,'r') # data
        mpress = finp_h['fit/m'].value # [1] pressure-correction-slope
        #with finp_h['meta'] as meta:
        meta = finp_h['meta']
        avr_pres = meta['avr_x_pres'].value # <scals w/ AoP-corr>
        avr_scal = meta['avr_y_scal'].value
        y,m,d = map(int,meta['ini_yyyy-mm-dd'].value[4:].split('-'))
        date_ini = datetime(y,m,d)
        y,m,d = map(int,meta['end_yyyy-mm-dd'].value[4:].split('-'))
        date_end = datetime(y,m,d)
        self.date_ini,self.date_end = date_ini,date_end # period of analysis
        avr_gh   = self.calc_gh_average(dir_src=self.dir_ncep) # average for whole period
        avr_scl, navr = 0.0, 0 # to compute <scal_wAoP_wPrs>
        #--- histos
        H = np.zeros((nbin,nbin))#, dtype=np.int64)
        for yyyy in range(date_ini.year, date_end.year+1):
            for mm in range(date_ini.month, date_end.month+1):
                dpath = 'data_wrejec/%04d/%02d'%(yyyy,mm)
                scl_wAoP_ = finp_d[dpath][:,4]/avr_scal - 1.
                press_    = finp_d[dpath][:,5]/avr_pres - 1.
                scl_wAoP_wPrs = scl_wAoP_ - mpress*press_
                cc    = ~np.isnan(scl_wAoP_wPrs) # flag clean data
                if find(cc).size>=1:
                    avr_scl += scl_wAoP_wPrs[cc].sum() # scalar
                    navr    += cc.nonzero()[0].size                 # scalar
                else:
                    print yyyy, mm, '(no-data-here?)'
                    continue
                tscl  = finp_d[dpath][:,0]+315964800 # utc-sec
                gh_   = ncep_gh(yyyy, mm, dir_src=self.dir_ncep, sync=True, tsync=tscl) 
                gh_   = gh_/avr_gh-1. # [1] normaliz dev
                print yyyy, mm, nanmin(gh_), nanmax(gh_)
                H += np.histogram2d(
                       gh_, scl_wAoP_wPrs, bins=(nbin,nbin), 
                       range=RANGE, normed=False
                     )[0]

        # we are supposed to have collected some stuff here!
        assert H.sum() > 0.0, \
            '\n [-] ERROR: the 2D-histogram has ZERO counts!!\n'

        avr_scl = avr_scal*(1.+avr_scl/navr) # average of clean data *only*
        finp_h.file.close(); finp_d.close()
        o = {
        'H2D': H,
        'avr_scal': avr_scl,
        'avr_gh'  : avr_gh,
        }
        return o
    
    def calc_gh_average(self, dir_src='../out/out.build_weather/ncep'):
        """ calc average in period of 'self.fname_inp'
        """
        date_ini, date_end = self.date_ini, self.date_end
        #--- calculate averages (for normalization later)
        avr_gh, navr = 0.0, 0
        for yyyy in range(date_ini.year, date_end.year+1):
            for mm in range(date_ini.month, date_end.month+1):
                print yyyy, mm
                gh = nanmean(ncep_gh(yyyy, mm, dir_src=dir_src, sync=False)[1])
                cc = ~isnan(gh)
                if cc.size==0:
                    continue
                avr_gh += gh[cc].sum()
                navr   += cc.nonzero()[0].size

        avr_gh /= 1.*navr
        print "\n [*] <geop height>: %g\n" % avr_gh
        return avr_gh
        

class Fit_Hist2D(object):
    def __init__(self, **kargs):
        """ load all, so we add the necessary arguments 
        as we build more methods 
        Load 2d-histogram data from .h5 file, and make
        a linear fit.
        input:
        - fname_inp : .h5 file
        """
        for name, value in kargs.iteritems():
            setattr(self,name,value)

    def fit_and_plot(self,**kargs):
        """ we need:
        - self.fname_inp
        input:
        - fname_fig : filename for fig
        - xylabel   : 2-tuple; labels for plot
        - h5key     : {'x','y','H'}, paths for data in .h5
        - cblim     : limits for colorbar
        WARNING: We open 'self.fname_inp' in 
                 append mode (r+), to add fit params.
        """
        fname_fig = kargs['fname_fig']
        xylabel   = kargs['xylabel']
        h5key     = kargs['h5key']
        CBMIN, CBMAX = kargs['cblim']

        finp = h5py.File(self.fname_inp, 'r+')[h5key['suffix']]
        #print " >> finp: ", finp.keys()
        x = 100.*finp[h5key['x']][...] # [%] percent
        y = 100.*finp[h5key['y']][...] # [%] percent
        H2D = finp[h5key['H']][...]

        assert H2D.sum() > 0.0, \
            '\n [-] the 2D-histogram has ZERO counts!\n'
        print "\n [*] sum(H2D): %g\n" % H2D.sum() 

        #--- make linear fit
        fit = LinearFit_Hist2D(x, y, H2D)
        m, b = fit['m'], fit['b']
        print " [+] fit: ", m, b 

        if np.isnan(m) or np.isnan(b):
            print "\n [-] ERR: fit parameters give NaNs!!, aborting..."
            finp.close()
            raise SystemExit
        else:
            finp[h5key['m']] = m
            finp[h5key['b']] = b
        fig = figure(1, figsize=kargs.get('figsize',(6,4)))
        ax  = fig.add_subplot(111)
        fig, ax = contour_2d(fig, ax, x, y, H2D, hscale='log',
            vmin=CBMIN, vmax=CBMAX)
        #--- fit
        LABEL_FIT = '10-years fit\nm=%2.2f' % m
        ax.plot(x, m*x+b, '--r', lw=3, label=LABEL_FIT)
        #--- save fig
        ax.legend(loc='best')
        ax.grid()
        ax.set_xlabel(xylabel[0], fontsize=kargs.get('xlabsize',15))
        ax.set_ylabel(xylabel[1], fontsize=kargs.get('ylabsize',15))
        # limits
        ax.set_xlim(kargs.get('xlim',[None,None]))
        ax.set_ylim(kargs.get('ylim',[None,None]))

        fig.savefig(fname_fig, dpi=135, bbox_inches='tight')
        close(fig)
        #--- close input file
        finp.file.close()


def data_rejection(q, data):
    mu, sigma = nanmean(data), nanstd(data)
    lim_inf = mu - q*sigma
    lim_sup = mu + q*sigma
    cc = (data>lim_inf) & (data<lim_sup)
    return cc


class mgr_data(object):
    """
    Parse the data around `nm` months respect to a 
    given `datetime`
    - make sure to read file only when necessary.
    - through method `self.parse()`, parse data that 
      belongs exactly to a window that spans `nm` months.
    """
    def __init__(self, fname_inp, fname_weather, nm, ini, end):
        self.fdata = h5py.File(fname_inp, 'r')
        self.fw    = h5py.File(fname_weather, 'r')
        self.nm    = nm
        # global bounds for initial, end `datetime`
        self.ini, self.end = ini, end
        # `local` bounds
        self.bounds = None

    def parse(self, date):
        """
        - read data from file *only* when `date` is out of the
        bounds (`ini_now`,`end_now`).
        NOTE: Take into accound the case when ini_now, end_now coincides
        with ini, end.
        - always pass data that has a total span of
          `nm` months around `date`
        - parse an existing pointer if i/o is unnecessary.
        """
        # this must never happen!
        assert date>=self.ini and date<=self.end,\
            ' --> NO WAY!: {date} should be in ({ini} -- {end})'.format(\
            date=date.strftime('%Y %B %d %H:%M'), 
            ini=self.ini.strftime('%Y %B %d %H:%M'), 
            end=self.end.strft('%Y %B $d %H:%M')
            )

        nm = self.nm
        delta = timedelta(days=nm*30)
        _i, _e = date-delta, date+delta

        # truncate local bounds to global ones
        if _i<self.ini: _i = self.ini
        if _e>self.end: _e = self.end

        bounds_now = [[_i.year,_i.month], [_e.year,_e.month]] 
        if self.bounds!=bounds_now: # to avoid unnecessary i/o
            #if date.day==1: 
            #    import pdb; 
            #    pdb.set_trace()
            self.data = self.read(_i, _e)

        #-- select `nm` months around `date`
        cc_s = (self.data['gps']>=date2gps(_i)) & (self.data['gps']<=date2gps(_e))
        cc_w = (self.data['tw']>=date2utc(_i)) & (self.data['tw']<=date2utc(_e))

        d = {}
        for nm in ('gps','ntanks','aop','scls_uncorr','scls_AoPcorr'):
            d[nm] = self.data[nm][cc_s]
        for nm in ('tw','press'):
            d[nm] = self.data[nm][cc_w]
        return d

    def read(self, _ini, _end):
        """
        read from file all data between the
        datetimes `ini` and `end`.
        """
        # list of all dates between the closed
        # interval [_ini,_end]
        _dates = [_ini+timedelta(days=n) for n in range((_end-_ini).days+1)]

        # list of all years/months in `_dates`
        ld = []
        for _d in _dates:
            pair = [_d.year,_d.month]
            if pair not in ld:
                ld.append(pair)
        """ now:
        ld = [2006,1],[2006,2],...,[2013,12]
        """
        # instantaneous bounds (to avoid unnecessary i/o)
        self.bounds = [ld[0],ld[-1]] #e.g. [[2006,1],[2013,12]]

        tw, press, data = [], [], []
        for year, month in ld:
            print " reading: ", year, month
            dpath = 'data/%04d/%02d'%(year,month)
            data  += [ self.fdata[dpath][...] ]
            tw    += [ self.fw['%04d/%02d/utc_sec'%(year,month)].value ]
            press += [ self.fw['%04d/%02d/pressure'%(year,month)].value ]
        
        # make them np-arrays
        tw    = np.concatenate(tw, axis=0)
        press = np.concatenate(press, axis=0)
        data  = np.concatenate(data, axis=0)
        print " ---total shape: ", data.shape

        return {
        'gps'           : data[:,0],    # gps time from scaler data
        'ntanks'        : data[:,1],
        'aop'           : data[:,2],
        'scls_uncorr'   : data[:,3],
        'scls_AoPcorr'  : data[:,4],
        'tw'            : tw,           # time from weather data
        'press'         : press,        # pressure from weather data
        }

def save2file(fo, col, date, data):
    dpath = 'data_wrejec/%04d/%02d' % (date.year,date.month)
    fo[dpath] = data
    # attributes
    for name, _col in col.iteritems():
        fo[dpath].attrs[name] = _col


def Rejection_And_JoinPress(fname_inp, fname_weather, year_lim, fname_out):
    """ rejection of outliers in scaler data in
    'fname_inp'. For rejection-criteria, we use
    bounded values for AoP data. """
    print ' leyendo %s\n' % fname_inp
    finp    = h5py.File(fname_inp, 'r')
    fw      = h5py.File(fname_weather, 'r')
    fo      = h5py.File(fname_out, 'w')
    year_ini, year_end = year_lim
    # nmbr of columns of a typical array
    ncol = finp['data/%04d/%02d'%(year_ini,1)].shape[1]
    nm = 4 # (multiple of 12) no of builks of data to process per loop count
    assert (12%nm==0),\
        " ---> ERROR: 'nm' must be multiple of 12."

    #--- create my list of years & months
    ly, lm = [], []
    for iy in range(year_end-year_ini+1):
        for im_ in range(1, 12, nm):
            lm += [[im for im in range(im_,im_+nm)]]
            ly += [ year_ini+iy ]
    """ now:
    lm = [1,2,3,4],[5,6,7,8],...
    ly = [2006,2006,2006,2007,...]
    """

    #--- for attribute settings
    col = {
    'gps'           : 0, # gps seconds
    'ntanks'        : 1, # nuber of tanks constributing
    'aop'           : 2, # averaged over array
    'scls_uncorr'   : 3, # <bis>
    'scls_AoPcorr'  : 4, # <bis>
    'pressure'      : 5, # synced pressure
    }

    for year, months in zip(ly,lm):
        nrows = 0
        tw, press, data = [], [], []
        for month in months:
            print " reading: ", year, month
            dpath = 'data/%04d/%02d'%(year,month)
            data  += [ finp[dpath][...] ]
            tw    += [ fw['%04d/%02d/utc_sec'%(year,month)].value ]
            press += [ fw['%04d/%02d/pressure'%(year,month)].value ]
        # make them np-arrays
        tw    = np.concatenate(tw, axis=0)
        press = np.concatenate(press, axis=0)
        data  = np.concatenate(data, axis=0)
        print " ---total shape: ", data.shape

        cc    = ~isnan(data[:,2]) # filter-out AoP-NaNs
        data  = data[cc,:]
        size_1	= np.size(data, axis=0) #counts *all* pure numbers (no NaNs)

        #--- filter-out outliers 'AoP'
        ind_r	= data_rejection(3.3, data[:,col['aop']]) # Auger-GAP: Sect 2.3
        #--- filter-out outliers 'ntanks'
        ind_tan = data_rejection(1.0, data[:,col['ntanks']]) # Auger-GAP: Sect 2.3

        IND	= ind_r & ind_tan # total filter
        size_2	= len(find(IND))
        if size_2<10:
            print year, months
            print " ----> WEIRD! size_2/size_1:%d/%d" %(size_2,size_1)
            raise SystemExit

        # info diagnostics
        percent	= 100.*(size_1-size_2)/size_1
        print ' %04d/(%02d-%02d), rechazo: %2.1f\n'%(year,months[0],months[-1],percent)
        # rebine pressure data
        utc_data = data[IND,col['gps']] + 315964800 # [utc]
        press_rebin = rebine(utc_data, tw, press)
        print " > time-sync ok."
        """# pressure correction
        scl_wAoP = data[IND,4]
        scl_wAoP_wPres = scl_wAoP - """
        # prepare output
        data_o = np.empty((len(find(IND)),ncol+1), dtype=np.float32)
        data_o[:,:-1] = data[IND,:]
        data_o[:,-1] = press_rebin
        # save to file
        for month in months:
            print " ---> saving: ", year, month
            dpath = 'data_wrejec/%04d/%02d'%(year,month)
            ti = date2gps(datetime(year,month,1))
            tf = date2gps(datetime(year+month/12,month%12+1,1))
            cc = (data_o[:,0]>=ti) & (data_o[:,0]<tf)
            fo[dpath] = data_o[cc,:]
            # associate column numbers to
            # variables names:
            for vname, _col in col.iteritems():
                fo[dpath].attrs[vname] = _col


def Rejection_And_JoinPress_ii(fname_inp, fname_weather, date_lim, fname_out):
    """ rejection of outliers in scaler data in
    'fname_inp'. For rejection-criteria, we use
    bounded values for AoP data. """
    print ' [*] reading %s\n' % fname_inp
    finp    = h5py.File(fname_inp, 'r')
    fw      = h5py.File(fname_weather, 'r')
    fo      = h5py.File(fname_out, 'w')
    date_ini, date_end = date_lim
    # nmbr of columns of a typical array
    ncol = finp['data/%04d/%02d'%(date_ini.year,date_ini.month)].shape[1]
    nm = 4 # [months] no of bulks of data to process per loop count
    OneDay = timedelta(days=1)

    #--- for attribute settings
    col = {
    'gps'           : 0, # gps seconds
    'ntanks'        : 1, # nuber of tanks constributing
    'aop'           : 2, # averaged over array
    'scls_uncorr'   : 3, # <bis>
    'scls_AoPcorr'  : 4, # <bis>
    'pressure'      : 5, # synced pressure
    }

    # data parser/handler
    # NOTE: makes sure we have data we need or a little more
    dp = mgr_data(fname_inp, fname_weather, nm, date_ini, date_end)

    # all dates that we'll process (includes `ini` and `end`)
    dates = [date_ini+timedelta(days=n) for n in range((date_end-date_ini).days+1)]
    data_o = My2DArray((1,len(col)),dtype=np.float32)  # TODO
    no = data_o.nrow

    for date in dates:
        print " ---> parsing data..."
        # parse scaler & weather data
        data   = dp.parse(date)

        #--- filter-out outliers 'AoP'
        ind_r	= data_rejection(3.3, data['aop']) # Auger-GAP: Sect 2.3
        #--- filter-out outliers 'ntanks'
        ind_tan = data_rejection(1.4, data['ntanks']) # Auger-GAP: Sect 2.3
        #--- data for today *only*
        ind_today = (data['gps']>=date2gps(date)) & (data['gps']<date2gps(date+OneDay))

        IND	    = (ind_r & ind_tan) & ind_today # total filter

        cc     = ~isnan(data['aop'][IND]) # filter-out AoP-NaNs
        # toda la raw-data (sin NaNs) para hoy
        size_1 = (~isnan(data['aop'][ind_today])).nonzero()[0].size
        # toda la raw-data (sin NaNs) filtrada, para hoy
        size_2 = (~isnan(data['aop'][IND])).nonzero()[0].size

        if size_2>1: # don't accumulate in `data_o`
            # info diagnostics
            # TODO: build a time series of the 'percent' value for
            # the whole period of processing, as diagnostics info.
            percent	= 100.*(size_1-size_2)/size_1
            print ' {date}, rechazo, size: {perc:2.1f} %, {size}\n'.format(
                date=date.strftime('%Y %B %d'), 
                perc=percent,
                size=data['gps'][IND].size,
            )
            #--- rebine pressure data
            utc_data = data['gps'][IND] + GPS2UTC # [utc]
            # rebine `data['press']` to the time domain `utc_data`
            print "--> rebining..."
            press_rebin = rebine(utc_data, data['tw'], data['press'])
            print " > time-sync ok."

            assert press_rebin.shape==data['aop'][IND].shape
            #--- accumalate on `data_o`
            for name, _col in col.iteritems():
                if name=='pressure': 
                    data_o[no:,_col] = press_rebin
                else:
                    data_o[no:,_col] = data[name][IND]
            print "--> saved 2 array."
            no = data_o.nrow # += data[name][IND].size # update origin
        #--- save to file
        # if i'm about to change month number or if this if the
        # last iteration (won't happend on 1st iteration).
        if (date+OneDay).month!=date.month or date==dates[-1]:
            print " ---> save2file()"
            # guardar todo en file
            save2file(fo, col, date, data_o[...])
            # reset data_o, no
            data_o = My2DArray((1,len(col)), dtype=np.float32)
            no = 0



def second_rejection(fname_inp, AoP_lim, year_lim, fname_out):
    """ rejection of outliers in scaler data in
    'fname_inp'. For rejection-criteria, we use
    bounded values for AoP data."""
    print ' leyendo %s\n' % fname_inp
    finp    = h5py.File(fname_inp, 'r')
    fo      = h5py.File(fname_out, 'w')
    aop_low, aop_upp = AoP_lim
    year_ini, year_end = year_lim
    ncol = finp['data/%04d/%02d'%(year_ini,1)].shape[1]
    nm = 4
    assert 12%nm==0,\
        " ---> ERROR: 'nm' must be multiple of 12."
    #--- create my list of years & months
    ly, lm = [], []
    for iy in range(year_end-year_ini+1):
        for im_ in range(1, 12, nm):
            lm += [[im for im in range(im_,im_+nm)]]
            ly += [ year_ini+iy ]
    """ now:
    lm = [1,2,3,4],[5,6,7,8],...
    ly = [2006,2006,2006,2007,...]
    """
    for year, months in zip(ly,lm):
        data = My2DArray((2,ncol), dtype=np.float32)
        nrows = 0
        for month in months:
            dpath = 'data/%04d/%02d'%(year,month)
            nt    = finp[dpath].shape[0]
            data.resize_rows(nrows+nt)
            data[nrows:nrows+nt,:] = finp[dpath][...]
            nrows += nt

        data    = ~isnan(data[:,2]) # filter-out AoP-NaNs
        #--- filter-out outliers 'AoP'
        size_1	= data.size(axis=0) # counts *all* pure numbers (no NaNs)
        ind_r	= data_rejection(3.3, data[:,2]) # Auger-GAP: Sect 2.3
        #--- filter-out outliers 'ntanks'
        ntanks  = data[:,1] # no of tanks
        u, sig  = nanmean(ntanks), nanstd(ntanks) 
        ind_tan	= (ntanks>=u-sig) & (ntanks<=u+sig) # Auger-GAP: Sect 2.3

        IND	= ind_r & ind_tan; # total filter
        size_2	= len(find(IND))
        if size_2<10:
            print year, months
            print " ----> WEIRD! size_2/size_1:%d/%d" %(size_2,size_1)
            raise SystemExit

        # info diagnostics
        percent	= 100.*(size_1-size_2)/size_1
        print ' %04d/(%02d-%02d), rechazo: %2.1f\n'%(year,months[0],months[-1],percent)
        # prepare output
        data_o = data[IND,:]
        for month in months:
            print " ---> saving: ", year, months
            dpath = 'data_wrejec/%04d/%02d'%(year,month)
            ti,tf=date2gps(datetime(year,month,1)),date2gps(datetime(year,month+1,1))
            cc = (data_o[:,0]>=ti) & (data_o[:,0]<tf)
            fo[dpath] = data_o[cc,:]

#EOF
