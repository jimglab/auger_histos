%%
%
% nota: dar a escoger si quiere ingresar data desde variable o archivo
%
%OPCION = input('\n MODO "var" (variables entorno) o "file" (data file). \n INGRESAR MODO: ', 's');
function [DATA] = adaptacion(DATA_LO, col_X_LO, col_Y_LO, DATA_HI, col_X_HI, col_Y_HI, RESOLUC)
%
[NF, NC] = size(DATA_HI);
[nf, nc] = size(DATA_LO);
DATA_col_Y_HI = 0;
n = 0;
k = 1;
MIN = 60;           % 1m = 60s
HORA = 60*MIN;      % 1h = 3600s
%fprintf('\n NOTA: - res_baseline = 7min aprox.')
%fprintf('\n       - res_presion = 5min aprox.')
%res_lo = input('\n Ingresa resolucion de DATA_LO en [min] = ');     % resolucion de baseline = 7min aprox.
                                                                    % resolucion de presion = 5min aprox.
res_lo = RESOLUC;
res_lo = res_lo*MIN;    % ahora esta en [seg].
%
% Nota: Se asume que el tiempo esta en la columna 1 de FILE_HI y de FILE_LO.
%
for i = 1:nf
    time_lo_i = DATA_LO(i, col_X_LO);
    for j = 1:NF
        time_hi_j = DATA_HI(j, col_X_HI);
        DATA_HI_aux = DATA_HI(j, col_Y_HI);
        % chequeo que:
        % - 'time_hi_j' este entre 'time_lo_i' y 'time_lo_ii'
        % - 'DATA_HI_aux' sea !=0 y !=inf, para no acumular mala data.
	cond_1	= time_hi_j >= time_lo_i - res_lo && time_hi_j <= time_lo_i + res_lo;
	cond_2	= DATA_HI_aux~=inf && DATA_HI_aux~=0 && ~isnan(DATA_HI_aux);
        if cond_1 && cond_2
            DATA_col_Y_HI = DATA_HI_aux + DATA_col_Y_HI;
            n = n + 1;
        end 
    end 
    if n~=0
        time_LO_adap(k) = DATA_LO(i, col_X_LO);
        DATA_LO_adap(k) = DATA_LO(i, col_Y_LO);
        DATA_HI_adap(k) = DATA_col_Y_HI/n;
        k = k + 1;
    end 
    % reseteo los valores
    DATA_col_Y_HI = 0;
    n = 0;
end
%
if (k>1)
        [Nfil, Ncol]=size(DATA_HI_adap);
        %DATA_LO = DATA_LO(1:Ncol,:);       % ahora 'DATA_LO' y 'DATA_HI_adap' tienen el mismo nro
                                            % de filas.
        DATA_HI_adap = DATA_HI_adap(1:Nfil,:);
        %
        DATA = cat(2, time_LO_adap', DATA_LO_adap', DATA_HI_adap');     % esta data contiene lo mismo de 'DATA_LO' y 'DATA_HI_adap', es
                                                                        % decir ahora son datos simultaneos.
else
        DATA = 77;      % valor flag
end
%%
