%
% Con este script, vamos a construir data corregida por presion.
% Vamos a obtener:
% - scalers, SIN AoP correc, CON pressure correc
% - scalers, CON AoP correc, CON pressure correc
%
fname_out	= 'data_final_2006-2013.dat'
data		= load('scals_wAoPcorrec_allarray_3pmtsON_wpress_correc_2006-2013_wrejec.dat');
time		= data(:, 1);
press		= data(:, 2);
ntanks		= data(:, 3);
aop		= data(:, 4);
scl_woAoPc	= data(:, 5);
scl_wAoPc	= data(:, 6);
clear data;

% sacado de "find_parameters_scalswoAoPcorr_and_press.py",
% es la pendiente de la correlacion de "scalers SIN AoPcorrec, con presion"
m_press_woAoP	= -6.9;			
% sacado de "histos_correl_wpress.py"
% es la pendiente de "scalers, CON AoPcorrec, con presion"
m_press_wAoP	= -6.7;		
%
avrg_press	= mean(press);

%-------------- scaler, SIN AoPcorrec, CON pressure correc
scl_woAoPc_wpressc	= scl_woAoPc - m_press_woAoP*(press - avrg_press);
%-------------- scalers, CON AoPcorrec, CON pressure correc
scl_wAoPc_wpressc	= scl_wAoPc - m_press_wAoP*(press - avrg_press);
%
%---------------------------data q vamos a guardar:
DATA(:, 1) = time;			% [seg_utc] tiempo utc
DATA(:, 2) = press;			% [mmHg] presion
DATA(:, 3) = ntanks;			% [1] nro de tanques, cuya data q pasaron todos los filtros para llegar hasta aqui
DATA(:, 4) = aop;			% [1] <AoP>_{3PMTs, array}
DATA(:, 5) = scl_woAoPc_wpressc;	% [cts/s/10m2] <scals^{w/o AoPcorr}>_{array}^{w/  press}
DATA(:, 6) = scl_wAoPc_wpressc;		% [cts/s/10m2] <scals^{w/  AoPcorr}>_{array}^{w/  press}
DATA(:, 7) = scl_woAoPc;		% [cts/s/10m2] <scals^{w/o AoPcorr}>_{array}^{w/o press}
DATA(:, 8) = scl_wAoPc;			% [cts/s/10m2] <scals^{w/  AoPcorr}>_{array}^{w/o press}

dlmwrite(fname_out, DATA, 'delimiter', ' ', 'precision', 13)
fprintf('\n hemos generado %s!\n', fname_out)
