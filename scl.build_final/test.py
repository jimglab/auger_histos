#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
from pylab import figure, savefig, close, find
import numpy as np
from datetime import datetime, timedelta
from h5py import File as h5
import os, sys
from shared_funcs import funcs as sf
import funcs as ff

year_ini, year_end = 2006, 2015
#fname_inp = '../out/out.scl.build_final/scalers_wAoPcorrection_%04d-%04d_allarray_3pmtsON_q2.5_w2datarejecs.h5'%(year_ini,year_end)
fname_inp	= '../out/out.scl.build_final/scalers_wAoPcorrection_%04d-%04d_allarray_3pmtsON_wdatarejec_q2.5_.dat'%(year_ini,year_end)
fname_weather = '../out/out.build_weather/weather_5min_%04d-%04d.h5'%(year_ini,year_end)
finp = h5(fname_inp, 'r')
fw   = h5(fname_weather, 'r')

date_ini = datetime(2006, 1, 1)
date_end = datetime(2015,12, 1)

#------ "original pressure"
fig = figure(1, figsize=(15,4))
ax  = fig.add_subplot(111)
for year in range(date_ini.year, date_end.year+1):
    for month in range(date_ini.month, date_end.month+1):
        print " reading: ", year, month
        tw    = fw['%04d/%02d/utc_sec'%(year,month)].value
        press = fw['%04d/%02d/pressure'%(year,month)].value 

        ax.plot((tw-1136073600)/(365*86400.), press, '-ob', ms=1, mec='none', alpha=0.3)

ax.grid()
#fig.savefig('test.png', dpi=135, bbox_inches='tight')
close(fig)

#------ other
print ' leyendo %s\n' % fname_inp
finp	= h5(fname_inp, 'r')
#aop_low, aop_upp = AoP_lim
ncol = finp['data/%04d/%02d'%(year_ini,1)].shape[1]
nm = 4 # (multiple of 12) no of builks of data to process per loop count
assert (12%nm==0),\
    " ---> ERROR: 'nm' must be multiple of 12."

#--- create my list of years & months
ly, lm = [], []
for iy in range(year_end-year_ini+1):
    for im_ in range(1, 12, nm):
        lm += [[im for im in range(im_,im_+nm)]]
        ly += [ year_ini+iy ]
""" now:
lm = [1,2,3,4],[5,6,7,8],...
ly = [2006,2006,2006,2007,...]
"""
fig = figure(1, figsize=(15,4))
ax  = fig.add_subplot(111)
for year, months in zip(ly,lm):
    data = sf.My2DArray((2,ncol), dtype=np.float32)
    nrows = 0
    tw, press, data = [], [], []
    for month in months:
        print " reading: ", year, month
        dpath = 'data/%04d/%02d'%(year,month)
        data  += [ finp[dpath][...] ]
        tw    += [ fw['%04d/%02d/utc_sec'%(year,month)].value ]
        press += [ fw['%04d/%02d/pressure'%(year,month)].value ]

    # make them np-arrays
    tw    = np.concatenate(tw, axis=0)
    press = np.concatenate(press, axis=0)
    data  = np.concatenate(data, axis=0)
    #ax.plot((tw-1136073600)/(365*86400.),press,'-ob',mec='none',ms=1,alpha=.5)
    cc    = ~np.isnan(data[:,2]) # filter-out AoP-NaNs
    data  = data[cc,:]
    size_1	= np.size(data, axis=0) #counts *all* pure numbers (no NaNs)

    #--- filter-out outliers 'AoP'
    ind_r	= ff.data_rejection(3.3, data[:,2]) # Auger-GAP: Sect 2.3
    #--- filter-out outliers 'ntanks'
    ind_tan = ff.data_rejection(1.0, data[:,1]) # Auger-GAP: Sect 2.3

    IND	= ind_r & ind_tan # total filter
    size_2	= len(find(IND))
    if size_2<10:
        print year, months
        print " ----> WEIRD! size_2/size_1:%d/%d" %(size_2,size_1)
        raise SystemExit

    # info diagnostics
    percent	= 100.*(size_1-size_2)/size_1
    print ' %04d/(%02d-%02d), rechazo: %2.1f\n'%(year,months[0],months[-1],percent)
    # rebine pressure data
    press_rebin = sf.rebine(data[IND,0], tw, press, nbef=5, naft=5)
    tt = data[IND,0]
    ax.plot((tt-1136073600)/(365*86400.),press_rebin,'-ob',mec='none',ms=1,alpha=.5)

ax.grid()
fig.savefig('test.png',dpi=135,bbox_inches='tight')
close(fig)


#EOF
