#!/usr/bin/env ipython
"""
 author: j.j. masias meza
 runtime: ~3min
"""
from pylab import *
from scipy.io.netcdf import netcdf_file
from datetime import datetime, timedelta
import numpy as np
import os, argparse
import shared_funcs.funcs as sf

#--- parse args
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    description="""
    This generates 2d histograms of the correlation between
    charge histogram counts (AoP-corrected) and atmospheric
    pressure.
    """,
)
parser.add_argument(
'-src', '--dir_src',
type=str,
help='input directory. Should be the output dir of "build_aop.corr_phys"',
)
parser.add_argument(
'-fi', '--fname_inp',
type=str,
help='input ASCII file: time-averaged charge-histograms. Should be the output dir of "build_avr_histos"',
)
parser.add_argument(
'-dst', '--dir_dst',
type=str,
help='output dir.',
)
parser.add_argument(
'-m', '--maskname',
type=str,
default='shape.ok_and_3pmt.ok',
help='Quality cut options: \'shape.ok_and_3pmt.ok\', \'3pmt.ok\', or \'all\'. We \
dont make quality cuts at this point. This is just to use the label of the \
previous stages of processing; it\'s healthy to keep track of it.'
)
parser.add_argument(
'-rx', '--range_x',
type=np.float64,
nargs=2,
default=[845., 880.],
metavar=('XMIN','XMAX'),
help='x-range (pressure limits) where to build the 2D histograms',
)
parser.add_argument(
'-ry', '--range_y',
type=np.float64,
nargs=2,
default=[0.7, 1.3],
metavar=('YMIN','YMAX'),
help='y-range (normalized rate limits) where to build the 2D histograms',
)
parser.add_argument(
'-bins', '--bins',
type=int,
default=50,
help='number of bins by which the input 2D histograms will be discretized.',
)
parser.add_argument(
'-mintank', '--mintank',
type=int,
default=150,
help="""
For each data-point in the input files, there is an associated 
number-of-tanks field. Such value is the number of SD tanks that 
were filtered-in in the time window of each timestamp, in the energy interval 
of that data-point.
So such value gives us the 'statistics' at each timestamp.
With this argument, we can choose a lower threshold to filter the
data-points by its 'statistics' weight.
""",
)
parser.add_argument(
'-ini', '--ini_date', 
type=str, 
action=sf.arg_to_datetime,
default='01/01/2006',
help='initial date',
)
parser.add_argument(
'-end', '--end_date', 
type=str, 
action=sf.arg_to_datetime,
default='31/12/2015',
help='end date',
)
pa = parser.parse_args()

#------------------------- grab typical values of the charge-histograms
print "\n ---> reading: %s\n" % pa.fname_inp
prom    = np.loadtxt(pa.fname_inp)
typic   = np.mean(prom[:,1:], axis=0) # average of the array in the whole period of analysis
lim     = np.empty(50*2).reshape(50,2)
#----------------------------------------------------------------------

nbins_mev = 50
H_uncorr  = np.zeros((nbins_mev,pa.bins,pa.bins))
H_corr    = np.zeros((nbins_mev,pa.bins,pa.bins))
RANGE     = [pa.range_x, pa.range_y]

# relevant only when using LL-weather station: this is a time
# interval where an "anomally" occurs in the pressure data.
# NOTE: normally, we use CLF-weather station.
JUMP_INI = datetime(2009, 4, 1)
JUMP_FIN = datetime(2009,10, 1)

avr_buff = {'press_sum': 0.0, 'press_nclean': 0}
ntot = 0
date = pa.ini_date
while date <= pa.end_date:
    yyyy  = date.year
    mm    = date.month
    dd    = date.day
    fname_inp = '%s/%04d/%04d_%02d_%02d.nc' % (pa.dir_src, yyyy, yyyy, mm, dd)
    #xx   = (date<JUMP_INI) or (date>JUMP_FIN)      # to avoid the pressure "anomally" in LL-station data
    if os.path.isfile(fname_inp):# and xx:
        try:
            f           = netcdf_file(fname_inp, 'r')
            print " [*] opening: %s" % fname_inp
            ntanks      = f.variables['ntanks'].data
            cc          = ntanks > pa.mintank
            rate_corr   = f.variables['aop.corrected_cnts_phys'].data[cc] # no normaliza bien :'( TODO: check this.
            rate_uncorr = f.variables['aop.uncorrected_cnts_phys'].data[cc]
            press       = f.variables['pressure'].data[cc]

            for i in range(nbins_mev):
                # w/o AoP correction
                cnts        = rate_uncorr[:,i] / typic[i]           # [1]
                H_uncorr[i,:,:] += np.histogram2d(press, cnts, 
                    bins=[pa.bins,pa.bins], range=RANGE, normed=False)[0]
                # w/ AoP correction
                cnts        = rate_corr[:,i] / typic[i]
                H_corr[i,:,:] += np.histogram2d(press, cnts, 
                    bins=[pa.bins,pa.bins], range=RANGE, normed=False)[0]

            # for global pressure average (in the time ini_date-end_date)
            _cc                      = ~np.isnan(press)
            avr_buff['press_sum']    += press[_cc].sum()
            avr_buff['press_nclean'] += _cc.nonzero()[0].size

            # number of files included in the 2D-histogram
            ntot += 1
            f.close()

        except KeyboardInterrupt:
            raise SystemExit("\n [!] KEYBOARD INTERRUPT...\n")

        except:
            print "\n [-] CORRUPT: %s" % fname_inp

    date    += timedelta(days=1)        # next day...

# calc pressure average
press_mean = avr_buff['press_sum'] / avr_buff['press_nclean']

#--------------------------------------------------------------------
# same header for all files
HEADER='lim-press  : %g:%g\n' % (pa.range_x[0],pa.range_x[1]) +\
       'lim-rate   : %g:%g\n' % (pa.range_y[0],pa.range_y[1]) +\
       'bins       : %d\n' % pa.bins +\
       'maskname   : %s\n' % pa.maskname +\
       'ini_date   : %s\n' % pa.ini_date.strftime('%d/%b/%Y %H:%M') +\
       'end_date   : %s\n' % pa.end_date.strftime('%d/%b/%Y %H:%M') +\
       'press_mean : %9.8g' % press_mean

print "\n [*] saving histograms..."
os.system("mkdir -p " + pa.dir_dst)
for i in range(nbins_mev):
    fname_out = '%s/H2Duncorr_%s_%02d.txt' % (pa.dir_dst, pa.maskname, i)
    np.savetxt(fname_out, H_uncorr[i], fmt='%9.9g', header=HEADER)

    fname_out = '%s/H2Dcorr_%s_%02d.txt' % (pa.dir_dst, pa.maskname, i)
    np.savetxt(fname_out, H_corr[i], fmt='%9.9g', header=HEADER)

print "\n ---> maskname: %s" % pa.maskname
print   " ---> 2D-histograms at: %s" % pa.dir_dst
#--------------------------------------------------------------------
#EOF
