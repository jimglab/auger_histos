## 2D-histograms of the correlation of pressure vs rates

```bash
./test.py -- \
    -src /media/scratch1/auger/build_aop.corr_phys \
    -fi /media/scratch1/auger/build_avr_histos/avr_histos__Jan2006-Dec2014.txt \
    -dst /media/scratch1/auger/build_hist2d_correl.pressure \
    -m shape.ok_and_3pmt.ok \
    -rx 845.0 880.0 \
    -ry 0.7 1.3 \
    -bins 50 \
    -ini 01/01/2006 \
    -end 31/12/2014
```

Full description:

	$ ./test.py -- -husage: test.py [-h] [-src DIR_SRC] [-fi FNAME_INP] [-dst DIR_DST]
				   [-m MASKNAME] [-rx XMIN XMAX] [-ry YMIN YMAX] [-bins BINS]
				   [-ini INI_DATE] [-end END_DATE]

	This generates 2d histograms of the correlation between charge histogram
	counts (AoP-corrected) and atmospheric pressure.

	optional arguments:
	  -h, --help            show this help message and exit
	  -src DIR_SRC, --dir_src DIR_SRC
							input directory. Should be the output dir of
							"build_aop.corr_phys" (default: None)
	  -fi FNAME_INP, --fname_inp FNAME_INP
							input ASCII file: average charge-histograms. Should be
							the output dir of "build_avr_histos" (default: None)
	  -dst DIR_DST, --dir_dst DIR_DST
							output dir. (default: None)
	  -m MASKNAME, --maskname MASKNAME
							Quality cut options: 'shape.ok_and_3pmt.ok',
							'3pmt.ok', or 'all'. We dont make quality cuts at this
							point. This is just to use the label of the previous
							stages of processing; it's healthy to keep track of
							it. (default: shape.ok_and_3pmt.ok)
	  -rx XMIN XMAX, --range_x XMIN XMAX
							x-range (pressure limits) where to build the 2D
							histograms (default: [845.0, 880.0])
	  -ry YMIN YMAX, --range_y YMIN YMAX
							y-range (normalized rate limits) where to build the 2D
							histograms (default: [0.7, 1.3])
	  -bins BINS, --bins BINS
							number of bins by which the input 2D histograms will
							be discretized. (default: 50)
	  -ini INI_DATE, --ini_date INI_DATE
							initial date (default: 01/01/2006)
	  -end END_DATE, --end_date END_DATE
							end date (default: 31/12/2015)

---
## make fits of the 2D-histograms

```bash
./test_fits.py -- \
    -src /media/scratch1/auger/build_hist2d_correl.pressure \
    -fi /media/scratch1/auger/build_avr_histos/avr_histos__Jan2006-Dec2014.txt \
    -m shape.ok_and_3pmt.ok \
    -fo /media/scratch1/auger/build_hist2d_correl.pressure/fit_params.txt \
    -plot /media/scratch1/auger/build_hist2d_correl.pressure/fits \
    -cbmax 6e4
```

Full description:

    $ ./test_fits.py -- -h
    usage: test_fits.py [-h] [-src DIR_SRC] [-fi FNAME_INP] [-m MASKNAME]
                        [-fo FNAME_OUT] [-plot DIR_FIG] [-cbmax CBMAX]

    This uses the 2d histograms of the correlations between the charge histogram
    counts and atmosph pressure, and calculates linear fits (at each energy Ed).
    These fits are performed using the values of the 2d histograms as weights.

    optional arguments:
      -h, --help            show this help message and exit
      -src DIR_SRC, --dir_src DIR_SRC
                            input directory. Should be the output dir of
                            "build_aop.corr_phys" (default: None)
      -fi FNAME_INP, --fname_inp FNAME_INP
                            input ASCII file: average charge-histograms. Should be
                            the output dir of "build_avr_histos" (default: None)
      -m MASKNAME, --maskname MASKNAME
                            Quality cut options: 'shape.ok_and_3pmt.ok',
                            '3pmt.ok', or 'all'. We dont make quality cuts at this
                            point. This is just to use the label of the previous
                            stages of processing; it's healthy to keep track of
                            it. (default: shape.ok_and_3pmt.ok)
      -fo FNAME_OUT, --fname_out FNAME_OUT
                            output ASCII file, containing the calculated fit
                            parameters. (default: None)
      -plot DIR_FIG, --plot DIR_FIG
                            Use it if you want to generate figure files on DIR_FIG
                            directory. If this argument is used, the .png files
                            will also be placed in DIR_FIG. (default: None)
      -cbmax CBMAX, --cbmax CBMAX
                            max value of color scale in the contour plots of the
                            2D histograms (default: 60000.0)

---
## make figure for SolPhys paper

Plot the linear fit of the relation between Histogram rates and AoP.

```bash
finp=~oauger/build_hist2d_correl.pressure__wll_l2/H2Dcorr_shape.ok_and_3pmt.ok_11.txt
./fit_single.py -- \
    -fi $finp \
    -plot ./test/paper.solphys_Histos.vs.AoP.png \
    -iE 11 \
    -cbmax 1e4 \
    -xplot 845. 880. \
    -yplot 0.9 1.1
```



<!--- EOF -->
