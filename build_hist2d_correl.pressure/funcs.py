#------------------------------------------------
from __future__ import print_function
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt 
import numpy as np
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib.colors import LogNorm
#------------------------------------------------
from pylab import *
from scipy.io.netcdf import netcdf_file
import os

def make_hist2d(fname, lim, cbmax, title, i):
    H2D     = np.loadtxt(fname, unpack=True) + 1. # le sumo uno para q sea >0.
    bins    = H2D.shape[0]

    aop     = linspace(2., 5., bins)
    scal    = linspace(lim[0], lim[1], bins)

    fig     = figure(1, figsize=(6, 4))
    ax      = fig.add_subplot(111)

    CBMIN   = 1.0
    CBMAX   = cbmax #6e6
    surf    = ax.contourf(aop, scal, H2D, facecolors=cm.jet(H2D), linewidth=0, 
        cmap=cm.gray_r, alpha=0.9, vmin=CBMIN, vmax=CBMAX, norm=LogNorm())

    m = cm.ScalarMappable(cmap=surf.cmap, norm=surf.norm)
    m.set_array(H2D)

    axcb = plt.colorbar(m)
    LABEL_COLOR_BAR = 'points per bin square'
    axcb.set_label(LABEL_COLOR_BAR, fontsize=20)
    ax.set_ylabel('partial-integrated rate [cts/s/m2]')
    ax.set_xlabel('AoP')
    ax.set_title(title)

    m.set_clim(vmin=CBMIN, vmax=CBMAX)
    ax.grid()
    plt.tight_layout()

    savefig('../figs/hist2d/hist2d_%02d.png'%i, dpi=135, bbox_inches='tight')
    close()


def make_fitplot_muon_poster(dir_fig, maskname, fname, stuff, cbmax, title):
    xlim    = stuff[0][0]
    ylim    = stuff[0][1]
    m       = stuff[1][0]
    b       = stuff[1][1]
    xvec    = stuff[2][0]
    yvec    = stuff[2][1]

    H2D     = np.loadtxt(fname, unpack=True) + 1. # le sumo uno para q sea >0.
    bins    = H2D.shape[0]

    aop     = linspace(xlim[0], xlim[1], bins)
    scal    = linspace(ylim[0], ylim[1], bins)

    fig     = figure(1, figsize=(6, 4))
    ax      = fig.add_subplot(111)

    CBMIN   = 1.0
    CBMAX   = cbmax #6e6
    surf    = ax.contourf(aop, scal, H2D, facecolors=cm.jet(H2D), linewidth=0, 
        cmap=cm.gray_r, alpha=0.9, vmin=CBMIN, vmax=CBMAX, norm=LogNorm())
    LABEL = '8yr fit:\nm:%3.3f' % (m)
    ax.plot(xvec, m*xvec+b, '--', c='red', alpha=0.6, lw=3., label=LABEL)

    m = cm.ScalarMappable(cmap=surf.cmap, norm=surf.norm)
    m.set_array(H2D)

    axcb = plt.colorbar(m)
    LABEL_COLOR_BAR = 'points per bin square'
    axcb.set_label(LABEL_COLOR_BAR, fontsize=14)
    ax.set_ylabel('$\langle H_{id,e}^{corr/AoP} \\rangle_{\\forall id}$', fontsize=19)
    ax.set_xlabel('pressure [hPa]', fontsize=17)
    ax.set_title(title, fontsize=18)
    ax.set_ylim(ylim[0], ylim[1])   # no deberia necesitarlo
    #ax.set_xlim(xlim[0], xlim[1])  # no deberia necesitarlo
    ax.legend(loc='best', fontsize=15)

    m.set_clim(vmin=CBMIN, vmax=CBMAX)
    ax.grid()
    plt.tight_layout()

    fname_fig = '%s/hist2d_muon.poster_%s' % (dir_fig, maskname)
    savefig('%s.png'%fname_fig, format='png', dpi=135, bbox_inches='tight')
    savefig('%s.eps'%fname_fig, format='eps', dpi=135, bbox_inches='tight')
    print(" --> generado: %s" % fname_fig)
    close()

#EOF
