#!/usr/bin/env ipython
"""
 author: j.j. masias meza
 runtime: ~1min
"""
import funcs as ff
import os, argparse
import shared_funcs.funcs as sf
import numpy as np
from datetime import datetime, timedelta

#--- parse args
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    description="""
    This uses the 2D-histograms of the correlations between 
    the charge histogram counts and atmosph pressure, and 
    calculates linear fits (at each energy Ed). These fits 
    are performed using the values of the 2d histograms as 
    weights.
    """,
)
parser.add_argument(
'-src', '--dir_src',
type=str,
help='input directory. Should be the output dir of "build_aop.corr_phys"',
)
parser.add_argument(
'-m', '--maskname',
type=str,
default='shape.ok_and_3pmt.ok',
help='Quality cut options: \'shape.ok_and_3pmt.ok\', \'3pmt.ok\', or \'all\'. We \
dont make quality cuts at this point. This is just to use the label of the \
previous stages of processing; it\'s healthy to keep track of it.'
)
parser.add_argument(
'-fo', '--fname_out',
type=str,
help='Output ASCII file, containing the calculated fit parameters.',
)
parser.add_argument(
'-plot', '--plot',
type=str,
metavar='DIR_FIG',
help='Use it if you want to generate figure files on DIR_FIG directory. \
If this argument is used, the .png files will also be placed in DIR_FIG.',
)
parser.add_argument(
'-cbmax', '--cbmax',
type=np.float64,
default=6e4,
help='Max value of color scale in the contour plots of the 2D histograms',
)
parser.add_argument(
'-fm', '--figmode',
type=str,
default='verb',
help='Figure mode: "verb" (verbose info in figure) or "paper" (pretty stuff to look good)',
)
pa = parser.parse_args()

#--- open a sample input file to grab some parameters
fsample = '%s/H2Dcorr_%s_00.txt' % (pa.dir_src,pa.maskname)
press_mean   = float(sf.ReadParam_from_2dHist(fsample,'press_mean')[0])
# NOTE: xlim and ylim are limits in units of deviations from respect 
# to their associated mean.
xlim    = [ (_/press_mean-1.) for _ in map(float, sf.ReadParam_from_2dHist(fsample,'lim-press' ))]
ylim    = [ (_ - 1.) for _ in map(float, sf.ReadParam_from_2dHist(fsample,'lim-rate'))]
bins    = int(sf.ReadParam_from_2dHist(fsample,'bins')[0])
date_ini_str = (sf.ReadParam_from_2dHist(fsample,'ini_date'))[:-1]
date_end_str = (sf.ReadParam_from_2dHist(fsample,'end_date'))[:-1]
assert pa.maskname == sf.ReadParam_from_2dHist(fsample, 'maskname')[0][1:-1]
# datetimes of the borders
date_ini = datetime.strptime(date_ini_str,'%d/%b/%Y %H:%M')
date_end = datetime.strptime(date_end_str,'%d/%b/%Y %H:%M')
# estimate the total number of years in this window
nyr      = (date_end - date_ini).days/365.

yval    = np.linspace(ylim[0], ylim[1], bins)
xval    = np.linspace(xlim[0], xlim[1], bins)

xvec    = np.empty(bins*bins)
yvec    = np.empty(bins*bins)

# build grid for the 2d-histograms
for i in range(bins):
    ini = i*bins
    fin = (i+1)*bins
    yvec[ini:fin] = yval
    xvec[ini:fin] = xval[i]*np.ones(bins)

#******************************************************
nbins_mev = 50    # number of deposited-energy (Ed) channels
dE        = 1000./nbins_mev  # resolution in Ed
m         = np.empty(nbins_mev, dtype=np.float64)
b         = np.empty(nbins_mev, dtype=np.float64)

os.system("mkdir -p " + pa.plot)
for i_h in range(nbins_mev):
    fname_inp = '%s/H2Dcorr_%s_%02d.txt' % (pa.dir_src, pa.maskname, i_h)
    H   = np.loadtxt(fname_inp) + 1.0  # (*1)

    H   = H.reshape(bins*bins)
    wei = np.sqrt(H)        # weights for fit
    p   = np.polyfit(xvec, yvec, 1, w=wei, cov=True)

    m[i_h]  = p[0][0]
    b[i_h]  = p[0][1]

    print " i: %02d/%02d" % (i_h+1, nbins_mev)
    stuff   = []
    title   = "Ed: %02d-%02d MeV" % (i_h*dE, i_h*dE+dE)
    stuff   += [[m[i_h], b[i_h]]]
    stuff   += [[xvec*100., yvec*100.]] # percentage units
    if pa.plot!='':
        fname_fig = pa.plot+'/'+fname_inp.split('/')[-1].replace('.txt','.png')
        sf.make_fitplot(fname_fig, fname_inp, stuff, 
            cblim=[1., pa.cbmax], 
            title=title, 
            figsize = (6,3.5) if pa.figmode=='paper' else (6,4),
            flabel='%d yr fit:\nm:%2.2f' % (int(nyr), m[i_h]), 
            cblabel='points per bin square', 
            xlabel='$\Delta$ pressure [%]', 
            ylabel= '$\Delta \langle H_{id,e}^{corr/AoP} \\rangle_{\\forall id}$ [%]' \
            if pa.figmode=='paper' else 'partial-integrated rate deviation [%]'
        )

#(*1): add 1 to avoid singularities, as weights are inversely proportional
print "\n DONE! :-)"
#******************************************************
Ed   = np.arange(0.5*dE, 1000.+0.5*dE, dE)
data_out = np.array([Ed, m, b])
HEADER = 'ini_date   : %s\n' % date_ini_str +\
         'end_date   : %s\n' % date_end_str +\
         'press_mean : %9.8g' % press_mean
np.savetxt(pa.fname_out, data_out.T, fmt='%8.7g', header=HEADER)
print "\n ---> generated: %s\n" % pa.fname_out
#EOF
