#!/usr/bin/env ipython
"""
 author: j.j. masias meza
 runtime: ~1min
"""
from funcs import *
import os, argparse
import shared_funcs.funcs as sf

#--- parse args
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    description="""
    This uses the 2D-histograms of the correlations between 
    the charge histogram counts and atmosph pressure, and 
    calculates linear fits (at each energy Ed). These fits 
    are performed using the values of the 2d histograms as 
    weights.
    """,
)
parser.add_argument(
'-fi', '--fname_inp',
type=str,
help='input ASCII file: average charge-histograms. Should be the output dir of "build_avr_histos"',
)
parser.add_argument(
'-plot', '--plot',
type=str,
metavar='DIR_FIG',
default='',
help='Use it if you want to generate figure files on DIR_FIG directory. \
If this argument is used, the .png files will also be placed in DIR_FIG.',
)
parser.add_argument(
'-iE', '--iE',
type=int,
default=11, # iE=11 for Ed=240MeV
help='energy index for charge-hist band.',
)
parser.add_argument(
'-cbmax', '--cbmax',
type=np.float64,
default=6e4,
help='max value of color scale in the contour plots of the 2D histograms',
)
parser.add_argument(
'-xplot', '--xplot',
type=float,
nargs=2,
metavar=('XMIN', 'XMAX'),
default=[845.,880.],
help='limits for plot in x-axis.',
)
parser.add_argument(
'-yplot', '--yplot',
type=float,
nargs=2,
metavar=('YMIN', 'YMAX'),
default=[0.7,1.3],
help='limits for plot in y-axis.',
)
pa = parser.parse_args()


#--- open a sample input file to grab some parameters
fsample = pa.fname_inp
xlim = map(float, sf.ReadParam_from_2dHist(fsample,'lim-press' ))
ylim = map(float, sf.ReadParam_from_2dHist(fsample,'lim-rate'))
bins = int(sf.ReadParam_from_2dHist(fsample,'bins')[0])
date_ini_str = ' '.join(sf.ReadParam_from_2dHist(fsample,'ini_date'))[1:-1]
date_end_str = ' '.join(sf.ReadParam_from_2dHist(fsample,'end_date'))[1:-1]

# los rangos lo saco del 'RANGE' en "../post.py"
#ylim    = [0.7, 1.3]        # los mismos q estan en "./build_H2D.asciis.py"
#xlim    = [845., 880.]      # <<bis>>
yval    = np.linspace(ylim[0], ylim[1], bins)
xval    = np.linspace(xlim[0], xlim[1], bins)

xvec    = np.empty(bins*bins)
yvec    = np.empty(bins*bins)

# construyo grilla
for i in range(bins):
    ini = i*bins
    fin = (i+1)*bins
    yvec[ini:fin] = yval
    xvec[ini:fin] = xval[i]*np.ones(bins)

#******************************************************
nbins_mev   = 50    # nro de canales de Ed
dE          = 1000./nbins_mev  # resolucion de Ed
#cbmax       = 6e4

os.system("mkdir -p " + pa.plot)

H   = np.loadtxt(pa.fname_inp) + 1.0  # (*1)

H   = H.reshape(bins*bins)
wei = np.sqrt(H)        # weights for fit
p   = np.polyfit(xvec, yvec, 1, w=wei, cov=True)

m, b = p[0][0], p[0][1]

print " i: %02d/%02d" % (pa.iE+1, nbins_mev)
stuff   = []
stuff   += [[m, b]]
stuff   += [[xvec, yvec]]
if pa.plot!='':
    fname_fig = pa.plot+'/'+pa.fname_inp.split('/')[-1].replace('.txt','.png')
    make_fitplot(fname_fig, pa.fname_inp, stuff, cblim=[1.,pa.cbmax], 
        title="Ed: %02d-%02d MeV"  % (pa.iE*dE, pa.iE*dE+dE),
        xylim=[pa.xplot,pa.yplot],
        flabel='10yr fit:\nm:%4.4f' % m,
        cblabel='points per bin square',
        xlabel='pressure [hPa]',
        ylabel='$\langle H_{id,e}^{corr/AoP} \\rangle_{\\forall id}$',
        )


print "\n DONE! :-)"
#******************************************************
data_out = np.array([m, b])
fname_ascii = pa.plot+'/'+pa.fname_inp.split('/')[-1].replace('.txt','__fit.txt')
np.savetxt(fname_ascii, data_out.T, fmt='%8.7g')
print "\n ---> generated: %s\n" % fname_ascii

#EOF
