#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
"""
- build 2D histograms of the correlations between the pressure-corrected rates and the geopotential height.
"""
from pylab import *
from scipy.io.netcdf import netcdf_file
from datetime import datetime, timedelta
import numpy as np
import os
from os.path import isfile, isdir
from scipy.interpolate import (
    splrep,     # Spline interpolation
    splev)      # Spline evaluator
from h5py import File as h5
from matplotlib.colors import LogNorm

HOME     = os.path.expanduser('~')
this_dir = os.environ['AUGER_REPO'] # repo dir
maskname = 'shape.ok_and_3pmt.ok'#'shape.ok_and_3pmt.ok'#'3pmt.ok' # 'all'
dir_src  = '%s/out/out.build_pressure.corr/%s/15min' % (this_dir, maskname)
dir_AvrHist = '%s/out/out.build_temp_hist.and.fits' % this_dir
dir_src_gdas = '%s/data_gdas' % HOME
dir_dst = '%s/out/out.build_temp_hist.and.fits/hist2d' % this_dir

# let's make sure our output dir exists
assert isdir(dir_dst), " doesn't exist: %s" % dir_dst

#---------------------- agarro valores tipicos para construir rangos
fname_avr= '%s/avr_histos_press_%s.txt' % (dir_AvrHist, maskname)
prom    = np.loadtxt(fname_avr)
typic   = np.nanmean(prom[:,1:], axis=0) # agarro el promedio del array de los 8 anios!
#-------------------------------------------------------------------

year_ini    = 2006
year_fin    = 2014      # inclusive
date        = datetime(year_ini,   1, 1)
MAX_DATE    = datetime(year_fin+1, 1, 1)
LEVEL       = 100  # [mb or hPa] isobaric surface
lname       = 'level_%04d' % (LEVEL)

ntot    = 0
Bins    = 100
hg_min, hg_max = 16050, 16650
cts_min, cts_max = 0.95, 1.1 #1.04 #1.1
ax_cts, ax_hg = np.linspace(cts_min, cts_max, Bins), np.linspace(hg_min, hg_max, Bins)
range_cts = (cts_min, cts_max)
range_hg  = (hg_min, hg_max)
RANGE   = (range_hg, range_cts)

#--------------------------------------------------------------------
day     = 86400./(365.*86400.)      # [yr]
dT      = 0.01 #0.05 #0.3 #0.05     # [yr]
ntot    = 0
date    = datetime(year_ini,   1, 1)
ch_Eds  = np.arange(0, 50) #(10,11,12,13)#(3,4,5)   # indices de los canales de Ed a promediar
nEd     = len(ch_Eds)
H2D     = np.zeros((nEd, Bins, Bins))
j=0; k=1; r=0.0; n=0
tt=[]; rr=[]
yyyy2 = date.year - 1 
missing = [] # list of missing input files
while date < MAX_DATE:
    yyyy    = date.year
    mm  = date.month
    dd  = date.day
    fname_inp = '%s/%04d/%04d_%02d_%02d.nc' % (dir_src, yyyy, yyyy, mm, dd)
    if not isfile(fname_inp):
        missing += [ fname_inp ]
        date += timedelta(days=1)   # next day...
        continue

    f   = netcdf_file(fname_inp, 'r')
    if yyyy2!=yyyy:
        fname_inp_gdas = '%s/test_%04d.h5' % (dir_src_gdas, yyyy)
        fg = h5(fname_inp_gdas, 'r')
        yyyy2 = yyyy

    dname = '%04d-%02d' % (yyyy, mm)
    path = '%s/%s' % (lname, dname)
    g_t = fg['%s/t'%path].value
    g_h = fg['%s/h'%path].value
    tck = splrep(g_t, g_h, s=0)

    #print "    --> Abriendo: %s" % fname_inp
    ntanks  = f.variables['ntanks'].data
    cc  = ntanks>150.

    if len(find(cc))>1: #mas de un dato es promedio de >150 tanques
        rate    = f.variables['cnts_press.corrected'].data
        time    = (f.variables['time'].data  - 1136073600.)/(86400.*365)    # anios desde 01/jan/2006
        cts     = np.zeros(96, dtype=np.float64); typ=0.0
        gh      = splev(time, tck, der=0)  # der: order of derivative to compute (less than "k")
        for i in ch_Eds:
            cts      = rate[:,i]    # vector
            typ      = typic[i] # escalar
            cts_norm = cts/typ
            H2D[i]   += np.histogram2d(gh, cts_norm, bins=[Bins, Bins], range=RANGE, normed=False)[0]

        print ' ---> date: ', date

    else:
        print ' ---> date (no tanks): ', date

    ntot += 1
    f.close()
    date += timedelta(days=1)            # next day...

print " ---> FINISH!! "
for i in range(nEd):
    chi, chf = ch_Eds[i]*20, (ch_Eds[i]+1)*20
    fname_out = '%s/H2D_%03d-%03d.MeV.txt' % (dir_dst, chi, chf)
    np.savetxt(fname_out, H2D[i], fmt='%4.4g')

print " ---> output: %s" % dir_dst

print "\n ---> these input-files were missing:\n", missing, "\n"
 
#EOF
