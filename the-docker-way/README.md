<!--- BOF -->

<!------- H I S T O G R A M S ------->
### Charge Histograms

All command line instructions belowe assume that you first execute in this directory:
    source funcs.sh


Read primitive data && run first stage of processing:

    docker__run_auger \
        -DirRep ~/auger_repo \
        -DirSrc /media/scratch2/auger/histograms \
        -DirDst /media/scratch1/auger/build_Histos \
        -DirLog /media/scratch1/auger/logs/build_Histos \
        -cname auger__buildHistos_2016 \
        -CMD /auger_repo/build_Histos/massive.sh \
            -DateIni 01 01 2016 \
            -DateEnd 31 12 2016


<!------- M O N I T ------->
### Monit data

Read primitive monit data && run 2nd stage of processing:

    docker__run_auger \
        -DirRep ~/auger_repo \
        -DirSrc /media/scratch2/auger/monit/sd \
        -DirDst /media/scratch1/auger/build_mc \
        -DirLog /media/scratch1/auger/logs/build_mc \
        -cname auger__build_mc__2016 \
        -CMD /auger_repo/build_mc/gral.sh \
            -DateIni 01 01 2016 \
            -DateEnd 31 12 2016 \
            -DirSrc /DataInp \
            -DirDst /DataOut \
            -DirLog /runlogs



<!------- S C A L E R S ------->
---
### Scalers



<!--- EOF -->
