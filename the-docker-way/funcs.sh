#!/bin/bash

function docker__run_auger(){
    # runs container that:
    #   - compiles histos code

    #--- grab args
    [[ $# -eq 0 ]] && {
        # TODO: print help.
        echo -e "\n [-] we need arguments! XD\n";
        return 1;
    }
    while [[ $# -gt 0 ]]; do
        key="$1"
        case $key in
            -DirRep)
            DirRep="$2"
            shift; shift
            ;;
            -DirSrc)
            DirSrc="$2"     # read data from here (original .root)
            shift; shift
            ;;
            -DirDst)
            DirDst="$2"     # output dir
            shift; shift
            ;;
            -DirLog)
            DirLog="$2"     # to save run logs
            shift; shift
            ;;
            -cname)
            CNAME="$2"
            shift; shift
            ;;
            -CMD)
            CMD=${@:2}  # read all the remaining stuff
            echo -e "\n [*] run command:\n $CMD\n"
            for _i in $(seq 1 $#); do
                shift
            done
            ;;
            *)
            echo -e "\n [-][$0] ERROR: bad argument!\n"
            return 1
        esac
    done

    echo " DirRep   : $DirRep"
    echo " DirSrc   : $DirSrc"
    echo " DirDst   : $DirDst"
    echo " DirLog   : $DirLog"

    DockerImage=lamp/auger:hegea
    #DockerImage=lamp/auger:hegea_and_conda
    
    docker run --rm -it \
        -e DISPLAY=unix$DISPLAY \
        -v /tmp/.X11-unix:/tmp/.X11-unix \
        -v /dev/shm:/dev/shm \
        --cap-add=SYS_ADMIN \
        --security-opt seccomp=${AUGER_REPO}/the-docker-way/sec_config.json \
        --name=$CNAME \
        -v ${DirRep}:/auger_repo \
        -v ${DirSrc}:/DataInp \
        -v ${DirDst}:/DataOut \
        -v ${DirLog}:/runlogs \
        -u masiasmj:masiasmj \
        $DockerImage \
        bash -c "$CMD"
        # NOTE: $CMD can be e.g.:
        #/auger_repo/build_Histos/massive.sh \
        #    -DateIni 01 01 2006 \
        #    -DateEnd 31 12 2007
}


function testt(){
    #--- grab args
    [[ $# -eq 0 ]] && {
        # TODO: print help.
        echo -e "\n [-] we need arguments! XD\n";
        return 1;
    }
    while [[ $# -gt 0 ]]; do
        key="$1"
        case $key in
            -DirRep)
            DirRep="$2"
            shift; shift
            ;;
            -DirSrc)
            DirSrc="$2"     # read data from here (original .root)
            shift; shift
            ;;
            -DirDst)
            DirDst="$2"     # output dir
            shift; shift
            ;;
            -DirLog)
            DirLog="$2"     # to save run logs
            shift; shift
            ;;
            -CMD)
            CMD="${@:2}"
            echo -e "\n [*] run command:\n $CMD\n"
            for _i in $(seq 1 $#); do
                shift
            done
            ;;
            *)
            echo -e "\n [-] ERROR: bad argument!\n"
            return 1
        esac
    done

    echo -e "\n > ${CMD}\n"
}


#EOF
