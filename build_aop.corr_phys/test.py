#!/usr/bin/env ipython
"""
author: j.j. masias meza

runtime: ~1min
"""
from funcs import *
from numpy import (
    nan, ones, zeros, empty, loadtxt
)
import pandas as pd
import argparse
import shared_funcs.funcs as sf

#--- parse args
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter,
description="""
Just take scaler and pressure data and merge them into a single file.
TODO: check if we can merge this processing stage with the next.
""",
)
parser.add_argument(
'-src', '--dir_src',
type=str,
help='input directory. Should be the output dir of "build_array.avrs"',
)
parser.add_argument(
'-fi', '--fname_inp',
type=str,
help='input ASCII file. Should be the output dir of "build_weather" (CLF-weather data; LL data not recommended)',
)
parser.add_argument(
'-dst', '--dir_dst',
type=str,
help='output dir.',
)
parser.add_argument(
'-ini', '--ini_date', 
type=str, 
action=sf.arg_to_datetime,
default='01/01/2006',
help='initial date',
)
parser.add_argument(
'-end', '--end_date', 
type=str, 
action=sf.arg_to_datetime,
default='31/12/2015',
help='end date',
)
pa = parser.parse_args()


#--- i/o directories
#maskname = 'shape.ok_and_3pmt.ok' #'shape.ok_and_3pmt.ok'#'3pmt.ok' # 'all'
#dir_src = '%s/auger/histogramas/netcdf/all.array.avrs/aop.corrected/%s/15min' % (home, maskname)
#dir_dst = '%s/out/out.build_aop.corr_phys/%s/15min' % (this_dir, maskname)


#--- grab pressure data && resample to 15min resolution
print("\n [*] reading pressure:\n     %s\n" % pa.fname_inp)
t_press, press  = loadtxt(pa.fname_inp, unpack=True, usecols=(0,1))
npress          = len(t_press)
index_time      = pd.PeriodIndex(t_press, freq='s')
press_5min      = pd.Series(press, index=index_time)  # input data
press_15min     = press_5min.resample('15Min', how='mean') # rebinned data
print("\n [+] finished rebining pressure data from +/-5min to 15min resolution\n")


ntime     = 96
nbins_mev = 50
ntot      = 0

# NaN arrays to flag non-available data
garbage = {
'1d'    : nan*ones(ntime),
'2d'    : nan*ones((ntime,50)),
}

date = pa.ini_date  # [datetime]
while date <= pa.end_date:
    yyyy      = date.year
    mm        = date.month
    dd        = date.day
    fname_inp = '%s/%04d/%04d_%02d_%02d.nc' % (pa.dir_src, yyyy, yyyy, mm, dd)
    fo        = file_mgr(pa.dir_dst, ntime, yyyy, mm, dd)

    if os.path.isfile(fname_inp):
        f     = netcdf_file(fname_inp, 'r')
        print(ccl.B + " [*] reading: %s" % fname_inp + ccl.W)
        fo.cts_corr     = f.variables['aop.corrected_cnts'].data
        fo.cts_uncorr   = f.variables['aop.uncorrected_cnts'].data
        fo.cts_corr_std = f.variables['aop.corrected_cnts_std'].data
        fo.time         = f.variables['time'].data
        fo.ntanks       = f.variables['ntanks'].data
        fo.aop_avr      = f.variables['aop_avrg_over_array'].data
    else:
        print(ccl.R + " [-] doesn't exist: %s" % fname_inp + ccl.W)
        fo.cts_corr     = garbage['2d']
        fo.cts_uncorr   = garbage['2d']
        fo.cts_corr_std = garbage['2d']
        fo.time         = garbage['1d']
        fo.ntanks       = garbage['1d']
        fo.aop_avr      = garbage['1d']

    tini     = date
    tfin     = date + timedelta(days=1)
    cc       = (press_15min.index >= tini) & (press_15min.index < tfin) # intervalo semiabierto [,) 
    fo.press = press_15min[cc]

    fo.save()
    date    += timedelta(days=1)
    ntot    += 1

print("\n [+] output: %s\n" % pa.dir_dst + ccl.W)
f.close()
#EOF
