## Merge scaler and weather data
Here, we just grab the charge-histogram data and weather files to merge them into a single file.

Inputs:
* `dir_src`: AoP corrected scalers, which is an output directory of [code_aop.correction/test.py](../code_aop.correction/test.py).
* `fname_inp`: pressure data, built by [build_weather/test.py](../build_weather/test.py).
* `dir_dst`: output directory.

```bash
./test.py -- \
    -src /media/scratch1/auger/code_aop.correction \
    -fi /media/scratch1/auger/build_weather/weather_5min_2006-2015.dat \
    -dst /media/scratch1/auger/build_aop.corr_phys \    
    -ini 01/01/2006 \
    -end 31/12/2014
```


Full description:

    $ ./test.py -- -h
    usage: test.py [-h] [-src DIR_SRC] [-fi FNAME_INP] [-dst DIR_DST]
                   [-ini INI_DATE] [-end END_DATE]

    Just take scaler and pressure data and merge them into a single file. TODO:
    check if we can merge this processing stage with the next.

    optional arguments:
      -h, --help            show this help message and exit
      -src DIR_SRC, --dir_src DIR_SRC
                            input directory. Should be the output dir of
                            "build_array.avrs" (default: None)
      -fi FNAME_INP, --fname_inp FNAME_INP
                            input ASCII file. Should be the output dir of
                            "build_weather" (CLF-weather data; LL data not
                            recommended) (default: None)
      -dst DIR_DST, --dir_dst DIR_DST
                            output dir. (default: None)
      -ini INI_DATE, --ini_date INI_DATE
                            initial date (default: 01/01/2006)
      -end END_DATE, --end_date END_DATE
                            end date (default: 31/12/2015)


<!--- EOF -->
