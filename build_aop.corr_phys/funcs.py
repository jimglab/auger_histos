"""
 author: j.j. masias meza
"""
from pylab import *
from scipy.io.netcdf import netcdf_file
from datetime import datetime, timedelta
import numpy as np
import os
import console_colors as ccl


class file_mgr:
    def __init__(self, dir_dst, ntime, yyyy, mm, dd):
        self.dir_dst    = '%s/%04d' % (dir_dst, yyyy)
        self.ntime      = ntime
        self.fname_out  = '%s/%04d_%02d_%02d.nc' % (self.dir_dst, yyyy, mm, dd) 
        os.system('mkdir -p %s' % self.dir_dst)


    def save(self):
        self.fout = netcdf_file(self.fname_out, 'w')
        self.fout.createDimension('ntime', self.ntime)
        self.fout.createDimension('nbins_mev', 50) 

        # guardamos cuentas cormalizadas corregidas por AoP
        dims    = ('ntime', 'nbins_mev')
        self.write_variable('aop.corrected_cnts_phys',   dims, self.cts_corr  , 'd', '[cts/m2/s]')
        self.write_variable('aop.uncorrected_cnts_phys', dims, self.cts_uncorr, 'd', '[cts/m2/s]')
        self.write_variable('aop.corrected_cnts_phys_std', dims, self.cts_corr_std, 'd', '[cts/m2/s]')

        # guardamos tiempo
        dims    = ('ntime',)
        self.write_variable('time', dims, self.time, 'd', '[sec-utc]')

        # AoP promediado sobre el array
        self.write_variable('aop_avrg_over_array', dims, self.aop_avr, 'd', '[1] AoP promediado sobre array')

        # nro de tanque q aportan al promedio
        self.write_variable('ntanks', dims, self.ntanks, 'd', '[1] nro de tanques q aportan')

        # nro de tanque q aportan al promedio
        self.write_variable('pressure', dims, self.press, 'd', '[mmHg] presion del "weather_5min_updated.31mar2015.dat"')

        self.fout.close()


    def write_variable(self, varname, dims, var, datatype, comments):
        dummy           = self.fout.createVariable(varname, datatype, dims)
        dummy[:]        = var 
        dummy.units     = comments


#EOF
