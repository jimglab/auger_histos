# run

<!--- ################################ -->
---
* To build temperature-corrected histograms:
```bash
OUT=../out/out.build_temp.corr/shape.ok_and_3pmt.ok/15min/histos_temp.corrected.h5
./build_temp.corr.py -- -o $OUT 
# see the rest of options in the default argument values:
./build_temp.corr.py -- -h

# then, it's healthy to "label" the final data:
ln -sf $OUT ./final_histos.h5
```


<!--- ################################ -->
---
* Generate figure (and ASCII data associated) of long-term profiles (use either `scals` or `histos`):
```bash
FINP=../out/out.build_temp.corr/shape.ok_and_3pmt.ok/15min/final_histos.h5
FAVR=../out/out.build_temp_hist.and.fits/avr_histos_press_shape.ok_and_3pmt.ok.txt
./ch_Eds_smoo2.py -- --mode scals --fprefix stest --odir . --fname_inp $FINP --fname_avr $FAVR 

# See argument descriptions:
./ch_Eds_smoo2.py -- -h
```


<!--- ################################ -->
---
# Read
* Basically, we walk into the data like this:

```python
from h5py import File as h5
f = h5('file.h5','r')

# time in fractions of years-since-01Jan2006, with resolution of 15min.
t = f['2008/01/07/t'][...]

# vector of the number of tanks aporting with valid-filtered-data at each 
# time during the day 7/Jan/2008.
ntanks = f['2008/01/07/tanks'][...]

# vector of the count-rate for the charge-histograms at the energy-bin centered 
# at Ed=30MeV. This data is syncronized with `ntanks` and `t` above.
cts = f['2008/01/07/cts_temp-corr_030MeV'][...]
```

<!--- EOF -->
