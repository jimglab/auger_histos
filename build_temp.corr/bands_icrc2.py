#!/usr/bin/env ipython
from pylab import *
import numpy as np
import os

this_dir        = os.environ['AUGER_REPO'] # repo dir
fname_scals     = '%s/actividad_solar/neutron_monitors/figuras/NMs_res_15days.dat' % os.environ['HOME']
dir_src         = '%s/out/out.build_temp.corr/ascii' % this_dir
fname_ScalsFromHistos   = '%s/smoo2_shape.ok_and_3pmt.ok_60-120.MeV.txt' % dir_src
fname_muon              = '%s/smoo2_shape.ok_and_3pmt.ok_200-280.MeV.txt' % dir_src
dir_fig   = '%s/out/out.build_temp.corr/figs' % this_dir
fname_fig = '%s/bands_icrc2.png' % dir_fig

t_sc,   r_sc    = np.loadtxt(fname_ScalsFromHistos).T
t_mu,   r_mu    = np.loadtxt(fname_muon).T
t_scls, r_scls  = np.loadtxt(fname_scals, usecols=(0,5)).T
t_scls      = (t_scls - 1136073600)/(86400.*365.)
r_scls      /= 1.*np.nanmean(r_scls)

fig = figure(1, figsize=(6, 4))
ax  = fig.add_subplot(111)

LW=4
ax.plot(t_scls, r_scls, '-',c='black',  label='scalers',  lw=3., alpha=.8, mec='None')
ax.plot(t_sc, r_sc, '-', c='red',  label='$H_{sc}$: (60-120)MeV',  lw=LW, alpha=.7, mec='None')
ax.plot(t_mu, r_mu, '-', c='blue',   label='$H_{\mu}$: (200-280)MeV', lw=LW, alpha=.5, mec='None')

LABELSIZE=15
ax.legend(loc='best')
ax.set_xlabel('years since Jan/2006', fontsize=LABELSIZE)
ax.set_ylabel('normalized rate', fontsize=LABELSIZE)
ax.set_xlim(0., 8.)
ax.grid()

savefig(fname_fig, format='png', dpi=135, bbox_inches='tight')
print " --> generamos: " + fname_fig 

close()
