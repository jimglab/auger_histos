#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
from scipy.io.netcdf import netcdf_file
from datetime import datetime, timedelta
import numpy as np
import os, argparse
from os.path import isfile, isdir
from scipy.interpolate import (
    splrep,     # Spline interpolation
    splev)      # Spline evaluator
from h5py import File as h5
from matplotlib.colors import LogNorm
import shared_funcs.funcs as sf


#--- retrieve args
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter,
description="""
Build temp-corrected charge-histograms.
"""
)
parser.add_argument(
'-dh', '--dir_hst',
type=str,
default='../out/out.build_pressure.corr/shape.ok_and_3pmt.ok/15min',
help='input directory of histograms data',
)
parser.add_argument(
'-dn', '--dir_ncep',
type=str,
default='../out/out.build_weather/ncep',
help='input directory of NCEP data',
)
parser.add_argument(
'-fa', '--fname_avr', 
type=str,
help="""
Input ASCII file. It's the time-averaged charge-histograms.
It should be the output of ../out.build_temp_hist.and.fits/build_avr_histos_press.txt.py.
"""
)
parser.add_argument(
'-fs', '--fname_slopes', 
type=str,
help="""
Input ASCII file. It has the fit parameters of the linear correlations w/ geopotential height.
It should be the output of ../out.build_temp_hist.and.fits/fitplots.py.
"""
)
parser.add_argument(
'-o', '--fname_out',
type=str,
default='../out/out.build_temp.corr/shape.ok_and_3pmt.ok/15min/test.h5',
help='filename of .h5 output',
)
pa = parser.parse_args()


#----------------- agarro valores tipicos para construir rangos
prom  = np.loadtxt(pa.fname_avr)
typic = np.nanmean(prom[:,1:], axis=0) # take the average, over the array, for the whole time period 
#--------------------------------------------------------------

#++++ slopes
ed, m_temp, b_temp = np.loadtxt(pa.fname_slopes, unpack=True)

#+++++++++++
ini_date = datetime.strptime(
    sf.ReadParam_from_2dHist(pa.fname_slopes,'ini_date'), 
    '%d/%b/%Y %H:%M\n'
    )
end_date = datetime.strptime(
    sf.ReadParam_from_2dHist(pa.fname_slopes,'end_date'), 
    '%d/%b/%Y %H:%M\n'
    )

LEVEL       = 100  # [mb or hPa] isobaric surface
lname       = 'level_%04d' % (LEVEL)

#--- calculate the average gh as a function of the time-period
gh_mean = sf.mean_gh(ini_date, end_date, dir_src=pa.dir_ncep, level=LEVEL)
print "\n <gh> : %g\n" % gh_mean

#-------------------------------------------------------------
ntot    = 0
date    = ini_date
ch_Eds  = np.arange(50)*20. + 10.

fo = h5(pa.fname_out, 'w')
fo['ch_Eds']   = ch_Eds           # save energy grid to file
fo['date_ini'] = ini_date.strftime('%d-%m-%Y %H:%M')
fo['date_end'] = end_date.strftime('%d-%m-%Y %H:%M')

j=0; k=1; r=0.0; n=0
tt, rr = [], []
yyyy2 = date.year - 1
missing = []

nu,sum_u = {},{}
nc,sum_c = {},{}
for Ed in ch_Eds:
    nu[Ed], sum_u[Ed] = 0, 0.0
    nc[Ed], sum_c[Ed] = 0, 0.0


while date <= end_date:
    yyyy = date.year
    mm   = date.month
    dd   = date.day
    fname_inp = '%s/%04d/%04d_%02d_%02d.nc' % (pa.dir_hst, yyyy, yyyy, mm, dd)
    if not isfile(fname_inp):
        missing += [ fname_inp ]
        date    += timedelta(days=1)   # next day...
        continue

    f   = netcdf_file(fname_inp, 'r')
    if yyyy2!=yyyy:
        fname_inp_gdas = '%s/test_%04d.h5' % (pa.dir_ncep, yyyy)
        fg = h5(fname_inp_gdas, 'r')
        yyyy2 = yyyy

    dname = '%04d-%02d' % (yyyy, mm)
    path = '%s/%s' % (lname, dname)
    g_t = fg['%s/t'%path].value
    g_h = fg['%s/h'%path].value
    tck = splrep(g_t, g_h, s=0)

    ntanks  = f.variables['ntanks'].data
    cc  = ntanks>150.

    if cc.nonzero()[0].size > 1: #mas de un dato es promedio de >150 tanques
        rate    = f.variables['cnts_press.corrected'].data
        time    = (f.variables['time'].data  - sf.date2utc(ini_date))/(86400.*365)    # years since 'ini_date'
        t_utc   = f.variables['time'].data  # [sec] UTC

        gh = splev(time, tck, der=0)  # der: order of derivative to compute (less than "k")
        path = '%04d/%02d/%02d' % (yyyy, mm, dd)
        fo['%s/t'%path] = time
        fo['%s/t_utc'%path] = t_utc
        fo['%s/gh'%path] = gh
        fo['%s/tanks'%path] = ntanks # number of tanks
        fo['%s/press'%path] = f.variables['pressure'].data # [mmHg]
        for i, Ed in zip(range(ch_Eds.size), ch_Eds):
            cts = rate[:,i] #/typic[i]
            print ' ---> date: ', date
            fo['%s/cts_%04dMeV'%(path, Ed)] = cts # temp-uncorrected, press-corr
            cc_u      =  ~np.isnan(cts)
            nu[Ed]    += cts[cc_u].size
            sum_u[Ed] += cts[cc_u].sum()

            dh = 100.*(gh-gh_mean)/gh_mean # [%] deviation of gh

            cts_corr  =  cts-m_temp[i]*dh*typic[i] # temp-corr,press-corr
            fo['%s/cts_temp-corr_%04dMeV'%(path,Ed)] = cts_corr
            cc_c      =  ~np.isnan(cts_corr)
            nc[Ed]    += cts_corr[cc_c].size
            sum_c[Ed] += cts_corr[cc_c].sum()

    else:
        print ' ---> date (no tanks): ', date

    ntot    += 1
    f.close()
    date    += timedelta(days=1)            # next day...

#--- averages
for Ed in ch_Eds:
    fo['mean/uncorr_%04dMeV'%Ed] = 1.*sum_u[Ed]/nu[Ed]
    fo['mean/corr_%04dMeV'%Ed] = 1.*sum_c[Ed]/nc[Ed]


print " ---> FINISH!! "
print " ---> output: %s" % fo.filename
fo.close()

print "\n ---> these input-files were missing:\n", missing, "\n"

#EOF
