#!/bin/bash

#--- functions
function recompile_code(){
    local auger_repo=/auger_repo
    local dir_exe=${auger_repo}/histos/Linux-x86_64         # binary directory
    cd ${dir_exe}/../cmt
    # repeat what's in my .bashrc
    . $HOME/root_auger/root/bin/thisroot.sh
    export CDASVERSION=$(basename $CDAS/version_cdas-*.tgz .tgz | sed -e 's/version_cdas-//' | sed -e 's/p[0-9]//')
    export CMTVERSION=$(basename $CDAS/CMT/*)
    export CMTROOT=$CDAS/CMT/$CMTVERSION
    export CMTINSTALLAREA=$CDAS/
    . ${CMTROOT}/mgr/setup.sh
    . $CDAS/Work/$CDASVERSION/cmt/cdas.sh
    export CMTPATH=$CDAS/:$CDAS/IoFd:$CMTPATH:$HOME
    ln -sf /auger_repo/histos/Linux-x86_64/histos.v0r5.exe \
        $HOME/CDAS//Linux-x86_64/bin/.

    # clean && compile
    echo -e "\n [*] make clean\n"
    make clean || return 1
    echo -e "\n [*] cmt config\n"
    cmt config || return 1
    echo -e "\n [*] make\n"
    make \
        || return 1 \
        && echo -e "\n [+] code for charge-histograms re-compiled OK! :D\n"
    cd -
}

# identify yourself
me=$(basename $0)


#--- grab args
[[ $# -eq 0 ]] && {
	# TODO: print help.
	echo -e "\n [-][$me] we need arguments! XD\n";
	return 1;
}
while [[ $# -gt 0 ]]; do
	key="$1"
	case $key in
		-DateIni)
		dd_ini=$2
		mm_ini=$3
		yyyy_ini=$4
		shift # past argument
		shift; shift; shift # past values
		;;
		-DateEnd)
		dd_end=$2
		mm_end=$3
		yyyy_end=$4
		shift # past argument
		shift; shift; shift # past values
		;;
        *)
        echo -e "\n [-][$me] ERROR: argument no recognized!\n"
        exit 1
    esac
done

#--- I/O dirs
DirRep=/auger_repo # repo dir
#DirSrc=/scratch/datos/datos_auger/data # input dir
DirSrc=/DataInp
#DirDst=/media/hdd_extern_hegea/data_auger/_tmp_/intermediate/histos # output dir
DirDst=/DataOut
#DirLog=$DirRep/logs/log.build_Histos
DirLog=/runlogs
for _d in $DirRep $DirSrc $DirDst $DirLog; do
    [[ -d ${_d} ]] \
        || {
        echo -e "\n [-] path (${_d}) doesnt exist!\n"
        exit 1; 
        }
done

#--- re-compile the C++ code
recompile_code \
    || {
    echo -e "\n [-][$me] error re-compiling\n";
    exit 1;
    }

# process all in this time period
${DirRep}/build_Histos/gral.sh \
    -DateIni ${dd_ini} ${mm_ini} ${yyyy_ini} \
    -DateEnd ${dd_end} ${mm_end} ${yyyy_end} \
    -DirRep $DirRep \
    -DirSrc $DirSrc \
    -DirDst $DirDst \
    -DirLog $DirLog \
    && echo -e "\n [+][$me] finished processing histos for period ${dd_ini}/${mm_ini}/${yyyy_ini} - ${dd_end}/${mm_end}/${yyyy_end}\n"

#EOF
