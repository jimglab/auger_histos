#!/usr/bin/env bash
#
#   general:
#   - process .root files from 'dir_src' to obtain intermediate 
#     files yyyy_mm_dd.nc in 'dir_dst' directory
#   - takes all .root files from each days and builds a .nc with
#     the *charge* histogram data of those days.
#   - to process a different year, change 'yyyy'
#
#   input:
#   - dir_exe:  directory from which read .root files. This dir
#               has the three structure suggested 
#               by 'src_dir' and 'FILES' (see code below)
#   - dir_dst:  directory to put the .nc files
#   - dir_log:  directory to save logs
#   - dir_exe:  directory that contains binary exe
#   - yyyy:     year to process
#
#   output:
#   files in 'dir_log' and 'dir_dst'
#
# author: j.j. masias meza
#
shopt -s nullglob
#----------------------------------------------
yyyy=2014 #2006
this_dir=$HOME/ccin2p3/in2p3_data/data_auger/procesamiento/histos.aop_all
dir_exe=${this_dir}/histos/Linux-x86_64             # binary directory
dir_log=${this_dir}/logs/log.build_Histos        # to save run logs
dir_src=$HOME/auger_ascii/histogramas/root          # read data from here (original .root)
dir_dst=${this_dir}/out/out.build_Histos         # output dir
#months=$(seq 1 1 12)                                # months to process
months=$(seq 2 1 2)                                # months to process
days=$(seq 1 1 21)                                  # days to process
#----------------------------------------------
exe=${dir_exe}/histos.v0r5.exe
if [ ! -f "$exe" ]; then 
    echo
    echo " ### ERROR ### no existe: " $exe
    echo
    exit 1
else
    echo " ---> using: " $exe
fi

echo
echo " ---> reading from: " ${dir_src} 
echo " ---> logs dir: " ${dir_log}
echo " ---> output dir: " ${dir_dst}
echo

dst_dir="${dir_dst}/$yyyy"
mkdir -p ${dst_dir}
for mm in $months; do
	mm=`printf "%02d\n" $mm`
	src_dir="${dir_src}/$yyyy/$mm"
	fname_log=${dir_log}/mon_${yyyy}_${mm}.log
	rm -f $fname_log
	for dd in $days; do
		dd=`printf "%02d\n" $dd`
		FILES="${src_dir}/sd_${yyyy}_${mm}_${dd}_*.root"
		ok=0
		#---- reviso si existen archivos con este nombre
		for f in $FILES; do
			if [ -f $f ]; then ok=1; fi
		done
		#--------------------
		if [ $ok == 1 ]; then
			#printf " procesando: %s\n" $FILES
			echo " procesando:"
			ls -lhtr $FILES
			fname_out=${dst_dir}/${yyyy}_${mm}_${dd}.nc
			$exe ${fname_out} $FILES >> ${fname_log} 2>&1
		else
			echo " procesando:"
			echo " missing data for: $yyyy/$mm/$dd"
		fi
		echo
		#--------------------
	done
done

echo
echo " ---> reading from: " ${dir_src} 
echo " ---> logs dir: " ${dir_log}
echo " ---> output dir: " ${dir_dst}
echo
#EOF
