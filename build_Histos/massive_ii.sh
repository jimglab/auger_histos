#!/bin/bash
YEARS=(2010 2011 2012 2013 2014)
#YEARS=(2009)
DirRep=$HOME/auger.histos_aop # repo dir
DirSrc=/scratch/datos/datos_auger/data # input dir
DirDst=/media/hdd_extern_hegea/data_auger/_tmp_/intermediate/histos # output dir

for year in ${YEARS[@]}; do
    ./gral.sh $DirRep $year $DirSrc $DirDst
    #src_dir=${DirSrc}/$year
    #echo $src_dir
done
