#!/usr/bin/env bash
#
#   general:
#   - process .root files from 'DirSrc' to obtain intermediate 
#     files yyyy_mm_dd.nc in 'DirDst' directory
#   - takes all .root files from each days and builds a .nc with
#     the *charge* histogram data of those days.
#   - to process a different year, change 'yyyy'
#
#   input:
#   - DirSrc :  directory from which read .root files. This dir
#               has the three structure suggested 
#               by 'SubDirSrc' and 'FILES' (see code below)
#   - DirDst :  directory to put the .nc files
#   - DirLog :  directory to save logs
#   - yyyy   :  year to process
#
#   output:
#   files in 'DirLog' and 'DirDst'
#
# author: j.j. masias meza
#
shopt -s nullglob

# just identify yourself
me=$(basename $0)

#--- grab args
[[ $# -eq 0 ]] && {
	# TODO: print help.
	echo -e "\n [-][$me] we need arguments! XD\n";
	exit 1;
}
while [[ $# -gt 0 ]]; do
	key="$1"
	case $key in
		-DateIni)
		dd_ini=$2
		mm_ini=$3
		yyyy_ini=$4
		shift # past argument
		shift; shift; shift # past values
		;;
		-DateEnd)
		dd_end=$2
		mm_end=$3
		yyyy_end=$4
		shift # past argument
		shift; shift; shift # past values
		;;
		-DirRep)
		DirRep="$2"
        shift; shift
        ;;
        -DirSrc)
        DirSrc="$2"     # read data from here (original .root)
        shift; shift
        ;;
        -DirDst)
        DirDst="$2"     # output dir
        shift; shift
        ;;
        -DirLog)
        DirLog="$2"     # to save run logs
        shift; shift
        ;;
		*)
		echo -e "\n [-] ERROR: bad argument in $me\n"
		exit 1
	esac
done

#----------------------------------------------
#DirSrc=$HOME/auger_ascii/histogramas/root          # read data from here (original .root)
#DirDst=${DirRep}/out/out.build_Histos         # output dir
#----------------------------------------------
exe_original=${DirRep}/histos/Linux-x86_64/histos.v0r5.exe
exe=$HOME/$(basename ${exe_original}) # use the copy (*)
if [ ! -f "${exe_original}" ]; then 
    echo -e "\n [-][$me] ERROR: doesn't exist: " ${exe_original} "\n"
    exit 1
else
    cp -pv ${exe_original} $exe
    echo " ---> using: " $exe
fi
# (*): this is so we can rebuild the C++ source when starting
# other Docker containers that mount the same directory $DirRep
# as volumes.



# number of UTC days associated to each date
usec_ini=$(date --date "${yyyy_ini}-${mm_ini}-${dd_ini} 00:00" -u +%s)
usec_end=$(date --date "${yyyy_end}-${mm_end}-${dd_end} 00:00" -u +%s)

udays_ini=$((${usec_ini}/86400))
udays_end=$((${usec_end}/86400))
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

echo
echo " ---> reading from: " ${DirSrc} 
echo " ---> logs dir: " ${DirLog}
echo " ---> output dir: " ${DirDst}
echo

udays=${udays_ini} # UTC days start at $udays_ini
while [[ ${udays} -le ${udays_end} ]]; do
    usec=$(($udays*86400)) # UTC days between ($udays_ini, $udays_end)
    dd=$(date --date "01 jan 1970 + $usec sec" -u +%d)
    mm=$(date --date "01 jan 1970 + $usec sec" -u +%m)
    yyyy=$(date --date "01 jan 1970 + $usec sec" -u +%Y)
    echo -e "\n [*][$me] date: $dd/$mm/$yyyy"

	SubDirSrc="${DirSrc}/$yyyy/$mm"
    SubDirDst="${DirDst}/$yyyy"
    mkdir -p ${SubDirDst}
	fname_log=${DirLog}/mon_${yyyy}_${mm}_${dd}.log
	rm -f $fname_log

    FILES="${SubDirSrc}/sd_${yyyy}_${mm}_${dd}_*.root"
    ok=0
    #--- check if we have files with these filenames (for today)
    for f in $FILES; do
        if [ -f $f ]; then ok=1; fi
    done
    #--------------------
    echo -e "\n [*][$me] processing:"
    if [ $ok == 1 ]; then
        #printf " procesando: %s\n" $FILES
        ls -lhtr $FILES
        fname_out=${SubDirDst}/${yyyy}_${mm}_${dd}.nc
        $exe ${fname_out} $FILES >> ${fname_log} 2>&1 \
            || {
            echo -e "\n [-][$me] ERROR: $exe exited with status $?\n";
            exit 1;
            }
    else
        echo -e "\n    [-][$me] missing data for: $dd/$mm/$yyyy\n"
    fi
    #--------------------

    udays=$(($udays+1))
done



echo "\n ---> reading from: ${DirSrc}"
echo " ---> logs dir    : ${DirLog}"
echo " ---> output dir  : ${DirDst}\n"
#EOF
