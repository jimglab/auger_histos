#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
from datetime import datetime, timedelta
import shared_funcs.funcs as sf
import pandas as pd
from scipy.io.netcdf import netcdf_file
import os, h5py, argparse

#++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++++++++++++++++++++++++++++++++++++++++++
# Classes to build times series w/ custom time
# resolution.
#++++++++++++++++++++++++++++++++++++++++++++++++++++
class cls__build_pressure_corr(object):
    """
    Manipulate data from the output of the
    stage 'build_pressure.corr'.
    """
    def __init__(self):
        """
        First thing to do is build the parser
        """
        self.help = """
        Module to read/process the output of the 'build_pressure.corr' stage.
        """
        self.parser = parser = argparse.ArgumentParser(
        description="""this gral description...""", 
        add_help=False
        )
        parser.add_argument(
        '-src', '--dir_src',
        type=str,
        help="""
        Input directory, where we have the AoP-and-pressure-corrected rates. 
        Should be the output of 'build_pressure.corr'.
        """,
        )
        parser.add_argument(
        '-dst', '--dir_dst',
        type=str,
        help='output directory',
        )
        parser.add_argument(
        '-fp', '--fprefix',
        type=str,
        default='smoo_',
        help="""
        Prefix for the output filenames.
        """,
        )
        parser.add_argument(
        '-mintank', '--ntanks_min',
        type=int,
        default=150,
        help="""
        For each data-point in the input files, there is an associated 
        number-of-tanks field. Such value is the number of SD tanks that 
        were filtered-in in the time window of each timestamp, in the energy interval 
        of that data-point.
        So such value gives us the 'statistics' at each timestamp.
        With this argument, we can choose a lower threshold to filter the
        data-points by its 'statistics' weight.
        """,
        )
        parser.add_argument(
        '-np', '--npoints',
        type=int,
        default=180,
        help="""
        Total number of data-points in the output.
        This determines the time resolution of the time series.
        If 0 (zero), the output data is in the original resolution.
        """,
        )
        parser.add_argument(
        '-chE', '--ch_Eds',
        type=int,
        nargs=2,
        default=None, # 10-13 is Muons; 3-5 is Scalers
        metavar=('chE_INI', 'chE_END'),
        help="""
        Indexes that define the limits for the energy closed-interval of interest.
        For ex., '10 13' means Muon band (i.e. 200-280 MeV), and '3 5' means Scaler band.
        These are indexes multiple of 20MeV (i.e. 12 means 12*20MeV=240MeV).
        Note that channel 0 belongs to the interval (0-20)MeV, and so on.
        """,
        )
        parser.add_argument(
        '-obs', '--observable',
        type=str,
        default='cnts_press.corrected',
        help="""
        name (key) of the variable we want to extract.
        If it doesn't exist, it will prompt the available variable names in the data.
        """,
        )
        parser.add_argument(
        '-norm', '--norm',
        action='store_true',
        default=False,
        help="""
        Wether or not to normalize the observable, by a factor given by the 
        time-average over the period of extraction.
        """,
        )
        parser.add_argument(
        '-utime', '--utime',
        type=np.float64,
        default=1.0, # [sec] the original units in the input data
        help='units of time (for the ASCII and the .png) in seconds. For ex., 86400. gives the output in days.',
        )

    def build_ts(self, pa):
        """
        Build a time series of the CR flux in a given [deposited] energy 
        range, for a given time window.
        The `observabke` argument refers to differente corrected versions
        of the CR rate.
        `ntank_min` : the minimum number of tanks that should
        be associated to the statistics of the output data.
        NOTE: default values are in the parser definition in __init__().
        """
        # time axis is measured relative to this date
        utc_origin  = sf.date2utc(pa.ini_date)

        if pa.npoints>0:
            dt_total = (pa.end_date - pa.ini_date).total_seconds() # [sec]
            dt       = 1.0*dt_total/pa.npoints # [sec]

        ntot    = 0
        k=1; r=0.0; n=0
        tt=[]; rr=[]

        date = pa.ini_date
        check_vname = 1
        while date < pa.end_date:
            fname_inp = '%s/%04d/%04d_%02d_%02d.nc' % (
                pa.dir_src, date.year, date.year, date.month, date.day
            )
            f   = netcdf_file(fname_inp, 'r')

            if check_vname:
                check_vname = 0 # do this check only **once**
                if pa.observable not in f.variables.keys():
                    print "\n [-] ERROR: '%s' doesn't exist in '%s'."%(
                        pa.observable, f.filename
                    )
                    print " [*] these variables do exist: "
                    for nm in f.variables.keys(): print "     - "+nm
                    print "\n [*] Aborting...\n"
                    raise SystemExit()

            ntanks  = f.variables['ntanks'].data
            cc  = ntanks > pa.ntanks_min

            if cc.nonzero()[0].size > 1: # more than one data-point is average of >ntanks_min tanks
                rate = f.variables[pa.observable].data
                time = f.variables['time'].data  - utc_origin  # [sec] desde ini_date
                cts  = np.zeros(rate.shape[0], dtype=np.float64)
                if pa.ch_Eds is not None: # if the observable is an energy channel
                    for i in range(pa.ch_Eds[0], pa.ch_Eds[1]+1):
                        cts += rate[:,i]    # vector
                        #typ += typic[i] # escalar
                else: # or the observable doesn't belong to any energy channel
                    cts += rate

                #cts_norm = cts/typ

                if pa.npoints>0:
                    aux  = np.nanmean(cts[cc])
                    if not(np.isnan(aux) or np.isinf(aux)):
                        r  += aux
                        n  += 1

                    if (time[0] > k*dt) and (n>0):
                        tt += [ (k-.5)*dt ]
                        rr += [ r/n ]
                        print " --> date: %2.2f" % time[0] + ", ", date, ", rr: ", r/n
                        k += 1
                        r = 0.0
                        n = 0

                elif pa.npoints==0: # this is much slower
                    for _t, _r in zip(time[cc], cts[cc]):
                        tt += [ _t ]
                        rr += [ _r ]

            ntot    += 1
            f.close()
            date    += timedelta(days=1)  # next day...

        print " ---> number of files read: %d" % ntot

        return tt, rr


class cls__build_temp_corr(object):
    def __init__(self,):
        """ build the parser first """
        self.help = """
        module to read/process out of the 'build_temp_corr' stage.
        """
        self.parser = parser = argparse.ArgumentParser(
        description="""this gral description...""", 
        add_help=False
        )
        parser.add_argument(
        '-fp', '--fprefix',
        type=str,
        default='smoo2',
        help='prefix for output filenames (for figure and ASCII files)',
        )
        parser.add_argument(
        '-odir', '--dir_dst',
        type=str,
        default='.',
        help='directory for output files',
        )
        parser.add_argument(
        '-finp', '--fname_inp',
        type=str,
        default='/path/to/final_histos.h5',
        help='path of .h5 file (final charge histogram data)',
        )
        parser.add_argument(
        '-favr', '--fname_avr',
        type=str,
        help="""
        Path of the ASCII .txt file. 
        It contains time-averaged (along several months) rates for each energy channel in the histograms data.
        Should be the output of 'build_temp_hist.and.fits'.
        """,
        )
        parser.add_argument(
        '-np', '--npoints',
        type=int,
        default=180,
        help="""
        Total number of data-points in the output.
        This determines the time resolution of the time series.
        If 0 (zero), the output data is in the original resolution.
        """,
        )
        parser.add_argument(
        '-obs', '--observable',
        type=str,
        default='cts_temp-corr',
        help="""
        name of the variable we want to extract.
        If it doesn't exist, it will prompt the available variable names in the data.
        """,
        )
        parser.add_argument(
        '-norm', '--norm',
        action='store_true',
        default=False,
        help="""
        Wether or not to normalize the observable, by a factor given by the 
        time-average over the period of extraction.
        """,
        )
        parser.add_argument(
        '-utime', '--utime',
        type=np.float64,
        default=1.0, # [sec] the original units in the input data
        help='units of time (for the ASCII and the .png) in seconds. For ex., 86400. gives the output in days.',
        )
        parser.add_argument(
        '-mintank', '--ntanks_min',
        type=int,
        default=150,
        help="""
        For each data-point in the input files, there is an associated 
        number-of-tanks field. Such value is the number of SD tanks that 
        were filtered-in in the time window of each timestamp, in the energy interval 
        of that data-point.
        So such value gives us the 'statistics' at each timestamp.
        With this argument, we can choose a lower threshold to filter the
        data-points by its 'statistics' weight.
        """,
        )
        parser.add_argument(
        '-chE', '--ch_Eds',
        type=int,
        nargs=2,
        default=None, # 10-13 is Muons; 3-5 is Scalers
        metavar='ch_Eds',
        help="""
        Indexes that define the limits for the energy [closed] interval of interest.
        For ex., '10 13' means Muon band (i.e. 200-280 MeV), and '3 5' mean Scaler band.
        These are indexes multiple of 20MeV (12 means 12*20MeV=240MeV).
        Note that channel 0 is the interval (0-20)MeV, and so on.
        """,
        )

    def build_ts(self, pa):
        #--- input data: temperature-corrected data
        fi = h5py.File(pa.fname_inp, 'r')

        # time axis is measured relative to this date
        utc_origin  = sf.date2utc(pa.ini_date)

        if pa.npoints>0:
            dt_total = (pa.end_date - pa.ini_date).total_seconds() # [sec]
            dt       = 1.0*dt_total/pa.npoints # [sec]

        ntot    = 0
        k=1; r=0.0; n=0
        tt=[]; rr=[]

        date = pa.ini_date
        check_vname = 1
        while date < pa.end_date:
            yyyy    = date.year
            mm      = date.month
            dd      = date.day
            path    = '%04d/%02d/%02d' % (yyyy, mm, dd)

            # sometimes data is not present in some dates due 
            # to filtering in previous data processing
            if path not in fi:
                date += timedelta(days=1)    # next day...
                continue

            if check_vname:
                check_vname = 0 # do this check only **once**
                if pa.observable+'_0010MeV' not in fi[path].keys() and \
                    pa.observable not in fi[path].keys():
                    print "\n [-] ERROR: '%s' doesn't exist in '%s'."%(
                        pa.observable, fi.filename
                    )
                    print " [*] these variables do exist (disregarding the _*MeV suffixes): "
                    for nm in fi[path].keys(): print "     - "+nm
                    print "\n [*] Aborting...\n"
                    raise SystemExit()

            ntanks  = fi['%s/tanks'%path][...]
            cc  = ntanks > pa.ntanks_min

            if cc.nonzero()[0].size > 1: # more than one data-point is average of >ntanks_min tanks
                time = fi['%s/t_utc'%path][...] - utc_origin # [sec] seconds since 'ini_date'
                cts  = np.zeros(time.size, dtype=np.float32)
                if pa.ch_Eds is not None: # if the observable is an energy channel (e.g. pres, gh)
                    for i in range(pa.ch_Eds[0], pa.ch_Eds[1]+1):
                        Ed  = i*20.+10.
                        cts += fi[path+'/%s_%04dMeV'%(pa.observable,Ed)][...]
                        #cts += rate[:,i]    # vector

                else: # or the observable doesn't belong to any energy channel
                    cts += fi[path+'/'+pa.observable][...]

                #cts_norm = cts/typ

                if pa.npoints>0:
                    aux  = np.nanmean(cts[cc])
                    if not(np.isnan(aux) or np.isinf(aux)):
                        r  += aux
                        n  += 1

                    if (time[0] > k*dt) and (n>0):
                        tt += [ (k-.5)*dt ]
                        rr += [ r/n ]
                        print " --> date: %2.2f" % time[0] + ", ", date, ", rr: ", r/n
                        k += 1
                        r = 0.0
                        n = 0

                elif pa.npoints<=0: # this is much slower
                    for _t, _r in zip(time[cc], cts[cc]):
                        tt += [ _t ]
                        rr += [ _r ]

            ntot    += 1
            date    += timedelta(days=1)  # next day...

        print " ---> number of files read: %d" % ntot
        fi.close()

        return tt, rr

#++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++++++++++++++++++++++++++++++++++++++++++

class scalers_final(object):
    def __init__(self, path):
        self.path = path

    def read(self, date_ini, date_end):
        """
        Organize data and return **as is** (w/o changing the
        time resolution).
        """
        data = {
        't_utc'         : sf.My2DArray(8, dtype=np.int64),
        'ntanks'        : sf.My2DArray(8, dtype=np.float32),
        'wAoP'          : sf.My2DArray(8, dtype=np.float32),
        'woAoP'         : sf.My2DArray(8, dtype=np.float32),
        'wAoP_wPrs'     : sf.My2DArray(8, dtype=np.float32),
        'wAoP_wPrs_wGh' : sf.My2DArray(8, dtype=np.float32),
        }
        print "\n [*] reading: \n   > " + '\n   > '.join(data.keys())

        n = 0   # total number of fields/timestamps, concatenating 
                # all the files below

        fname_inp = self.path
        f = h5py.File(fname_inp, 'r')
        date = date_ini
        while date <= date_end:
            for vnm in data.keys():
                dpath = '{vnm}/{yyyy}/{mm:02d}'.format(
                    vnm=vnm, yyyy=date.year, mm=date.month
                )
                _ = f[dpath][...]
                data[vnm][n:n+_.size] = _

            n       += _.size
            date    += timedelta(days=1)
        
        for vnm in data.keys():
            data[vnm] = data[vnm][:n]

        return data

    def extract(self, date_ini, date_end, vnames, res=0):
        """
        res     : output time-resolution [sec]
        vnames  : names/keys of the output variables [list]
        """
        # data, w/ original time-resolution
        data = self.read(date_ini, date_end)
        print " [*] extracting..."

        # if any in `vnames` is not valid, report it, and list
        # all the valid keys.
        assert all([(nm in data.keys()) for nm in vnames]), \
            '\n [-] at least one of these not found: %r\n' % vnames +\
            ' Available: \n * %s\n' % ('\n * '.join(data.keys()))

        # resample if res>0
        if res > 0:
            out = {}
            # resample
            tindex = pd.PeriodIndex(data['t_utc'], freq='s')
            # iterate over the variable keys
            for nm in data.keys():
                _aux = pd.Series(data[nm], index=tindex)
                out[nm] = _aux.resample('%dS'%res, how='mean').values

        else:
            out = data

        # return the data in the same order as listed in `vnames`
        return [ out[vnm] for vnm in ['t_utc',]+vnames ]


class hsts_wPressCorr(object):
    def __init__(self, path):
        self.path = path

    def read(self, date_ini, date_end, nEd=50):
        """
        nEd     : number of deposited-energy bins
        """
        data = {
        'utcsec'            : sf.My2DArray(8, dtype=np.int64),
        'Hist'              : sf.My2DArray((8,nEd), dtype=np.float32),
        'Hist_wAoP'         : sf.My2DArray((8,nEd), dtype=np.float32),
        'Hist_wAoP_wPress'  : sf.My2DArray((8,nEd), dtype=np.float32),
        }
        print "\n [*] reading: \n   > " + '\n   > '.join(data.keys())

        n = 0 # total number of fields/timestamps, concatenating all the files below
        date = date_ini
        while date <= date_end:
            fnm = self.path + '/{year}/{year}_{month:02d}_{day:02d}.nc'.format(
                year=date.year, month=date.month, day=date.day
            )
            if not os.path.isfile(fnm):
                date += timedelta(days=1)
                print " [*] Skipping %s"%date.strftime('%d/%b/%Y %H:%M')
                continue
            fi = netcdf_file(fnm, 'r')
            nf = fi.variables['time'].data.size # number of timestamps for this file
            data['utcsec'][n:n+nf] = fi.variables['time'].data.copy()
            # Histograms, w/o AoP-correction, w/o pressure-correction
            data['Hist'][n:n+nf,:] = fi.variables['cnts_aop.uncorrected'].data.copy()
            # Histograms w/ AoP-correction, w/o pressure-correction
            data['Hist_wAoP'][n:n+nf,:] = fi.variables['cnts_press.uncorrected'].data.copy()
            # Histograms w/ AoP-correction, w/ pressure-correction
            data['Hist_wAoP_wPress'][n:n+nf,:] = fi.variables['cnts_press.corrected'].data.copy()
            #_ntanks 	= fi.variables['ntanks'].data.copy()
            #_aop_avr	= fi.variables['aop_avrg_over_array'].data.copy() # <AoP>_{all-array}
            n += nf
            date += timedelta(days=1)
        
        assert n>0, '\n [-] no data has been collected!\n'

        for nm in data.keys():
            data[nm] = data[nm][:n]

        return data

    def extract(self, date_ini, date_end, vnames, res=0, nEd=50):
        """
        res     : output time-resolution [sec]
        nEd     : number of deposited-energy channels
        vnames  : names/keys of the output variables [list]
        """
        # data, w/ original time-resolution
        data = self.read(date_ini, date_end, nEd)
        print " [*] extracting..."

        # if any in `vnames` is not valid, report it, and list
        # all the valid keys.
        assert all([(nm in data.keys()) for nm in vnames]), \
            '\n [-] at least one of these not found: %r\n' +\
            ' Available: \n * %s\n'%(vnames, '\n * '.join(data.keys()))

        # resample if res>0
        if res > 0:
            out = {}
            # resample
            tindex = pd.PeriodIndex(data['utcsec'], freq='s')
            # iterate over the variable keys
            for nm in data.keys():
                if data[nm].ndim == 1:
                    _aux = pd.Series(data[nm], index=tindex)
                    aux = _aux.resample('%dS'%res, how='mean').values

                elif data[nm].ndim == 2:
                    # NOTE: pd.Series() doesn't handle stuff that is not
                    # 1D-dimensional! So we resamepl one column at a time.
                    aux = sf.My2DArray((8,data[nm].shape[1]), dtype=np.float32)

                    # iterate over energy dimension
                    # NOTE: i don't know how to resample the whole thing 
                    # just along one of the dimensions.
                    for i in range(data[nm].shape[1]):
                        _aux = pd.Series(data[nm][:,i], index=tindex)
                        _ = _aux.resample('%dS'%res, how='mean')
                        aux[0:_.values.size,i] = _.values

                else:
                    raise SystemExit(\
                    ' [-] dont know what to do when ndim=%d\n'%data[nm].ndim
                    )

                # NOTE: just return ().values; otherwise, it'll return values 
                # AND timestamps (datetime objects).
                # NOTE: the `how=` argument is deprecated in future Pandas versions.
                # NOTE: return the `aux[...]` since it's a My2DArray object. This
                # means that [...] returns the "used" array, and not the whole
                # allocated array.
                out[nm] = aux[...]
            del aux

        else:
            out = data

        # return the data in the same order as listed in `vnames`
        return [ out[vnm] for vnm in ['utcsec',]+vnames ]

class hsts_wGhCorr(object):
    def __init__(self, path):
        self.path = path

    def read(self, date_ini, date_end, nEd=50, chE=None):
        """
        nEd     : number of deposited-energy bins
        chE     : list of energy channels to read. If None, all
                  channels will be read.
        """
        data = {
        'utcsec'    : sf.My2DArray(8, dtype=np.int64),
        'gh'        : sf.My2DArray(8, dtype=np.float32),
        'press'     : sf.My2DArray(8, dtype=np.float32),
        'cts'       : sf.My2DArray((8,nEd), dtype=np.float32),
        'cts_wGh'   : sf.My2DArray((8,nEd), dtype=np.float32),
        }
        print "\n [*] reading: \n   > " + '\n   > '.join(data.keys())

        dE = 1000./nEd
        n = 0 # total number of fields/timestamps, concatenating all the files below
        fi = h5py.File(self.path, 'r')
        date = date_ini
        while date <= date_end:
            gname = '%04d/%02d/%02d' % (date.year,date.month,date.day)
            if gname not in fi: # some dates are not present (*) #TODO: elaborate.
                date += timedelta(days=1)
                print " [*] Skipping %s"%date.strftime('%d/%b/%Y %H:%M')
                continue

            nt = fi[gname+'/t_utc'].size
            data['utcsec'][n:n+nt]  = fi[gname+'/t_utc'][...]
            data['gh'][n:n+nt]      = fi[gname+'/gh'][...]
            data['press'][n:n+nt]   = fi[gname+'/press'][...]
            #--- read the energy channels
            # confirm the list of channels to read
            if chE is None: chE = range(nEd)
            # NOTE: we'll only populate the energy indexes 'iE' of 
            # 'data[key][iE,:]' that are specified in 'chE'. The rest
            # of the array is filled with NaNs (according to the 
            # implementation of the 'My2DArray' class.
            for iE in chE:
                Ed  = (iE + 0.5) * dE
                data['cts'][n:n+nt,iE] = fi[gname+'/cts_%04dMeV'%Ed][...]
                data['cts_wGh'][n:n+nt,iE] = fi[gname+'/cts_temp-corr_%04dMeV'%Ed][...]

            n += nt
            date += timedelta(days=1)
    
        # truncate the My2DArray instances to populated data only.
        for nm in data.keys():
            data[nm] = data[nm][:n]

        return data

    def extract(self, date_ini, date_end, vnames, res=0, nEd=50, chE=[10,11,12,13]):
        """
        nEd     : number of deposited-energy bins
        chE     : list of energy channels (indexes) to read. If None,
                  all channels will be read.
        """
        # data, w/ original time-resolution
        data = self.read(date_ini, date_end, nEd, chE=chE)
        print " [*] extracting..."

        # if any in `vnames` is not valid, report it, and list
        # all the valid keys.
        assert all([(nm in data.keys()) for nm in vnames]), \
            '\n [-] at least one of these not found: %r\n' +\
            ' Available: \n * %s\n'%(vnames, '\n * '.join(data.keys()))

        # resample if res>0
        if res > 0:
            out = {}
            # resample
            tindex = pd.PeriodIndex(data['utcsec'], freq='s')
            # iterate over the variable keys
            for nm in data.keys():
                if data[nm].ndim == 1:
                    _aux = pd.Series(data[nm], index=tindex)
                    aux = _aux.resample('%dS'%res, how='mean', fill_method='ffill').values

                elif data[nm].ndim == 2:
                    # NOTE: pd.Series() doesn't handle stuff that is not
                    # 1D-dimensional! So we resamepl one column at a time.
                    aux = sf.My2DArray((8,data[nm].shape[1]), dtype=np.float32)

                    # iterate over energy dimension
                    # NOTE: i don't know how to resample the whole thing 
                    # just along one of the dimensions.
                    for i in range(data[nm].shape[1]):
                        _aux = pd.Series(data[nm][:,i], index=tindex)
                        _ = _aux.resample('%dS'%res, how='mean')
                        aux[0:_.values.size,i] = _.values

                else:
                    raise SystemExit(\
                    ' [-] dont know what to do when ndim=%d\n'%data[nm].ndim
                    )

                # NOTE: just return ().values; otherwise, it'll return values 
                # AND timestamps (datetime objects).
                # NOTE: the `how=` argument is deprecated in future Pandas versions.
                # NOTE: return the `aux[...]` since it's a My2DArray object. This
                # means that [...] returns the "used" array, and not the whole
                # allocated array.
                out[nm] = aux[...]
            del aux

        else:
            out = data

        # return the data in the same order as listed in `vnames`
        return [ out[vnm] for vnm in ['utcsec',]+vnames ]


class NMs_mix1(object):
    def __init__(self, path,):
        self.fname_inp  = path

    def read(self, date_ini, date_end):
        """
        extract data from field `vname`, in the time
        interval `date_ini` - `date_end`
        NOTE: this data was downloaded from:
        http://www.nmdb.eu/nest/
        """
        # TODO: convert to resolution `self.res`
        conv = {
        0: lambda s: sf.date2utc(datetime.strptime(s, "%Y-%m-%d %H:%M:%S")),
        1: lambda s: float(s) if s!='null' else np.nan,
        2: lambda s: float(s) if s!='null' else np.nan,
        3: lambda s: float(s) if s!='null' else np.nan,
        4: lambda s: float(s) if s!='null' else np.nan,
        5: lambda s: float(s) if s!='null' else np.nan,
        6: lambda s: float(s) if s!='null' else np.nan,
        }
        VNAMEs = ('utcsec','ATHN','MXCO','ROME','NEWK','KIEL','THUL')
        print "\n [*] reading: \n   > " + '\n   > '.join(VNAMEs)

        data = np.genfromtxt(self.fname_inp, 
            converters=conv, 
            skip_header=24, 
            names = VNAMEs,
            delimiter=';',
            comments='--', 
            )
        #return [ data[vname] for vname in ['utcsec',]+vnames ]
        return data

    def extract(self, date_ini, date_end, vnames, res=0):
        """
        read && adjust to coarser resolution
        """
        # data, w/ original time-resolution
        data = self.read(date_ini, date_end)
        print " [*] extracting..."

        # if any in `vnames` is not valid, report it, and list
        # all the valid keys.
        assert all([(nm in data.dtype.names) for nm in vnames]), \
            '\n [-] at least one of these not found: %r\n' +\
            ' Available: \n * %s\n'%(vnames, '\n * '.join(data.keys()))

        # resample if res>0
        if res > 0:
            out = {}
            # resample
            tindex = pd.PeriodIndex(data['utcsec'], freq='s')
            # iterate over the variable keys
            for nm in data.dtype.names:
                _aux = pd.Series(data[nm], index=tindex)
                out[nm] = _aux.resample('%dS'%res, how='mean').values

        else:
            out = data

        # return the data in the same order as listed in `vnames`
        return [ out[vnm] for vnm in ['utcsec',]+vnames ]



def detect_skiprows(fname_inp):
    f     = open(fname_inp, 'r')
    stop  = False
    nskip = 0
    for l in f.readlines():
        nskip += 1
        if not stop and l.startswith('*****'):
            stop = True
            break
    return nskip

class NM_mcmurdo(object):
    def __init__(self, path):
        """
        res     : output resolution, in seconds.
        """
        self.dir_src    = path

    def extract(self, date_ini, date_end, vnames, res=0):
        """
        extract data directly from the original files
        downloaded from:
        http://neutronm.bartol.udel.edu/~pyle/bri_table.html
        """
        year_ini, year_end = date_ini.year, date_end.year
        fnames = [ self.dir_src+'/BRI%04d.txt'%year for year in 
            range(year_ini,year_end+1) ]
        _data = {}
        data = { # sf.My2DArray(
        'utcsec': sf.My2DArray(8, dtype=np.int64), # this **has** to be long
        'McMurdo-Corr': sf.My2DArray(8, dtype=np.float32),
        'McMurdo-Uncorr': sf.My2DArray(8, dtype=np.float32),
        'McMurdo-Press': sf.My2DArray(8, dtype=np.float32),
        'Swarthmore/Newark-Corr': sf.My2DArray(8, dtype=np.float32),
        'Swarthmore/Newark-Uncorr': sf.My2DArray(8, dtype=np.float32),
        'Swarthmore/Newark-Press': sf.My2DArray(8, dtype=np.float32),
        'South Pole-Corr': sf.My2DArray(8, dtype=np.float32),
        'South Pole-Uncorr': sf.My2DArray(8, dtype=np.float32),
        'South Pole-Press': sf.My2DArray(8, dtype=np.float32),
        'Thule-Corr': sf.My2DArray(8, dtype=np.float32),
        'Thule-Uncorr': sf.My2DArray(8, dtype=np.float32),
        'Thule-Press': sf.My2DArray(8, dtype=np.float32)
        }

        print "\n [*] reading: \n   > " + '\n   > '.join(data.keys())

        # if any in `vnames` is not valid, report it, and list
        # all the valid keys.
        assert all([(vnm in data.keys()) for vnm in vnames]), \
            '\n [-] at least one of these not found: %r\n' +\
            ' Available: \n * %s\n'%(vnames,'\n * '.join(data.keys()))
        n = 0 # total number of timestamps overall the files
        for year in range(year_ini, year_end+1):
            fname_inp = fnames[year-year_ini]
            #_data[year] = {}
            nskip = detect_skiprows(fname_inp)
            # data of ONE file
            fdata = np.loadtxt(fname_inp,
                skiprows=nskip,
                comments='*****'
                )
            nf = fdata.shape[0]
            yyyy,mm,dd,HH,MM = np.array(fdata[:,[0,1,2,3,4]],dtype=int).T
            data['utcsec'][n:n+nf] = np.vectorize(sf.date2utc)(
                np.vectorize(datetime)(yyyy,mm,dd,HH,MM)
                )
            data['McMurdo-Corr'][n:n+nf]             = fdata[:,5]
            data['McMurdo-Uncorr'][n:n+nf]           = fdata[:,6]
            data['McMurdo-Press'][n:n+nf]            = fdata[:,7]
            data['Swarthmore/Newark-Corr'][n:n+nf]   = fdata[:,8]
            data['Swarthmore/Newark-Uncorr'][n:n+nf] = fdata[:,9]
            data['Swarthmore/Newark-Press'][n:n+nf]  = fdata[:,10]
            data['South Pole-Corr'][n:n+nf]          = fdata[:,11]
            data['South Pole-Uncorr'][n:n+nf]        = fdata[:,12]
            data['South Pole-Press'][n:n+nf]         = fdata[:,13]
            data['Thule-Corr'][n:n+nf]               = fdata[:,14]
            data['Thule-Uncorr'][n:n+nf]             = fdata[:,15]
            data['Thule-Press'][n:n+nf]              = fdata[:,16]
            n += nf
            #data[year] = np.

        # truncate [to valid data]
        for nm in data.keys():
            data[nm] = data[nm][:n]

        if res > 0:
            out = {}
            # resample
            tindex = pd.PeriodIndex(data['utcsec'], freq='s')
            for nm in data.keys():
                aux     = pd.Series(data[nm], index=tindex)
                # just return ().values; otherwise, it'll return values AND 
                # timestamps (datetime objects)
                # NOTE: the `how=` argument is deprecated in future Pandas versions.
                out[nm] = aux.resample('%dS'%res, how='mean').values
                del aux
        else:
            out = data

        #import pdb; pdb.set_trace()
        return [ out[vnm] for vnm in ['utcsec',]+vnames ]



#EOF
