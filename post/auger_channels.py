#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
"""
Include comm-crisis dates:
     17apr-8/jun 2009 (dato de Piera en hangout del 25.feb.2016)
"""
import numpy as np
from datetime import datetime, timedelta
import os, argparse
import shared_funcs.funcs as sf
import funcs as ff
from pylab import figure, close, show
import sys

#--- parse args
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    description="""
    This generates rates averaged over an interval of energy 
    channels (defined in 'ch_Eds').
    """,
)
parser.add_argument(
'-iHmu', '--src_auger_Hmu',
type=str,
nargs=2,
default=None,
metavar=('KIND','PATH'),
help="""
Input file for Auger Muon-Histograms (Hmu).
The KIND sub-argument must be wPress or wGh, refering to pressure-corrected and gh-corrected data.
""",
)
parser.add_argument(
'-iHsc', '--src_auger_Hsc',
type=str,
nargs=2,
default=None,
metavar=('KIND','PATH'),
help="""
Input file for Auger Scaler-Histograms (Hsc). 
The KIND sub-argument must be wPress or wGh, refering to pressure-corrected and gh-corrected data.
""",
)
parser.add_argument(
'-iScls', '--src_auger_scals',
type=str,
default='',
help="""
Input file for Auger Scalers.
""",
)
parser.add_argument(
'-fig', '--fname_fig',
type=str,
help="""
output figure.
""",
)
parser.add_argument(
'-xlim', '--xlim',
type=float,
nargs=2,
metavar=('TMIN','TMAX'),
help='range for time domain in the plot, in units of the -unit argument.',
)
parser.add_argument(
'-unit', '--unit',
type=float,
default=86400.,
help='time units (in seconds) for the plot. For ex., for time in year units, use -unit $((86400*365)).',
)
parser.add_argument(
'-ylim', '--ylim',
type=float,
nargs=2,
default=[None,None], #[0.96, 1.15],
metavar=('YMIN','YMAX'),
help='range for vertical axis. If not used, there are no limits set.',
)
parser.add_argument(
'-tnorm', '--tnorm',
type=float,
nargs=2,
default=[7.6, 8.5],
metavar=('TMIN-NORM','TMAX-NORM'),
help="""time range where to normalize all time series (in 
units determined by -unit)""",
)
parser.add_argument(
'-figsize', '--figsize',
type=float,
nargs=2,
default=[7, 4],
help='figure size, in inches.',
)
parser.add_argument(
'-longres', '--longres',
action='store_true',
default=False,
help="""If used, long resolution change is used for all datasets.
""",
)
parser.add_argument(
'-ini', '--ini_date', 
action=sf.arg_to_datetime,
default='01/01/2006',
help='initial date',
)
parser.add_argument(
'-end', '--end_date', 
action=sf.arg_to_datetime,
default='31/12/2015',
help='end date',
)
pa = parser.parse_args()


#--- disable some warnings
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)


utc_jan06   = sf.date2utc(pa.ini_date) #1136073600.

#--- auger::Hmu
if pa.src_auger_Hmu is not None:
    assert os.path.isdir(pa.src_auger_Hmu[1]) or os.path.isfile(pa.src_auger_Hmu[1])

    # grab the pressure-corrected histos?
    if pa.src_auger_Hmu[0] == 'wPress':
        data_hsts = ff.hsts_wPressCorr(pa.src_auger_Hmu[1])
        t_hmu, _hmu = data_hsts.extract(
            date_ini = pa.ini_date, 
            date_end = pa.end_date, 
            vnames = ['Hist_wAoP_wPress'], 
            res = (86400*15) if pa.longres else 3600,
            )
        t_hmu = (t_hmu - sf.date2utc(pa.ini_date))/pa.unit
        hmu   = np.nanmean(_hmu[:,10:13+1], axis=1)
    # grab the temperature-corrected histos?
    elif pa.src_auger_Hmu[0] == 'wGh':
        aux = ff.cls__build_temp_corr()
        _pa = aux.parser.parse_args(("""
        --fname_inp %s --npoints %d --observable cts_temp-corr
        --ntanks_min 150 --ch_Eds 10 13
        """ % (pa.src_auger_Hmu[1], 120 if pa.longres else 0)).split())
        _pa.ini_date = pa.ini_date
        _pa.end_date = pa.end_date
        t_hmu, hmu = aux.build_ts(_pa)
        # TODO: fix 'res' argument so that it can accept/process 
        # values res<=2days correctly.
        t_hmu = np.array(t_hmu)/pa.unit
        hmu   = np.array(hmu/np.nanmean(hmu))
        del aux
    else:
        raise SystemExit()

#--- auger::Hsc
if pa.src_auger_Hsc is not None:
    assert os.path.isdir(pa.src_auger_Hsc[1]) or os.path.isfile(pa.src_auger_Hsc[1])

    # grab the pressure-corrected histos?
    if pa.src_auger_Hsc[0] == 'wPress':
        data_hsts = ff.hsts_wPressCorr(pa.src_auger_Hsc[1])
        t_hsc, _hsc = data_hsts.extract(
            date_ini = pa.ini_date, 
            date_end = pa.end_date, 
            vnames = ['Hist_wAoP_wPress'], 
            res = (86400*15) if pa.longres else 3600,
            )
        t_hsc = (t_hsc - sf.date2utc(pa.ini_date))/pa.unit
        hsc   = np.nanmean(_hsc[:,3:5+1], axis=1)
    # grab the temperature-corrected histos?
    elif pa.src_auger_Hsc[0] == 'wGh':
        aux = ff.cls__build_temp_corr()
        _pa = aux.parser.parse_args(("""
        --fname_inp %s --npoints %d --observable cts_temp-corr
        --ntanks_min 150 --ch_Eds 3 5
        """ % (pa.src_auger_Hsc[1], 120 if pa.longres else 0)).split())
        _pa.ini_date = pa.ini_date
        _pa.end_date = pa.end_date
        t_hsc, hsc = aux.build_ts(_pa)
        t_hsc = np.array(t_hsc)/pa.unit
        hsc   = np.array(hsc/np.nanmean(hsc))
        del aux
    else:
        raise SystemExit()

#--- auger::scalers
if pa.src_auger_scals != '':
    assert os.path.isfile(pa.src_auger_scals)
    data_scals = ff.scalers_final(pa.src_auger_scals)
    vnames = ['woAoP', 'wAoP', 'wAoP_wPrs', 'wAoP_wPrs_wGh', 'ntanks']
    dscls = {}
    for vnm in ['t_utc',]+vnames: dscls[vnm]=None

    _ = data_scals.extract(
        date_ini = pa.ini_date,
        date_end = pa.end_date,
        vnames = vnames,
        res = (86400*15) if pa.longres else 3600.,
        )

    for i in range(len(['t_utc',]+vnames)): 
        vnm = (['t_utc',]+vnames)[i]
        dscls[vnm] = _[i]


#---------------------------------------------------

import locale

print "\n [*] making figure..."
#--- figure
fig	= figure(1, figsize=pa.figsize)
# force the locale to be in english!
locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
ax	= fig.add_subplot(111)
t_ini, t_end = pa.tnorm #7.6, 8.5 # interval where to take the average rates

o_nm = {
'ms'    : 3,
'mec'   : 'None',
'alpha' : 0.8,
}
o_a = {
'lw'    : 2,
}

#--- Auger-Scalers
if pa.src_auger_scals != '':
    # corrected Scalers (w/ AoP and Pressure corrections)
    tscls = (dscls['t_utc'] - utc_jan06)/pa.unit
    rscls = dscls['wAoP_wPrs']/np.nanmean(dscls['wAoP_wPrs'][(tscls>=t_ini) & (tscls<=t_end)])
    ax.plot(tscls, rscls, '-', c='black', label='Scalers', alpha=0.9, **o_a)
    # uncorrected Scalers (w/o any correction)
    rscls_uncorr = dscls['woAoP']/np.nanmean(dscls['woAoP'])
    ax2 = ax.twinx()
    ax2.plot(tscls, rscls_uncorr, '--', c='gray', alpha=0.7, lw=2., 
        label='uncorrected\n Scalers')
    #ax2.set_ylim(0., 150.)
    ax2.set_ylabel('uncorrected Scalers')

#--- scaler Histograms: Hsc
if pa.src_auger_Hsc is not None:
    r_hsc = hsc/np.nanmean(hsc[(t_hsc>=t_ini) & (t_hsc<=t_end)])
    ax.plot(t_hsc, r_hsc, '-', c='red', label='$H_{sc}$: (60-120)MeV', 
        alpha=0.6, **o_a)

#--- muon Histograms: Hmu
if pa.src_auger_Hmu is not None:
    r_hmu = hmu/np.nanmean(hmu[(t_hmu>=t_ini) & (t_hmu<=t_end)])
    ax.plot(t_hmu, r_hmu, '-', c='blue', label='$H_{\mu}$: (200-280)MeV', 
        alpha=0.6, **o_a)


ax.grid(1)
ax.set_xlim(pa.xlim)
ax.set_ylim(pa.ylim)
#ax.autoscale_view()
#fig.tight_layout()

tscale_str = 'years' if pa.unit==86400*365 else \
    ('months' if pa.unit==86400*30 else \
    ('days' if pa.unit==86400 else \
    ('hours' if pa.unit==3600 else\
    ('minutes' if pa.unit==60 else 'time'))))
ax.set_xlabel('%s since %s' % (tscale_str, pa.ini_date.strftime('%d/%b/%Y')))
ax.set_ylabel('normalized rate')

# legends
# NOTE: nice way to fit the labels outside the figure.
# Sources:
# https://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot#4701285
# https://stackoverflow.com/questions/10101700/moving-matplotlib-legend-outside-of-the-axis-makes-it-cutoff-by-the-figure-box
h1, lab1 = ax.get_legend_handles_labels()
#lgd  = ax.legend(h1, lab1, loc=(0.0,1.05), ncol=2)
lgd  = ax.legend(h1, lab1, loc='lower left', fontsize=12)
bbox_extra = [ lgd, ]

h2, lab2 = ax2.get_legend_handles_labels()
lgd2 = ax2.legend(h2, lab2, loc='best', fontsize=12)
bbox_extra += [ lgd2 ]

print " [*] saving: " + pa.fname_fig
fig.savefig(pa.fname_fig, dpi=100, bbox_extra_artists=bbox_extra, bbox_inches='tight')
close(fig)

#EOF
