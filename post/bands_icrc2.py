#!/usr/bin/env ipython
"""
 author: j.j. masias meza

 description:
 generates figures of the generated data in "post/ch_Eds_smooth.py", 
 which are the time profiles of the ICRC 2015 proceeding.
 IMPORTANT: Note that in "post/ch_Eds_smooth.py", you need to run with
 ch_Eds=(3,4,5), and ch_Eds=(10,11,12,13).

"""
from pylab import *
import numpy as np
import os

home        = os.environ['HOME']
this_dir = '%s/ccin2p3/in2p3_data/data_auger/procesamiento/histos.aop_all' % home
dir_src  = '%s/out/out.post' % this_dir
dir_dst  = '%s/out/out.post/figs' % this_dir

maskname= 'shape.ok_and_3pmt.ok'#'shape.ok_and_3pmt.ok'#'3pmt.ok' # 'all'
fname_ScalsFromHistos 	= '%s/ascii/smoo_%s_60-120.MeV.txt' % (dir_src, maskname)
fname_muon		= '%s/ascii/smoo_%s_200-280.MeV.txt' % (dir_src, maskname)
#fname_scals 	= '%s/actividad_solar/neutron_monitors/figuras/NMs_res_15days.dat' % home
t_sc,   r_sc    = np.loadtxt(fname_ScalsFromHistos).T
t_mu,   r_mu    = np.loadtxt(fname_muon).T
#t_scls, r_scls  = np.loadtxt(fname_scals, usecols=(0,5)).T
#t_scls 		= (t_scls - 1136073600)/(86400.*365.)
#r_scls		/= 1.*np.nanmean(r_scls)

fig	= figure(1, figsize=(6, 4))
ax	= fig.add_subplot(111)

LW=4
#ax.plot(t_scls, r_scls, '-',c='black',  label='scalers',  lw=3., alpha=.8, mec='None')
ax.plot(t_sc, r_sc, '-', c='red',  label='$H_{sc}$: (60-120)MeV',  lw=LW, alpha=.7, mec='None')
ax.plot(t_mu, r_mu, '-', c='blue', label='$H_{\mu}$: (200-280)MeV', lw=LW, alpha=.5, mec='None')

LABELSIZE=15
ax.legend(loc='best')
ax.set_xlabel('years since Jan/2006', fontsize=LABELSIZE)
ax.set_ylabel('normalized rate', fontsize=LABELSIZE)
ax.set_xlim(0., 8.)
ax.grid()

fname_fig = '%s/bands_icrc2' % dir_dst

savefig('%s.png'%fname_fig, format='png', dpi=135, bbox_inches='tight')
savefig('%s.eps'%fname_fig, format='eps', dpi=135, bbox_inches='tight')

print " --->  generated: %s.png" % fname_fig

close()
