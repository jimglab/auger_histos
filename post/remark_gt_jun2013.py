#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
from pylab import *
from ch_Eds_smooth import (
    tt, rr, nn, ch_Eds, dir_dst, ntot, year_ini, year_fin
)

#fname_fig = '%s/figs/remark_gt_jun2013_.png' % dir_dst
fname_fig = '%s/figs/remark_gt_jun2013_%02d-%02d.MeV_%04d-%04d.png' % (dir_dst, ch_Eds[0]*20, (ch_Eds[-1]+1)*20, year_ini, year_fin)
#---- make figure
fig = figure(1, figsize=(6, 4))
ax  = fig.add_subplot(111)
ax.grid()
ax.set_xlabel('years since jan/2006')
ax.set_xlim(-.1, 10.1)  # until jan/2016
ax.set_ylim(0.95, 1.1)
TITLE = 'integrated Ed channels\nEd/MeV: %02d-%02d' % (ch_Eds[0]*20, (ch_Eds[-1]+1)*20)
ax.plot(tt[0,:], rr[0,:], '-o', alpha=0.6, mec='none', c='purple', ms=5.)

#--- remark the extended analysis
cc = tt[0,:]>7.5 # >jun/2013
ax.plot(tt[0,cc], rr[0,cc], '-o', mec='none', c='black', ms=5.)

ax.set_title(TITLE)
print " ---> nro archivos leidos: %d" % ntot
print "\n guardando figura..."
savefig(fname_fig, dpi=135, bbox_inches='tight')
print " ---> generamos: %s" % fname_fig
close()
#EOF
