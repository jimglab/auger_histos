#!/usr/bin/env ipython
"""
 author: j.j masias meza

 description:
 This generates average rates in wider channels (defined in 'ch_Eds').
 - for muon hump: (10,11,12,13)
 - for scalers:(3,4,5)
 NOTE: these are indices multiple of 20MeV (12 means 12*20MeV=240MeV)
 Note that channel 0 is the interval (0-20)MeV, and so on.

"""
from pylab import figure, close, show
from scipy.io.netcdf import netcdf_file
from datetime import datetime, timedelta
import numpy as np
import os, argparse, sys
import shared_funcs.funcs as sf
import funcs as ff

#--- parse args
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    description="""
    This generates rates averaged over an interval of energy 
    channels (defined in 'ch_Eds').
    """,
)
# subparsers
subparsers = parser.add_subparsers(description=
    """
    Use one of the submodules below.
    """,
    )
# config the subparsers
for mod_name in ['build_pressure_corr', 'build_temp_corr']:
    # grab the class
    mod = getattr(ff, 'cls__'+mod_name)()
    # grab the parser of that class
    subparser_ = subparsers.add_parser(
    mod_name, 
    help=mod.help,
    parents=[mod.parser], 
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    # all subparser will have this options in common:
    subparser_.add_argument(
    '-ini', '--ini_date', 
    type=str, 
    action=sf.arg_to_datetime,
    default='01/01/2006',
    help='initial date',
    )
    subparser_.add_argument(
    '-end', '--end_date', 
    type=str, 
    action=sf.arg_to_datetime,
    default='31/12/2015',
    help='end date',
    )

pa = parser.parse_args()

# grab the name of the module selected in the CLI
mod_selected = sys.argv[1]
print "\n ---> Using %s\n"%mod_selected 
mod = getattr(ff,'cls__'+mod_selected)()

# build the times series
tt, rr = getattr(mod, 'build_ts')(pa)

#print mod_selected
#sys.exit(1)

# i/o paths
fname_fig = '%s/%s_%s-%s.png' % (
    pa.dir_dst,
    pa.fprefix,
    pa.ini_date.strftime('%d%b%Y'), pa.end_date.strftime('%d%b%Y')
    )
fname_out = '%s/%s_%s-%s.txt' % (
    pa.dir_dst,
    pa.fprefix,
    pa.ini_date.strftime('%d%b%Y'), pa.end_date.strftime('%d%b%Y')
    )
os.system("mkdir -p " + pa.dir_dst)

# check
assert pa.npoints>=0, \
    '\n [-] npoints must be >=0 !!\n'

#--------------------------------------------------------------------
nn  = len(tt)
tt  = np.array(tt).reshape(1, nn)
rr  = np.array(rr).reshape(1, nn)

if pa.norm:     # normalize by the average in this time-window
    rr /= np.nanmean(rr)

if __name__=='__main__':
    tlim = 0.0, (pa.end_date-pa.ini_date).total_seconds()/pa.utime
    #---- make figure
    fig = figure(1, figsize=(6, 4))
    ax  = fig.add_subplot(111)
    ax.grid()
    ax.set_xlabel('time since %s' % pa.ini_date.strftime('%d/%b/%Y %H:%M'))
    ax.set_xlim(tlim)
    ax.set_ylim(0.95, 1.1)

    
    if pa.ch_Eds is not None: # if the observable is an energy channel
        TITLE = pa.observable +'\nintegrated Ed channels\nEd/MeV: %02d-%02d' % (
            pa.ch_Eds[0]*20, (pa.ch_Eds[1]+1)*20
        )
    else:
        TITLE = pa.observable
    ax.plot(tt[0,:]/pa.utime, rr[0,:], '-o', alpha=0.6, mec='none', c='purple', ms=3.5)
    ax.set_title(TITLE)
    print "\n [*] saving figure..."
    fig.savefig(fname_fig, dpi=135, bbox_inches='tight')
    print " ---> generated : %s" % fname_fig
    close(fig)

    #--- save data to ascii file
    data_out    = np.concatenate([tt, rr], axis=0)
    np.savetxt(fname_out, data_out.T, fmt='%3.4g')
    print " ---> generated : %s" % fname_out
    #print " ---> input     : \n ", pa.dir_src
#EOF
