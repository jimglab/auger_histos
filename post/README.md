# Visualization

These directory doesn't contain any scripts making any more corrections to the data.
These scripts read data only for visualization.

---
## Comparison of Auger rates with Neutron Monitors

```bash
./auger_and_NMs.py --pdb -- \
    -imix /media/scratch2/space_weather/NMs/mixed/2005Jan01-2018Jan02__corr_for_efficiency_1h.txt \
    -imurdo /media/scratch2/space_weather/NMs/mcmurdo \
    -issn /media/scratch2/space_weather/sunspot_numbers/ssn_monthly.txt \
    -iHmu wGh /media/scratch1/auger/build_temp.corr/gh-corrected-histos.h5 \
    -iHsc wGh /media/scratch1/auger/build_temp.corr/gh-corrected-histos.h5 \
    -ini 01/01/2006 \
    -end 31/12/2014 \
    -fig ./test6.png
```


---
## Generate a coarser time series, for a given energy interval

This generates a rate for a partial integration of the rates in a given energy interval.
Note, however that the generated rate is normalized.
The time resolution of the output is determined by the `-np` argument (see help below).
```bash
# using the 'build_pressure_corr' module
./ch_Eds_smooth.py -- build_pressure_corr \
    -src ~oauger/build_pressure.corr__wclf_l1 \
    -dst ~oauger/post \
    -mintank 150 \
    -np 400 \
    -norm \
    -obs cnts_press.corrected \
    -ini 01/01/2006 \
    -end 31/12/2015 \
    -fprefix test \
    -chE 10 13 \
    -utime $((365.*86400))

# using the 'build_temp_corr' module
./ch_Eds_smooth.py -- build_temp_corr \
    -fp Hmu.wGh \
    -odir ./test \
    -finp ~oauger/build_temp.corr/gh-corrected-histos.h5 \
    -favr ~oauger/build_temp_hist.and.fits/avr_histos.txt \
    -np 500 \
    -obs cts_temp-corr \
    -mintank 150 \
    -chE 10 13 \
    -ini 01/01/2006 \
    -end 31/12/2015 \
    -norm \
    -utime $((365.*86400))
```

Build a long-term profile of the geo-potential height itself:

```bash
./ch_Eds_smooth.py -- build_temp_corr \
    -fp gh \
    -odir ./test \
    -finp ~oauger/build_temp.corr/gh-corrected-histos.h5 \
    -favr ~oauger/build_temp_hist.and.fits/avr_histos.txt \
    -np 500 \
    -obs gh \
    -mintank 150 \
    -ini 01/01/2006 \
    -end 31/12/2015 \
    -norm \
    -utime $((365.*86400))
```


Full help:

    $ ./ch_Eds_smooth.py -- -h

    usage: ch_Eds_smooth.py [-h] {build_pressure_corr,build_temp_corr} ...

    This generates rates averaged over an interval of energy channels (defined in
    'ch_Eds').

    optional arguments:
      -h, --help            show this help message and exit

    subcommands:
      Use one of the submodules below.

      {build_pressure_corr,build_temp_corr}
        build_pressure_corr
                            Module to read/process the output of the
                            'build_pressure.corr' stage.
        build_temp_corr     module to read/process out of the 'build_temp_corr'
                            stage.

Help of the `build_temp_corr` module:

    $ ./ch_Eds_smooth.py -- build_temp_corr -h

    usage: ch_Eds_smooth.py build_temp_corr [-h] [-fp FPREFIX] [-odir DIR_DST]
                                            [-finp FNAME_INP] [-favr FNAME_AVR]
                                            [-np NPOINTS] [-obs OBSERVABLE]
                                            [-norm] [-utime UTIME]
                                            [-mintank NTANKS_MIN]
                                            [-chE ch_Eds ch_Eds] [-ini INI_DATE]
                                            [-end END_DATE]

    optional arguments:
      -h, --help            show this help message and exit
      -fp FPREFIX, --fprefix FPREFIX
                            prefix for output filenames (for figure and ASCII
                            files) (default: smoo2)
      -odir DIR_DST, --dir_dst DIR_DST
                            directory for output files (default: .)
      -finp FNAME_INP, --fname_inp FNAME_INP
                            path of .h5 file (final charge histogram data)
                            (default: /path/to/final_histos.h5)
      -favr FNAME_AVR, --fname_avr FNAME_AVR
                            Path of the ASCII .txt file. It contains time-averaged
                            (along several months) rates for each energy channel
                            in the histograms data. Should be the output of
                            'build_temp_hist.and.fits'. (default: None)
      -np NPOINTS, --npoints NPOINTS
                            Total number of data-points in the output. This
                            determines the time resolution of the time series. If
                            0 (zero), the output data is in the original
                            resolution. (default: 180)
      -obs OBSERVABLE, --observable OBSERVABLE
                            name of the variable we want to extract. If it doesn't
                            exist, it will prompt the available variable names in
                            the data. (default: cts_temp-corr)
      -norm, --norm         Wether or not to normalize the observable, by a factor
                            given by the time-average over the period of
                            extraction. (default: False)
      -utime UTIME, --utime UTIME
                            units of time (for the ASCII and the .png) in seconds.
                            For ex., 86400. gives the output in days. (default:
                            1.0)
      -mintank NTANKS_MIN, --ntanks_min NTANKS_MIN
                            For each data-point in the input files, there is an
                            associated number-of-tanks field. Such value is the
                            number of SD tanks that were filtered-in in the time
                            window of each timestamp, in the energy interval of
                            that data-point. So such value gives us the
                            'statistics' at each timestamp. With this argument, we
                            can choose a lower threshold to filter the data-points
                            by its 'statistics' weight. (default: 150)
      -chE ch_Eds ch_Eds, --ch_Eds ch_Eds ch_Eds
                            Indexes that define the limits for the energy [closed]
                            interval of interest. For ex., '10 13' means Muon band
                            (i.e. 200-280 MeV), and '3 5' mean Scaler band. These
                            are indexes multiple of 20MeV (12 means
                            12*20MeV=240MeV). Note that channel 0 is the interval
                            (0-20)MeV, and so on. (default: None)
      -ini INI_DATE, --ini_date INI_DATE
                            initial date (default: 01/01/2006)
      -end END_DATE, --end_date END_DATE
                            end date (default: 31/12/2015)


---
## Values of the correction-slopes vs energy (Ed)

Generates a curve of the values of the correction slopes (for AoP and pressure corrections) as a function of the deposited energy Ed:
```bash
./corr.slopes_vs_Ed.py -- \
    -fmaop ${AUGER_REPO}/paper/correl__Histos_vs_AoP/fit.txt ${sfname_out3} \
    -fmpress ${AUGER_REPO}/paper/correl__Histos_vs_Press/fit.txt ${AUGER_REPO}/paper/correl__Scals_vs_Press.h5 \
    -fig ${AUGER_REPO}/paper/corr.slopes_vs_Ed.png
```



---

<!--- EOF -->
