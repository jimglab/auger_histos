#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
"""
Include comm-crisis dates:
     17apr-8/jun 2009 (dato de Piera en hangout del 25.feb.2016)
"""
import numpy as np
from datetime import datetime, timedelta
import os, argparse
import shared_funcs.funcs as sf
import funcs as ff
from pylab import figure, close, show
import sys

#--- parse args
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    description="""
    This generates rates averaged over an interval of energy 
    channels (defined in 'ch_Eds').
    """,
)
parser.add_argument(
'-imix', '--src_mix',
type=str,
help="""
Input file for Neutron Monitor (NM) data, containing data from several NM stations.
""",
)
parser.add_argument(
'-imurdo', '--src_murdo',
type=str,
help="""
Input directory for McMurdo data; containing the original files downloaded from Bartol website.
"""
)
parser.add_argument(
'-issn', '--src_ssn',
type=str,
help="""
Input file for sunsport numbers. Monthly data recommended.
""",
)
parser.add_argument(
'-iHmu', '--src_auger_Hmu',
type=str,
nargs=2,
default=None,
metavar=('KIND','PATH'),
help="""
Input file for Auger Muon-Histograms (Hmu).
The KIND sub-argument must be wPress or wGh, refering to pressure-corrected and gh-corrected data.
""",
)
parser.add_argument(
'-iHsc', '--src_auger_Hsc',
type=str,
nargs=2,
default=None,
metavar=('KIND','PATH'),
help="""
Input file for Auger Scaler-Histograms (Hsc). 
The KIND sub-argument must be wPress or wGh, refering to pressure-corrected and gh-corrected data.
""",
)
parser.add_argument(
'-iScls', '--src_auger_scals',
type=str,
default='',
help="""
Input file for Auger Scalers.
""",
)
parser.add_argument(
'-fig', '--fname_fig',
type=str,
help="""
output figure.
""",
)
parser.add_argument(
'-xlim', '--xlim',
type=float,
nargs=2,
metavar=('TMIN','TMAX'),
help='range for time domain in the plot, in units of the -unit argument.',
)
parser.add_argument(
'-unit', '--unit',
type=float,
default=86400.,
help='time units (in seconds) for the plot. For ex., for time in year units, use -unit $((86400*365)).',
)
parser.add_argument(
'-ylim', '--ylim',
type=float,
nargs=2,
default=[None,None], #[0.96, 1.15],
metavar=('YMIN','YMAX'),
help='range for vertical axis. If not used, there are no limits set.',
)
parser.add_argument(
'-tnorm', '--tnorm',
type=float,
nargs=2,
default=[7.6, 8.5],
metavar=('TMIN-NORM','TMAX-NORM'),
help="""time range where to normalize all time series (in 
units determined by -unit)""",
)
parser.add_argument(
'-figsize', '--figsize',
type=float,
nargs=2,
default=[7, 4],
help='figure size, in inches.',
)
parser.add_argument(
'-longres', '--longres',
action='store_true',
default=False,
help="""If used, long resolution change is used for all datasets.
""",
)
parser.add_argument(
'-ini', '--ini_date', 
action=sf.arg_to_datetime,
default='01/01/2006',
help='initial date',
)
parser.add_argument(
'-end', '--end_date', 
action=sf.arg_to_datetime,
default='31/12/2015',
help='end date',
)
pa = parser.parse_args()


#--- disable some warnings
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)


utc_jan06   = sf.date2utc(pa.ini_date) #1136073600.

#--- mix
if pa.src_mix is not None:
    mix = ff.NMs_mix1(pa.src_mix)
    t_mix, athenas, kiel = mix.extract(
        date_ini = pa.ini_date, 
        date_end = pa.end_date,
        vnames = ['ATHN','KIEL'], 
        res = (86400*30) if pa.longres else 1800,
        )

#--- murdo
if pa.src_murdo is not None:
    murdo_mgr = ff.NM_mcmurdo(path=pa.src_murdo) 
    t_murdo, murdo = murdo_mgr.extract(
        date_ini = pa.ini_date, 
        date_end = pa.end_date,
        vnames=['McMurdo-Corr',],
        res = (86400*15) if pa.longres else 3600,
        )

#--- SSN (sunspot numbers)
if pa.src_ssn is not None:
    data_ssn = np.loadtxt(pa.src_ssn)
    ssn      = data_ssn[:,2]
    t_ssn    = np.zeros(data_ssn.shape[0])
    for i in range(data_ssn.shape[0]):
        yyyy, mm, dd = map(int, data_ssn[i,[0,1]]) + [1,]
        t_ssn[i]     = sf.date2utc(datetime(yyyy, mm, dd, 0, 0)) # [utc-sec]

#--- auger::Hmu
if pa.src_auger_Hmu is not None:
    assert os.path.isdir(pa.src_auger_Hmu[1]) or os.path.isfile(pa.src_auger_Hmu[1])

    # grab the pressure-corrected histos?
    if pa.src_auger_Hmu[0] == 'wPress':
        data_hsts = ff.hsts_wPressCorr(pa.src_auger_Hmu[1])
        t_hmu, _hmu = data_hsts.extract(
            date_ini = pa.ini_date, 
            date_end = pa.end_date, 
            vnames = ['Hist_wAoP_wPress'], 
            res = (86400*15) if pa.longres else 3600,
            )
        t_hmu = (t_hmu - sf.date2utc(pa.ini_date))/pa.unit
        hmu   = np.nanmean(_hmu[:,10:13+1], axis=1)
    # grab the temperature-corrected histos?
    elif pa.src_auger_Hmu[0] == 'wGh':
        aux = ff.cls__build_temp_corr()
        _pa = aux.parser.parse_args(("""
        --fname_inp %s --npoints %d --observable cts_temp-corr
        --ntanks_min 150 --ch_Eds 10 13
        """ % (pa.src_auger_Hmu[1], 120 if pa.longres else 0)).split())
        _pa.ini_date = pa.ini_date
        _pa.end_date = pa.end_date
        t_hmu, hmu = aux.build_ts(_pa)
        # TODO: fix 'res' argument so that it can accept/process 
        # values res<=2days correctly.
        t_hmu = np.array(t_hmu)/pa.unit
        hmu   = np.array(hmu/np.nanmean(hmu))
        del aux
    else:
        raise SystemExit()

#--- auger::Hsc
if pa.src_auger_Hsc is not None:
    assert os.path.isdir(pa.src_auger_Hsc[1]) or os.path.isfile(pa.src_auger_Hsc[1])

    # grab the pressure-corrected histos?
    if pa.src_auger_Hsc[0] == 'wPress':
        data_hsts = ff.hsts_wPressCorr(pa.src_auger_Hsc[1])
        t_hsc, _hsc = data_hsts.extract(
            date_ini = pa.ini_date, 
            date_end = pa.end_date, 
            vnames = ['Hist_wAoP_wPress'], 
            res = (86400*15) if pa.longres else 3600,
            )
        t_hsc = (t_hsc - sf.date2utc(pa.ini_date))/pa.unit
        hsc   = np.nanmean(_hsc[:,3:5+1], axis=1)
    # grab the temperature-corrected histos?
    elif pa.src_auger_Hsc[0] == 'wGh':
        aux = ff.cls__build_temp_corr()
        _pa = aux.parser.parse_args(("""
        --fname_inp %s --npoints %d --observable cts_temp-corr
        --ntanks_min 150 --ch_Eds 3 5
        """ % (pa.src_auger_Hsc[1], 120 if pa.longres else 0)).split())
        _pa.ini_date = pa.ini_date
        _pa.end_date = pa.end_date
        t_hsc, hsc = aux.build_ts(_pa)
        t_hsc = np.array(t_hsc)/pa.unit
        hsc   = np.array(hsc/np.nanmean(hsc))
        del aux
    else:
        raise SystemExit()


#--- auger::scalers
if pa.src_auger_scals != '':
    assert os.path.isfile(pa.src_auger_scals)
    data_scals = ff.scalers_final(pa.src_auger_scals)
    vnames = ['wAoP', 'wAoP_wPrs', 'wAoP_wPrs_wGh', 'ntanks']
    dscls = {}
    for vnm in ['t_utc',]+vnames: dscls[vnm]=None

    _ = data_scals.extract(
        date_ini = pa.ini_date,
        date_end = pa.end_date,
        vnames = vnames,
        res = (86400*15) if pa.longres else 3600.,
        )

    for i in range(len(['t_utc',]+vnames)): 
        vnm = (['t_utc',]+vnames)[i]
        dscls[vnm] = _[i]


#---------------------------------------------------
import locale

print "\n [*] making figure..."
#--- figure
fig	= figure(1, figsize=pa.figsize)
# force the locale to be in english!
locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
ax	= fig.add_subplot(111)
t_ini, t_end = pa.tnorm #7.6, 8.5 # interval where to take the average rates

o_nm = {
'ms'     : 3,
'mec'   : 'None',
'alpha' : 0.6,
}
o_a = {
'lw' : 2,
'alpha' : 0.6,
}

# Sunsport number (in 2nd vertical axis)
if pa.src_ssn is not None:
    ax2 = ax.twinx()
    ax2.plot((t_ssn-utc_jan06)/pa.unit, ssn, '--', alpha=0.6, lw=2., label='SSN', color='brown')
    ax2.set_ylim(0., 150.)
    ax2.set_ylabel('sunspot number (SSN)')

# McMurdo
if pa.src_murdo is not None:
    tmurdo  = (t_murdo-utc_jan06)/pa.unit
    r_murdo = murdo/np.nanmean(murdo[(tmurdo>=t_ini) & (tmurdo<=t_end)])
    ax.plot(tmurdo, r_murdo, '-o', c='gray', label='McMurdo (0.01 GV)', **o_nm)


if pa.src_mix is not None:
    # Kiel
    t_kiel = (t_mix-utc_jan06)/pa.unit
    r_kiel = kiel/np.nanmean(kiel[(t_kiel>=t_ini) & (t_kiel<=t_end)])
    ax.plot(t_kiel, r_kiel, '-o', c='orange', label='Kiel (2.36 GV)', **o_nm)
    # Athenas
    t_ath = (t_mix-utc_jan06)/pa.unit
    r_ath = athenas/np.nanmean(athenas[(t_ath>=t_ini) & (t_ath<=t_end)])
    ax.plot(t_ath, r_ath, '-o', c='green', label='Athens (8.53 GV)', **o_nm)

# Auger-Scalers
if pa.src_auger_scals != '':
    tscls = (dscls['t_utc'] - utc_jan06)/pa.unit
    rscls = dscls['wAoP_wPrs']/np.nanmean(dscls['wAoP_wPrs'][(tscls>=t_ini) & (tscls<=t_end)])
    ax.plot(tscls, rscls, '-', c='fuchsia', label='Scalers', **o_a)

# scaler Histograms: Hsc
if pa.src_auger_Hsc is not None:
    r_hsc = hsc/np.nanmean(hsc[(t_hsc>=t_ini) & (t_hsc<=t_end)])
    ax.plot(t_hsc, r_hsc, '-', c='red', label='$H_{sc}$', **o_a)

# muon Histograms: Hmu
if pa.src_auger_Hmu is not None:
    r_hmu = hmu/np.nanmean(hmu[(t_hmu>=t_ini) & (t_hmu<=t_end)])
    ax.plot(t_hmu, r_hmu, '-', c='blue', label='$H_{\mu}$', **o_a)

ax.grid(1)
ax.set_xlim(pa.xlim)
ax.set_ylim(pa.ylim)
#ax.autoscale_view()
#fig.tight_layout()

tscale_str = 'years' if pa.unit==86400*365 else \
    ('months' if pa.unit==86400*30 else \
    ('days' if pa.unit==86400 else \
    ('hours' if pa.unit==3600 else\
    ('minutes' if pa.unit==60 else 'time'))))
ax.set_xlabel('%s since %s' % (tscale_str, pa.ini_date.strftime('%d/%b/%Y')))
ax.set_ylabel('normalized rate')

# legends
# NOTE: nice way to fit the labels outside the figure.
# Sources:
# https://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot#4701285
# https://stackoverflow.com/questions/10101700/moving-matplotlib-legend-outside-of-the-axis-makes-it-cutoff-by-the-figure-box
h1, lab1 = ax.get_legend_handles_labels()
lgd  = ax.legend(h1, lab1, loc=(0.0,1.05), ncol=2)
bbox_extra = [ lgd, ]

if pa.src_ssn is not None:
    h2, lab2 = ax2.get_legend_handles_labels()
    lgd2 = ax2.legend(h2, lab2, loc='best')
    bbox_extra += [ lgd2 ]

print " [*] saving: " + pa.fname_fig
fig.savefig(pa.fname_fig, dpi=100, bbox_extra_artists=bbox_extra, bbox_inches='tight')
close(fig)

#EOF
