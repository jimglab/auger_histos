#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
import argparse, os
import shared_funcs.funcs as sf
import numpy as np
from scipy.io.netcdf import netcdf_file
from datetime import datetime, timedelta
from pylab import figure, close

"""
Availables variables in the input:

The `yyyy_mm_dd.nc` files have these key-named contents:
* `time`: `(ntime,)` UTC-seconds in a uniform sequence of 15min for this day `dd/mm/yyyy`.
* `aop_avrg_over_array`: `(ntime,)` averages of the AoP values over the SD array, from the `mc..root` files.
* `aop_avrg_over_array_from.sd`: `(ntime,)` averages of the AoP values over the SD array, from the `sd..root` files.
* `ntanks`: `(ntime,)` number of tanks contributing with non-gap data values to the averages.
* `aop.uncorrected_cnts`: `('ntime', 'nbins_mev')` scaler counts without AoP correction.
* `aop.corrected_cnts`: `('ntime', 'nbins_mev')` AoP-corrected charge-histogram counts.
* `aop.corrected_cnts_std`: `('ntime', 'nbins_mev')` associated standard deviations.
"""

#--- parse args
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter,
description="""
Extracts (and plot) slices of the output data of the processing stage 'code_aop.correction'.
""",
)
parser.add_argument(
'-src', '--dir_src',
type=str,
help='input directory. Should be the output dir of "build_array.avrs"',
)
parser.add_argument(
'-fop', '--fout_pref',
type=str,
help='prefix for the output filenames (ASCII and .png files).'
)
parser.add_argument(
'-iE', '--iE',
type=int,
default=11, # iE=11 for Ed=240MeV
metavar='iE',
help='energy index for charge-hist band. For ex., iE=11 for Ed=240MeV = (iE+1)*20MeV.',
)
parser.add_argument(
'-ini', '--ini_date', 
type=str, 
action=sf.arg_to_datetime,
default='01/01/2006',
help='initial date',
)
parser.add_argument(
'-end', '--end_date', 
type=str, 
action=sf.arg_to_datetime,
default='31/12/2015',
help='end date',
)
pa = parser.parse_args()


t       = []
ntanks  = []
cts     = []
#---------------------------------
fnull = 0
date  = pa.ini_date
while date <= pa.end_date:
    yyyy    = date.year
    mm      = date.month
    dd      = date.day

    fname_inp   = '%s/%04d/%04d_%02d_%02d.nc' % (pa.dir_src, yyyy, yyyy, mm, dd)
    if not os.path.isfile(fname_inp):
        fnull   += 1
        date    += timedelta(days=1)
        continue

    print " [*] Reading: %s" % fname_inp
    f       = netcdf_file(fname_inp, 'r')

    t       += f.variables['time'].data.tolist()  # [sec-utc]
    ntanks  += f.variables['ntanks'].data.tolist()
    cts     += f.variables['aop.corrected_cnts'].data[:,pa.iE].tolist()

    date    += timedelta(days=1)

# total requested files
nreq = (pa.end_date - pa.ini_date).days + 1
# report
print "\n [*] there were %d missing files out of %d files\n"%(fnull, nreq)

#--- save to ascii
dsize = len(t)
data = np.empty((dsize,3), dtype=np.float64)
data[:,0] = t
data[:,1] = ntanks
data[:,2] = cts
np.savetxt(pa.fout_pref+'.txt', X=data, fmt='%9.8g')

#--- plot
fig     = figure(1, figsize=(6,4))
ax      = fig.add_subplot(111)

ax.plot([(_-t[0])/86400. for _ in t], cts, 'o')

ax.grid(1)
ax.set_xlabel('days since ' + pa.ini_date.strftime('%d/%b/%Y %H:%M'))
ax.set_ylabel('count rate @ iE=%d (%g MeV)' % (pa.iE, (pa.iE+1)*20.))

fig.savefig(pa.fout_pref+'.png', dpi=150, bbox_inches='tight')
close(fig)

#EOF
