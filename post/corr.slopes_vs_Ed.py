#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
import os, argparse
from os.path import isdir, isfile
from pylab import (
    figure, close, savefig, plot, scatter, show
)
import h5py
import matplotlib.patches as patches
import matplotlib.transforms as transforms
from matplotlib.patches import Ellipse
import numpy as np
from matplotlib.gridspec import GridSpec
import matplotlib.pyplot as plt

#--- parse args
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    description="""
    This generates rates averaged over an interval of energy 
    channels (defined in 'ch_Eds').
    """,
)
parser.add_argument(
'-fmaop', '--fname_maop',
type=str,
nargs=2,
metavar=('FNAME_HISTOS', 'FNAME_SCALS'),
help='Fit-parameter files for AoP corrections.'
)
parser.add_argument(
'-fmpress', '--fname_mpress',
type=str,
nargs=2,
metavar=('FNAME_HISTOS', 'FNAME_SCALS'),
help='Fit-parameter files for pressure corrections.'
)
parser.add_argument(
'-fig', '--fname_fig',
type=str,
help='Output figure filename.'
)
pa = parser.parse_args()

#--- extract data
# Histograms vs AoP
Ed, m_aop   = np.loadtxt(pa.fname_maop[0], usecols=(0,1)).T
# Histograms vs Pressure
Ed, m_press = np.loadtxt(pa.fname_mpress[0], usecols=(0,1)).T
# Histogramas vs Geop-height
#Ed, m_hg    = np.loadtxt(fname_m_hg, usecols=(0,1)).T
# Scalers vs AoP
f1 = h5py.File(pa.fname_maop[1], 'r')
m_aop_scals = f1['fit/m'].value
f1.close()
# Scalers vs Pressure
f2 = h5py.File(pa.fname_mpress[1], 'r')
m_press_scals = f2['H2D_w.press/fit/m'].value
f2.close()


#-------- figura
fig = figure(1, figsize=(7,3))
gs  = GridSpec(nrows=2, ncols=1)
gs.update(left=0.35, right=0.98, hspace=0.23, wspace=0.25)
ax  = []
ax  += [ plt.subplot(gs[0, 0]) ]
ax  += [ plt.subplot(gs[1, 0]) ]
#ax  += [ plt.subplot(gs[2, 0]) ]
nax = len(ax)

for i in range(nax):
    #-------- low energy
    trans   = transforms.blended_transform_factory( 
                ax[i].transData, ax[i].transAxes)
    rect1   = patches.Rectangle((60.0, 0.0), width=60.0, height=1,
                transform=trans, color='red', alpha=0.3)
    ax[i].add_patch(rect1)
    #-------- high energy
    trans   = transforms.blended_transform_factory( 
                ax[i].transData, ax[i].transAxes)
    rect1   = patches.Rectangle((200.0, 0.0), width=80.0, height=1,
                transform=trans, color='blue', alpha=0.3)
    ax[i].add_patch(rect1)
    #---------------------
    #ax[i].set_aspect(0.5) # aspect ratio w/ space? 
    # See post "Python/Matplotlib: controlling the aspect ratio in gridspec" in
    # stackoverflow.
    ax[i].grid(ls=':')
    ax[i].set_xlim(50., 350.)
    ax[i].tick_params(labelsize=12)

    #----- xlabel
    # Put the label only in the lowest panel
    if(i==nax-1):
        ax[i].set_xlabel('$E_d$ [MeV]', fontsize=17)
    else:
        ax[i].set_xlabel('') #(label=None)
        #ax[i].get_xaxis().set_ticks([])
        ax[i].xaxis.set_ticklabels([])

#--- plot-kwds
opt = {
'c'     : 'black',
'ms'    : 4,
'lw'    : 1.,
}

#----- ax[1]
maop_scals	= m_press_scals #-0.003496 # TODO: change here!
ax[1].plot(Ed[3:], m_press[3:], '-o', **opt)
TEXT='$m^{S,press}$=%2.2f' % maop_scals
ax[1].annotate(TEXT, xy=(210., -2.5), fontsize=14)
ax[1].set_ylabel('$m^{H,press}_{e}$', fontsize=18)
ax[1].legend(loc='best')
ax[1].set_ylim(-3.2, -0.8)

#----- ax[0]
ax[0].plot(Ed[3:], m_aop[3:], '-o', **opt)
maop_scals	= m_aop_scals #0.21 # TODO: change here!
TEXT='$m^{S,AoP}$=%2.2f' % maop_scals 
ax[0].annotate(TEXT, xy=(220.,0.), fontsize=14)
ax[0].set_ylabel('$m^{H,AoP}_{e}$', fontsize=18)
ax[0].legend(loc='lower left')
ax[0].set_ylim(-0.2, +0.8)

##----- ax[2]
#ax[2].plot(Ed, 100.*m_hg, '-o', **opt)
##TEXT='$m^{S,AoP}$=%1.1f%%'% (100.*maop_scals)
##ax[2].annotate(TEXT, xy=(220.,0.), fontsize=18)
#ax[2].set_ylabel('$m^{H,hg}_{e}$\n[%]', fontsize=20)
#ax[2].set_ylim(-1.3, 0.0)

#------ save figure
fig.savefig(pa.fname_fig, dpi=100, format='png', bbox_inches='tight')
close(fig)
#EOF
