## Build pressure correction to AoP-corrected charge histograms

Before executing we must have available these inputs:
* `dir_src`    : data that have synced data of AoP-corrected histos && pressure in a single file
* `fname_inp1` : long-term time-averages of histograms for each channel
* `fname_inp2` : fit values for correlations between AoP-corrected histos and pressure
* `dir_dst`    : output directory path.

```bash
./test.py -- \
    -src /media/scratch1/auger/build_aop.corr_phys \
    -fi1 /media/scratch1/auger/build_avr_histos/avr_histos__Jan2006-Dec2014.txt \
    -fi2 /media/scratch1/auger/build_hist2d_correl.pressure/fit_params.txt \
    -dst /media/scratch1/auger/build_pressure.corr \
    -ini 01/01/2006 \
    -end 31/12/2014
```

---
Full help:

	$ ./test.py -- -husage: test.py [-h] [-src DIR_SRC] [-fi1 FNAME_INP1] [-fi2 FNAME_INP2]
				   [-dst DIR_DST] [-ini INI_DATE] [-end END_DATE]

	Generates pressure-corrected charge histogram counts as a function of time,
	for the period (INI_DATE - END_DATE) inclusive.

	optional arguments:
	  -h, --help            show this help message and exit
	  -src DIR_SRC, --dir_src DIR_SRC
							input directory, where we have the AoP-corrected
							rates. Should be the output dir of
							"build_aop.corr_phys" (default: None)
	  -fi1 FNAME_INP1, --fname_inp1 FNAME_INP1
							Input ASCII file: time-averaged rates of each Ed
							channel, assumed in the same period as specified by
							the arguments of this script (i.e. INI_DATE -
							END_DATE). Should be the output dir of
							'build_avr_histos' (default: None)
	  -fi2 FNAME_INP2, --fname_inp2 FNAME_INP2
							input ASCII file, containing the fit parameters of the
							correlation between rates and pressute. Should be the
							output of "build_hist2d_correl.pressure". (default:
							None)
	  -dst DIR_DST, --dir_dst DIR_DST
							output directory (default: None)
	  -ini INI_DATE, --ini_date INI_DATE
							initial date (default: 01/01/2006)
	  -end END_DATE, --end_date END_DATE
							end date (default: 31/12/2015)


<!--- EOF -->
