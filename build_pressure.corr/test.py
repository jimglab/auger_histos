#!/usr/bin/env ipython
"""
 author: j.j. masias meza
 runtime: ~5-10min (assuming data is not in cache memory)
"""
import funcs as ff
import argparse, os
import shared_funcs.funcs as sf
from scipy.io.netcdf import netcdf_file
import numpy as np
import console_colors as ccl
from datetime import datetime, timedelta

#--- parse args
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    description="""
    Generates pressure-corrected charge histogram counts as
    a function of time, for the period (INI_DATE - END_DATE) inclusive.
    """,
)
parser.add_argument(
'-src', '--dir_src',
type=str,
help='input directory, where we have the AoP-corrected rates. Should be the output dir of "build_aop.corr_phys"',
)
parser.add_argument(
'-fi1', '--fname_inp1',
type=str,
help="""
Input ASCII file: time-averaged rates of each Ed channel, assumed in the same period as specified by the arguments of this script (i.e. INI_DATE - END_DATE). 
Should be the output dir of 'build_avr_histos'
""",
)
parser.add_argument(
'-fi2', '--fname_inp2',
type=str,
help='input ASCII file, containing the fit parameters of the correlation between rates and pressute. Should be the output of "build_hist2d_correl.pressure".',
)
parser.add_argument(
'-dst', '--dir_dst',
type=str,
help='output directory',
)
parser.add_argument(
'-ini', '--ini_date', 
type=str, 
action=sf.arg_to_datetime,
default='01/01/2006',
help='initial date',
)
parser.add_argument(
'-end', '--end_date', 
type=str, 
action=sf.arg_to_datetime,
default='31/12/2015',
help='end date',
)
pa = parser.parse_args()

#---------------------------------------- agarro valores tipicos para construir rangos
prom       = np.loadtxt(pa.fname_inp1)
typic      = np.mean(prom[:,1:], axis=0) # time-averaged charge-histogram, in the whole time period of analysis
# read some values in the header of the fit-parameters file
PRESS_AVR  = float(sf.ReadParam_from_2dHist(pa.fname_inp2,'press_mean')[0])
mpress	   = np.loadtxt(pa.fname_inp2, usecols=(1,))
#-------------------------------------------------------------------------------------

bins	    = 50
n	    = 96
ntot	    = 0			# monitoring

"""
 filters handler 'filter_mgr()':
    - en intervalos de 4 meses determina el promedio de:
      - el nro de tanques
      - AoP
    - filtra los periodos en q el nro de tanques se aparta de 1 sigma respecto
      del nro promedio (centrado) de tanques.
    - filtra los periodos en q el AoP instantaneo se aparta 3.3 sigma respeto
      del AoP promedio (centrado).
    - La salida son ambos sigmas. Uso estos valores para filtrar valores del 
      dia 'date' al momento de construir los 'aop.corrected_cnts_phys'.
"""

date    = pa.ini_date
fmgr	= ff.filter_mgr(pa.dir_src, bins, n, date, pa.end_date)
while date <= pa.end_date:
    yyyy      = date.year
    mm	      = date.month
    dd	      = date.day
    fname_inp = '%s/%04d/%04d_%02d_%02d.nc' % (pa.dir_src, yyyy, yyyy, mm, dd)
    if os.path.isfile(fname_inp): # if no input for this date, don't generate ouput
        try:
            #----- deduce limits to filter data
            fmgr.sigmas_today(date) # calculate means and errors
            min_aop,    max_aop     = fmgr.avr_aop - fmgr.std_aop, fmgr.avr_aop + fmgr.std_aop
            min_ntanks, max_ntanks  = fmgr.avr_ntanks - fmgr.std_ntanks, fmgr.avr_ntanks + fmgr.std_ntanks
            filter_bad = np.isnan(fmgr.avr_aop) or np.isnan(fmgr.avr_ntanks) #True:all bad
            if filter_bad:
                #import pdb; pdb.set_trace()
                raise SystemExit(" ----> WHAT HAPPENED WITH THE FILTER??!!")
            #-------------------------------
            f       = netcdf_file(fname_inp, 'r')
            print ccl.R + " [*] Opening: %s" % fname_inp + ccl.W
            m       = f.variables['aop.corrected_cnts_phys'].data # aop.corrected_cnts_phys(ntime, nbins_mev)
            press   = f.variables['pressure'].data		  # pressure(ntime)
            rate_corr   = np.empty(n*50).reshape(n,50)
            rate_uncorr = np.empty(n*50).reshape(n,50)
            for i in range(50):	 # corregimos para c/canal de Ed
                rate  = m[:,i] / typic[i] # [1] todas las cuentas para el canal 'i'
                slope = mpress[i]         # [1] pendiente para la i-esima energia Ed
		# pressure correction
                rate_corr[:,i]   = rate - slope*(press/PRESS_AVR-1.) # [1] corregidos por efecto barometrico
                rate_uncorr[:,i] = rate	  # guardamos sin corregir tmb

            #********************************
            fo	  	= ff.file_mgr(pa.dir_dst, n, yyyy, mm, dd)
            fo.time	= f.variables['time'].data			# sec-utc
            fo.ntanks 	= f.variables['ntanks'].data.copy()
            fo.aop_avr	= f.variables['aop_avrg_over_array'].data.copy()	# AoP promediado sobre el array
            fo.cts_std	= f.variables['aop.corrected_cnts_phys_std'].data.copy()
            fo.cts_aop_uncorr = f.variables['aop.uncorrected_cnts_phys'].data.copy() # aop.uncorrected_cnts_phys(ntime, nbins_mev)
            fo.press	= press			# [hPa]
	        # pressure-corrected rates
            fo.cts_corr 	= rate_corr*typic	# cts_corr(ntime, nbins_mev): cuentas corregidas promediadas sobre el array
            fo.cts_uncorr	= rate_uncorr*typic # cuentas *sin* corregir
            #--- filtro: True for good data
            cc		= (fo.aop_avr>min_aop) & (fo.aop_avr<max_aop)
            cc		&= (fo.ntanks>min_ntanks) & (fo.ntanks<max_ntanks)
            fo.set_filter(cc)
            #----------
            fo.save()		# archiva las cuentas corregidas en .nc

            ntot    += 1
            f.close()
            del fo

        except KeyboardInterrupt:
            print " ---> KEYBOARD-INTERRUPT! @post.py"
            raise SystemExit
        
        """except:
            print " ---> CORRUPTO!: %s" % fname_inp
            pass"""

    else:
        print " [-] no file: %s" % fname_inp

    date += timedelta(days=1)
    if date.month!=mm:
        print ccl.Gn + " ---> mes: %02d/%04d" % (mm,yyyy) + ccl.W

print "\n [*] output: %s\n" % pa.dir_dst
#EOF
