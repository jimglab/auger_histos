from pylab import *
from scipy.io.netcdf import netcdf_file
from datetime import datetime, timedelta
import numpy as np
import os
import console_colors as ccl

class file_mgr(object):
    def __init__(self, dir_dst, ntime, yyyy, mm, dd):
        self.dir_dst 	= '%s/%04d' % (dir_dst, yyyy)
        self.ntime	= ntime
        self.fname_out 	= '%s/%04d_%02d_%02d.nc' % (self.dir_dst, yyyy, mm, dd)
        os.system('mkdir -p %s' % self.dir_dst)

    def save(self):
        cc = self.cc # filtro
        self.fout = netcdf_file(self.fname_out, 'w')
        self.fout.createDimension('ntime', self.ntime)
        self.fout.createDimension('nbins_mev', 50)

        # guardamos cuentas cormalizadas corregidas por AoP
        dims	= ('ntime', 'nbins_mev')
        self.cts_corr[~cc,:] = np.nan
        self.cts_uncorr[~cc,:] = np.nan
        self.cts_aop_uncorr[~cc,:] = np.nan
        self.cts_std[~cc,:] = np.nan
        self.write_variable('cnts_press.corrected'  , dims, self.cts_corr  , 'd', '[cts/m2/s]')
        self.write_variable('cnts_press.uncorrected', dims, self.cts_uncorr, 'd', '[cts/m2/s]')
        self.write_variable('cnts_aop.uncorrected', dims, self.cts_aop_uncorr, 'd', '[cts/m2/s]')
        self.write_variable('cnts_std_aop.corrected', dims, self.cts_std, 'd', '[cts/m2/s] sts dev a lo del array')

        # guardamos tiempo
        dims	= ('ntime',)
        self.write_variable('time', dims, self.time, 'd', '[sec-utc]')

        # guardamos presion
        self.write_variable('pressure', dims, self.press, 'd', '[mmHg]')

        # AoP promediado sobre el array
        self.aop_avr[~cc] = np.nan
        self.write_variable('aop_avrg_over_array', dims, self.aop_avr, 'd', '[1] AoP promediado sobre array')

        # nro de tanque q aportan al promedio
        self.ntanks[~cc] = np.nan
        self.write_variable('ntanks', dims, self.ntanks, 'd', '[1] nro de tanques q aportan')

        self.fout.close()

    def set_filter(self, cc):
        self.cc = cc # True para data ok
    
    def write_variable(self, varname, dims, var, datatype, comments):
        dummy           = self.fout.createVariable(varname, datatype, dims)
        dummy[:]        = var
        dummy.units     = comments



class filter_mgr(object):
    def __init__(self, dir_src, bins, ntime, date_ini, date_max):
        self.dir_src	= dir_src
        self.bins	= bins
        self.ntime	= ntime
        self.date_ini	= date_ini
        self.date_max	= date_max

    def sigmas_today(self, date_now):
        # limites temporales para los valores medios y sigmas
        date_i	= date_now - timedelta(days=60)	# 2 meses antes
        date_f	= date_now + timedelta(days=60)	# 2 meses despues
        BINS	 = 50	# nro de bines del histograma 1D
        LIMS_AOP    = [2.6, 4.6]
        LIMS_NTANKS = [100., 2000.]
        basura  = np.nan*np.ones(self.ntime)

        date	= date_i
        #aop	= 0.0
        #ntanks	= 0.0
        H_aop	 = np.zeros(BINS)
        H_ntanks = np.zeros(BINS)
        while date < date_f:
            yyyy    = date.year
            mm      = date.month
            dd      = date.day
            fname_inp   = '%s/%04d/%04d_%02d_%02d.nc' % (self.dir_src, yyyy, yyyy, mm, dd) 
            if os.path.isfile(fname_inp):
                f       = netcdf_file(fname_inp, 'r')
                #print ccl.B + "    --> Abriendo: %s" % fname_inp + ccl.W
                aop    = f.variables['aop_avrg_over_array'].data
                ntanks = f.variables['ntanks'].data
                #if not(aop==basura or ntanks=basura): # no acumulo basura
                #print " ---> shape: ", aop.shape; raw_input()
                #print " histosg1d: ", np.histogram(aop, bins=BINS, range=LIMS_AOP); raw_input()
                H_aop 	 += np.histogram(aop, bins=BINS, range=LIMS_AOP)[0]
                H_ntanks += np.histogram(ntanks, bins=BINS, range=LIMS_NTANKS)[0]
                #ntot	 += 1  # nro de dias q tomo en cuenta

            date += timedelta(days=1)
        
        array_aop	= np.linspace(LIMS_AOP[0], LIMS_AOP[1], BINS)
        array_ntanks	= np.linspace(LIMS_NTANKS[0], LIMS_NTANKS[1], BINS)
        ntot_aop	= np.sum(H_aop)		# integral del histograma 1D
        ntot_ntanks	= np.sum(H_ntanks)	# integral del histograma 1D
        avr_aop 	= np.sum(H_aop*array_aop)/(1.*ntot_aop)
        avr_ntanks	= np.sum(H_ntanks*array_ntanks)/(1.*ntot_ntanks)

        aux_aop		= np.sum(H_aop*np.power(array_aop - avr_aop, 2.0))
        std_aop		= np.sqrt(aux_aop/(1.*ntot_aop))

        aux_ntanks	= np.sum(H_ntanks*np.power(array_ntanks - avr_ntanks, 2.0))
        std_ntanks	= np.sqrt(aux_ntanks/(1.*ntot_ntanks))

        self.avr_aop	= avr_aop
        self.std_aop	= std_aop
        self.avr_ntanks	= avr_ntanks
        self.std_ntanks	= std_ntanks

#EOF
