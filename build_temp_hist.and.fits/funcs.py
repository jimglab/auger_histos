#------------------------------------------------
import numpy as np
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib.colors import LogNorm
#------------------------------------------------
from pylab import figure, show, close
from scipy.io.netcdf import netcdf_file
import os

def make_hist2d(fname, lim, cbmax, title, i):
    H2D = np.loadtxt(fname, unpack=True) + 1. # le sumo uno para q sea >0.
    bins    = H2D.shape[0]

    aop = np.linspace(2., 5., bins)
    scal    = np.linspace(lim[0], lim[1], bins)

    fig     = figure(1, figsize=(6, 4))
    ax  = fig.add_subplot(111)

    CBMIN   = 1.0
    CBMAX   = cbmax #6e6
    surf    = ax.contourf(aop, scal, H2D, facecolors=cm.jet(H2D), linewidth=0, 
            cmap=cm.gray_r, alpha=0.9, vmin=CBMIN, vmax=CBMAX, norm=LogNorm())

    m = cm.ScalarMappable(cmap=surf.cmap, norm=surf.norm)
    m.set_array(H2D)

    axcb = plt.colorbar(m)
    LABEL_COLOR_BAR = 'points per bin square'
    axcb.set_label(LABEL_COLOR_BAR, fontsize=20)
    ax.set_ylabel('partial-integrated rate [cts/s/m2]')
    ax.set_xlabel('AoP')
    ax.set_title(title)

    m.set_clim(vmin=CBMIN, vmax=CBMAX)
    ax.grid()
    plt.tight_layout()

    fig.savefig('../figs/hist2d/hist2d_%02d.png'%i, dpi=135, bbox_inches='tight')
    close(fig)


def make_fitplot(fname, stuff, cbmax, title, i):
    xlim    = stuff[0][0]
    ylim    = stuff[0][1]
    m   = stuff[1][0]
    b   = stuff[1][1]
    xvec    = stuff[2][0]
    yvec    = stuff[2][1]

    H2D = np.loadtxt(fname, unpack=True) + 1. # le sumo uno para q sea >0.
    bins    = H2D.shape[0]

    aop = np.linspace(xlim[0], xlim[1], bins)
    scal    = np.linspace(ylim[0], ylim[1], bins)

    fig     = figure(1, figsize=(6, 4))
    ax  = fig.add_subplot(111)

    CBMIN   = 1.0
    CBMAX   = cbmax #6e6
    surf    = ax.contourf(aop, scal, H2D, facecolors=cm.jet(H2D), linewidth=0, 
            cmap=cm.gray_r, alpha=0.9, vmin=CBMIN, vmax=CBMAX, norm=LogNorm())
    ax.plot(xvec, m*xvec+b, '-', c='red', alpha=0.6, label='m:%2.1g'%m)

    m = cm.ScalarMappable(cmap=surf.cmap, norm=surf.norm)
    m.set_array(H2D)

    axcb = plt.colorbar(m)
    LABEL_COLOR_BAR = 'points per bin square'
    axcb.set_label(LABEL_COLOR_BAR, fontsize=12)
    ax.set_ylabel('partial-integrated normalized rate')
    ax.set_xlabel('AoP')
    ax.set_title(title)
    #ax.set_ylim(ylim[0], ylim[1])  # no deberia necesitarlo
    #ax.set_xlim(xlim[0], xlim[1])  # no deberia necesitarlo

    m.set_clim(vmin=CBMIN, vmax=CBMAX)
    ax.grid()
    plt.tight_layout()

    fig.savefig('../../figs/hist2d_fit/hist2d_%02d.png'%i, dpi=135, bbox_inches='tight')
    close()

def make_fitplot_muon(fname, stuff, cbmax, title, fname_fig=None):
    xlim    = stuff[0][0]
    ylim    = stuff[0][1]
    m       = stuff[1][0]
    b       = stuff[1][1]
    xvec    = stuff[2][0]
    yvec    = stuff[2][1]
    H2D     = np.loadtxt(fname, unpack=True) + 1. # le sumo uno para q sea >0.
    bins    = H2D.shape[0]
    aop     = np.linspace(xlim[0], xlim[1], bins)
    scal    = np.linspace(ylim[0], ylim[1], bins)

    import locale
    fig     = figure(1, figsize=(6, 4))
    # NOTE: PATCH! because for some reason, the above line that executes
    # the figure() function, automatically changes the LC_TIME locale
    # to spanish!! So we force the 'en_US' by hand!
    # Sources/Hints:
    # https://stackoverflow.com/questions/985505/locale-date-formatting-in-python#985517
    # https://stackoverflow.com/questions/14547631/python-locale-error-unsupported-locale-setting#36257050
    locale.setlocale(locale.LC_ALL, 'en_US.UTF-8') 
    ax      = fig.add_subplot(111)

    CBMIN   = 1.0
    CBMAX   = cbmax #6e6
    surf    = ax.contourf(aop, scal, H2D, facecolors=cm.jet(H2D), linewidth=0, 
            cmap=cm.gray_r, alpha=0.9, vmin=CBMIN, vmax=CBMAX, norm=LogNorm())
    LABEL = '8yr fit:\n m=%3.2g\n b=%3.1f' % (m, b)
    ax.plot(xvec, m*xvec+b, '--', lw=3., c='red', alpha=0.6, label=LABEL)

    m = cm.ScalarMappable(cmap=surf.cmap, norm=surf.norm)
    m.set_array(H2D)

    axcb = fig.colorbar(m)
    LABEL_COLOR_BAR = 'points per bin square'
    axcb.set_label(LABEL_COLOR_BAR, fontsize=12)
    ax.set_ylabel('$H_{id,e}$ ')
    ax.set_xlabel('geopotential height (h) [%]\n(deviation from average)')
    ax.set_title(title)
    #ax.set_ylim(ylim[0], ylim[1])  # no deberia necesitarlo
    #ax.set_xlim(xlim[0], xlim[1])  # no deberia necesitarlo
    m.set_clim(vmin=CBMIN, vmax=CBMAX)
    ax.grid()
    ax.legend(loc='best', fontsize=12)

    if fname_fig==None:
        #fname_fig = '../../figs/hist2d_fit/hist2d_muon_%02d'%i
        fname_fig = './hist2d_test'
    
    fig.savefig('%s.png'%fname_fig, format='png', dpi=135, bbox_inches='tight')
    close(fig)


def make_fitplot_muon_poster(fname, stuff, cbmax, title, i, fname_fig):
    xlim    = stuff[0][0]
    ylim    = stuff[0][1]
    m   = stuff[1][0]
    b   = stuff[1][1]
    xvec    = stuff[2][0]
    yvec    = stuff[2][1]

    H2D = np.loadtxt(fname, unpack=True) + 1. # le sumo uno para q sea >0.
    bins    = H2D.shape[0]

    aop = linspace(xlim[0], xlim[1], bins)
    scal    = linspace(ylim[0], ylim[1], bins)

    fig     = figure(1, figsize=(6, 4))
    ax  = fig.add_subplot(111)

    CBMIN   = 1.0
    CBMAX   = cbmax #6e6
    surf    = ax.contourf(aop, scal, H2D, facecolors=cm.jet(H2D), linewidth=0, 
            cmap=cm.gray_r, alpha=0.9, vmin=CBMIN, vmax=CBMAX, norm=LogNorm())
    LABEL = '8yr fit:\n m=%3.2f' % (m)
    ax.plot(xvec, m*xvec+b, '--', lw=3., c='red', alpha=0.6, label=LABEL)

    m = cm.ScalarMappable(cmap=surf.cmap, norm=surf.norm)
    m.set_array(H2D)

    axcb = plt.colorbar(m)
    LABEL_COLOR_BAR = 'points per bin square'
    axcb.set_label(LABEL_COLOR_BAR, fontsize=14)
    ax.set_ylabel('$H_{e}$', fontsize=17)
    ax.set_xlabel('$\Delta h_g$ (deviation from mean) [%]', fontsize=19)
    ax.set_title(title, fontsize=18)
    #ax.set_ylim(ylim[0], ylim[1])  # no deberia necesitarlo
    #ax.set_xlim(xlim[0], xlim[1])  # no deberia necesitarlo

    m.set_clim(vmin=CBMIN, vmax=CBMAX)
    ax.grid()
    plt.tight_layout()
    ax.legend(loc='best', fontsize=15)

    #fname_fig = '../../figs/hist2d_fit/hist2d_muon.poster_%02d'%i
    savefig('%s.png'%fname_fig, format='png', dpi=135, bbox_inches='tight')
    savefig('%s.eps'%fname_fig, format='eps', dpi=135, bbox_inches='tight')
    close()
#EOF
