#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
"""
- calculate an average (along the years; e.g. 2006-2013) histogram, so that later we can use it to normalize individual histograms before making the temperature corrections.

runtime:
real    0m52.411s
user    0m3.384s
sys     0m0.876s
"""
import numpy as np
from scipy.io.netcdf import netcdf_file
import os, argparse
from datetime import datetime, timedelta
import console_colors as ccl
import shared_funcs.funcs as sf

#--- parse args
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    description="""
    This generates rates averaged over an interval of energy 
    channels (defined in 'ch_Eds').
    """,
)
parser.add_argument(
'-src', '--dir_src',
type=str,
help="""
Input directory, where we have the AoP-and-pressure-corrected rates. 
Should be the output of 'build_pressure.corr'.
""",
)
parser.add_argument(
'-nm', '--nmonth',
type=int,
default=4,
metavar='NW_MONTH',
help='width of window (in months) of the running averages.',
)
parser.add_argument(
'-ntm', '--ntmonth',
type=int,
default=8*12, # 9 years in units of months
metavar='NTOTAL_MONTH',
help='total number of months of the whole time period to be \
processed (in months). It **has** to be a multiple of NW_MONTH, so \
that the running windows fit exactly on the whole time period of analysis.',
)
parser.add_argument(
'-ini', '--ini_date', 
type=str, 
action=sf.arg_to_datetime,
metavar='IniDateCenter',
default='01/03/2006',
help='initial date, which will be taken as the center of the 1st \
time-window for the calculation of running averages. It should be \
IniDateCenter=<IniDate> + 0.5*NW_MONTH, where <IniDate> is the \
intended initial date to take into account in the whole time period \
of processing. So for ex., if we want to analyze since 01/Jan/2006, and \
NW_MONTH=4, we should specify here 01/Mar/2006 (i.e. 60 days after 01/Jan/2006).',
)
parser.add_argument(
'-m', '--maskname',
type=str,
default='shape.ok_and_3pmt.ok',
help='Quality cut options: \'shape.ok_and_3pmt.ok\', \'3pmt.ok\', or \'all\'. We \
dont make quality cuts at this point. This is just to use the label of the \
previous stages of processing; it\'s healthy to keep track of it.'
)
parser.add_argument(
'-fo', '--fname_out',
type=str,
help='output ASCII file.',
)
pa = parser.parse_args()

nshifts = pa.ntmonth/pa.nmonth
avr_tot = np.zeros((nshifts,50), dtype=np.float32) #inicilizar en cero!
time    = np.zeros((nshifts,1), dtype=np.float32)
nothing = np.nan*np.ones(50)

for i in range(nshifts):
    center_shift    = i*30*pa.nmonth # [days] cada cuanto muevo el centro de mi promedio
    center          = pa.ini_date + timedelta(days=center_shift)
    time[i]         = sf.date2utc(center)  # [seg utc] tiempo del centro
    print " ---> center: ", center
    ntot    = 0 # para dsps hallar el promedio de todos loq  aportaron

    for shift in range(-30*pa.nmonth/2, 30*pa.nmonth/2): # ventana de promedio centrado
        date  = center + timedelta(days=shift)
        yyyy_ = date.year
        mm_   = date.month
        dd_   = date.day
        #fname   = '%s/%04d/%04d_%02d_%02d.nc' % (pa.dir_src, yyyy_, yyyy_, mm_, dd_)
        fname = '{src}/{year}/{year}_{mm:02d}_{dd:02d}.nc'.format(
            src=pa.dir_src, year=date.year, mm=date.month, dd=date.day
            )

        if os.path.isfile(fname):
            try:
                f = netcdf_file(fname, 'r')
                print ccl.B + "    [*] Opening: %s " % fname + ccl.W
                m = f.variables['cnts_press.corrected'].data # shape: (ntime, nbins_mev)
                avr_aux = np.nanmean(m, axis=0) # promedio temporal de todos los canales
                #if prod(avr_aux==nothing, dtype=bool):
                if np.isnan(avr_aux[1]):
                    continue
                else:
                    avr_tot[i,:] += avr_aux
                    ntot         += 1
                f.close()
            
            except KeyboardInterrupt:
                raise SystemExit(
                "\n [!] KEYBOARD-INTERRUPT while processing %s\n"%date.strftime("%d/%b/%Y")
                )

            except:
                print ccl.R + " [*] CORRUPT!: %s " % fname + ccl.W
                pass

    avr_tot[i,:] /= 1.*ntot
    """
    if ntot<=0:
        print " ----> RIDICULOUS!!!: \n ntot:%d\n filename: %s" %(ntot, f.filename)
        raise SystemExit
    """
                
#-----------------------------------------------------------------------
data    = np.concatenate([time, avr_tot], axis=1)
HEADER	= 'maskname: %s\n' % pa.maskname + \
	'ntmonth: %d\n' % pa.ntmonth + \
	'nmonth: %d\n' % pa.nmonth + \
	'IniDateCenter: %s' % pa.ini_date.strftime('%d/%b/%Y %H:%M')
np.savetxt(pa.fname_out, data, fmt='%10.9g', header=HEADER)
print ccl.G + "\n [+] generated: %s\n" % pa.fname_out + ccl.W

#EOF
