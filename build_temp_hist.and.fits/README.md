## Geopotential-height corrections to charge-histograms:

In this directory, we:

1. `build_avr_histos_press.txt.py`: 
    calculate a time-averaged (along the years; e.g. 2006-2013) charge-histogram, so that 
    later we can use it to normalize individual Histograms before making the temperature corrections.
	Full help:

		$ ./build_avr_histos_press.txt.py -- -h
		usage: build_avr_histos_press.txt.py [-h] [-src DIR_SRC] [-nm NW_MONTH]
											 [-ntm NTOTAL_MONTH] [-ini IniDateCenter]
											 [-m MASKNAME] [-fo FNAME_OUT]

		This generates rates averaged over an interval of energy channels (defined in
		'ch_Eds').

		optional arguments:
		  -h, --help            show this help message and exit
		  -src DIR_SRC, --dir_src DIR_SRC
								Input directory, where we have the AoP-and-pressure-
								corrected rates. Should be the output of
								'build_pressure.corr'. (default: None)
		  -nm NW_MONTH, --nmonth NW_MONTH
								width of window (in months) of the running averages.
								(default: 4)
		  -ntm NTOTAL_MONTH, --ntmonth NTOTAL_MONTH
								total number of months of the whole time period to be
								processed (in months). It **has** to be a multiple of
								NW_MONTH, so that the running windows fit exactly on
								the whole time period of analysis. (default: 96)
		  -ini IniDateCenter, --ini_date IniDateCenter
								initial date, which will be taken as the center of the
								1st time-window for the calculation of running
								averages. It should be IniDateCenter=<IniDate> +
								0.5*NW_MONTH, where <IniDate> is the intended initial
								date to take into account in the whole time period of
								processing. So for ex., if we want to analyze since
								01/Jan/2006, and NW_MONTH=4, we should specify here
								01/Mar/2006 (i.e. 60 days after 01/Jan/2006).
								(default: 01/03/2006)
		  -m MASKNAME, --maskname MASKNAME
								Quality cut options: 'shape.ok_and_3pmt.ok',
								'3pmt.ok', or 'all'. We dont make quality cuts at this
								point. This is just to use the label of the previous
								stages of processing; it's healthy to keep track of
								it. (default: shape.ok_and_3pmt.ok)
		  -fo FNAME_OUT, --fname_out FNAME_OUT
								output directory, recommended in the form
								path/<maskname> (default: None)


2. `./build_H2D_Ed.ascii.py`: 
    build 2D histograms of the correlations between the pressure-corrected 
    rates and the geopotential height.

3. `./fitplots.py`: 
    perform linear fits to the correlations above, using the 2D-histograms. 
    Generates .png figures if `DISPLAY` environment variable exists.

<!--- EOF -->
