#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
from pylab import *
import numpy as np
import os
import matplotlib.patches as patches
import matplotlib.transforms as transforms
from matplotlib.patches import Ellipse

maskname = 'shape.ok_and_3pmt.ok'
this_dir = os.environ['AUGER_REPO'] # repo dir
dir_src   = '%s/out/out.build_temp_hist.and.fits' % this_dir
fname_inp = '%s/fits_%s.txt' % (dir_src, maskname)
#fname_fig = 'fit_params_press2_%s' % maskname
fname_fig = '%s/fits_%s_temp' % (dir_src, maskname)

Ed, maop = np.loadtxt(fname_inp, usecols=(0,1)).T

fig = figure(1, figsize=(6, 4))
ax  = fig.add_subplot(111)
#-------- baja energia
trans   = transforms.blended_transform_factory( 
                ax.transData, ax.transAxes)
rect1   = patches.Rectangle((60.0, 0.0), width=60.0, height=1,
                transform=trans, color='red', alpha=0.3)
ax.add_patch(rect1)
#-------- alta energia
trans   = transforms.blended_transform_factory( 
                ax.transData, ax.transAxes)
rect1   = patches.Rectangle((200.0, 0.0), width=80.0, height=1,
                transform=trans, color='blue', alpha=0.3)
ax.add_patch(rect1)
#---------------------

#maop_scals	= -0.003496
ax.plot(Ed[3:], 100.*maop[3:], '-o', c='black', label='histograms')
#TEXT='$m^{S,press}$=%1.2f%%'%(100.*maop_scals)
#ax.annotate(TEXT, xy=(500., -.38), fontsize=18)
#ax.plot(Ed, (100.*maop_scals)*np.ones(Ed.size), '--', lw=5, c='gray', label='scalers')

ax.set_xlabel('Ed [MeV]', fontsize=15)
ax.set_ylabel('$m^{H,h_g}_{e}$ [1/%]', fontsize=17)
ax.set_ylim(-1.1, 1.5)
ax.legend(loc='best')
ax.grid()

savefig('%s.png'%fname_fig, format='png', dpi=135, bbox_inches='tight')
#savefig('%s.eps'%fname_fig, format='eps', dpi=135, bbox_inches='tight')
close()

