#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
"""
- build 2D histograms of the correlations between the pressure-corrected rates and the geopotential height.
- runtime: ~1.5min
"""
#from pylab import *
from scipy.io.netcdf import netcdf_file
from datetime import datetime, timedelta
import numpy as np
import os, argparse
from os.path import isfile, isdir
from scipy.interpolate import (
    splrep,     # Spline interpolation
    splev)      # Spline evaluator
import h5py
from matplotlib.colors import LogNorm
import shared_funcs.funcs as sf

#--- parse args
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    description="""
    Build 2D histograms of the correlations w/ geop height.
    """,
)
parser.add_argument(
'-src', '--dir_src',
type=str,
help='input directory. Should be the output dir of "build_pressure.corr"',
)
parser.add_argument(
'-ncep', '--dir_ncep',
type=str,
help='input directory with pre-processed data of NCEP. Should be the output dir of "build_weather/build_ncep_gh.py".',
)
parser.add_argument(
'-fa', '--fname_avr',
type=str,
help='input ASCII file: time-averaged charge-histograms. Should be the output dir of "./build_avr_histos_press.txt.py"',
)
parser.add_argument(
'-dst', '--dir_dst',
type=str,
help='output dir.',
)
parser.add_argument(
'-rx', '--range_x',
type=np.float64,
nargs=2,
default=[16050, 16650],
metavar=('XMIN','XMAX'),
help='x-range (gh limits) where to build the 2D histograms',
)
parser.add_argument(
'-ry', '--range_y',
type=np.float64,
nargs=2,
default=[0.95, 1.1],
metavar=('YMIN','YMAX'),
help='y-range (normalized rate limits) where to build the 2D histograms',
)
parser.add_argument(
'-bins', '--bins',
type=int,
default=200,
help='number of bins by which the input 2D histograms will be discretized.',
)
parser.add_argument(
'-mintank', '--mintank',
type=int,
default=150,
help="""
For each data-point in the input files, there is an associated 
number-of-tanks field. Such value is the number of SD tanks that 
were filtered-in in the time window of each timestamp, in the energy interval 
of that data-point.
So such value gives us the 'statistics' at each timestamp.
With this argument, we can choose a lower threshold to filter the
data-points by its 'statistics' weight.
""",
)
parser.add_argument(
'-ini', '--ini_date', 
type=str, 
action=sf.arg_to_datetime,
default='01/01/2006',
help='initial date',
)
parser.add_argument(
'-end', '--end_date', 
type=str, 
action=sf.arg_to_datetime,
default='31/12/2015',
help='end date',
)
pa = parser.parse_args()

#maskname = 'shape.ok_and_3pmt.ok'#'shape.ok_and_3pmt.ok'#'3pmt.ok' # 'all'
#dir_src  = '%s/out/out.build_pressure.corr/%s/15min' % (this_dir, maskname)
#dir_AvrHist = '%s/out/out.build_temp_hist.and.fits' % this_dir
#dir_src_gdas = '%s/out/out.build_weather/ncep'%this_dir #'%s/data_gdas'%HOME
#dir_dst = '%s/out/out.build_temp_hist.and.fits/hist2d' % this_dir

# create output dir if necessary
os.system("mkdir -p " + pa.dir_dst)

#---------------------- agarro valores tipicos para construir rangos
#fname_avr= '%s/avr_histos_press_%s.txt' % (dir_AvrHist, maskname)
prom    = np.loadtxt(pa.fname_avr)
typic   = np.nanmean(prom[:,1:], axis=0) # agarro el promedio del array de los 8 anios!
#-------------------------------------------------------------------

LEVEL       = 100  # [mb or hPa] isobaric surface
lname       = 'level_%04d' % (LEVEL)

hg_min,  hg_max  = pa.range_x #16050, 16650
cts_min, cts_max = pa.range_y #0.95, 1.1 #1.04 #1.1
ax_hg     = np.linspace(hg_min,  hg_max,  pa.bins)
ax_cts    = np.linspace(cts_min, cts_max, pa.bins), 
range_hg  = (hg_min, hg_max)
range_cts = (cts_min, cts_max)
RANGE     = (range_hg, range_cts)

#--------------------------------------------------------------------
day     = 86400./(365.*86400.)      # [yr]
ntot    = 0
ch_Eds  = np.arange(0, 50) #(10,11,12,13)#(3,4,5)   # indices de los canales de Ed a promediar
nEd     = len(ch_Eds)
H2D     = np.zeros((nEd, pa.bins, pa.bins), dtype=np.float64)
j=0; k=1; r=0.0; n=0
tt=[]; rr=[]
missing = [] # list of missing input files

date  = pa.ini_date #datetime(year_ini,   1, 1)
yyyy2 = date.year - 1 
while date <= pa.end_date:
    yyyy = date.year
    mm   = date.month
    dd   = date.day
    fname_inp = '%s/%04d/%04d_%02d_%02d.nc' % (pa.dir_src, yyyy, yyyy, mm, dd)
    if not isfile(fname_inp):
        missing += [ fname_inp ]
        date	+= timedelta(days=1)   # next day...
        continue

    f = netcdf_file(fname_inp, 'r')

    # everytime we change year, need to open another .h5 file
    if yyyy2!=yyyy:
        fname_inp_ncep = '%s/test_%04d.h5' % (pa.dir_ncep, yyyy)
        fg = h5py.File(fname_inp_ncep, 'r')
        yyyy2 = yyyy

    dname = '%04d-%02d' % (yyyy, mm)
    path  = '%s/%s' % (lname, dname)
    g_t   = fg['%s/t'%path].value
    g_h   = fg['%s/h'%path].value
    tck   = splrep(g_t, g_h, s=0)

    #print "    --> Abriendo: %s" % fname_inp
    ntanks = f.variables['ntanks'].data
    cc     = ntanks>pa.mintank

    if cc.nonzero()[0].size > 1: # (*1)
        rate = f.variables['cnts_press.corrected'].data
        time = (f.variables['time'].data  - sf.date2utc(pa.ini_date))/(86400.*365) # years since `pa.ini_date` (*2)
        cts  = np.zeros(96, dtype=np.float64)
        gh   = splev(time, tck, der=0) # der: order of derivative to compute (less than "k")
        for i in ch_Eds:
            cts_norm = rate[:,i]/typic[i] # normalize by the typical in each channel
            H2D[i] += np.histogram2d(
		gh, 
		cts_norm, 
		bins=[pa.bins, pa.bins], 
		range=RANGE, 
		normed=False
	    )[0]

        print ' [*] date: ', date

    else:
        print ' [-] date (too few tanks!): ', date

    ntot += 1
    f.close()
    date += timedelta(days=1)	# next day...

# (*1) there's more than 1 data point, which is an average 
# of >pa.mintank stations.
# (*2) time units don't matter since we are interested in building 
# just the correlations.

print "\n [+] FINISHED building the 2d-histograms!\n"

#--------------------------------------------------------------------
# same header for all files
HEADER='lim-gh     : %g:%g\n' % (pa.range_x[0],pa.range_x[1]) +\
       'lim-rate   : %g:%g\n' % (pa.range_y[0],pa.range_y[1]) +\
       'bins       : %d\n' % pa.bins +\
       'ini_date   : %s\n' % pa.ini_date.strftime('%d/%b/%Y %H:%M') +\
       'end_date   : %s' % pa.end_date.strftime('%d/%b/%Y %H:%M')

print "\n [*] saving histograms..."
os.system("mkdir -p " + pa.dir_dst)
for i in range(nEd):
    chi, chf = ch_Eds[i]*20, (ch_Eds[i]+1)*20
    fname_out = '%s/H2D_%03d-%03d.MeV.txt' % (pa.dir_dst, chi, chf)
    np.savetxt(fname_out, H2D[i], fmt='%4.4g', header=HEADER)

print " [+] output dir: %s" % pa.dir_dst

if len(missing)>0:
    print "\n [*] these input-files were missing:\n", missing, "\n"
 
#EOF
