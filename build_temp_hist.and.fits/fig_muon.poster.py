#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
#from funcs import *
import funcs as ff
import numpy as np
from scipy.io.netcdf import netcdf_file
import os

bins 	= 100 #50 # nro de bines de los archivos "../ascii/H_%01d.txt"
# los rangos lo saco del 'RANGE' en "../post.py"
gh_mean = 16367.4   # altura potencial promedio a 100hPa (ver ~/data_gdas/stats)
xlim = 100.*(np.array([16050, 16650]) - gh_mean)/gh_mean
ylim = [0.95, 1.10] 

yval	= np.linspace(ylim[0], ylim[1], bins)
xval	= np.linspace(xlim[0], xlim[1], bins)

xvec	= np.empty(bins*bins)
yvec 	= np.empty(bins*bins)

# construyo grilla
for i in range(bins):
	ini	= i*bins
	fin	= (i+1)*bins
	yvec[ini:fin] = yval
	xvec[ini:fin] = xval[i]*np.ones(bins)

#******************************************************
cbmax 	= 1e3
lims	= [xlim, ylim]
#m 	= np.empty(50, dtype=np.float64)
#b 	= np.empty(50, dtype=np.float64)

this_dir = os.environ['AUGER_REPO'] # repo dir
maskname = 'shape.ok_and_3pmt.ok' #'shape.ok_and_3pmt.ok'#'3pmt.ok' # 'all'
#for i_h in range(50):
i_h	= 11	# nro del canal Ed
#fname 	  = '../ascii/H_muon_%s.txt' % (maskname)
dir_src   = '%s/out/out.build_temp_hist.and.fits/hist2d' % this_dir
fname_inp = '%s/H2D_muon_%s.txt' % (dir_src, maskname)
fname_fig = '%s/../fig_muon.poster' % dir_src #'./test.png'

H	= np.loadtxt(fname_inp) + 1.0
#bins	= size(H, 0)
print " ---> ", H.shape

H	= H.reshape(bins*bins)
wei	= np.sqrt(H)		# factores de peso
p	= np.polyfit(xvec, yvec, deg=1, w=wei, cov=True)

m_muon 		= p[0][0]
b_muon    	= p[0][1]
m_muon_err	= p[1][0][0]

print " [%s] i: %02d/%02d" % (maskname, i_h, 50)
stuff	= []
title   = "$E_d$: %02d-%02d MeV"  % (i_h*20, i_h*20+20.)
stuff	+= [[xlim, ylim]]
stuff	+= [[m_muon, b_muon]]
stuff 	+= [[xvec, yvec]]
ff.make_fitplot_muon_poster(fname_inp, stuff, cbmax, title, i_h, fname_fig)

print " LISTO! :-)"

print " m: %4.7f +- %4.7f" % (m_muon, m_muon_err)
print " b: ", b_muon
#print p
#******************************************************
#Ed	 = np.arange(10., 1010., 20.)
#data_out = np.array([Ed, m, b])
#fname_out= './fit_params_%s_MUON.txt' % maskname
#np.savetxt(fname_out, data_out.T, fmt='%8.7g')
#print " ---> generado: %s" % fname_out
