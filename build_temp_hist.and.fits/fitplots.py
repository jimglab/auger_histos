#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
"""
- perform linear fits to the correlations above, using the 2D-histograms.

runtime:
~57sec
"""
#from funcs import *
import funcs as ff
from os.path import isdir, isfile
import argparse, os
import numpy as np
import shared_funcs.funcs as sf
from datetime import datetime, timedelta

#--- parse args
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    description="""
    Build 2D histograms of the correlations w/ geop height.
    """,
)
parser.add_argument(
'-src', '--dir_src',
type=str,
help='input directory. Should be the output dir of "./build_H2D_Ed.ascii.py"',
)
parser.add_argument(
'-df', '--dir_fig',
type=str,
help='output dir, where an ASCII file containing the linear fit parameters, and .png figures associated to those fits.',
)
parser.add_argument(
'-fo', '--fname_out',
type=str,
help='output ASCII file, containing the fit parameters.',
)
parser.add_argument(
'-cbmax', '--cbmax',
type=np.float64,
default=1e2,
help='max value of color scale in the contour plots of the 2D histograms',
)
pa = parser.parse_args()

nEd       = 50

# we can plot *only if* we have a DISPLAY
graphics  = 'DISPLAY' in os.environ
if graphics:
    print " --> we'll produce figures at: %s\n" % pa.dir_fig

#--- open a sample input file to grab some parameters
fsample = '%s/H2D_020-040.MeV.txt' % pa.dir_src
_xlim = map(float, sf.ReadParam_from_2dHist(fsample,'lim-gh' ))
ylim = map(float, sf.ReadParam_from_2dHist(fsample,'lim-rate'))
bins = int(sf.ReadParam_from_2dHist(fsample,'bins')[0])
ini_date = datetime.strptime(
    sf.ReadParam_from_2dHist(fsample,'ini_date'), 
    '%d/%b/%Y %H:%M\n'
    )
end_date = datetime.strptime(
    sf.ReadParam_from_2dHist(fsample,'end_date'), 
    '%d/%b/%Y %H:%M\n'
    )

yval	= np.linspace(ylim[0], ylim[1], bins)
_xval	= np.linspace(_xlim[0], _xlim[1], bins)

# get the average value in x-dimension
H2D_sample = np.loadtxt(fsample)
# accurate estimation of the mean by just using the 2D-histograms
# TODO: we can actually use the NCEP data directly, and calculate
# the exact mean (to be consistent with foreward stages of processing) by
# specifying the 'ini_date' and 'end_date'.
xmean = np.sum(np.sum(H2D_sample, axis=0) * _xval)/H2D_sample.sum()
print " ---> xmean:", xmean
assert xmean>_xlim[0] and xmean<_xlim[1]
# normalize version of _xlim
xlim = [ 100.*(_ - xmean)/xmean for _ in _xlim ]
xval	= np.linspace(xlim[0], xlim[1], bins)

xvec	= np.empty(bins*bins)
yvec 	= np.empty(bins*bins)
#import pdb; pdb.set_trace()


# construyo grilla
for i in range(bins):
    ini	= i*bins
    fin	= (i+1)*bins
    yvec[ini:fin] = yval
    xvec[ini:fin] = xval[i]*np.ones(bins)

#******************************************************
os.system("mkdir -p "+pa.dir_fig)
ch_Eds  = np.arange(0, nEd)
fp  = {'m':[], 'b':[]}  # [dictionary] fit parameters
for i_h in range(nEd):
    chi, chf    = ch_Eds[i_h]*20, (ch_Eds[i_h]+1)*20
    fname_inp   = '%s/H2D_%03d-%03d.MeV.txt' % (pa.dir_src, chi, chf)
    H	        = np.loadtxt(fname_inp) + 1e-7 #1.0
    H	= H.reshape(bins*bins)
    wei	= np.sqrt(H)		# factores de peso
    p	= np.polyfit(xvec, yvec, deg=1, w=wei, cov=True)

    m, b    = p[0][0], p[0][1]
    m_err   = p[1][0][0]

    print " i: %02d/%02d" % (i_h, 50)
    stuff   = []
    title   = "$E_d$: %02d-%02d MeV"  % (i_h*20, i_h*20+20.)
    stuff   += [[xlim, ylim]]
    stuff   += [[m, b]]
    stuff   += [[xvec, yvec]]
    if graphics:
        fname_fig = '%s/H2D_%03d-%03d.MeV' % (pa.dir_fig, chi, chf)
        ff.make_fitplot_muon(fname_inp, stuff, pa.cbmax, title, fname_fig=fname_fig)

    fp['m'] += [ m ]
    fp['b'] += [ b ]


if graphics:
    print "\n ---> figures finished!\n"

data_o = np.array([ch_Eds*20.+10., fp['m'], fp['b']]).T
HEADER = 'ini_date   : %s\n' % ini_date.strftime('%d/%b/%Y %H:%M') +\
         'end_date   : %s' % end_date.strftime('%d/%b/%Y %H:%M')
np.savetxt(pa.fname_out, data_o, fmt='%g', header=HEADER)
print " ---> output ascii: " + pa.fname_out + "\n"

if graphics:
    print " ---> output figs : " + pa.dir_fig + "\n"
else:
    print " ---> no figures produced. "
#EOF
