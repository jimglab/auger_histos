#!/bin/bash

DirRep=$HOME/auger.histos_aop   # repo dir
script=./gral.sh
YEARS=(2011 2012 2013 2014)

if [ ! -f $script ]; then
    echo -e " --> DOESN'T EXIST: "$script && exit 1
fi

for year in ${YEARS[@]}; do
    DirSrc="/scratch/datos/datos_auger/monit/sd/$year"
    DirDst="$HOME/_tmp_/intermediate/monit/$year"
    #--- check if input exists
    if [ ! -d $DirSrc ]; then
        echo -e " this source doesn't exist: \n "$DirSrc
    fi
    #echo " ok: " $DirSrc
    #--- execute c++ code
    $script $DirRep $year $DirSrc $DirDst
done
