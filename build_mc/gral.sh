#!/bin/bash
#
# Esto convierte los .root a .nc, filtrando
# solo la data q nos interesa.
# Los archivos q genera este script son necesarios para
# correr los scripts en "../build_ReducedHistos"!
#
# author: j.j. masias meza

shopt -s nullglob

# indentity
me=$(basename $0)

#--- grab args
[[ $# -eq 0 ]] && {
	# TODO: print help.
	echo -e "\n [-][$me] we need arguments! XD\n";
	exit 1;
}
while [[ $# -gt 0 ]]; do
	key="$1"
	case $key in
		-DateIni)
		dd_ini=$2
		mm_ini=$3
		yyyy_ini=$4
		shift # past argument
		shift; shift; shift # past values
		;;
		-DateEnd)
		dd_end=$2
		mm_end=$3
		yyyy_end=$4
		shift # past argument
		shift; shift; shift # past values
		;;
        -DirSrc)
        DirSrc="$2"     # read data from here (original .root)
        shift; shift
        ;;
        -DirDst)
        DirDst="$2"     # output dir
        shift; shift
        ;;
        -DirLog)
        DirLog="$2"     # to save run logs
        shift; shift
        ;;
		*)
		echo -e "\n [-] ERROR: bad argument in $me: $1\n"
		exit 1
	esac
done


#----------------------------------------------
exe=$HOME/CDAS/MoIO/v5r6/Linux-x86_64/readmc-scaler3.exe
#DirSrc=$3 #"/scratch/datos/datos_auger/monit/sd/$yyyy"
#DirDst=$4 #"/home/masiasmj/_tmp_/intermediate/monit/$yyyy"
#src_dir="$HOME/auger_ascii/monit/root/$yyyy"
#dst_dir="${this_dir}/out/out.build_mc.nc/$yyyy"
#dir_log=${this_dir}/logs/log.build_mc

mkdir -p $DirDst	# si no existe, esto corre x las puras!
[[ -f $exe ]] \
    || {
    echo -e " [-] $exe doesn't exist!\n";
    exit 1;
    }

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# number of UTC days associated to each date
usec_ini=$(date --date "${yyyy_ini}-${mm_ini}-${dd_ini} 00:00" -u +%s)
usec_end=$(date --date "${yyyy_end}-${mm_end}-${dd_end} 00:00" -u +%s)

udays_ini=$((${usec_ini}/86400))
udays_end=$((${usec_end}/86400))
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

udays=${udays_ini} # UTC days start at $udays_ini
while [[ ${udays} -le ${udays_end} ]]; do
    usec=$(($udays*86400)) # UTC days between ($udays_ini, $udays_end)
    dd=$(date --date "01 jan 1970 + $usec sec" -u +%d)
    mm=$(date --date "01 jan 1970 + $usec sec" -u +%m)
    yyyy=$(date --date "01 jan 1970 + $usec sec" -u +%Y)
    echo -e "\n [*][$me] date: $dd/$mm/$yyyy"

	fname_log=${DirLog}/mon_${yyyy}_${mm}_${dd}.log
	rm -f $fname_log

    FILES="${DirSrc}/${yyyy}/mc_${yyyy}_${mm}_${dd}_*.root"
    ok=0
    #---- reviso si existen archivos con este nombre
    for f in $FILES; do
        if [ -f $f ]; then ok=1; fi
    done
    #--------------------
    echo -e "\n [*] processing $yyyy/$mm/$dd:\n"
    if [ $ok == 1 ]; then
        ls -lhtr $FILES
        SubDirDst=$DirDst/$yyyy && mkdir -p $SubDirDst
        fname_out=${SubDirDst}/${yyyy}_${mm}_${dd}.nc
        $exe $fname_out $FILES >> $fname_log 2>&1 \
            || echo -e "\n    [*][$me] $(basename $exe) exited with status $?\n"
            # NOTE: we might have several days where data CAN'T be 
            # processed. The C++ code throws segmentation fault 
            # errors or others. For more info see the runlogs.
        # TODO: implement a handler for Keyboard Interrupt! Sometimes
        # we need to abort a long run from terminal!
    else
        echo -e "\n ###### MISSING DATA ######\n"
    fi

    udays=$(($udays+1))
done


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#for mm in $months; do
#	mm=`printf "%02d\n" $mm`
#	fname_log=${DirLog}/mon_${yyyy}_${mm}.log
#	rm -f $fname_log
#	for dd in $days; do
#		dd=`printf "%02d\n" $dd`
#		FILES="${src_dir}/mc_${yyyy}_${mm}_${dd}_*.root"
#		ok=0
#		#---- reviso si existen archivos con este nombre
#		for f in $FILES; do
#			if [ -f $f ]; then ok=1; fi
#		done
#		#--------------------
#        echo " procesando $yyyy/$mm/$dd:"
#		if [ $ok == 1 ]; then
#			#printf " procesando: %s\n" $FILES
#			ls -lhtr $FILES
#			fname_out=${dst_dir}/${yyyy}_${mm}_${dd}.nc
#			$exe $fname_out $FILES >> $fname_log 2>&1 
#                #|| {
#                #echo -e "\n [-][$me] ERROR: $(basename $exe) exited with status $?\n"
#                #}
#		else
#			#echo " procesando:"
#			echo " ###### MISSING DATA ######"
#		fi
#		echo
#		#--------------------
#	done
#done

echo -e "\n [+] output dir: $DirDst\n"

#EOF
