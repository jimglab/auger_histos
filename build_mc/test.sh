#!/bin/bash
#
# Esto convierte los .root a .nc, filtrando
# solo la data q nos interesa.
# Los archivos q genera este script son necesarios para
# correr los scripts en "../build_ReducedHistos"!
#
# author: j.j. masias meza

shopt -s nullglob
#----------------------------------------------
yyyy=2009
this_dir=$HOME/auger.histos_aop   # repo dir
exe=$HOME/CDAS/MoIO/v5r6/Linux-x86_64/readmc-scaler3.exe
src_dir="/scratch/datos/datos_auger/monit/sd/$yyyy"
dst_dir="/home/masiasmj/_tmp_/intermediate/monit/$yyyy"
#src_dir="$HOME/auger_ascii/monit/root/$yyyy"
#dst_dir="${this_dir}/out/out.build_mc.nc/$yyyy"
dir_log=${this_dir}/logs/log.build_mc

mkdir -p $dst_dir	# si no existe, esto corre x las puras!

months=$(seq 1 1 12)
days=$(seq 1 1 31)
for mm in $months; do
	mm=`printf "%02d\n" $mm`
	fname_log=${dir_log}/mon_${yyyy}_${mm}.log
	rm -f $fname_log
	for dd in $days; do
		dd=`printf "%02d\n" $dd`
		FILES="${src_dir}/mc_${yyyy}_${mm}_${dd}_*.root"
		ok=0
		#---- reviso si existen archivos con este nombre
		for f in $FILES; do
			if [ -f $f ]; then ok=1; fi
		done
		#--------------------
        echo " procesando $yyyy/$mm/$dd:"
		if [ $ok == 1 ]; then
			#printf " procesando: %s\n" $FILES
			ls -lhtr $FILES
			fname_out=${dst_dir}/${yyyy}_${mm}_${dd}.nc
			$exe $fname_out $FILES >> $fname_log 2>&1
		else
			#echo " procesando:"
			echo " ###### MISSING DATA ######"
		fi
		echo
		#--------------------
	done
done
echo " ---> generamos data en: " $dst_dir
#EOF
