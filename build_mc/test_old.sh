#!/bin/bash
# Se corre asi,
# por ej: ./recupera_monits.sh 2011 1 6
# Eso indica el año 2011, desde Enero hasta Junio.
#
dir_code=`printf "$HOME/CDAS/MoIO/v5r6/Linux-x86_64/"`
#cd $dir_code
exe=$dir_code/readmc-scaler3.exe

YYYY=$1
MES_i=$2                 # desde Enero
MES_f=$3                # hasta Diciembre

dir_src=`printf "/scratch/datos/datos_auger/monit/sd/%04d/" $YYYY`
dir_dst=./ #`printf "$HOME/auger_ascii/monit/scaler3/%04d/" $YYYY`
mkdir $dir_dst

for MM in $(seq $MES_i 1 $MES_f)
do
	file_src=`printf "%smc_%04d_%02d_*.root" $dir_src $YYYY $MM`					# .root
    file_dst=`printf "%smc_scaler3_%04d_%02d.dat" $dir_dst $YYYY $MM`
    $exe $file_src > $file_dst
    wait
	echo $YYYY", "$MM
done
#
##
