## data reduction && BZ2 --> HDF5

Take the original `.bz2` scaler files, and make HDF5 versions with coarser time-resolution (5min).
Process one year at a time (given as command-line argument); the runtime is ~(11-20) hours.

Run (runtime ~12hs running 8 years in parallel):
```bash
./massive_generation_scalers.py -- \
    -inp /media/scratch2/auger/monit/sd \
    -out /media/scratch1/auger/scl.build_scals \
    -y 2015
```

The output of this data goes as input for [make_AoPHist2d.py](../scl.build_AoPcorr/make_AoPHist2d.py) and [massive_AoP_correc_mpi.py](../scl.build_AoPcorr/massive_AoP_correc_mpi.py). See the other [README.md](../scl.build_AoPcorr/README.md).

<!--- EOF -->
