#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
import recupera_y_reduce_scaler as rr
import numpy as np
import os, sys
from os.path import isfile, isdir
from h5py import File as h5

"""
Los argumentos q le paso a este script se guardan en 'sys.argv'
Para verlos, puedes:
print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)
print sys.argv[1]
"""

YYYY    = int(sys.argv[1])                      # el 1er y unico argumento es el anio
cf      = 0                                     # contador de archivos leidos
DIR_SRC = "/scratch/datos/datos_auger/monit/sd"
DIR_DST = "/home/masiasmj/auger_ascii/data_scalers_/%04d" % YYYY
wred    = 300 # [seg] ancho de reduccion (para reducir los .bz2)

os.system('mkdir -p '+DIR_DST)

for MM in range(1, 12+1):                                         # los 12 meses
    fileout         = "%s/scals_%04d_%02d.h5" % (DIR_DST, YYYY, MM)
    data_avrg_acc   = np.linspace(1,2,1)                    # re-inicializo la matriz de este mes

    for DD in range(1, 32):                                 # prueba con 31 dias en c/mes
        filename        = '%s/%04d/scaler_%04d_%02d_%02d_00h00.dat.bz2' % (DIR_SRC , YYYY, YYYY, MM, DD)
        if isfile(filename):
            print("\n > leyendo: %s" % filename)
            (n_data, data_avrg_tmp) = rr.reducir_dia(filename, wred)
            if(n_data > 0):
                data_avrg_acc           = rr.poner_en_cola(data_avrg_acc, data_avrg_tmp)
                cf                      = cf + 1

    print("\n > hemos leido %d archivos!" % cf)
    print(" > escribiendo en archivo '%s'" % fileout)
    #np.savetxt(fileout, data_avrg_acc, fmt='%12.2f')        # genero un archivo por c/mes
    fo = h5(fileout, 'w')
    fo['time'] = data_avrg_acc[:,0]             # [gps seconds]
    fo['ntanks'] = data_avrg_acc[:,1]           # average number of tanks whithin 'wred' seconds.
    fo['avr_total_cnt'] = data_avrg_acc[:,2]    # average total number of counts in the entire array, within 'wred' seconds.

    nblks, ncol = data_avrg_acc[:,3:].shape # shape de la data
    for i in range(4, ncol+4):
        id = i - 2
        dpath = 'scl_%04d' % id
        fo[dpath] = data_avrg_acc[:,1+id]
    fo.close()

#EOF
