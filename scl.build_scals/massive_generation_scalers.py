#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
import recupera_y_reduce_scaler as rr
import numpy as np
import os, sys, argparse
from os.path import isfile, isdir
from h5py import File as h5
import shared_funcs.funcs as sf
from datetime import datetime, timedelta

#--- retrieve args
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
'-inp', '--dir_inp', 
type=str, 
default='/scratch/datos/datos_auger/monit/sd',
help='input directory',
)
parser.add_argument(
'-out', '--dir_out', 
type=str, 
default="/home/masiasmj/auger_ascii/data_scalers_ii",
help='output directory',
)
parser.add_argument(
'-ini', '--ini_date', 
type=str, 
action=sf.arg_to_utcsec,
default='01/01/2006',#[2006,1,1],
help='initial date',
)
parser.add_argument(
'-end', '--end_date', 
type=str, 
action=sf.arg_to_utcsec,
default='31/12/2015',#[2006,1,1],
help='end date',
)
pa = parser.parse_args()


cf      = 0   # contador de archivos leidos
wred    = 300 # [seg] ancho de reduccion (para reducir los .bz2)

os.system('mkdir -p '+pa.dir_out)
OneDay   = timedelta(days=1)
date_ini = pa.ini_date
date_end = pa.end_date
# all dates to process
dates = [date_ini+timedelta(days=n) \
    for n in range((pa.end_date-pa.ini_date).days+1)]

#--- monthly buffer
# NOTE: it has to be float64 because the 1st column will be GPS-seconds, so
# we need the full mantissa to be accurate at seconds level.
mbuff = sf.My2DArray((1,2001), dtype=np.float64)

for date in dates:
    yyyy,mm,dd = date.year, date.month, date.day
    fname_inp = '%s/%04d/scaler_%04d_%02d_%02d_00h00.dat.bz2' % (pa.dir_inp, yyyy, yyyy, mm, dd)
    if not isfile(fname_inp): # skip to next `date`
        continue 

    print "\n > reading: %s"%fname_inp
    n_data, data_avr_tmp = rr.reducir_dia(fname_inp, wred)
    if n_data==0: # skip to next `date`
        continue

    mbuff[mbuff.nrow:,:] = data_avr_tmp
    cf += 1 # counter of input files successfully read

    # Save data of this month if this is the last day of 
    # the month or if this is the last `date`.
    if (date+OneDay).month!=date.month or date==dates[-1]:
        fname_out = "%s/scals_%04d_%02d.h5" % (pa.dir_out, yyyy, mm)
        print "\n [*] saving to file:\n     %s\n"%fname_out
        #--- filter-out garbage data, using My2DArray() implementations
        mbuff = mbuff[...]
        #--- save to file
        fo = h5(fname_out, 'w')
        fo['time']          = mbuff[:,0] # [gps seconds]
        fo['ntanks']        = mbuff[:,1] # average number of tanks whithin 'wred' seconds.
        fo['avr_total_cnt'] = mbuff[:,2] # average total number of counts in the entire array, within 'wred' seconds.
        nblks, ncol = mbuff[:,3:].shape 
        for i in range(4, ncol+4):
            id = i - 2
            dpath = 'scl_%04d' % id
            fo[dpath] = mbuff[:,1+id]
        fo.close()
        #--- restart buffer
        mbuff = sf.My2DArray((1,2001), dtype=np.float64)


print "\n [+] we've read %d files in total!" % cf
print "\n [+] output:\n %s" % pa.dir_out

#EOF
