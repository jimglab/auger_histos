#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
from numpy import *
import numpy as np
import bz2
#

#line = f.readline()
# en line.split() es un string q contiene la 1ra linea del archivo
# Nota. hay q hacer varias veces "line =..." para leer todo el archivo
#print line.split()[0]

#-----------------------function para colocar la matriz 'arr' debajo de 'arr_acc'
def poner_en_cola(arr_acc, arr):
    if (arr_acc.size == 1 and arr.size > 0):        # si aun no tiene elementos, lo "inauguro"
        arr_acc = arr 
    if (arr_acc.size > 1  and arr.size > 0):        # si ya tiene elementos, le agrego
        arr_acc = np.concatenate([arr_acc, arr], 0) # coloco 'arr' debajo de 'arr_acc'
    arr_acc = np.array(arr_acc, dtype=np.float32) # just reasonable precision
    return arr_acc


def reducir_dia(fname_inp, wndw=300):
    """ 
    inputs:
    fname_inp   : archivo inputs .bz2 
    wndw        : hallare promed en ventanas de 300seg (5min) (*)
    """
    FILE    = bz2.BZ2File(fname_inp, 'r')
    ny      = 2001          # number of columns, according to general format
    nx      = 86400         # max number of entries the file can have
    #vec    = [[0 for x in xrange(ny)] for x in xrange(nx)]
    data    = np.zeros((nx, ny))
    i       = 0             # counter of rows in the file

    for i in range(nx):
        try:                        # si no surgen errores, q lea
            line    = FILE.readline()
            vec_tmp = map(float, line.split());
            if(np.size(vec_tmp)==ny): # register, only if this a line with 2001 fields
                data[i,:] = vec_tmp
            if(i%5000==0): print("  line: %d" % i)
            #if(i>66000): break # pa q me lea un pedacito nomas :)
        except:
            break                   # if there's an error, stop reading this file

    n_data  = i                     # total rows registered in this file

    #--- pos-processing
    # (filtros y colocar nan's)
    data_msk = ma.masked_where((data<1300.) | (data>2600.), data)

    print(" > corrigo el mascareo de los tiempos...")
    # corregir el mascareo de los tiempos:
    # pongo False en la columna 0 de 'vec_mask.mask' xq pertenece a los tiempos, siempre y cuando tiempos!=0
    for i in range(0, nx):
        if(data[i][0] != 0):                # mientras el tiempo se != 0
            data_msk.mask[i][0] = False     # tiempo [gps secs]
            data_msk.mask[i][1] = False     # nro de estaciones funcionando
            data_msk.mask[i][2] = False     # nro total de pulsos del array


    # coloco nan's q corresponde al "filtro"
    data[where(ma.getmask(data_msk)==True)] = np.nan

    #---------------------------------
    # una vez construido la data cruda y habiendo filtrado la data-basura y reemplazado con nan's,

    n_blks      = n_data / wndw         # how many blocks of 'wndw' I have in 'n_data'
    data_avrg   = np.zeros((n_blks, ny))

    for i in range(n_blks):
        blk         = data[i*wndw:(i+1)*wndw, :]
        blk         = np.ma.masked_array(blk, np.isnan(blk))
        blk_avrg    = np.mean(blk, axis=0)
        blk_avrg[where(ma.getmask(blk_avrg)==True)] = np.nan    # los q resultaron ser promedios de todos nan, q sean nan tmb
        data_avrg[i]    = blk_avrg

    print(" [+] reduced file: %s\n" % fname_inp)
    #import pdb; pdb.set_trace()

    return (n_data, data_avrg)
"""
(*) como la data .bz2 no es uniforme (a veces faltan algunos segundos), las ventanas "fisicas" no seran de 300seg siempre. A veces seran de menos, a veces de mas. Lo q *si* es cierto es q agarra bloques de *300 datos* q casi siempre corresponden a 300seg.
"""
#EOF
