#!/usr/bin/env ipython
# -*- coding: utf-8 -*- 
from pylab import find, pause, figure, savefig, close
from datetime import datetime, time, timedelta
import numpy as np
import console_colors as ccl
from scipy.io.netcdf import netcdf_file
#from ShiftTimes import ShiftCorrection, ShiftDts
import os
import matplotlib.patches as patches
import matplotlib.transforms as transforms
import h5py
from h5py import File as h5
from numpy import (
    mean, median, nanmean, nanmedian, std, nan, 
    isnan, min, max, zeros, ones, size, loadtxt
)
from os.path import isfile, isdir

#from read_NewTable import tshck, tini_icme, tend_icme, tini_mc, tend_mc, n_icmes, MCsig
#from z_expansion_gulisano import z as z_exp

def flags2nan(VAR, FLAG):
        cond            = VAR < FLAG
        VAR             = np.array(VAR)
        VAR[~cond]      = np.nan
        return VAR


def date_to_utc(fecha):
    utc = datetime(1970, 1, 1, 0, 0, 0, 0)
    sec_utc = (fecha - utc).total_seconds()
    return sec_utc


def dates_from_omni(t):
    time = []
    n = len(t)
    for i in range(n):
        yyyy = t[i][0]
        mm = t[i][1]
        dd = t[i][2]
        HH = t[i][3]
        MM = t[i][4]
        SS = t[i][5]
        uSS = t[i][6]
        time += [datetime(yyyy, mm, dd, HH, MM, SS, uSS)]

    return time


def utc_from_omni(file):
    t = np.array(file.variables['time'].data)
    dates = dates_from_omni(t)
    n = len(dates)
    time = np.zeros(n)
    for i in range(n):
        time[i] = date_to_utc(dates[i])

    return time

def selecc_data(data, tshk):
    time    = data[0]       #[s] utc sec
    rate    = data[1]

    day      = 86400.        # [seg]
    utc      = datetime(1970, 1, 1, 0, 0, 0, 0)
    tshk_utc = (tshk - utc).total_seconds()

    ti      = tshk_utc - 10.*day        # [seg] utc
    tf      = tshk_utc + 30.*day
    cond    = (time > ti) & (time < tf)

    time    = (time[cond] - tshk_utc) / day     # [days] since shock
    rate    = rate[cond]
    return (time, rate)

def selecc_window(data, tini, tend):
    time    = data[0]       #[s] utc sec
    y       = data[1]

    day         = 86400.                # [seg]
    utc         = datetime(1970, 1, 1, 0, 0, 0, 0)
    tini_utc    = (tini - utc).total_seconds()      # [s] utc sec
    tend_utc    = (tend - utc).total_seconds()      # [s] utc sec

    ti      = tini_utc              # [seg] utc
    tf      = tend_utc
    cond    = (time > ti) & (time < tf)

    time    = (time[cond] - tini_utc) / day     # [days] since 'ti'
    y       = y[cond]
    return (time, y)


def enoughdata(var, fgap):
    n       = len(var)
    ngood   = len(find(~isnan(var)))
    fdata   = 1.*ngood/n        # fraccion de data sin gaps
    if fdata>=(1.-fgap):
        return True
    else:
        return False


def averages_and_std(n_icmes, t_shck, ti_icme, dTday, nbin, t_utc, VAR, fgap):
        day = 86400.
        nok=0; nbad=0
        adap = []
        for i in range(n_icmes):
            dT      = (ti_icme[i] - t_shck[i]).total_seconds()/day  # [day]
            if dT>dTday:
                dt      = dT/nbin
                t, var  = selecc_window(
                                [t_utc, VAR],
                                t_shck[i], ti_icme[i]
                                )
                if enoughdata(var, fgap):           # pido q haya mas del 80% NO sean gaps
                    adap    += [adaptar(nbin, dt, t, var)]
                    nok     +=1
                else:
                    continue
            else:
                print " i:%d ---> Este evento es muy chico!, dT/day:%g" % (i, dT)
                nbad +=1

        VAR_adap = zeros(nbin*nok).reshape(nok, nbin)
        for i in range(nok):
                VAR_adap[i,:] = adap[i][1]

        VAR_avrg = zeros(nbin)
        VAR_std = zeros(nbin)
        ndata = zeros(nbin)
        for i in range(nbin):
            cond = ~isnan(VAR_adap.T[i,:])
            ndata[i] = len(find(cond))      # nro de datos != flag
            VAR_avrg[i] = mean(VAR_adap.T[i,cond])  # promedio entre los valores q no tienen flag
            VAR_std[i] = std(VAR_adap.T[i,cond])    # std del mismo conjunto de datos

        tnorm   = adap[0][0]
        return [nok, nbad, tnorm, VAR_avrg, VAR_std, ndata]

def adaptar(n, dt, t, r):
    #n  = int(5./dt)        # nro de puntos en todo el intervalo de ploteo
    tt  = zeros(n)
    rr  = zeros(n)
    for i in range(n):
        tmin    = i*dt
        tmax    = (i+1.)*dt
        cond    = (t>tmin) & (t<tmax)
        tt[i]   = mean(t[cond])
        rr[i]   = mean(r[cond])
    return [tt/(n*dt), rr]


def adaptar(nwndw, dT, n, dt, t, r):
    #n  = int(5./dt)        # nro de puntos en todo el intervalo de ploteo
    tt  = zeros(n)
    rr  = zeros(n)
    _nbin_  = n/(1+nwndw[0]+nwndw[1])   # nro de bins en la sheath
    for i in range(n):
        tmin    = (i-nwndw[0]*_nbin_)*dt
        tmax    = tmin + dt
        cond    = (t>tmin) & (t<tmax)
        tt[i]   = mean(t[cond])#; print "tt:", t[i]; pause(1)
        rr[i]   = mean(r[cond])
    return [tt/dT, rr]          # tiempo normalizado x la duracion de la sheath


def adaptar_ii(nwndw, dT, n, dt, t, r, fgap):
    tt      = zeros(n)
    rr      = zeros(n)
    _nbin_  = n/(1+nwndw[0]+nwndw[1])   # nro de bins en la sheath/mc
    cc      = (t>0.) & (t<dT)           # intervalo de la sheath/mc
    #print " r[cc]: ", r[cc]
    if len(r[cc])==0:           # no hay data en esta ventana
        rr      = nan*ones(n)
        enough  = False
    else:
        enough  = enoughdata(r[cc], fgap)   # [bool] True si hay mas del 80% de data buena.

    if not(enough): 
        rr  = nan*ones(n)   # si no hay suficiente data, este evento no aporta

    for i in range(n):
        tmin    = (i-nwndw[0]*_nbin_)*dt
        tmax    = tmin + dt
        cond    = (t>=tmin) & (t<=tmax)
        #tt[i]   = mean(t[cond])#; print "tt:", t[i]; pause(1) # bug
        tt[i]   = tmin + .5*dt          # bug corregido
        if enough:
            #cc    = ~isnan(r[cond])    # no olvidemos filtrar los gaps
            #rr[i] = mean(r[cond][cc])
            rr[i] = nanmean(r[cond])

    return enough, [tt/dT, rr]          # tiempo normalizado x la duracion de la sheath/mc/etc


def selecc_window_ii(nwndw, data, tini, tend):
    time = data[0]       #[s] utc sec
    y    = data[1]

    day     = 86400.                # [seg]
    utc     = datetime(1970, 1, 1, 0, 0, 0, 0)
    tini_utc = (tini - utc).total_seconds()  # [s] utc sec
    tend_utc = (tend - utc).total_seconds()  # [s] utc sec

    dt   = tend_utc - tini_utc
    ti   = tini_utc - nwndw[0]*dt            # [seg] utc
    tf   = tend_utc + nwndw[1]*dt
    cond = (time > ti) & (time < tf)

    time = (time[cond] - tini_utc) / day     # [days] since 'ti'
    y    = y[cond]
    return (time, y)


def averages_and_std_ii(nwndw,
        SELECC, #MCsig, MCwant,
        n_icmes, tini, tend, dTday, nbin, t_utc, VAR):
        day = 86400.
        nok=0; nbad=0
        adap = []
        for i in range(n_icmes):
            dT      = (tend[i] - tini[i]).total_seconds()/day  # [day]
            if ((dT>dTday) & SELECC[i]):# (MCsig[i]>=MCwant)):
                dt      = dT*(1+nwndw[0]+nwndw[1])/nbin
                t, var  = selecc_window_ii(
                            nwndw,              # nro de veces hacia atras y adelante
                            [t_utc, VAR],
                            tini[i], tend[i]
                            )
                adap    += [adaptar(nwndw, dT, nbin, dt, t, var)]       # rebinea usando 'dt' como el ancho de nuevo bineo
                nok     +=1
            else:
                print " i:%d ---> Filtramos este evento!, dT/day:%g" % (i, dT)
                nbad +=1

        VAR_adap = zeros(nbin*nok).reshape(nok, nbin)
        for i in range(nok):
            VAR_adap[i,:] = adap[i][1]

        VAR_avrg = zeros(nbin)
        VAR_medi = zeros(nbin)
        VAR_std  = zeros(nbin)
        ndata    = zeros(nbin)
        for i in range(nbin):
            cond = ~isnan(VAR_adap.T[i,:])
            ndata[i] = len(find(cond))      # nro de datos != flag
            VAR_avrg[i] = mean(VAR_adap.T[i,cond])  # promedio entre los valores q no tienen flag
            VAR_medi[i] = median(VAR_adap.T[i,cond])# mediana entre los valores q no tienen flag
            VAR_std[i] = std(VAR_adap.T[i,cond])    # std del mismo conjunto de datos

        tnorm   = adap[0][0]
        return [nok, nbad, tnorm, VAR_avrg, VAR_medi, VAR_std, ndata]


def mvs_for_each_event(VAR_adap, nbin, nwndw, Enough):
    nok         = size(VAR_adap, axis=0)
    mvs         = zeros(nok)            # valores medios por cada evento
    binsPerTimeUnit = nbin/(1+nwndw[0]+nwndw[1])    # nro de bines por u. de tiempo
    start       = nwndw[0]*binsPerTimeUnit  # en este bin empieza la estructura (MC o sheath)
    for i in range(nok):
        aux = VAR_adap[i, start:start+binsPerTimeUnit]  # (*)
        cc  = ~isnan(aux)                   # pick good-data only
        #if len(find(cc))>1:
        if Enough[i]:       # solo imprimo los q tienen *suficiente data*
            print ccl.G
            print "id %d/%d: "%(i+1, nok), aux[cc]
            print ccl.W
            mvs[i] = mean(aux[cc])
        else:
            mvs[i] = nan
    #(*): esta es la serie temporal (de esta variable) para el evento "i"
    pause(1)
    return mvs


def diff_dates(tend, tini):
    n       = len(tend)
    diffs   = np.nan*np.ones(n)
    for i in range(n):
        ok  = type(tend[i]) == type(tini[i]) == datetime    # ambos deben ser fechas!
        if ok:
            diffs[i] = (tend[i] - tini[i]).total_seconds()
        else:
            diffs[i] = np.nan

    return diffs    #[sec]


def write_variable(fout, varname, dims, var, datatype, comments):
    dummy           = fout.createVariable(varname, datatype, dims)
    dummy[:]        = var
    dummy.units     = comments


def calc_beta(Temp, Pcc, B):
    # Agarramos la definicion de OMNI, de:
    # http://omniweb.gsfc.nasa.gov/ftpbrowser/magnetopause/Reference.html
    # http://pamela.roma2.infn.it/index.php
    # Beta = [(4.16*10**-5 * Tp) + 5.34] * Np/B**2 (B in nT)
    #
    beta = ((4.16*10**-5 * Temp) + 5.34) * Pcc/B**2
    return beta


def thetacond(ThetaThres, ThetaSh):
    if ThetaThres<=0.:
        print ccl.Rn + ' ----> BAD WANG FILTER!!: ThetaThres<=0.'
        print ' ----> Saliendo...' + ccl.Rn
        raise SystemExit
        #return ones(len(ThetaSh), dtype=bool)
    else:
        return (ThetaSh > ThetaThres)


def wangflag(ThetaThres):
    if ThetaThres<0:
        return 'NaN'
    else:
        return str(ThetaThres)


def makefig(medVAR, avrVAR, stdVAR, nVAR, tnorm,
        SUBTITLE, YLIMS, YLAB, fname_fig):
    fig     = figure(1, figsize=(13, 6))
    ax      = fig.add_subplot(111)

    ax.plot(tnorm, avrVAR, 'o-', color='black', markersize=5, label='mean')
    ax.plot(tnorm, medVAR, 'o-', color='red', alpha=.5, markersize=5, markeredgecolor='none', label='median')
    inf     = avrVAR + stdVAR/np.sqrt(nVAR)
    sup     = avrVAR - stdVAR/np.sqrt(nVAR)
    ax.fill_between(tnorm, inf, sup, facecolor='gray', alpha=0.5)
    trans   = transforms.blended_transform_factory(
                ax.transData, ax.transAxes)
    rect1   = patches.Rectangle((0., 0.), width=1.0, height=1,
                transform=trans, color='blue',
                alpha=0.3)
    ax.add_patch(rect1)

    ax.legend(loc='upper right')
    ax.grid()
    ax.set_ylim(YLIMS)
    TITLE = SUBTITLE
    ax.set_title(TITLE)
    ax.set_xlabel('time normalized to MC passage time [1]', fontsize=14)
    ax.set_ylabel(YLAB, fontsize=20)
    savefig(fname_fig, format='png', dpi=180, bbox_inches='tight')
    close()


#--- chekea q el archivo no repita elementos de la 1ra columna
def check_redundancy(fname, name):
    f = open(fname, 'r')
    dummy   = {}
    for line in f:
        ll          = line.split(' ')
        varname     = ll[0]
        dummy[varname] = 0

    dummy_names = dummy.keys()
    dummy_set   = set(dummy_names)
    redundancy  = len(dummy_set)<len(dummy_names)

    overwriting = name in dummy_set

    if redundancy or overwriting:
        return True
    else:
        return False


class general:
    def __init__(self):
        self.name = 'name'


class boundaries:
    pass

def read_hsts_data(fname, typic, ch_Eds):
    """
    code adapted from ...ch_Eds_smoo2.py
    """
    f   = h5(fname, 'r')

    # initial date
    datestr = f['date_ini'].value
    yyyy, mm, dd = map(int, datestr.split('-'))
    INI_DATE = datetime(yyyy, mm, dd)

    # final date
    datestr = f['date_end'].value
    yyyy, mm, dd = map(int, datestr.split('-'))
    END_DATE = datetime(yyyy, mm, dd)

    date = INI_DATE
    tt, rr = [], []
    ntot, nt = 0, 0
    while date < END_DATE:
        yyyy, mm, dd = date.year, date.month, date.day
        path    = '%04d/%02d/%02d' % (yyyy, mm, dd)
        try:
            dummy = f[path] # test if this exists!
        except:
            date    += timedelta(days=1)    # next day...
            continue

        ntanks  = f['%s/tanks'%path][...]
        cc  = ntanks>150.
        ncc = len(find(cc))

        if ncc>1: #mas de un dato tiene >150 tanques
            time    = f['%s/t_utc'%path][...] # utc secs
            cts, typ = np.zeros(96, dtype=np.float64), 0.0
            for i in ch_Eds:
                Ed  =  i*20.+10.
                cts += f['%s/cts_temp-corr_%04dMeV'%(path,Ed)][...]
                typ += typic[i] # escalar

            cts_norm = cts/typ
            #aux  = np.nanmean(cts_norm[cc])
            tt += [ time[cc] ]
            rr += [ cts_norm[cc] ]
            ntot += 1 # files read ok
            nt += ncc # total nmbr ok elements

        date    += timedelta(days=1)        # next day...

    #--- converting tt, rr to 1D-numpy.arrays
    t, r = nans(nt), nans(nt)
    ini, end = 0, 0
    for i in range(ntot):
        ni = len(tt[i])
        t[ini:ini+ni] = tt[i]
        r[ini:ini+ni] = rr[i]
        ini += ni

    f.close()
    return t, r



def nans(sh):
    return np.nan*np.ones(sh)



class events_mgr:
    def __init__(self, gral, bd):
        self.data_name  = gral.data_name
        self.bd         = bd
        #self.dir_plots  = gral.dirs['dir_plots']
        #self.dir_ascii  = gral.dirs['dir_ascii']
        self.gral       = gral
        self._dirs_     = gral.dirs

        #self.f_events   = netcdf_file(gral.fnames['table_richardson'], 'r')
        print " -------> archivos input leidos!"

        #--- put False to all possible data-flags (all CR detector-names must be included in 'self.CR_observs')
        self.names_ok   = ('Auger_BandMuons', 'Auger_BandScals',  'Auger_scals', 'McMurdo', 'ACE', 'ACE_o7o6')
        for name in self.names_ok:
            read_flag   = 'read_'+self.data_name
            setattr(self, read_flag, False) # True: if files are already read

        #--- names of CR observatories
        self.CR_observs = ( #debe *incluir* a los metodos 'load_data_..()'
            'Auger_scals', 'Auger_BandMuons', 'Auger_BandScals',\
            'McMurdo')

        #--- just a check for load_data_.. methods
        for att_name in dir(events_mgr): # iterate on all methods
            if att_name.startswith('load_data_'):
                att_suffix = att_name.replace('load_data_', '')
                assert att_suffix in self.names_ok,\
                    " ---> uno de los metodos '%s' no esta tomando en cuenta en 'self.CR_observs' (%s) " % (att_name, att_suffix)

        self.data_name_ = str(self.data_name) # nombre de la data input inicial (*1)
        self.IDs_locked = False      # (*2)
        """
        (*1):   si despues cambia 'self.data_name', me voy a dar
                cuenta en la "linea" FLAG_001.
        (*2):   lock in lock_IDs().
                True: if the id's of the events have been
                fixed/locked, so that later analysis is
                resctricted only with theses locked id's.
        """


    def run_all(self):
        #----- seleccion de eventos
        #self.filter_events()
        #print "\n ---> filtrado de eventos (n:%d): OK\n" % (self.n_SELECC)
        #----- load data y los shiftimes "omni"
        print " ---> cargando data..."
        self.load_files_and_timeshift_ii()
        #----- rebineo y promedios
        #self.rebine()
        #self.rebine_final()
        #----- hacer ploteos
        #self.make_plots()
        #----- archivos "stuff"
        #self.build_params_file()


    """
    collects data from filtered events
    """
    def collect_data(self):
        """
        rebineo de c/evento
        """
        nvars   = self.nvars #len(VARS)
        n_icmes = self.tb.n_icmes
        bd      = self.bd
        VARS    = self.VARS
        nbin    = self.nBin['total']
        nwndw   = [self.nBin['before'], self.nBin['after']]
        day     = 86400.

        #---- quiero una lista de los eventos-id q van a incluirse en c/promedio :-)
        IDs     = {}
        Enough  = {}
        nEnough = {}
        self.__ADAP__       = ADAP    = []   # conjunto de varios 'adap' (uno x c/variable)
        for varname in VARS.keys():
            IDs[varname]        = []
            Enough[varname]     = []
            nEnough[varname]    = 0     # contador

        # recorremos los eventos:
        nok=0; nbad=0;
        nnn     = 0     # nro de evento q pasan el filtro a-priori
        self.out = {}
        self.out['events_data'] = {} # bag to save data from events
        for i in range(n_icmes):
            ok=False
            try: #no todos los elementos de 'tend' son fechas (algunos eventos no tienen fecha definida)
                dT  = (bd.tend[i] - bd.tini[i]).total_seconds()/day  # [day]
                # this 'i' event must be contained in our data
                ok  =  date_to_utc(bd.tini[i]) >= self.t_utc[0] #True
                ok  &= date_to_utc(bd.tend[i]) <= self.t_utc[-1]
            except:
                continue    # saltar al sgte evento 'i'

            ADAP += [ {} ] # agrego un diccionario a la lista
            #np.set_printoptions(4)         # nro de digitos a imprimir cuando use numpy.arrays
            if (ok & self.SELECC[i]):# (MCsig[i]>=MCwant)):  ---FILTRO--- (*1)
                nnn += 1
                print ccl.Gn + " id:%d ---> dT/day:%g" % (i, dT) + ccl.W
                print self.tb.tshck[i]
                nok +=1
                evdata = self.out['events_data']['id_%03d'%i] = {} # evdata is just a pointer
                # recorremos las variables:
                for varname in VARS.keys():
                    dt      = dT*(1+nwndw[0]+nwndw[1])/nbin
                    t, var  = selecc_window_ii(
                        nwndw=nwndw, #rango ploteo
                        data=[self.t_utc, VARS[varname]['value']],
                        tini=bd.tini[i],
                        tend=bd.tend[i]
                    )
                    evdata['t_days'] = t
                    evdata[varname] = var

                    if self.data_name in self.CR_observs:   # is it CR data?
                        rate_pre = getattr(self, 'rate_pre_'+self.data_name)
                        var = 100.*(var - rate_pre[i]) / rate_pre[i]

                    # rebinea usando 'dt' como el ancho de nuevo bineo
                    out       = adaptar_ii(nwndw, dT, nbin, dt, t, var, self.fgap)
                    enough    = out[0]       # True: data con menos de 100*'fgap'% de gap
                    Enough[varname]         += [ enough ]
                    ADAP[nok-1][varname]    = out[1]  # donde: out[1] = [tiempo, variable]

                    if enough:
                        IDs[varname]     += [i]
                        nEnough[varname] += 1

            else:
                print ccl.Rn + " id:%d ---> dT/day:%g" % (i, dT),\
                      " (SELECC: ", self.SELECC[i], ')' + ccl.W
                nbad +=1

        print " ----> len.ADAP: %d" % len(ADAP)
        self.__nok__    = nok
        self.__nbad__   = nbad
        #self.out = {}
        self.out['nok']     = nok
        self.out['nbad']    = nbad
        self.out['IDs']     = IDs
        self.out['nEnough'] = nEnough
        self.out['Enough']  = Enough


    # def rebine(self):


    def lock_IDs(self):
        """
        This assumes that 'IDs' has only *one* key.
        That is, len(IDs)=1 !!
        """
        IDs = self.out['IDs']
        varname = IDs.keys()[0]
        self.restricted_IDs = IDs[varname]
        self.IDs_locked = True

        #dummy = np.array(self.restricted_IDs)
        #np.savetxt('./__dummy__', dummy)


    # def rebine_final(self):


    def __getattr__(self, attname):
        if attname[:10]=='load_data_':
            return self.attname


    def get_events_data(self):
        self.out = {
            'events_data': [] #{}
        }
        # ini: left border
        # end: right border
        self.nev = 0 # nro of event
        for ini, end in zip(self.bd.tini, self.bd.tend):
            self.get_event_data(ini, end)
            self.nev += 1


    def get_event_data(self, ini, end):
        nwndw  = [2., 2.] # my-window-times before, and after
        i      = self.nev # event index
        VARS   = self.VARS
        evdata = self.out['events_data']
        self.out['events_data'] += [ {} ] # evdata is just a pointer
        # recorremos las variables:
        for varname in VARS.keys():
            t, var  = selecc_window_ii(
                nwndw = nwndw, #rango ploteo
                data  = [self.t_utc, VARS[varname]['value']],
                tini  = ini,
                tend  = end,
            )
            """--- en principio, no tengo rate_pre para cualquier ventana
            if self.data_name in self.CR_observs:   # is it CR data?
                rate_pre = getattr(self, 'rate_pre_'+self.data_name)
                var = 100.*(var - rate_pre[i]) / rate_pre[i]
            """

            evdata[i]['t_days'] = t
            evdata[i][varname] = var


    def save2file(self):
        evdata = self.out['events_data']
        ndata  = len(self.VARS.keys())
        for i in range(self.nev):
            t = evdata[i]['t_days']
            nt = size(t)
            data_out = nans((nt, 1+ndata)) # allocate memory block
            data_out[:,0] = t
            for name, j in zip(self.VARS.keys(), range(ndata)):
                data_out[:,j+1] = evdata[i][name]

            ini = self.bd.tini[i]
            yyyy, mm, dd    = ini.year, ini.month, ini.day
            HH, MM          = ini.hour, ini.minute
            fname_out = '%s/wndw_%s_%04d-%02d-%02d_%02dh:%02dm.txt' % (self._dirs_['dir_out'], self.data_name, yyyy, mm, dd, HH, MM)
            np.savetxt(fname_out, data_out, fmt='%g')


    def load_files_and_timeshift_ii(self):
        read_flag = 'read_'+self.data_name # e.g. self.read_Auger
        """
        if not(read_flag in self.__dict__.keys()): # do i know u?
             setattr(self, read_flag, False) #True: if files are already read
        """
        #--- read data and mark flag as read!
        if not( getattr(self, read_flag) ):
            attname = 'load_data_'+self.data_name
            getattr(self, attname)()    # call method 'load_data_...()'
            self.read_flag = True       # True: ya lei los archivos input
            #setattr(self, read_flag, True) # True: if files are already read

        #--- check weird case
        if not(self.data_name in self.names_ok):
            print " --------> BAD 'self.data_name'!!!"
            for name in self.names_ok:
                read_flag = getattr(self, 'read_'+self.data_name)
                print " ---> self.read_%s: " % name, read_flag

            print " exiting.... "
            raise SystemExit


    # def load_data_ACE_o7o6(self):


    def load_data_Auger_scals(self):
        """
        solo cargamos Auger Scalers
        """
        tb          = self.tb
        nBin        = self.nBin
        bd          = self.bd
        day         = 86400.
        fname_inp   = self.gral.fnames[self.data_name]
        f5          = h5py.File(fname_inp, 'r')
        self.t_utc  = t_utc = f5['auger/time_seg_utc'][...].copy() #data_murdo[:,0]
        CRs         = f5['auger/sc_wAoP_wPres'][...].copy() #data_murdo[:,1]
        print " -------> variables leidas!"

        self.VARS   = VARS = {} #[]
        VARS['CRs.'+self.data_name] = {
            'value' : CRs,
            'lims'  : [-1.0, 1.0],
            'label' : 'Auger Scaler rate [%]'
        }
        self.nvars  = len(VARS.keys())
        #---------
        self.aux = aux = {}
        aux['SELECC']    = self.SELECC


    def load_data_Auger_BandMuons(self):
        """
        para leer la data de histogramas Auger
        """
        fname_inp   = self.gral.fnames[self.data_name]
        f5          = h5py.File(fname_inp, 'r')
        ch_Eds      = (10, 11, 12, 13)

        fname_avr   = self.gral.fnames[self.data_name+'_avrs']#average histos
        prom        = loadtxt(fname_avr)
        typic       = nanmean(prom[:,1:], axis=0)#agarro el promedio del array de los 8 anios!
        self.t_utc, CRs = read_hsts_data(fname_inp,  typic, ch_Eds)
        print " -------> variables leidas!"

        self.VARS   = VARS = {} #[]
        VARS['CRs.'+self.data_name] = {
            'value' : CRs,
            'lims'  : [-1.0, 1.0],
            'label' : 'Auger (muon band) [%]'
        }
        self.nvars  = len(VARS.keys())
        #---------
        self.aux = aux = {}
        aux['SELECC']    = self.SELECC


    def load_data_Auger_BandScals(self):
        """
        para leer la data de histogramas Auger, banda scalers
        """
        tb          = self.tb
        nBin        = self.nBin
        bd          = self.bd
        day         = 86400.
        fname_inp   = self.gral.fnames[self.data_name]
        f5          = h5py.File(fname_inp, 'r')
        ch_Eds      = (3, 4, 5)

        fname_avr   = self.gral.fnames[self.data_name+'_avrs']#average histos
        prom        = loadtxt(fname_avr)
        typic       = nanmean(prom[:,1:], axis=0)#agarro el promedio del array de los 8 anios!
        self.t_utc, CRs = read_hsts_data(fname_inp,  typic, ch_Eds)
        print " -------> variables leidas!"

        self.VARS   = VARS = {} #[]
        VARS['CRs.'+self.data_name] = {
            'value' : CRs,
            'lims'  : [-1.0, 1.0],
            'label' : 'Auger (muon band) [%]'
        }
        self.nvars  = len(VARS.keys())
        #---------
        self.aux = aux = {}
        aux['SELECC']    = self.SELECC


    def load_data_McMurdo(self):
        tb          = self.tb
        nBin        = self.nBin
        bd          = self.bd
        day         = 86400.
        fname_inp   = self.gral.fnames[self.data_name]
        data_murdo  = np.loadtxt(fname_inp)
        self.t_utc  = t_utc = data_murdo[:,0]
        CRs         = data_murdo[:,1]
        print " -------> variables leidas!"

        self.VARS   = VARS = {} 
        VARS['CRs.'+self.data_name] = {
            'value' : CRs,
            'lims'  : [-8.0, 1.0],
            'label' : 'mcmurdo rate [%]'
        }
        self.nvars  = len(VARS.keys())
        #---------

        self.aux = aux = {}
        aux['SELECC']    = self.SELECC


    # def load_data_ACE(self):

    # def make_plots(self):

    # def build_params_file(self):

    # def filter_events(self):


#+++++++++++++++++++++++++++++++++
if __name__=='__main__':
    print " ---> this is a library!\n"

#EOF
