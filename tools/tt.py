#!/usr/bin/env ipython
#from pylab import *
#from numpy import *
from datetime import datetime, time, timedelta
#------------ shared libraries:
"""
--- antes de modificar cosas, tener en cuenta los bugs en: 
'../../shared_lib/COMENTARIOS.txt' 

IMPORTANT:
    - Note that 'structure' argument refers to MC, sheath, ICME,
      sheath-of-icme, taking the following possible values:
      'i'       : ICME
      'mc'      : MC
      'sh.i'    : sheath of ICME
      'sh.mc'   : sheath of MC,

      and 'events_mgr.filter_events()' uses this flag to know which 
      average values it will use to filter events.
"""
import sys
from shared_funcs import * #c_funcs import *
#------------------------------
#from read_NewTable import tshck, tini_icme, tend_icme, tini_mc, tend_mc, n_icmes, MCsig
#from ShiftTimes import *
import numpy as np
#from z_expansion_gulisano import z as z_exp
import console_colors as ccl
#import read_NewTable as tb


HOME                = os.environ['HOME']
PAO                 = os.environ['PAO']
PAO_PROCESS         = os.environ['PAO_PROCESS']
gral                = general()
#---- cosas input
gral.fnames = fnames = {}
#fnames['Auger_scals']     = '%s/data_auger/estudios_AoP/data/unir_con_presion/data_final_2006-2013.h5' % PAO
fnames['Auger_BandMuons'] = '%s/data_auger/data_histogramas/all.array.avrs/temp.corrected/shape.ok_and_3pmt.ok/15min/test_temp.corrected.nc' % PAO
fnames['Auger_BandMuons_avrs'] = '%s/long_trends/code_figs/avr_histos_press_shape.ok_and_3pmt.ok.txt' % PAO_PROCESS  # average histogram
fnames['Auger_BandScals'] = fnames['Auger_BandMuons']
fnames['Auger_BandScals_avrs'] = fnames['Auger_BandMuons_avrs']

#fnames['table_richardson']  = '%s/ASOC_ICME-FD/icmes_richardson/data/rich_events_ace.nc' % HOME
for name in fnames.keys():
    assert isfile(fnames[name]),\
        " --> NO EXISTE: " + fnames[name]

#---- directorios de salida
gral.dirs =  dirs   = {}
#dirs['dir_plots']   = '../plots'
#dirs['dir_ascii']   = '../ascii'
dirs['dir_out']     = './out'
#dirs['suffix']      = '_auger_'    # sufijo para el directorio donde guardare

#--- bordes de estructura
bounds      = boundaries()
#bounds.tini = tb.tshck      #tb.tini_mc #tb.tshck 
#bounds.tend = tb.tini_icme    #tb.tend_mc #tb.tini_mc
bounds.tini = [
    datetime(2011, 2, 17, 0, 0, 0),
    datetime(2012, 3, 8, 0, 0, 0)
]
bounds.tend = [
    datetime(2011, 2, 24, 0, 0, 0), 
    datetime(2012, 3, 16, 0, 0, 0)
]

#++++++++++++++++++++++++++++++++++++++++++++++++ Auger Scalers
gral.data_name = 'Auger_BandMuons'
em = events_mgr(gral, bounds)
em.load_files_and_timeshift_ii()
em.get_events_data()
em.save2file()

em.data_name = 'Auger_BandScals'
em.load_files_and_timeshift_ii()
em.get_events_data()
em.save2file()

#emgr.run_all()
