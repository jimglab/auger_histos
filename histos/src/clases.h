#define JUN2013 1 //comment to de-activate the correct code for version>=23
#include "header.h"

class oper{
	public:
		oper(void){};
		oper(int, char **);
		int argc;
		char **argv;
		EventPos *pos;
		void operate_event(AugerIoSd*);
		int nb;
		int errors[10];
		int id;
		Doub HistoInMev[MAXINMEV];
		Doub range[MAXBIN];
		Doub mev, vemq, vemp, qm, QM, base, low, hig;
		int bin, ti, tf;
		size_t ji, jf;
		bool count_errors(int, int);
		Doub empk, anti;
		void find_em_peak_and_dip();
		int x12, x11, x02, x01, fl4, fl3, fl2, fl1, i, j;
		Doub s1;
		//Doub histInt100, histInt150, rate1, rate2, rate3, rate4, ratew1, ratew2;
		Doub rate1, rate2, ratew1, ratew2;
		Doub ve, va;
		void build_histos(int, int);
		TEcEvent *event;
		TH1F *histoq;
		void generate_diagnostics();
		void report();
		MatDoub M;
		//void out_to_screen(int, int);
		int nhistos;
		int ncols, nrows_new;
		void resize();
		void out_to_array(int, int, int);
		int nEd;		// nro de bines integrados (*)
		int nchE;		// nro total de bines de energias q van al 'M[][]'
		int fpmt;		// flag de estado de los PMT (debe ser 7,15,23,31,etc para q los 3 esten ok)
		// (*): se usa en save
};


oper::oper(int argcc, char **argvv){
	argc	= argcc;
	argv 	= argvv;
	// params para las dimensiones de 'M[][]'
	nEd	    = 10;	// nro de bines integrados (=10 para la ICRC)
	nchE	= (MAXINMEV-1)/nEd;	// para nEd=10, (MAXINMEV-1)/nEd=100.
	ncols 	= 13 + nchE;		// nro total de columnas para 'M[][]'
	int init_nrows = 9e3;		// nro de filas inicial para M[][]
	M.resize(init_nrows, ncols);	// inicializo con algun tamanio
	cerr << " rows: "<< M.nrows() << endl; 
	cerr << " cols: "<< M.ncols() << endl; 
	//printf(" hhheeeeeyyyyyy.....\n");
	//cout << argv[0]<< endl;
	//cout << argv[1]<< endl;
}


bool oper::count_errors(int k, int l){
	/*if(r[id][l]) { 
	  errors[3]++;
	  continue;
	}*/
	if (event->fCalibStations[k].fPmt[l].fCalibratedState < 3) {
	  errors[4]++;
	  return 1;
	  //continue;
	}
	ti = (int) event->fCalibStations[k].Calib->StartSecond;
	tf = (int) event->fCalibStations[k].Calib->EndSecond;

    Doub static delta_t;
    if (event->fCalibStations[k].Calib->Version < 23){
        delta_t = tf - ti;
    }
    else{ 
        /* mail de Bertou, 30.apr.2015:
         * el nuevo campo "EndSecond" contiene el valor siguiente:
         * 256*256*256*(EndSecond-StartSecond) + 256*256*ToTd + 256*MoPs + ToT
         * Asi que tenes que "bitshiftear" de 24 bits, y te queda la diferencia.
         * Lo tenes que hacer si la version de calibracion es 23 o mas. */
        #ifdef JUN2013
        delta_t = int(tf/pow(2,24)); //(tf >> 24);
        #else
        delta_t = tf - ti;
        #endif
    }

	//if ((tf - ti) != 61)  {
	if (delta_t != 61)  {
	  errors[5]++;
	  return 1;
	}

	vemq = event->fCalibStations[k].fPmt[l].VemCharge();
	vemp = event->fCalibStations[k].fPmt[l].VemPeak();
	if (vemq > MAXINCHG)  {
	  errors[6]++;
	  return 1;
	  //continue;
	}
	mev = vemq / VEM2MEV;
	//TH1F *histoq = event->fCalibStations[k].HCharge(l);
	histoq = event->fCalibStations[k].HCharge(l);
	if (histoq->GetMaximum() < 500)  {
	  errors[7]++;
	  return 1;
	  //continue;
	}
	return 0;
	//cerr << " 333333" << endl;
}



void oper::build_histos(int k, int l){
        for (unsigned int j = 0; j < MAXINMEV; j++) HistoInMev[j] = 0.0;

        base = event->fCalibStations[k].Calib->Base[l];
        if (event->fCalibStations[k].Calib->Version == 13) base *= 19.0;     // 19 bins in version 13
        else base *= 20.0;
        bin = histoq->GetNbinsX();
        for (int i = 0; i < (bin - 1); i++) 
          range[i + 1] = (histoq->GetBinCenter(i + 1) - histoq->GetBinCenter(i)) / 2.0 + histoq->GetBinCenter(i) - base;
        range[0] = histoq->GetBinCenter(0) - base - (histoq->GetBinCenter(1) - histoq->GetBinCenter(0)) / 2.0;
        range[bin] = histoq->GetBinCenter(bin - 1) - base + (histoq->GetBinCenter(bin - 1) - histoq->GetBinCenter(bin - 2)) / 2.0;
        gsl_histogram *h = gsl_histogram_alloc(bin);
        gsl_histogram_set_ranges(h, range, bin + 1);
        for (int i = 0; i < bin ; i++)
          gsl_histogram_accumulate(h, histoq->GetBinCenter(i) - base, histoq->GetBinContent(i));
        // gsl_histogram_fprintf(stderr, h, "%g", "%g");
        bool cond;
        for (int i = 0; i < MAXINMEV; i++) {
          HistoInMev[i] = 0.0;
          qm = mev * i;
          QM = qm + mev;

          #ifdef JUN2013
          cond = (QM >= range[bin]) || (qm<range[0]);
          #else
          cond = (QM >= range[bin]);
          #endif
          //if (QM >= range[bin]) break;
          if (cond) break;

          gsl_histogram_find(h, qm, &ji);
          gsl_histogram_find(h, QM, &jf);
          for (int j = (int) ji; j <= (int) jf; j++) {
            gsl_histogram_get_range(h, j, &low, &hig); 
            HistoInMev[i] += gsl_histogram_get(h, (size_t) j) * (hig - low);
          }
          gsl_histogram_get_range(h, ji, &low, &hig);
          HistoInMev[i] -= (qm - low) * gsl_histogram_get(h, ji);
          gsl_histogram_get_range(h, jf, &low, &hig);
          HistoInMev[i] -= (hig - QM) * gsl_histogram_get(h, jf);
          HistoInMev[i] /= 61.0;
        }
}


void oper::find_em_peak_and_dip(){
        empk = 0.0;
        for (int i = 0; i <= 120; i++) if (HistoInMev[i] > empk) empk = HistoInMev[i];
        empk *= 0.75;

        anti = 10000.0;
        for (int i = 120; i <= 180; i++) if (HistoInMev[i] < anti) anti = HistoInMev[i];
        anti *= 1.25;
}


void oper::generate_diagnostics(){
        x12 = x11 = x02 = x01 = fl4 = fl3 = fl2 = fl1 = i = j = 0;
        s1 = 0.0;
        //histInt100 = histInt150 = rate1 = rate2 = rate3 = rate4 = ratew1 = ratew2 = 0.0;
        rate1 = rate2 = ratew1 = ratew2 = 0.0;

        for (i = 0; i < MAXINMEV; i++) {
          //if (i >= 100) histInt100 += HistoInMev[i];
          //if (i >= 150) histInt150 += HistoInMev[i];
          //if (i >= 220 && i <= 260) rate3 += HistoInMev[i];
          //if (i >= 320 && i <= 340) rate4 += HistoInMev[i];
          if (i >= 340 && i <= 380) ratew1 += HistoInMev[i];
          if (i >= 580 && i <= 620) ratew2 += HistoInMev[i];
          if (i > 9 && i < 240 && !fl4) { // parabolic fit...
            if (HistoInMev[i] > empk && !fl1) {
              for (j = i - 10; j < i; j++) s1 += sign(HistoInMev[i] - HistoInMev[j]);
              if (s1 > 4) {
                fl1 = 1; x01 = i; s1 = 0.0;
              }
            }
            if ( HistoInMev[i] < empk && fl1 && !fl2) {
              for (j = i - 10; j < i; j++) s1 += sign(HistoInMev[i] - HistoInMev[j]);
              if (s1 < -4) {
                fl2 = 1; x02 = i; s1 = 0.0;
              }
            }
            if ( HistoInMev[i] < anti && !fl3) {
              for (j = i - 10; j < i; j++) s1 += sign(HistoInMev[i] - HistoInMev[j]);
              if (s1 < -4) {
                fl3 = 1; x11 = i; s1 = 0.0;
              }
            }
            if ( HistoInMev[i] > anti && fl3 && !fl4) {
              for (j = i - 10; j < i; j++) s1 += sign(HistoInMev[i] - HistoInMev[j]);
              if (s1 > 4) {
                fl4 = 1; x12 = i; s1 = 0.0;
              }
            }
          }
        }
        ve = fitHisto(x01, x02, HistoInMev, &rate1);
        va = fitHisto(x11, x12, HistoInMev, &rate2);
}


void oper::operate_event(AugerIoSd *inp){
	nb = (int) inp->LastEvent();
	for (int i = 0; i<10; i++) errors[i] = 0;

	//####################################################### LOOKING EVENTS IN ROOT
	for(EventPos pos = inp->FirstEvent(); pos < inp->LastEvent(); pos = inp->NextEvent()) {
		if (!(pos%100)) { cerr << pos << "/" << nb << endl;}
		event = new TEcEvent(pos);
		mev = vemq = vemp = qm = QM = base = low = hig = 0.0;
		bin = ti = tf = 0;
		ji = jf = (size_t) 0;

		errors[9]++;
		for (unsigned int k = 0; k < event->fCalibStations.size(); k++) {
			id = event->fCalibStations[k].Id;	// id del tanque
			if(event->fCalibStations[k].Error) {
				errors[1]++;
				continue;
			}
			errors[0]++;
			if(event->fCalibStations[k].fSigInVEM < 0) {
				errors[2]++;
				continue;
			}

			// Flag, si los 3 PMTs estan ok (debe ser 7, 15, 23, 31, etc)
			fpmt = (event->fCalibStations[k].Calib->TubeMask & 0x7);
			//fpmt = (event->fCalibStations[k].PMQuality->TubeMask & 0x7); // version mejorada de Isabelle

			// charge to mev
			for(unsigned int l = 0; l < kIoSd::NPMT; l++) {
				if(count_errors(k, l)) continue;
				build_histos(k, l);
				find_em_peak_and_dip();	//electromagnetic peak & dip
				generate_diagnostics();	// 

				errors[8]++;
				nhistos = errors[8];

				if(M.nrows() <= nhistos) {
					//getchar();
					resize();
				}
				//out_to_screen(k, l);
				out_to_array(nhistos-1, k, l);


			}
		}
		//##################################################### FINAL ACTIONS...
		delete event;
	}
	/*for(int i=0; i<10; i++)
		cerr << M[nhistos-1][i] << endl;*/
}


void oper::resize(){
	printf(" resizing --> ");
	nrows_new = 2*M.nrows();
	MatDoub aux(M);
	M.resize(nrows_new, ncols);
	printf(" (%d, %d)\n", M.nrows(), M.ncols());
	for(int i=0; i<aux.nrows(); i++)
		for(int j=0; j<aux.ncols(); j++)
			M[i][j] = aux[i][j];
	aux.resize(2, 2);			// "liberar" memoria
}


void oper::out_to_array(int h, int k, int l){
	int j=0;
	M[h][j++] = ti + GPS2UTC + 30;             		// 0 tiempo
	M[h][j++] = id;                                  	// 1 Station Id
	M[h][j++] = l;                                          // 2 PMT
	M[h][j++] = vemq/vemp;                                  // 3 (Vem Charge)/(Vem Peak)
	M[h][j++] = fpmt;					// 4 flag TubeMask (debe ser 7,15,23,31,etc para q los 3 PMTs esten ok)
	M[h][j++] = rate1;                                      // 5 rate1
	M[h][j++] = rate2;                                      // 6 rate2
	M[h][j++] = ratew1;                                     // 7 Water level 1
	M[h][j++] = ratew2;                                     // 8 Water level 2
	M[h][j++] = ve;                                         // 9 Pos of 1st max
	M[h][j++] = va;                                         // 10 Pos of 1st min (**)
	M[h][j++] = (x02 - x01);                                // 11 Width 1st max
	M[h][j++] = (x12 - x11);                                // 12 Width 1st min (**)

        for (int i = 0; i < nchE; i++){			  	// para nEd=10, nchE=100.
		M[h][j] = 0.0;
		for(int k=0; k<nEd; k++)
			M[h][j] += HistoInMev[i*nEd + k + 1]; 	// 13->112 [cts/10MeV] HistoInMev (**)
		j++;
	}
}
// (**): Notar q estoy excluyendo el bin Ed=0MeV.
// Solo estoy recorriendo desde 1MeV hasta 100MeV; siendo Mil cananales en
// total los q tomo en cuenta para asignarle a 'M[h][j]'.

/*
void oper::out_to_screen(int k, int l){
        cout 
          << ti + GPS2UTC + 30 << " "                                    // 1 tiempo
          << event->Id << " "                                            // 2 Event Id
          << id << " "                                                   // 3 Station Id
          //<< z[id] << " "                                                // 4 tank altitude
          << l << " "                                                    // 5 PMT
          << vemq << " "                                                 // 6 Vem Charge
          << vemp << " "                                                 // 7 Vem Peak
          << event->fCalibStations[k].Calib->Version << " "              // 8 Calib Version
          << histInt100 << " "                                           // 9 int_100^\infty
          << histInt150 << " "                                           // 10 int_150^\infty
          << rate1 << " "                                                // 11 rate1
          << rate2 << " "                                                // 12 rate2
          << rate3 << " "                                                // 13 rate3
          << rate4 << " "                                                // 14 rate4
          << ratew1 << " "                                               // 15 Water level 1
          << ratew2 << " "                                               // 16 Water level 2
          << ve << " "                                                   // 17 Pos of 1st max
          << va << " "                                                   // 18 Pos of 1st min
          << (x02 - x01) << " "                                          // 19 Width 1st max
          << (x12 - x11) << " "                                          // 20 Width 1st min
          ;
        for (int i = 0; i < MAXINMEV; i++) cout << HistoInMev[i] << " "; // 21->1021 HistoInMev
        cout << endl;
}
*/

void oper::report(){
  //####################################################### LOOKING EVENTS IN ROOT
    cerr << "In " << errors[9] << " events we found " << errors[0] << " histograms." << endl;
    cerr << "Station error : " << errors[1] << " (" << 100.*errors[1]/errors[0] << " \%)" << endl;
    cerr << "No SigInVem() : " << errors[2] << " (" << 100.*errors[2]/errors[0] << " \%)" << endl;
    cerr << "Raining PMT   : " << errors[3] << " (" << 100.*errors[3]/errors[0] << " \%)" << endl;
    cerr << "PMT Failure   : " << errors[4] << " (" << 100.*errors[4]/errors[0] << " \%)" << endl;
    cerr << "Bad Time Cal  : " << errors[5] << " (" << 100.*errors[5]/errors[0] << " \%)" << endl;
    cerr << "Bad VemQ      : " << errors[6] << " (" << 100.*errors[6]/errors[0] << " \%)" << endl;
    cerr << "Too noisy     : " << errors[7] << " (" << 100.*errors[7]/errors[0] << " \%)" << endl;
    cerr << "Good histos   : " << errors[8] << " (" << 100.*errors[8]/errors[0] << " \%)" << endl;
}
