/*****************************************************************
 * histos v0r5                                                   *
 * Requirements:         CDAS v4r8                               *
 * previous release:     histos v0r4                             *
 * based on:             event-dump-03.cpp && pds.cpp v0r3       *
 * begin:                09/Oct/2008                             *
 * by:                   Hernan G. Asorey (2008)                 *
 * email:                asoreyh at gmail.com                    *
 *****************************************************************/
/* History
 *
 * v0r0 - 28/05/08
 * First release
 *
 * v0r1 - 03/06/08
 * 1) New search algorithm for HistInChg[i]. it is 3 times faster than v0r0
 * 2) New column distribution
 * 3) Calculate integrated histogram 100-1000 MeV
 * 4) Calculate integrated histogram 150-1000 MeV
 * 5) Calculate ratio to determine muon hump position
 * 6) Calculate ratio to determine water level 
 * 7) Show vem charge and veam peak
 *
 * v0r2 - 12/06/08
 * Array included
 *
 * v0r3 - 18/06/08
 * using GSL for rebining histograms
 *
 * v0r4 - 09/10/08
 * including new computations
 * 
 * v0r5 - 16 02 2012
 * estimating error rates
 *
 */

//******************************preprocessor
#include <iostream>
#include <signal.h>
#include <IoAuger.h>
#include "STCconstants.h"
#include <TMath.h>
#include "math.h"
#include "TCdasUtil.h"
#include "Ec.h"
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_multifit.h>
#include <netcdf.h>
#include <netcdfcpp.h>
#include "my_typedefs.h"

#define PRES 3
#define MAXINMEV (1001)
#define MAXINCHG (240.0)
#define VEM2MEV (240.0)
#define MAXBIN (2048)
#define GPS2UTC (315964800)
#define kNbTankMax (2000)
#define kArrayFileName "../crunch/array.xyz"
#define kRainPMTFileName "../crunch/rainPMT.txt"

static void my_error_handler (const char *reason, const char *file, int line, int err);
static int status = 0;

inline double sign(double x) {
  return (x < 0.0 ? -1.0 : 1.0);
}

double fitHisto(int xi, int xf, double* h, double* r) {
  double y1, x1, x2, x3, x4, xy, x2y, x, y, v1, v2, v;
  v = v1 = v2 = x = y = y1 = x1 = x2 = x3 = x4 = xy = x2y = 0.0;
  int n = 0;
  for (int i = xi; i <= xf; i++) {
    x = i; y = h[i];
    x1  += x;
    y1  += y;
    xy  += x * y;
    x *= i;
    x2  += x;
    x2y += x * y;
    x *= i;
    x3  += x;
    x *= i;
    x4  += x;
    n++;
  }
  v1 = x1 * x2 * x2y - n * x2y * x3 - x2 * x2 * xy + n * x4 * xy + x2 * x3 * y1 - x1 * x4 * y1;
  v2 = x1 * x1 * x2y - n * x2 * x2y + n * x3 * xy + x2 * x2 * y1 - x1 * (x2 * xy + x3 * y1);
  v = v1 / (2.0 * v2); // note v1 = -a1 and v2 = a2, so we have v = -b / 2a !
  *r = y1;
  return v;
}
//#######################################################
/* This is the name of the data file we will create. */
#define FILE_NAME "test_float.nc"

/* We are writing 2D data, a 6 x 12 lat-lon grid. We will need two
 * netCDF dimensions. */
#define NDIMS 2
#define NLAT 6
#define NLON 12
#define LAT_NAME "latitude"
#define LON_NAME "longitude"

/* Names of things. */
#define TEMP_NAME "temperature"
#define UNITS "units"

/* These are used to construct some example data. */
#define SAMPLE_TEMP 9.0

/* Handle errors by printing an error message and exiting with a
 * non-zero status. */
#define ERR(e) {printf("Error: %s\n", nc_strerror(e)); return 2;}

