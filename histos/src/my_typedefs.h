//##########################################################################
template <class T>
class NRmatrix {
private:
        int nn;
        int mm;
        T **v;
public:
        NRmatrix();
        NRmatrix(int n, int m);                 // Zero-based array
        NRmatrix(int n, int m, const T &a);     //Initialize to constant
        NRmatrix(int n, int m, const T *a);     // Initialize to array
        NRmatrix(const NRmatrix &rhs);          // Copy constructor
        NRmatrix & operator=(const NRmatrix &rhs);      //assignment
        typedef T value_type; // make T available externally
        inline T* operator[](const int i);      //subscripting: pointer to row i
        inline const T* operator[](const int i) const;
        inline int nrows() const;
        inline int ncols() const;
        void resize(int newn, int newm); // resize (contents not preserved)
        void assign(int newn, int newm, const T &a); // resize and assign a constant value
        ~NRmatrix();
};

template <class T>
NRmatrix<T>::NRmatrix() : nn(0), mm(0), v(NULL) {}

template <class T>
NRmatrix<T>::NRmatrix(int n, int m) : nn(n), mm(m), v(n>0 ? new T*[n] : NULL)
{
        int i,nel=m*n;
        if (v) v[0] = nel>0 ? new T[nel] : NULL;
        for (i=1;i<n;i++) v[i] = v[i-1] + m;
}

template <class T>
NRmatrix<T>::NRmatrix(int n, int m, const T &a) : nn(n), mm(m), v(n>0 ? new T*[n] : NULL)
{
        int i,j,nel=m*n;
        if (v) v[0] = nel>0 ? new T[nel] : NULL;
        for (i=1; i< n; i++) v[i] = v[i-1] + m;
        for (i=0; i< n; i++) for (j=0; j<m; j++) v[i][j] = a;
}

template <class T>
NRmatrix<T>::NRmatrix(int n, int m, const T *a) : nn(n), mm(m), v(n>0 ? new T*[n] : NULL)
{
        int i,j,nel=m*n;
        if (v) v[0] = nel>0 ? new T[nel] : NULL;
        for (i=1; i< n; i++) v[i] = v[i-1] + m;
        for (i=0; i< n; i++) for (j=0; j<m; j++) v[i][j] = *a++;
}

template <class T>
NRmatrix<T>::NRmatrix(const NRmatrix &rhs) : nn(rhs.nn), mm(rhs.mm), v(nn>0 ? new T*[nn] : NULL)
{
        int i,j,nel=mm*nn;
        if (v) v[0] = nel>0 ? new T[nel] : NULL;
        for (i=1; i< nn; i++) v[i] = v[i-1] + mm;
        for (i=0; i< nn; i++) for (j=0; j<mm; j++) v[i][j] = rhs[i][j];
}

template <class T>
NRmatrix<T> & NRmatrix<T>::operator=(const NRmatrix<T> &rhs)
// postcondition: normal assignment via copying has been performed;
//              if matrix and rhs were different sizes, matrix
//              has been resized to match the size of rhs
{
        if (this != &rhs) {
                int i,j,nel;
                if (nn != rhs.nn || mm != rhs.mm) {
                        if (v != NULL) {
                                delete[] (v[0]);
                                delete[] (v);
                        }
                        nn=rhs.nn;
                        mm=rhs.mm;
                        v = nn>0 ? new T*[nn] : NULL;
                        nel = mm*nn;
                        if (v) v[0] = nel>0 ? new T[nel] : NULL;
                        for (i=1; i< nn; i++) v[i] = v[i-1] + mm;
                }
                for (i=0; i< nn; i++) for (j=0; j<mm; j++) v[i][j] = rhs[i][j];
        }
        return *this;
}

template <class T>
inline T* NRmatrix<T>::operator[](const int i)  //subscripting: pointer to row i
{
#ifdef _CHECKBOUNDS_
if (i<0 || i>=nn) {
        throw("NRmatrix subscript out of bounds");
}
#endif
        return v[i];
}

template <class T>
inline const T* NRmatrix<T>::operator[](const int i) const
{
#ifdef _CHECKBOUNDS_
if (i<0 || i>=nn) {
        throw("NRmatrix subscript out of bounds");
}
#endif
        return v[i];
}

template <class T>
inline int NRmatrix<T>::nrows() const
{
        return nn;
}

template <class T>
inline int NRmatrix<T>::ncols() const
{
        return mm;
}

template <class T>
void NRmatrix<T>::resize(int newn, int newm)
{
        int i,nel;
        if (newn != nn || newm != mm) {
                if (v != NULL) {
                        delete[] (v[0]);
                        delete[] (v);
                }
                nn = newn;
                mm = newm;
                v = nn>0 ? new T*[nn] : NULL;
                nel = mm*nn;
                if (v) v[0] = nel>0 ? new T[nel] : NULL;
                for (i=1; i< nn; i++) v[i] = v[i-1] + mm;
        }
}

template <class T>
void NRmatrix<T>::assign(int newn, int newm, const T& a)
{
        int i,j,nel;
        if (newn != nn || newm != mm) {
                if (v != NULL) {
                        delete[] (v[0]);
                        delete[] (v);
                }
                nn = newn;
                mm = newm;
                v = nn>0 ? new T*[nn] : NULL;
                nel = mm*nn;
                if (v) v[0] = nel>0 ? new T[nel] : NULL;
                for (i=1; i< nn; i++) v[i] = v[i-1] + mm;
        }
        for (i=0; i< nn; i++) for (j=0; j<mm; j++) v[i][j] = a;
}

template <class T>
NRmatrix<T>::~NRmatrix()
{
        if (v != NULL) {
                delete[] (v[0]);
                delete[] (v);
        }
}
//
//##########################################################################################
//##########################################################################################
//##########################################################################################
//
template <class T>
class NRvector {
private:
        int nn; // size of array. upper index is nn-1
        T *v;
public:
        NRvector();
        explicit NRvector(int n);               // Zero-based array
        NRvector(int n, const T &a);    //initialize to constant value
        NRvector(int n, const T *a);    // Initialize to array
        NRvector(const NRvector &rhs);  // Copy constructor
        NRvector & operator=(const NRvector &rhs);      //assignment
        typedef T value_type; // make T available externally
        inline T & operator[](const int i);     //i'th element
        inline const T & operator[](const int i) const;
        inline int size() const;
        void resize(int newn); // resize (contents not preserved)
        void assign(int newn, const T &a); // resize and assign a constant value
        ~NRvector();
};

// NRvector definitions

template <class T>
NRvector<T>::NRvector() : nn(0), v(NULL) {}

template <class T>
NRvector<T>::NRvector(int n) : nn(n), v(n>0 ? new T[n] : NULL) {}

template <class T>
NRvector<T>::NRvector(int n, const T& a) : nn(n), v(n>0 ? new T[n] : NULL)
{
        for(int i=0; i<n; i++) v[i] = a;
}

template <class T>
NRvector<T>::NRvector(int n, const T *a) : nn(n), v(n>0 ? new T[n] : NULL)
{
        for(int i=0; i<n; i++) v[i] = *a++;
}

template <class T>
NRvector<T>::NRvector(const NRvector<T> &rhs) : nn(rhs.nn), v(nn>0 ? new T[nn] : NULL)
{
        for(int i=0; i<nn; i++) v[i] = rhs[i];
}

template <class T>
NRvector<T> & NRvector<T>::operator=(const NRvector<T> &rhs)
// postcondition: normal assignment via copying has been performed;
//              if vector and rhs were different sizes, vector
//              has been resized to match the size of rhs
{
        if (this != &rhs)
        {
                if (nn != rhs.nn) {
                        if (v != NULL) delete [] (v);
                        nn=rhs.nn;
                        v= nn>0 ? new T[nn] : NULL;
                }
                for (int i=0; i<nn; i++)
                        v[i]=rhs[i];
        }
        return *this;
}

template <class T>
inline T & NRvector<T>::operator[](const int i) //subscripting
{
#ifdef _CHECKBOUNDS_
if (i<0 || i>=nn) {
        throw("NRvector subscript out of bounds");
}
#endif
        return v[i];
}

template <class T>
inline const T & NRvector<T>::operator[](const int i) const     //subscripting
{
#ifdef _CHECKBOUNDS_
if (i<0 || i>=nn) {
        throw("NRvector subscript out of bounds");
}
#endif
        return v[i];
}

template <class T>
inline int NRvector<T>::size() const
{
        return nn;
}

template <class T>
void NRvector<T>::resize(int newn)
{
        if (newn != nn) {
                if (v != NULL) delete[] (v);
                nn = newn;
                v = nn > 0 ? new T[nn] : NULL;
        }
}

template <class T>
void NRvector<T>::assign(int newn, const T& a)
{
        if (newn != nn) {
                if (v != NULL) delete[] (v);
                nn = newn;
                v = nn > 0 ? new T[nn] : NULL;
        }
        for (int i=0;i<nn;i++) v[i] = a;
}

template <class T>
NRvector<T>::~NRvector()
{
        if (v != NULL) delete[] (v);
}

// end of NRvector definitions
//##################################################
typedef double Doub;
typedef NRvector<Doub> VecDoub;
typedef NRmatrix<Doub> MatDoub;
//##################################################
//
