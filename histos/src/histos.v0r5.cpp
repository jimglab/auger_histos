#include "clases.h"

// Return this to OS if there is a failure.
static const int NC_ERR = 2;
//#######################################################
int main (int argc, char *argv[]) {
	if (argc<3) {
		cout<<"histos v0r4\n";
		cout<<"\nUso: " << argv[0] << " fname_out.nc <input.root>\n" << endl;
		return 1;
	}
	char *fname_out;
	fname_out = argv[1];
	gsl_set_error_handler (&my_error_handler);
	cerr << " guardando en: "<< fname_out << endl;

	//############################################## PROGRAM INITIALIZATION
	//#################################### INPUTS/OUTPUTS
	//StdOUT StdERR
	cout.setf(ios::fixed); cout.precision(PRES);

	//Input/Output objects (files)
	oper opp(argc, argv);
	/* 
	 * "input()" recibe:
	 * argc-2: nro de files .root
	 * argv+2: direccion de memoria del 1er .root
	 */
	AugerIoSd input(argc-2, argv+2);
	opp.operate_event(&input);
	opp.report();
  
	//#####################################################################
	//################################# OPEN FILE 'fname_out' IN WRITE-MODE
	// NOTA: despues q proceso el .root (arriba), escribe
	// 	 el archivo .nc. Por lo q si cancelas la
	// 	 ejecucion durante el procesamiento, no sobre-escribes
	// 	 el .nc ya existente (en caso ya existiera).
	//
	NcError err(NcError::silent_nonfatal);
	NcFile dataFile(fname_out, NcFile::Replace);

	NcDim *rows, *cols;
	if (!(cols = dataFile.add_dim("ncols", opp.ncols)))
		return NC_ERR;
	if (!(rows = dataFile.add_dim("nhistos", opp.nhistos)))
		return NC_ERR;

	NcVar *var;
	//if (!(var = dataFile.add_var("matrix", ncFloat, rows, cols)))
	if (!(var = dataFile.add_var("matrix", ncDouble, rows, cols)))
		return NC_ERR;

	if (!var->add_att("units", "[1]"))
		return NC_ERR;

	// guardo la matriz "opp.M" en .nc
	if (!var->put(&opp.M[0][0], opp.nhistos, opp.ncols))
		return NC_ERR;
	//#####################################################################

	return EXIT_SUCCESS;
}


static void my_error_handler (const char *reason, const char *file, int line, int err) {
  if (0) printf ("(caught [%s:%d: %s (%d)])\n", file, line, reason, err);
  status = 1;
}

//EOF
