#!/usr/bin/env ipython
#from pylab import *
#import h5py as h5
from scipy.io import loadmat
import numpy as np


#-------- binned [relative] difference between 'a' and 'b': a[1]-b[1]
# where:
# b[1] has many more elements than a[1]
# dbin: x-width of bin
# a[0]: x-component of a
# a[1]: y-component of a
# b[0]: x-component of b
# b[1]: y-component of b
def binned_diff(a, b, dbin):
    by_binned = np.zeros(a[0].size, dtype=np.float64)
    for j in range(a[0].size):
        ax_min  = a[0][j] - dbin
        ax_max  = a[0][j] + dbin
        cc      = (b[0]>ax_min) & (b[0]<ax_max)  #(sond_h>hmin) & (sond_h<hmax)
        by_binned[j] = np.nanmean(b[1][cc])

    diff = 2.*(a[1]-by_binned)/(a[1]+by_binned)
    return diff


#-------- rebining of 'b' to resolution of 'a'
# where:
# b[1] is denser than a[1] (IMPORTANT!)
# dbin: x-width of bin
# a[0]: x-component of a
# a[1]: y-component of a
# b[0]: x-component of b
# b[1]: y-component of b
def rebine(a, b):
    by_binned = np.zeros(a[0].size, dtype=np.float64)
    for j in range(a[0].size):
        if(j==0):
            ax_lo   = 0.5*(a[0][0]+a[0][1]) # semisuma de los 2 primeros
            cc      = b[0] <= ax_lo
        elif(j==a[0].size-1):
            ax_hi   = 0.5*(a[0][-2]+a[0][-1]) # semisuma de los 2 ultimos
            cc      = b[0] >= ax_hi
        else:
            ax_min  = 0.5*(a[0][j-1] + a[0][j])
            ax_max  = 0.5*(a[0][j] + a[0][j+1])
            cc      = (b[0]>ax_min) & (b[0]<ax_max)  #(sond_h>hmin) & (sond_h<hmax)
        by_binned[j] = np.nanmean(b[1][cc])

    return by_binned


#fp  = h5.File(fname, 'r')
fname   = '../data/datos_Jimmy_nuevos_GDAS_.mat'
f       = loadmat(fname)

#++++++++++ sondeo
sond_h          = f['altura_year'][:,0]
sond_rho        = f['Densidadyear'][:,0]*1.e3

sond_h_djf      = f['altura_djf'][:,0]
sond_rho_djf    = f['Densidad_djf'][:,0]*1.e3

sond_h_winter   = f['altura_jja'][:,0]
sond_rho_winter = f['Densidad_jja'][:,0]*1.e3

#++++++++++ corsika
crsk_h_std      = f['altura_std'][:,0]
crsk_rho_std    = f['rho_std'][:,0]*1.e3

crsk_h_summer   = f['altura_subsum'][:,0]
crsk_rho_summer = f['rho_subsum'][:,0]*1.e3

crsk_h_winter   = f['altura_subwint'][:,0]
crsk_rho_winter = f['rho_subwint'][:,0]*1.e3



#++++++++++ gdas
gdas_h_summer   = f['DEFalturamean_GDAS'][0,:]/(1.0e3)
gdas_rho_summer = f['Densidad_DEFmean_GDAS'][0,:]*1.0e3

gdas_h_winter   = f['JJAalturamean_GDAS'][0,:]/(1.0e3)
gdas_rho_winter = f['Densidad_JJAmean_GDAS'][0,:]*1.0e3

gdas_h_autumn   = f['MAMalturamean_GDAS'][0,:]/(1.0e3)
gdas_rho_autumn = f['Densidad_MAMmean_GDAS'][0,:]*1.0e3

gdas_h_spring   = f['SONalturamean_GDAS'][0,:]/(1.0e3)
gdas_rho_spring = f['Densidad_SONmean_GDAS'][0,:]*1.0e3

# altura promedio de las 3 estaciones
gdas_h = np.nanmean( [  gdas_h_summer,
                        gdas_h_winter,
                        gdas_h_autumn,
                        gdas_h_spring ],
                        axis=0)

# densidad promedio de las 3 estaciones
gdas_rho = np.nanmean([ gdas_rho_summer,
                        gdas_rho_winter,
                        gdas_rho_autumn,
                        gdas_rho_spring ],
                        axis=0)

#+++++++++++++++++++++++++++++++ restas relativas
# standard (annual average)
diff_crsk = binned_diff(    [crsk_h_std, crsk_rho_std],
                            [sond_h, sond_rho],
                            dbin=0.5)
diff_gdas = binned_diff(    [gdas_h, gdas_rho],
                            [sond_h, sond_rho],
                            dbin=0.5)
# summer
diff_crsk_summer = binned_diff(     [crsk_h_summer, crsk_rho_summer],
                                    [sond_h_djf, sond_rho_djf],
                                    dbin=0.5)
diff_gdas_summer = binned_diff(     [gdas_h_summer, gdas_rho_summer],
                                    [sond_h_djf, sond_rho_djf],
                                    dbin=0.5)
# winter
diff_crsk_winter = binned_diff(     [crsk_h_winter, crsk_rho_winter],
                                    [sond_h_winter, sond_rho_winter],
                                    dbin=0.5)
diff_gdas_winter = binned_diff(     [gdas_h_winter, gdas_rho_winter],
                                    [sond_h_winter, sond_rho_winter],
                                    dbin=0.5)

#+++++++++++++++++++++++++++++++++++++++++++++++++ rebineos
# standard (annual average)
reb_sond = rebine( [crsk_h_std, crsk_rho_std],
                    [sond_h, sond_rho],
                 )
