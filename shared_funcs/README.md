# My2DArray 

Class that handles dynamic memory allocation, without lose of data.
The objective member is a 2D numpy array.

```python
from funcs import My2DArray
import numpy as np

m = My2DArray((4,5))
```

Set elements with slices:
```python
m[1:3,2] = [66,77]
m[1:,2] = [33,44,55]
m[1:,2] = [3,4,5,6,7,8]
```

Append arrays in a vertical-stack fashion (along `axis=0`). See `__setitem__` for details.

```python
a1 = np.zeros((10,5))
a2 = np.ones ((15,5))

m[ 8:,:] = a1
m[18:,:] = a2 
```

Print only the non-trivial contents (see `__getitem__` method).
```python
print m[...]
print m[:]
```

Or print the whole allocated array:
```python
print m[:-1]
print m.this[...]
print m[:,:]
# etc..
```

Print the non-trivial size in the dimension of `axis=0`:
```python
print m.nrow
# to see the shape of the full-allocated array:
print m.shape
```

<!--- EOF -->
