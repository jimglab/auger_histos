#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
""" runtime:
    nothing 
"""
from pylab import figure, close, find
import numpy as np
from h5py import File as h5
from shared_funcs.fhist2d import contour_2d
import argparse, sys
from datetime import datetime, timedelta


parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
'-fio', '--fname_io',
type=str,
default=None,
help='output filename of previous script, containg the countings\
of the 2D-histogram (showing the correlation between the raw-\
scalers and AoP, at individual tank level).'
)
parser.add_argument(
'-fig', '--fname_fig',
type=str,
default=None,
help='output figure filename (always .png).'
)
parser.add_argument(
'-cbmax', '--cbmax',
type=float,
default=1e7,
help='max limit for the colorbar of the 2D-histogram.',
)
parser.add_argument(
'-ylim', '--ylim',
type=float,
nargs=2,
default=[-30.,+40.],
metavar=('YMIN','YMAX'),
help='y-axis limits for the plot (not for domain itself).',
)
parser.add_argument(
'-fm', '--figmode',
type=str,
default='verb',
help='figure mode: "verb" (verbose info in figure) or "paper" (pretty stuff to look good)',
)
pa = parser.parse_args()


f         = h5(pa.fname_io, 'r')

H        = f['H2D'][...]
bins     = f['bins'].value
AOP_AVR  = f['AOP_AVR'].value
xlim     = f['xaxis_lim'][...]
ylim     = f['yaxis_lim'][...]
ini, end = f['period'][...]
# datetimes of the date-borders
dini     = ini.strftime('%d/%m/%Y %H:%M')
dend     = end.strftime('%d/%m/%Y %H:%M')
# number of years (aprox.)
nyr      = (dend - dini).days/365.

#--- calculate averages
x = np.linspace(xlim[0],xlim[1],bins)
y = np.linspace(ylim[0],ylim[1],bins)
xvec,yvec = np.empty(bins*bins),np.empty(bins*bins)
# build grid
for i in range(bins):
    ini = i*bins
    fin = (i+1)*bins
    yvec[ini:fin] = y
    xvec[ini:fin] = x[i]*np.ones(bins)
wei = np.sqrt(H.reshape(bins*bins))        # factores de peso
# average values
avr_aop = f['AOP_AVR'].value # real average
#avr_aop = np.sum(xvec*wei**2)/np.sum(wei**2)  # an estimate
avr_scl = np.sum(yvec*wei**2)/np.sum(wei**2) # an estimate
print "<aop>: ", avr_aop
print "<scl>: ", avr_scl

# normalized-deviation-versions [%] of absolute observables
x_    = 100.*(x/avr_aop-1.)
y_    = 100.*(y/avr_scl-1.)
xvec_ = 100.*(xvec/avr_aop-1.)
yvec_ = 100.*(yvec/avr_scl-1.)
#--- fig
fig = figure(1, figsize=(6,3.) if pa.figmode=='paper' else (6,4))
ax  = fig.add_subplot(111)

fig,ax = contour_2d(fig,ax,x_,y_,H.T,hscale='log',
         vmin=1,vmax=pa.cbmax,alpha=1.,
         cb_fontsize=14 if pa.figmode=='paper' else 12)
#--- fit
p   = np.polyfit(xvec_, yvec_, 1, w=wei, cov=True)
m   = p[0][0] # slope
b   = p[0][1] # ordinate to origin

#--- label
if pa.figmode=='verb':
    LABEL_FIT = 'm=%2.2f\nb=%2.2g' % (m,b)
elif pa.figmode=='paper':
    LABEL_FIT = '%d yr fit:\nm=%2.2f' % (int(nyr), m)
else: sys.exit(1)

ax.plot(x_,m*x_+b,'--r',lw=3,label=LABEL_FIT)
ax.legend(loc='best')
f.close()
#--- save fit-info to .h5
fo = h5(pa.fname_io,'r+')
AlreadyHas = ('fit' in fo.keys()) and ('meta' in fo.keys())
if AlreadyHas:
    fo.pop('fit'); fo.pop('meta') # over-write datasets
fo['fit/m'] = m
fo['fit/b'] = b
fo['meta/scals_avr'] = avr_scl
fo['meta/aop_avr'] = avr_aop
print " [+] wrote fit-info to file."
fo.close()
#--- savefig
#ax.set_ylim(100.*(ylim/avr_scl-1.))
ax.set_ylim(pa.ylim)
ax.grid()
ax.set_xlabel('$\Delta AoP$ [%]', fontsize=17 if pa.figmode=='paper' else 14)
ax.set_ylabel('$\Delta S_{id}$ [%]', fontsize=17 if pa.figmode=='paper' else 14)
ax.tick_params(labelsize=12 if pa.figmode=='paper' else 9)
fig.savefig(pa.fname_fig, dpi=135, bbox_inches='tight')
print "\n [+] we generated: %s\n" % pa.fname_fig 
close(fig)
#EOF
