%%
% - Este codigo CONCATENA y SINCRONIZA la data de scalers y monitoreo, con el fin de tener <AoP> y scalers
%   en un solo archivo.
% - Este codigo depende de las salidas que genera "recupera_scalers_at_hegea.m" en
% $HOME/ccin2p3/in2p3_data/data_auger/data_monit/codigos_procesamiento/.
%
%
function correccion_AoP_scalers(m, aop_avr, YEAR_INI, YEAR_FIN, DIR_SCALS, DIR_MONITS, Q, file_out)

MIN_DATA        = 300;                           % nro minimo de datos q quiero para construir los scalers corregidos

MIN             = 60.;                          % [seg]
WNDW            = 30.*MIN;                      % ventana total en q hago promedio corrido de los monits
                                                % OJO: escoji 30min xq es +/- el maximo hueco de tiempo en q un
                                                % tanque del todo array tira un dato de AoP

LOW_SCAL    = 1700.;
UPP_SCAL    = 2300.;
D_SCAL      = (UPP_SCAL - LOW_SCAL)/15.;
%YEAR_INI        = 2008;
%YEAR_FIN        = 2011;
%ruta_dst        = sprintf('/home/masiasmj/auger_ascii/data_auger/estudios_AoP/data');
%file_out        = sprintf('%s/scalers_wAoPcorrection_%04d-%04d_allarray_3pmtsON.dat', ruta_dst, YEAR_INI, YEAR_FIN);

n               = 1;                    % tamanio de la data final
for YYYY = YEAR_INI:YEAR_FIN
  fprintf(' anio:%04d\n\n', YYYY);

  for MM = 1:12
    fprintf('   mes: %d\n', MM);
    % -----------------------------leo scalers
    dir_scal    = sprintf('%s/%04d', DIR_SCALS, YYYY);
    fname_scal  = sprintf('%s/scals_%04d_%02d.dat', dir_scal, YYYY, MM);
    data_scal   = load(fname_scal);             % estos scalers ya estan filtrados a un rango "razonable"
    fprintf('   scaler leido.   %s\n', fname_scal);
    n_scal      = size(data_scal, 1);
    % -----------------------------leo monits "scaler3"
    dir_monit           = sprintf('%s/%04d', DIR_MONITS, YYYY);
    fname_monit         = sprintf('%s/mc_scaler3_%04d_%02d.dat', dir_monit, YYYY, MM);
    data_monit          = load(fname_monit);
    fprintf('   monit leido.   %s\n', fname_monit);
    three_pmtsON        = data_monit(:, 3)==7 & (data_monit(:,6)<4.6) & (data_monit(:,6)>2.6); % 3PMTs ok, y filtro rango razonable de AoP
    data_monit          = data_monit(three_pmtsON, [1 2 6]);    % me quedo solo con las columnas [id, gps_sec, AoP]

    for i = 1:n_scal
      t_scl             = data_scal(i, 1);                      % tiempo de la data scalers
      ind               = data_monit(:,2) >= t_scl-WNDW/2. & data_monit(:,2) <= t_scl+WNDW/2.; % selecc data para esta ventana
      blk_monit         = data_monit(ind, [1 3]);               % bloque de data scalers para esta ventana temporal (solo id y AoP)
      if size(blk_monit, 1) > MIN_DATA                          % pregunto si hay mas de 'MIN_DATA' tanques en esta ventana temporal
        IDs             = unique(blk_monit(:,1));               % lista de tanques q aportan c/data monit
        n_IDs           = size(IDs, 1);                         % nro de tanques q aportan c/data monit
        scals_uncorr    = data_scal(i, 2+IDs);                  % vector de scalers para los tanques 'IDs'
        for j = 1:n_IDs
          ind     = blk_monit(:,1) == IDs(j);
          aop(j)  = mean(blk_monit(ind,2));
        end
        scals_corr        = scals_uncorr - m*(aop - aop_avr);     % scalers corregidos, por tanque (*)

        %---------------------- filtro NaNs
        ind          = ~isnan(scals_corr);          % indices de los q NO son NaN, ni en 'scals_uncorr', ni en 'aop'
        scals_corr   = scals_corr(ind);                      % sin NaNs
        aop          = aop(ind);
        scals_uncorr = scals_uncorr(ind);
        %----------------------------------

        %---------------- "rechazo de datos"
        bineo_histog = [LOW_SCAL:D_SCAL:UPP_SCAL];      % dominio ("bineo") del histograma q uso para rechazar datos
        ind          = data_rejection(Q, bineo_histog, scals_corr);
        scals_corr   = scals_corr(ind);         % scalers corregidos q no incluyen "extranjeros"
        aop          = aop(ind);
        scals_uncorr = scals_uncorr(ind);
        n_IDs_corr   = size(scals_corr, 2);                  % nro de tanques, donde puedo corregir por AoP (*)
        % (*) simultaneamente, limpio de NaNs en AoP y en scalers, y limpio de valores "extranjeros"
        %-----------------------------------
        DATA(n, 1)        = t_scl;                                % tiempo n-esimo
        DATA(n, 2)        = n_IDs_corr;
        DATA(n, 3)        = mean(aop);                        % <AoP> promediado en todo el array q aporto en esta ventana
        DATA(n, 4)        = mean(scals_uncorr);               % <scalers sin corregir> promediado en el array
        DATA(n, 5)        = mean(scals_corr);                     % <scalers corregidos> promediado en el array
        if isnan(DATA(n, 4))  % me avisa si hizo un NaN (no debe ser asi!)
          whos;
          fprintf(' CORRUPTO!!!!!, en i:%d\n', i)
          %return;  % termina el script
        end
        n                 = n + 1;
        clear aop;                        % reseteo xq asigno elemento a elemento en el prox ciclo de "for"
      end
    end
  end
end
%
dlmwrite(file_out, DATA, 'delimiter', ' ', 'precision', 13)
%
% (*) algunos pueden tener NaNs debido a q en 'data_scals' los hay.
%%

