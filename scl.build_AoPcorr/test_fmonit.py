#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
import numpy as np
import funcs as ff
import os, sys
from scipy.io.netcdf import netcdf_file
from h5py import File as h5
from numpy import (
    loadtxt, size, mean, isnan, array,
    ones, zeros, empty, nanmean, nanmedian, nanstd,
    concatenate,
)
from glob import glob
from os.path import isfile, isdir
from datetime import datetime, timedelta
from MonLib import funcs as mf
from pylab import pause, find, figure, close
from shared_funcs.funcs import My2DArray, date2gps


def test_func(m, aop_avr, date_range, DIR_SCALS, DIR_MONITS, Q, fname_out, sdarray=np.arange(2, 1999+1), aop_lim=(2.6, 4.6)):
    """
    NOTE: esta funcion es casi igual al funcs::correccion_AoP_scalers(), 
    y sirve para testear dicha funcion.
     - Este codigo CONCATENA y SINCRONIZA la data de scalers y monitoreo, con el fin de tener <AoP> y scalers
       en un solo archivo.
     - Este codigo depende de las salidas que genera "recupera_scalers_at_hegea.m" en
     $HOME/ccin2p3/in2p3_data/data_auger/data_monit/codigos_procesamiento/.
     inputs:
     - sdarray: array sobre el cual se hace la correccion (todo el SD 
                array por defecto).
    """
    MIN_DATA  = 300           # nro minimo de datos q quiero para construir los scalers corregidos
    MIN_TANKS = 300
    MIN       = 60.           # [seg]
    WNDW      = 30.*MIN       # ventana total en q hago promedio corrido de los monits
    GPS2UTC   = 315964800     # where utc=gps+GPS2UTC
                                    # OJO: escoji 30min xq es +/- el maximo hueco de tiempo en q un
                                    # tanque del todo array tira un dato de AoP
    aop_min, aop_max = aop_lim
    n           = 0                # tamanio de la data final
    DATA        = My2DArray((8,5), dtype=np.float32)
    OneDay      = timedelta(days=1)

    date_ini    = date_range[0] #datetime(YEAR_INI, 1, 1)
    date_end    = date_range[1] #datetime(YEAR_FIN, 12, 31)
    date        = date_ini
    yyyy,mm,dd  = date.year, date.month, date.day
    #--- gral data for output
    fname_out_tmp = fname_out+'__tmp__'
    fo = h5(fname_out_tmp, 'w')
    fo['dir_scals'] = DIR_SCALS
    fo['dir_monit'] = DIR_MONITS
    fo['sd_array']  = sdarray
    fo['q_rejection'] = Q        # rejection factor
    fo['AoP_slope_correction'] = m
    while date <= date_end:
        if yyyy!=date.year or mm!=date.month or date==date_ini:
            print " year/month: ", date.year, date.month
            if mm!=date.month or date==date_ini:
                # ---------------------------- leo scalers
                dir_scal    = '%s/%04d' % (DIR_SCALS, date.year)
                fname_scal  = '%s/scals_%04d_%02d.h5' % (dir_scal, date.year, date.month) # estos scalers ya estan filtrados a un rango "razonable"
                fscl        = h5(fname_scal, 'r')
                print '   scaler leido.   %s\n' % fname_scal
                #--- reset buffer
                n    = 0            # size of data for today
                DATA = My2DArray((8,5), dtype=np.float32)

        yyyy, mm, dd = date.year, date.month, date.day
        #--- extraigo del file de scalers, solo el extracto para hoy
        tscl_   = fscl['time'].value # [gps sec]
        gpshoy  = date2gps(date)     # [gps sec]
        cc      = (tscl_>=gpshoy) & (tscl_<=(gpshoy+86400.))
        tscl    = fscl['time'][cc]   # [gps sec]
        scl     = {} # dict for all array (scalers for today!)
        for Id in range(2, 1999+1): # TODO: replace with 'sdarray'
            scl[Id] = fscl['scl_%04d'%Id][cc]

        nscl = tscl.size #nmbr of scalers for today (size of tscl[:] & scl[Id][:])
        print " datehoy: ", date
        if nscl<=1: # hay scalers para hoy?
            date += OneDay
            continue

        #---------------------- leo monits "scaler3"
        inp = {
        'date'      : date,
        'dir_mon'   : DIR_MONITS,
        'stations'  : sdarray,
        }
        fmh = ff.fmonit_handler(inp, params=None, verbose=False)
        if not fmh.files_ok:
            date += OneDay
            continue
        print " ---> syncing monits w/ scalers... @", date
        for i in range(nscl):
            ts     = tscl[i] # [gps sec]
            ntanks = 0       # tank counter
            ind    = {}      # conditionals for each tank
            # NOTE: 'ind[Id]' filters-in data:
            # - within window (t-WNDW/.2, t+WNDW/2.)
            # - with 3 PMTs with Ok status in tank 'Id'
            # - with reasonable AoP values
            for Id in map(int, fmh.mon_t.keys()): #range(2, 1999+1):
                tmon    = fmh.mon_t['%04d'%Id]-GPS2UTC  # [gps sec]
                cc      = (tmon>=ts-WNDW/2.) & (tmon<=ts+WNDW/2.)
                fpmt    = fmh.mon_pmtStat['%04d'%Id]
                cc     &= ((fpmt==7) | (fpmt==15)) | (fpmt==23)
                aop     = fmh.mon_aop['%04d'%Id]
                cc     &= (aop>aop_min) & (aop<aop_max)
                if len(find(cc))>1: # tiene data este ID?
                    ind[Id] = cc
                    ntanks += 1

            #--- check before slicing data below
            if ntanks<10:
                continue # not enough tanks!, so abort && next 'ts'

            sc_uncorr = np.empty(ntanks, dtype=np.float32)
            #--- grab scals from all array for today
            IDs = ind.keys() # IDs con data
            for j, Id in zip(range(len(IDs)), IDs): 
                sc_uncorr[j] = scl[Id][i] 

            aop = np.empty(ntanks, dtype=np.float32)
            for j, Id in zip(range(len(IDs)), IDs):
                aop[j] = fmh.mon_aop['%04d'%Id][ind[Id]].mean()


#++++++++++++++++++++++++++++++++++++++++++++++++++++
"""
First, we should run ./make_AoPHist2d.py && ./fit_hist2d.py, so we can
take from its output-file .h5 the value for 'aop_avr' here.

runtime:
~---?
"""

m       = 530.33 #396.595 # 530.33; from .h5 generated by ./fit_hist2d.py
aop_avr = 3.402 #3.47 # 3.402; from .h5 generated by ./fit_hist2d.py
Q       = 2.5
#date_ini = datetime(2008, 7, 26)
date_ini = datetime(2006, 1, 1)
date_end = datetime(2015, 12, 31)
date_range = (date_ini, date_end)
year_ini, year_end = date_ini.year, date_end.year

ruta_dst        = '../out/out.scl.build_final'
#fname_out       = '%s/scalers_wAoPcorrection_%04d-%04d_allarray_3pmtsON_wdatarejec_q%2.1f_.h5'%(ruta_dst, year_ini, year_end, Q)
fname_out       = './test_fmonit.h5'
DIR_SCALS       = '/home/masiasmj/auger_ascii/data_scalers_'
DIR_MONITS      = '/home/masiasmj/_tmp_/intermediate/monit' # los netcdf .nc
sdarray         = np.arange(2, 1999+1) # all SD array

test_func(
    m, 
    aop_avr, 
    date_range,
    DIR_SCALS, 
    DIR_MONITS, 
    Q, 
    fname_out, 
    sdarray=sdarray,
    aop_lim=(2.0,4.6),
)

#EOF
