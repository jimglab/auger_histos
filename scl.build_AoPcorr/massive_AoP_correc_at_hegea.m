m               = 396.595;                      % "pendiente de correccion" sacado del ajuste lineal
aop_avr         = 3.47;                         % valor medio <AoP>, de los 8 anios, de *todo* el array

YEAR_INI        = 2006;
YEAR_FIN        = 2013;
Q		= 2.5;

ruta_dst        = sprintf('/home/masiasmj/auger_ascii/data_auger/estudios_AoP/data');
file_out        = sprintf('%s/scalers_wAoPcorrection_%04d-%04d_allarray_3pmtsON_wdatarejec_q%2.1f.dat', ruta_dst, YEAR_INI, YEAR_FIN, Q);

DIR_SCALS       = sprintf('/home/masiasmj/auger_ascii/data_scalers');
DIR_MONITS      = sprintf('/home/masiasmj/auger_ascii/monit/scaler3');

correccion_AoP_scalers(m, aop_avr, YEAR_INI, YEAR_FIN, DIR_SCALS, DIR_MONITS, Q, file_out)
