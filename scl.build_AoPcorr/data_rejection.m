function IND = data_rejection(Q, BINEO_HISTOG, SCALS)
	[mu, sigma] = normfit(SCALS);
	% 'Q' es el factor q eligo para decidir el "rango de rechazo"
	lim_inf = mu - Q*sigma;
	lim_sup	= mu + Q*sigma;
	IND = SCALS>lim_inf & SCALS<lim_sup;
% NOTA: rechazo lo que este fuera del rango ('lim_inf':'lim_sup')
