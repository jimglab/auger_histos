#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
import argparse
from shared_funcs.funcs import (
arg_to_utcsec, gps2date, My2DArray
)
import numpy as np
from h5py import File as h5
from datetime import datetime, timedelta
from pylab import figure, close

def zip_yyyymm(ini, end):
    """
    given:
    mi, yi = ini.month, ini.year
    me, ye = end.month, end.year
    returns a zip() of the two lists:
    [yi, yi, ..., ye]         # (**)
    [mi, mi+1, mi+2, ..., me] # (*)
    (*)  the necessary sequence of months from the 
         initial datetime 'ini' until 'end'.
    (**) the corresponding sequence of year of the
         former months.
    """
    mi, yi = ini.month, ini.year
    m_, y_ = [], []
    end_ = end + timedelta(days=31)
    while yi!=end_.year or mi!=end_.month:
        m_ += [mi]; y_ += [yi]
        if mi==12:
            mi=1; yi+=1
        else:
            mi+=1
    return zip(y_,m_)


class mgr_comparison(object):
    def __init__(self,bd,fname_inp_old,fname_inp_new):
        self.ini_gps, self.end_gps = bd
        self.fname_inp_old = fname_inp_old
        self.fname_inp_new = fname_inp_new

    def extract_old(s,SdId=None):
        if SdId is None:
            t, aop, uncorr, corr = \
            np.loadtxt(s.fname_inp_old)[:,[0,2,3,4]].T
            cc = (t>=s.ini_gps) & (t<=s.end_gps)
            t_      = t[cc]
            aop_    = aop[cc]
            uncorr_ = uncorr[cc]
            corr_   = corr[cc]
            return t_, aop_, uncorr_, corr_
        else:
            Id,t,fpmt,aop=np.loadtxt(s.fname_inp_old)[:,[0,1,2,6]].T
            cc  = (t>=s.ini_gps) & (t<=s.end_gps)
            cc &= Id==SdId
            cc &= fpmt==7
            return t[cc],aop[cc],fpmt[cc],Id[cc]

    def extract_new(self,):
        f = h5(self.fname_inp_new,'r')
        ini = gps2date(self.ini_gps) # datetime
        end = gps2date(self.end_gps) # datetime
        # list the years/months involved in
        # my date-range (ini,end)
        yyyymm = zip_yyyymm(ini, end) # zip()
        dd = My2DArray((2,4), dtype=np.float32)
        n = 0
        for yyyy, mm in yyyymm:
            print yyyy, mm
            d = f['data/%04d/%02d'%(yyyy,mm)][...]
            #dd.resize_rows(n+d.shape[0])
            dd[n:n+d.shape[0],:] = d[:,[0,2,3,4]]
            n += d.shape[0]
        f.close()
        return dd[:n,:]

#--- retrieve args
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
'-ini', '--ini_date', 
type=str, 
action=arg_to_utcsec,
default='25/03/2006',
help='initial date',
)
parser.add_argument(
'-end', '--end_date', 
type=str, 
action=arg_to_utcsec,
default='28/11/2013',
help='end date',
)
parser.add_argument(
'-new', '--fname_inp_new',
type=str,
default=None,
help='input .h5 filename of the new version of data.'
)
parser.add_argument(
'-old', '--fname_inp_old',
type=str,
default=None,
help='input .dat (ASCII) filename of the old version of data.'
)
parser.add_argument(
'-fig', '--fname_fig',
type=str,
default=None,
help='filename of figure .png output.'
)
parser.add_argument(
'-id', '--SdId',
type=int,
default=None,
help='ID number of the SD tank (in case the old-file is \
the original "mc_scaler3_yyyy_mm.nc").'
)

pa = parser.parse_args()

print pa.ini_date
#utc = gps + 315964800
ini_gps = pa.ini_date - 315964800
end_gps = pa.end_date - 315964800
print ini_gps, end_gps

fname_inp_old = pa.fname_inp_old #'../out/out.scl.build_final/scalers_wAoPcorrection_2006-2013_allarray_3pmtsON_wdatarejec_q2.5.dat'
#fname_inp_new = '../out/out.scl.build_final/scalers_wAoPcorrection_2006-2015_allarray_3pmtsON_wdatarejec_q2.5_MPI.h5'
fname_inp_new = pa.fname_inp_new

m = mgr_comparison([ini_gps,end_gps], fname_inp_old, fname_inp_new)
data_old = m.extract_old(pa.SdId)
data_new = m.extract_new().T

o_t,o_aop,o_uncorr = data_old[0],data_old[1],data_old[2]
n_t,n_aop,n_uncorr = data_new[0],data_new[1],data_new[2]

fig = figure(1, figsize=(8,4))
ax  = fig.add_subplot(111)

o_t_ = (o_t - ini_gps)/86400. # [days]
n_t_ = (n_t - ini_gps)/86400. # [days]
dt   = (end_gps-ini_gps)/86400.

opt = {'ms':2, 'alpha':.5, 'mec':'none'}
ax.plot(n_t_, n_uncorr, '-ob', label='new', **opt)
ax.plot(o_t_, o_uncorr, '-or', label='old', **opt)

ax.legend(loc='best')
ax.grid(True)
ini = gps2date(ini_gps)
ax.set_xlabel('days since %02d/%02d/%04d'%(ini.day,ini.month,ini.year))
ax.set_xlim(0.,dt)
fig.savefig(pa.fname_fig,dpi=200,bbox_inches='tight')
close(fig)

#EOF
