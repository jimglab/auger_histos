#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
"""
runtime:
~18.8hs
"""
import funcs as ff
import numpy as np
from datetime import datetime
import argparse, os
import shared_funcs.funcs as sf
from mpi4py import MPI
from datetime import datetime, timedelta
import h5py


parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter,
description="""
Build 2D-histograms of AoP-correlations with Scalers.
""",
)
parser.add_argument(
'-o', '--fname_out',
type=str,
default='../out/out.scl.build_AoPcorr/hist2d_correl_scals.vs.aop.h5',
help='filename of the .h5 output (containing the 2D-histogram)'
)
parser.add_argument(
'-is', '--inp_scals',
type=str,
default='{HOME}/auger_ascii/data_scalers_ii'.format(**os.environ),
help='directory of input scalers; i.e. raw data \
      files *.bz2 (e.g.  from Lyon server)'
)
parser.add_argument(
'-im', '--inp_monit',
type=str,
default='{HOME}/_tmp_/intermediate/monit'.format(**os.environ),
help='directory of intermediate monit files; i.e. files *.nc (e.g. \
      processed from the raw sd_*.root)'
)
parser.add_argument(
'-b', '--bins',
type=int,
default=200,
help='number of bins for the square 2D-histogram\
 (recommended: 200).'
)
parser.add_argument(
'-fc', '--fcache',
type=str,
default='', # but default, it doesn't use cache
metavar='CACHE-FILE',
help="""
Use if it\'s intended to use the "cache system" to store daily 2D-histograms.
By default, it doesn't use the cache system.
""",
)
parser.add_argument(
'-p', '--period', 
type=str, 
nargs=2,
action=sf.arg_to_datetime,
metavar=('DateIni', 'DateEnd'),
default=['01/01/2006','31/12/2015'],
help='initial date',
)
pa = parser.parse_args()

#-----------------------------------MPI
comm     = MPI.COMM_WORLD
rank     = comm.Get_rank()
wsize    = comm.Get_size()
date_bd  = sf.equi_dates(pa.period[0], pa.period[1] + timedelta(days=1), wsize)
# start/end dates for each proc
date_ini = date_bd[rank]
date_end = date_bd[rank+1] - timedelta(days=1)
# date string-format (to save in the .h5)
FDATE = '%d-%m-%Y %H:%M'
#--------------------------------------

# report the partition of time windows
if rank == 0:
    print " ---> Repartition of time windows:"
    for _r in range(wsize):
        print " [*][rank:{rank}/{maxrank}] period for processing:\
            {ini} - {end}".format(
            rank = _r,
            maxrank = wsize - 1,
            ini = date_bd[_r],
            end = date_bd[_r+1] - timedelta(days=1),
            )


aop_lim = [2.6, 4.6]
# this is aprox -30% and +40% respect to the average Scaler 
# rate (estimated for the 2006-2015 period)
scl_lim = [1312.5, 2625.0]
# minimum number of tanks involved in each timestamp
ntanks_min = 300

# define the names of the sub-caches
if pa.fcache:
    fcaches = [ '%s/.%s_%02d_.h5' % \
        (os.path.dirname(pa.fname_out), os.path.basename(pa.fname_out), _rank) \
        for _rank in range(wsize) ]
else:
    fcaches = []


if pa.fcache:
    if rank==0:
        meta = {
        'bins'    : pa.bins,
        'range_x' : aop_lim,
        'range_y' : scl_lim,
        'mintank' : ntanks_min,
        }
        # if the file doesn't exist, we'll build it later
        if os.path.isfile(pa.fcache):
            stat = ff.split_h2d_cache(pa.fcache, fcaches, pa.period, wsize, meta.keys())
            if stat>0: comm.Abort()

    # all processors must wait until the sub-caches are ready (if using).
    comm.Barrier()

# input arguments
inp = {
'dir_scals'   : pa.inp_scals,  
'dir_monits'  : pa.inp_monit, 
'fname_cache' : fcaches[rank] if pa.fcache else '', # cache-file for each processor
}
#-----------------------------------------
# build the 2D-histograms
hm = ff.Correl_wAoP(**inp)
out = hm.build_hist2d(
    date_range=[date_ini, date_end], # 2-list of datetimes
    sdarray=np.arange(2,1999+1), 
    aop_lim = aop_lim,
    scl_lim = scl_lim,
    bins=pa.bins,
    MIN_TANKS=ntanks_min,
    )

print " @@ [r:%d] H2D.sum: %g" % (rank, out['H2D'].sum())
print " @@ [r:%d] aop_n: %d" % (rank, out['AoP_buff']['aop_n'])
# the H2D contribution of processor 'rank'
H2D      = out['H2D']
AoP_buff = np.array(
    [out['AoP_buff']['aop_sum'], 1.*out['AoP_buff']['aop_n']],
    dtype=np.float64,
    )

#-----------------------------------------
#--- i/o for 2D-hists
# send data from "root"
if rank!=0: 
    print " [r:%03d --> root] sending H2D" % rank
    comm.Send([H2D, MPI.FLOAT], dest=0, tag=777)

# unify the data from *all* the processors
if rank==0:
    print "\n [r:0] <--- now gathering all H2D data :)"
    # buffer to recieve the 2D-histograms
    buff = np.empty(H2D.shape, dtype=np.float64) 

    for sender in range(1, wsize):
        print " [root <-- r:%03d] recieving H2D" % sender
        comm.Recv([buff, MPI.FLOAT], source=sender, tag=777)
        H2D         += buff


#-----------------------------------------
#--- i/o for <AoP>
if rank!=0:
    print " [r:%03d --> root] sending AoP-buffer" % rank
    comm.Send([AoP_buff, MPI.FLOAT], dest=0, tag=888)

# unify the data from *all* the processors
if rank==0:
    # to recieve the AoP accumulated values
    buff_aop = np.empty(2, dtype=np.float64) 
    for sender in range(1, wsize):
        print " [root <-- r:%03d] recieving AoP-buffer" % sender
        comm.Recv([buff_aop, MPI.FLOAT], source=sender, tag=888)
        # Combine info of root processor, with those of
        # the other processors:
        AoP_buff[0]    += buff_aop[0] # aop_sum
        AoP_buff[1]    += buff_aop[1] # aop_n
    # NOTE: AoP_buff is an statistic of just the current
    # processed files; it doesn't take into account the
    # cache information up to this point.

# metadata to append in the output cache
if rank==0 and pa.fcache:
    if os.path.isfile(pa.fcache):
        #--- correct the AoP stuff
        fcu = h5py.File(pa.fcache, 'r')
        # before grabbing the AoP cached-values, check consistency
        # between the current period and the cache period. 
        _speriod = fcu['meta/period'].value # 2-list of time borders
        # we should satisfy (*):
        # - the start-time of cache should be AFTER the current one
        # - the end-time of the cache should be BEFORE the current one
        if datetime.strptime(_speriod[0], FDATE) >= pa.period[0] and \
            datetime.strptime(_speriod[1], FDATE) <= pa.period[1]:
            aopbuff_valid = True # (*)
        else:
            aopbuff_valid = False # (*)
            print "\n [!] the AoP mean value from caches can not be used"
            print "     contribute to the new AoP-mean, since the cache period"
            print "     is %r, and the current run is %r\n" % (_speriod, pa.period)
        # NOTE: if the above condition doesn't satisfy, our correction
        # to the 'AoP_buff[]' is not valid!
        # TODO: (*) [add feature] in case the cache has time borders that don't 
        # fall inside the 'pa.period', the current run should be able to grab 
        # just the useful part (i.e. just the data that intersects in time).

        # now grab AoP cache-values
        _aop_mean    = fcu['meta/aop_mean'].value if aopbuff_valid else np.nan # (*)
        _aop_n       = fcu['meta/aop_n'].value if aopbuff_valid else np.nan # (*)
        # combine with the information of the cache
        AoP_buff[0] += AoP_buff[0] + _aop_mean*_aop_n # aop_sum
        AoP_buff[1] += AoP_buff[1] + _aop_n # aop_n
        # NOTE: now 'AoP_buff' is a combined statistic between
        # the current processed files, and the ones that were
        # already processed to build the cache 'pa.fcache'.
        fcu.close()

    #--- save all caches in a single file
    # NOTE: we overwrite the unified 'pa.fcache'
    ff.unify_h2d_cache(pa.fcache, fcaches, pa.fname_out, wsize, AoP_buff, pa.period, **meta)


#-----------------------------------------
#--- save results
if rank==0:
    with np.errstate(divide='raise'):
        # don't pass this un-noticed!
        AoP_mean = AoP_buff[0]/AoP_buff[1] # this **is** the actual global average

    print "\n >>> H2D.mean : %g" % H2D.mean()
    print   " >>> <AoP>    : %g\n" % AoP_mean
    #--- now save to file
    fo = h5py.File(pa.fname_out, 'w')
    fo['bins']      = pa.bins
    fo['xaxis_lim'] = np.array(aop_lim)
    fo['yaxis_lim'] = np.array(scl_lim)
    fo['AOP_AVR']   = AoP_mean
    fo['H2D']       = H2D
    # global start/end date-borders
    ini, end        = date_bd[0], date_bd[-1] - timedelta(days=1)
    fo['period']    = [ ini.strftime(FDATE), end.strftime(FDATE) ]
    fo.close()
    print "\n [+] we generated: %s\n" % pa.fname_out


#EOF
