# Chain of data-processing:

<!------------------------------------
 -------  hist2d: scals vs AoP  ------
 ------------------------------------->
---
**Build a 2D-histogram**, from the raw-data (runtime: **~16-18hs** for 10 years of data) of the correlation between raw-scalers and AoP values (at individual-SD tank level), and save it into an .h5 file:
```bash
./make_AoPHist2d.py -- \
    -is /media/scratch1/auger/scl.build_scals \
    -im /media/scratch1/auger/build_mc \
    -b 200 \
    -p 01/01/2006 31/12/2015 \
    -o /media/scratch1/auger/scl.build_AoPcorr/hist2d_scals.vs.aop.h5
```

Full help:

    $ ./make_AoPHist2d.py -- -h
    usage: make_AoPHist2d.py [-h] [-o FNAME_OUT] [-is INP_SCALS] [-im INP_MONIT]
                             [-b BINS] [-p PERIOD PERIOD]

    optional arguments:
      -h, --help            show this help message and exit
      -o FNAME_OUT, --fname_out FNAME_OUT
                            filename of the .h5 output (containing the 2D-
                            histogram) (default: ../out/out.scl.build_AoPcorr/hist
                            2d_correl_scals.vs.aop.h5)
      -is INP_SCALS, --inp_scals INP_SCALS
                            directory of input scalers; i.e. raw data files *.bz2
                            (e.g. from Lyon server) (default:
                            /home/masiasmj/auger_ascii/data_scalers_ii)
      -im INP_MONIT, --inp_monit INP_MONIT
                            directory of intermediate monit files; i.e. files *.nc
                            (e.g. processed from the raw sd_*.root) (default:
                            /home/masiasmj/_tmp_/intermediate/monit)
      -b BINS, --bins BINS  number of bins for the square 2D-histogram
                            (recommended: 200). (default: 200)
      -p PERIOD PERIOD, --period PERIOD PERIOD
                            initial date (default: ['01/01/2006', '31/12/2015'])



<!------------------------------------
 ----------  fit the hist2d  ---------
 -------------------------------------->
---
**Fit the 2D-histograms**. 
Also append fit-info and stats into previous .h5 output, and generate a .png figure of the fit.
```bash
./fit_hist2d.py -- \
    -input /media/scratch1/auger/scl.build_AoPcorr/histos2d_scals.vs.aop.h5 \
    -fig /media/scratch1/auger/scl.build_AoPcorr/histos2d_scals.vs.aop.png
```

Full help:

    $ ./fit_hist2d.py -- -h
    usage: fit_hist2d.py [-h] [-input H5INPUT] [-fig FNAME_FIG]

    optional arguments:
      -h, --help            show this help message and exit
      -input H5INPUT, --h5input H5INPUT
                            output filename of previous script, containg the
                            countingsof the 2D-histogram (showing the correlation
                            between the raw-scalers and AoP, at individual tank
                            level). (default: None)
      -fig FNAME_FIG, --fname_fig FNAME_FIG
                            output figure filename (always .png). (default: None)


<!------------------------------------
 ----  build AoP-corrected scals  ----
 ------------------------------------>
---
**Build the AoP-corrected scalers** (using the .h5 output of the previous scripts).
Takes:
- **~16hs** running in parallel in 4 processors.
- **~1hr** in 16 processors.
```bash
INP=/media/scratch1/auger/scl.build_AoPcorr/histos2d_scals.vs.aop.h5
SCL=/media/scratch1/auger/scl.build_scals
MON=/media/scratch1/auger/build_mc
OUT=/media/scratch1/auger/scl.build_AoPcorr/scals_wAoPcorr_wrejec.h5
mpirun -np 4 ./massive_AoP_correc_mpi.py -- \
    --h5input $INP  \
    --dir_scl $SCL  \
    --dir_mon $MON  \
    --Qvalue 2.5 \
    -ini 01/01/2006 \
    -end 31/12/2015 \
    -o $OUT
```

Full help:

    $ ./massive_AoP_correc_mpi.py -- -h
    usage: massive_AoP_correc_mpi.py [-h] [-input H5INPUT] [-scl DIR_SCL]
                                     [-mon DIR_MON] [-Q QVALUE] [-ini INI_DATE]
                                     [-end END_DATE] [-o FNAME_OUT]

    optional arguments:
      -h, --help            show this help message and exit
      -input H5INPUT, --h5input H5INPUT
                            filename of the .h5 containing the average and fit
                            values (the output of fit_hist2d.py). (default: None)
      -scl DIR_SCL, --dir_scl DIR_SCL
                            input directory of raw scalers (preprocessed, but
                            still raw) (default:
                            /home/masiasmj/auger_ascii/data_scalers_)
      -mon DIR_MON, --dir_mon DIR_MON
                            input directory of monit data (default:
                            /home/masiasmj/_tmp_/intermediate/monit)
      -Q QVALUE, --Qvalue QVALUE
                            factor of data rejection (AoP-value rejection from
                            local means). (default: 2.5)
      -ini INI_DATE, --ini_date INI_DATE
                            initial date (default: 01/01/2006)
      -end END_DATE, --end_date END_DATE
                            end date (default: 31/12/2015)
      -o FNAME_OUT, --fname_out FNAME_OUT
                            output .h5 filename. (default: ./test_out.h5)



---
# Testing & Cross-checks:

* Script to extract data from the **old** (ASCII .dat) and **new** (HDF5 .h5) versions, in a given time-window (specified by the `-ini` and `-end` arguments), and plot them into a .png figure.
See `./comparison_validity.py -- -h` for more details.
```bash
OLD=~/auger_ascii/data_auger/estudios_AoP/data/scalers_wAoPcorrection_2006-2013_allarray_3pmtsON_wdatarejec_q2.5.dat
NEW=../out/out.scl.build_final/scals_wAoPcorrection_wrejec.h5
./comparison_validity.py -- -ini 01/01/2006 -end 30/03/2006  -new $NEW  -old $OLD -fig ./test1.png
#To super-plot data from a specific SD tank (e.g. 710) from the old-format ASCII data "mc_scaler3_...dat":
./comparison_validity.py -- -ini 01/09/2008 -end 10/11/2009 -new ../out/out.scl.build_final/scals_wAoPcorrection_wrejec.h5 -old ~/auger_ascii/data_auger/data_monit/codigos_procesamiento/test_old/mc_scaler3_2008_09.dat -id 710 -fig ./test2.png
```


---
* Script to extract monit-data (AoP) in order to see if our implementations are giving the data in the desired time-window.
NOTE: Previously, we should already have run `./make_AoPHist2d.py` and `./fit_hist2d.py`.
```bash
./test_fmonit.py
```


---
* Script to compare individual-SD-data (e.g. 710; see inside script) of old and new formats of intermediate files :+1:.
NOTE: This can be reproduced if we use the `./readmc-scaler3.cc` to generate the old-format.
```bash
$ cd ~/CDAS/MoIO/v5r6/cmt
$ make clean && cmt config && make
$ cd ~/auger.histos_aop/build_mc
$ ./test_old.sh 2008 9 9 # runs the interval Sep/2008-Sep/2008
# Note that the output will be in '~/auger.histos_aop/build_mc'
$ cd ~/auger.histos_aop/scl.build_AoPcorr
$ ./compare_monits.py -- --date 03/09/2008 --dir_old ~/auger.histos_aop/build_mc  # this pops-up an interactive ipython plot comparing the old and new versions. They ARE EXACTLY THE SAME.
```


---
* Scripts to compare the AoP values used by scaler-treatment, with the AoP values used in the histogram-treatment.
Note that both AoP groups are sons of the same father (i.e. the `mc..root` files), so these differences are related to the running-average-routines, which are wider for charge-histogram-treatment since the resolution in histograms is coarser than in scalers.
This explains why we see softer changes in AoP values from histograms.

To generate the figure:
```bash
$ ./compare_w.hsts.py -- -ini 03/09/2008 -end 15/09/2008 -new ../out/out.scl.build_final/scals_wAoPcorrection_wrejec.h5 -fig ./test_wh.png
```
<!--- EOF -->
