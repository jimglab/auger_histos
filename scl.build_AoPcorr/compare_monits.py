#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
import shared_funcs.funcs as sf
import funcs as ff
from datetime import datetime, timedelta
import numpy as np
from pylab import pause, find, figure, close, show
import argparse, os


#--- retrieve args
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
'-d', '--date', 
type=str, 
action=sf.arg_to_utcsec,
default='03/09/2008',
help='day to plot',
)
parser.add_argument(
'-dold', '--dir_old',
type=str,
default='/home/masiasmj/auger_ascii/monit/scaler3/{yyyy}',
help='directory of the "mc_scaler3_yyyy_mm.dat" (i.e. old-version).',
)
pa = parser.parse_args()


date = sf.utc2date(pa.date)
inp = {'date':date, 'dir_mon':'/home/masiasmj/_tmp_/intermediate/monit','stations':range(2,1999+1)}
fmh = ff.fmonit_handler(inp, params=None, verbose=False) # read monit files
print sf.utc2date(fmh.mon_t['0710'][0]), '-->',sf.utc2date(fmh.mon_t['0710'][-1])

#old = np.loadtxt('/home/masiasmj/auger_ascii/data_auger/estudios_AoP/data/scalers_wAoPcorrection_2006-2013_allarray_3pmtsON_wdatarejec_q2.5.dat')
#print old.shape
#print sf.gps2date(old[0,0]), '-->', sf.gps2date(old[-1,0])

print " --> Loading old-version..."
fname_old='{dir_old}/mc_scaler3_{yyyy}_{mm:02d}.dat'.format(yyyy=date.year, mm=date.month, dir_old=pa.dir_old)
os.system('ls -lhtr '+fname_old)
mon = np.loadtxt(fname_old)
print sf.gps2date(mon[0,1]), sf.gps2date(mon[-1,1])
# conds of old version
ccold = mon[:,0]==710
print len(find(ccold)) 
# conds of new version
PmtOk_new = fmh.mon_pmtStat['0710']==7
print len(find(PmtOk_new))

print " ---> Plotting..."
fig = figure(1, figsize=(6,4))
ax  = fig.add_subplot(111)
ax.plot(mon[ccold,1], mon[ccold,6], '-or', ms=3, lw=1, label='old')
ax.plot(fmh.mon_t['0710'][PmtOk_new]-315964800., fmh.mon_aop['0710'][PmtOk_new], '-ob',ms=3,lw=1,label='new')
ax.grid(True)
ax.legend(loc='best')
print " ---> Showing..."
show()
close(fig)

#EOF
