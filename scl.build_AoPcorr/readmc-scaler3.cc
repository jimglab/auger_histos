#include "TSDMonCal.h"
#include "TChain.h"
using namespace std;

// Simple MC data read, based on BigBrother package
int main(int argc, char** argv) {
  TSDMonCal SDMonCal;
  TSDMonCal *fSDMonCal=&SDMonCal;
  double aop_0, aop_1, aop_2;			// peak & charge avrgs from 3 PMTs

  //cout  << "# Id StartSecond Duration Dyn[0..2] Peak[0..2] TubeMask" << endl;
  for (int fn=1;fn<argc;fn++) {
    TChain* fChain = new TChain("SDMonCal");
    fChain->SetBranchStatus("fRawMonitoring.fListOfMembers",0);
    fChain->SetBranchStatus("fCalibration.fListOfMembers",0);
    fChain->Add(argv[fn]);
    fChain->SetBranchAddress("SDMonCalBranch",&fSDMonCal);
    int fNumberOfEntries = (int)fChain->GetEntries();
    cerr << "Opening " << argv[fn] << endl;
    for (int i=0; i<fNumberOfEntries; i++) {
      if (i%10000==0) cerr << i << "/" << fNumberOfEntries << endl;
      fChain->GetEvent(i);
      if (fSDMonCal->fMonitoring.fIsMonitoring != 1 || fSDMonCal->fCalibration.fIsCalibration != 1) continue;
      if (fSDMonCal->fLsId<10 ||  fSDMonCal->fLsId>1999) continue;

      TSDCalibration* c = &(fSDMonCal->fCalibration);

      aop_0 = c->fArea[0] / c->fPeak[0];
      aop_1 = c->fArea[1] / c->fPeak[1];
      aop_2 = c->fArea[2] / c->fPeak[2];

      cout  << fSDMonCal->fLsId << " " 
       << c->fStartSecond << " "
       << c->fTubeMask << " "
       << aop_0 << " "					// AoP PMT #0
       << aop_1 << " "					// AoP PMT #1
       << aop_2 << " "					// AoP PMT #2
       << (aop_0 + aop_1 + aop_2)/3.			// AoP average
       << endl;
    }
    delete fChain;
  }
  return 0;
}
