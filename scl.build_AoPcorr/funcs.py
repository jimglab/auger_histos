#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
import numpy as np
import os, sys
from scipy.io.netcdf import netcdf_file
import h5py
from numpy import (
    loadtxt, size, mean, isnan, array,
    ones, zeros, empty, nanmean, nanmedian, nanstd,
    concatenate,
)
from glob import glob
from os.path import isfile, isdir
from datetime import datetime, timedelta
#import types
from MonLib import funcs as mf
from pylab import pause, find, figure, close
import shared_funcs.funcs as sf



def data_rejection(Q, data):
    """ 
    - data rejection around arithmetic mean, outside
    Q-times the standard-deviation.
    - Any NaNs are associated to False values in output.
    """
    #mu, sigma = normfit(SCALS);
    mu, sigma = np.nanmean(data), np.nanstd(data)
    # 'Q' es el factor q eligo para decidir el "rango de rechazo"
    inf = mu - Q*sigma
    sup = mu + Q*sigma
    cc = (data>inf) & (data<sup)
    return cc


def correccion_AoP_scalers_ii(m, aop_avr, YEAR_INI, YEAR_FIN, DIR_SCALS, DIR_MONITS, Q, file_out):
    """
     - Este codigo CONCATENA y SINCRONIZA la data de scalers y monitoreo, con el fin de tener <AoP> y scalers
       en un solo archivo.
     - Este codigo depende de las salidas que genera "recupera_scalers_at_hegea.m" en
     $HOME/ccin2p3/in2p3_data/data_auger/data_monit/codigos_procesamiento/.
    """
    MIN_DATA        = 300           # nro minimo de datos q quiero para construir los scalers corregidos
    MIN             = 60.           # [seg]
    WNDW            = 30.*MIN       # ventana total en q hago promedio corrido de los monits
                                    # OJO: escoji 30min xq es +/- el maximo hueco de tiempo en q un
                                    # tanque del todo array tira un dato de AoP
    LOW_SCAL    = 1700.
    UPP_SCAL    = 2300.
    D_SCAL      = (UPP_SCAL - LOW_SCAL)/15.
    n           = 0                # tamanio de la data final
    DATA        = sf.My2DArray((8,5), dtype=np.float64)
    for YYYY in range(YEAR_INI, YEAR_FIN+1):
        print ' anio:%04d\n\n' % YYYY

        for MM in range(1, 12+1):
            print '   mes: %d\n' % MM
            # ---------------------------- leo scalers
            dir_scal    = '%s/%04d' % (DIR_SCALS, YYYY)
            fname_scal  = '%s/scals_%04d_%02d.h5' % (dir_scal, YYYY, MM)
            #data_scal   = np.loadtxt(fname_scal).T # estos scalers ya estan filtrados a un rango "razonable"
            fscl        = h5py.File(fname_scal, 'r')
            print '   scaler leido.   %s\n' % fname_scal
            n_scal      = fscl['time'].value.size
            #n_scal      = np.size(data_scal, axis=0)
            print " ----> nscal: ", n_scal
            #----------------------------- leo monits "scaler3"
            dir_monit   = '%s/%04d' % (DIR_MONITS, YYYY)
            fname_monit = '%s/mc_scaler3_%04d_%02d.dat' % (dir_monit, YYYY, MM)
            data_monit  = np.loadtxt(fname_monit)
            print '   monit leido.   %s\n' % fname_monit
            three_pmtsON =  data_monit[:,2]==7    # 3PMTs ok
            three_pmtsON &= data_monit[:,5]<4.6  
            three_pmtsON &= data_monit[:,5]>2.6   # y filtro rango razonable de AoP
            print " ----> shape: ", three_pmtsON.shape, data_monit.shape
            data_monit = data_monit[three_pmtsON,:][:,[0,1,5]] # me quedo solo con las columnas [id, gps_sec, AoP]

            for i in range(n_scal):
                t_scl       = fscl['time'][i] # [gps sec] tiempo de la data scalers
                ind         = (data_monit[:,1]>=t_scl-WNDW/2.) & (data_monit[:,1] <= t_scl+WNDW/2.) # selecc data para esta ventana
                blk_monit   = data_monit[ind,:][:,[0,2]] # bloque de data scalers para esta ventana temporal (solo id y AoP)
                if size(blk_monit, axis=0) > MIN_DATA:   # pregunto si hay mas de 'MIN_DATA' tanques en esta ventana temporal
                    IDs               = np.unique(blk_monit[:,0])   # lista de tanques q aportan c/data monit
                    n_IDs             = size(IDs, axis=0)           # nro de tanques q aportan c/data monit
                    scals_uncorr = np.empty(n_IDs)
                    for i_ in range(n_IDs):
                        scals_uncorr[i_] = fscl['scl_%04d'%id][i] 
                    aop = np.empty(n_IDs, dtype=np.float32)
                    for j in range(n_IDs):
                        ind    = blk_monit[:,0] == IDs[j]
                        aop[j] = mean(blk_monit[ind,1])

                    scals_corr = scals_uncorr - m*(aop - aop_avr) # scalers corregidos, por tanque (*)
                    #---------------------- filtro NaNs 
                    ind             = ~np.isnan(scals_corr)      # indices de los q NO son NaN, ni en 'scals_uncorr', ni en 'aop'
                    scals_corr      = scals_corr[ind]         # sin NaNs
                    aop             = aop[ind]
                    scals_uncorr    = scals_uncorr[ind]
                    #---------------- "rechazo de datos"
                    bineo_histog    = np.arange(LOW_SCAL, UPP_SCAL+D_SCAL, D_SCAL)
                    ind             = data_rejection(Q, bineo_histog, scals_corr)
                    scals_corr      = scals_corr[ind]               # scalers corregidos q no incluyen "extranjeros"
                    aop             = aop[ind]
                    scals_uncorr    = scals_uncorr[ind]
                    print " -----> other shape: ", scals_corr.shape
                    n_IDs_corr      = size(scals_corr, axis=0)               # nro de tanques, donde puedo corregir por AoP (*)
                    # (*) simultaneamente, limpio de NaNs en AoP y en scalers, y limpio de valores "extranjeros"
                    DATA[n,0]   = t_scl                 # tiempo n-esimo
                    DATA[n,1]   = n_IDs_corr
                    DATA[n,2]   = mean(aop)             # <AoP> promediado en todo el array q aporto en esta ventana
                    DATA[n,3]   = mean(scals_uncorr)    # <scalers sin corregir> promediado en el array
                    DATA[n,4]   = mean(scals_corr)      # <scalers corregidos> promediado en el array
                    if isnan(DATA[n,3]):  # me avisa si hizo un NaN (no debe ser asi!)
                        print locals().keys()
                        print ' -----> CORRUPTO!!!!!, en i:%d\n' % i
                        #return;    % termina el script

                    n += 1
                    del aop         # reseteo xq asigno elemento a elemento en el prox ciclo de "for"

    DATA = DATA[:n,:]
    np.savetxt(file_out, DATA, format='%13f')
    # (*) algunos pueden tener NaNs debido a q en 'data_scals' los hay.


class fmonit_handler:
    def __init__(self, inp, params=None, verbose=True):
        """
        NOTE: this is as adaptation from {REPO}/build_ReducedHistos/funcs.py::generate_id_histos() class.
        OJO: hay parametros aqui q deben ser cambiados a
             a mano si algunas de las dimensiones de los 
             archivos input cambian (e.g. 'self.nreduced', 
             'self.ch_0').
        """
        self.verbose    = verbose
        MIN             = 60.               # [seg] 1min
        self.yyyy       = inp['date'].year
        self.mm         = inp['date'].month
        self.dd         = inp['date'].day
        self.dir_mon    = inp['dir_mon']
        self.stations   = inp['stations']   # [numeric array]
        self.nstations  = len(inp['stations'])

        #------ agarremos la data de monitoreo
        if self.check_monit_files():        # chequeo si existen
            self.get_monit_files()      # declaro punteros a esos files
            self.grab_monit_data()      # leo la data de monitoreo
            self.files_ok = True
            #print " TERMINAMOS MONIT!"; raw_input()
        else:
            self.files_ok = False

        #------------------------------------------------
        self.nsave  = 0
    """
    (*2)    A veces, histos.cpp intenta recuperar data de un .root
            mal-cerrado.
            Por ej, en el caso del archivo "2012_06_21.root", despues
            de recuperar data, el codigo crashea sin llegar a cerrar
            el .nc que estuvo generando.
    (*3)    Lo que grabe en el archivo input "yyyy_mm_dd.nc" es una
            version reducida de los histogramas originales. Hice
            integrales parciales en anchos de 'self.nreduced'. Por
            ej, para self.nreduced=10, el archivo input tiene
            100 canales, de los 1000 originales en los .root
    """


    def check_monit_files(self):
        """ checkea si *existen* los inputs para ayer/hoy"""
        try:
            hoy = datetime(self.yyyy, self.mm, self.dd, 0,0) + timedelta(days=1)
            ayer    = hoy - timedelta(days=1)
            yyyy    = ayer.year
            mm  = ayer.month
            dd  = ayer.day
            self.fname_monit_ayer = '%s/%04d/%04d_%02d_%02d.nc' % (self.dir_mon, yyyy, yyyy, mm, dd)
            self.ok_monit_ayer  = isfile(self.fname_monit_ayer)
        except:
            self.ok_monit_ayer  = False

        self.fname_monit_hoy    = '%s/%04d/%04d_%02d_%02d.nc' % (self.dir_mon, hoy.year, hoy.year, hoy.month, hoy.day)
        self.ok_monit_hoy       = isfile(self.fname_monit_hoy)
        self.ok_monit           = (self.ok_monit_ayer or self.ok_monit_hoy)

        return self.ok_monit    # True si al menos uno de los 2 archivos existe; False otherwise.


    def get_monit_files(self):
        """ declare pointers to files (works OK together with
            self::grab_monit_data() method) """
        self.ok_both = self.ok_one = False
        try:
            print " leyendo:\n", self.fname_monit_ayer, self.fname_monit_hoy
            os.system('ls -lhtr %s %s' % (self.fname_monit_ayer, self.fname_monit_hoy))
            self.finp_monit_hoy  = netcdf_file(self.fname_monit_hoy , 'r')
            self.finp_monit_ayer = netcdf_file(self.fname_monit_ayer, 'r')
            self.ok_both = True
        except:
            try:
                self.finp_monit_ayer = netcdf_file(self.fname_monit_ayer, 'r')
                self.ok_one = True
            except:
                self.ok_monit = False # existen, pero estan corruptos
                print " files de monitoreo CORRUPTOS! :("
        print " --> ok_monit: ", self.ok_monit
        print " --> ok_both/ok_one: ", self.ok_both, "/", self.ok_one
        #raw_input()


    def grab_monit_data(self):
        """ declare pointers to data && works OK whether 'self.finp_monit_hoy' ||
        'self.finp_monit_ayer' points to somewhere or not."""
        # inicializo diccionarios:
        self.mon_t       = {}
        self.mon_aop     = {}
        self.mon_pmtStat = {} # status of PMTs
        print " self.ok_monit_ayer:", self.ok_monit_ayer
        print " self.ok_monit_hoy :", self.ok_monit_hoy
        # leo data de c/tanque
        for id in self.stations:
            ok      = False
            ok_ayer = False
            ok_hoy  = False
            #----------------- unificamos la data de ayer y hoy
            try:
                if self.ok_monit_ayer:                  # el archivo existe
                    try:
                        dataid_ayer = self.finp_monit_ayer.variables['m_id.%04d'%id].data
                        ok_ayer     = True
                    except:
                        ok_ayer     = False

                if self.ok_monit_hoy:                       # el archivo existe
                    try:
                        dataid_hoy = self.finp_monit_hoy.variables['m_id.%04d'%id].data
                        ok_hoy     = True
                    except:
                        ok_hoy     = False

                if (ok_ayer & ok_hoy):  # si este tanque tiene data para ambos dias
                    dataid = concatenate([dataid_ayer, dataid_hoy], axis=0)
                    del dataid_ayer
                    del dataid_hoy
                elif ok_ayer:           # adopto la data de uno de los 2 dias
                    dataid          = dataid_ayer
                    del dataid_ayer
                elif ok_hoy:            # o bien solo hay data de hoy o ninguno de ambos dias.
                    dataid          = dataid_hoy    # esto falla si no hay data hoy tampoco, y entonces
                    del dataid_hoy                  # dire q " no data for id".
                if ok_ayer or ok_hoy:
                    ok              = True          # llego aqui si adopte data de por lo menos uno de los 2 dias
                    if self.verbose: print " [id:%04d] ayer/hoy: %d/%d" % (id, int(ok_ayer), int(ok_hoy))
            except:
                if self.verbose: print " [id:%04d] no data :*(" % id
                #continue

            #----------------------- ahora si leo la data unificada 'dataid'
            if ok:
                #print " --> grabbing data.... "; raw_input()
                if len(dataid)>1:
                    #print " IM HERE!"
                    nid                    = size(dataid, axis=0)
                    name                   = '%04d' % id
                    self.mon_t[name]       = array(dataid[:,0])  # (*1)
                    self.mon_pmtStat[name] = array(dataid[:,1])  # (*1)
                    self.mon_aop[name]     = array(dataid[:,2])  # (*1)
                else:
                    print " [id:%04d] no data" % id

        #--- cerramos archivos de monitoreo
        if self.ok_both:
            self.finp_monit_hoy.close()
            self.finp_monit_ayer.close()
        if self.ok_one:
            self.finp_monit_ayer.close()
        """
        (*1) columnas en la matriz 'm_%04d' de los archivos de monitoreo:
            col0: utc seg
            col1: flag del tanque (estado de los pmts; 7, 15, 23, 31, etc for 3PMTs=ok)
            col2: <AoP>_{3pmts}
        """


def gps2date(t):
    date_utc = datetime(1970, 1, 1, 0, 0, 0, 0)
    t_utc = t + 315964800 # [utc sec]
    date = date_utc + timedelta(days=(t_utc/86400.))
    return date


def date2gps(date):
    date_utc = datetime(1970, 1, 1, 0, 0, 0, 0)
    utcsec = (date - date_utc).total_seconds() # [utc sec]
    gpssec = utcsec - 315964800  # [gps sec]
    return gpssec


def correccion_AoP_scalers(m, aop_avr, date_range, DIR_SCALS, DIR_MONITS, Q, fname_out, sdarray=np.arange(2, 1999+1), aop_lim=(2.6, 4.6), MIN_TANKS=300):
    """
    - this function CONCATENATES and SYNC the scaler and monitoring data, with
      the aim of having AoP and scalers in one single file.

    - Este codigo depende de las salidas que genera "recupera_scalers_at_hegea.m" en
     $HOME/ccin2p3/in2p3_data/data_auger/data_monit/codigos_procesamiento/.

    inputs:
    - sdarray: array sobre el cual se hace la correccion (todo el SD 
                array por defecto).
    """
    MIN         = 60.           # [seg]
    WNDW        = 30.*MIN       # ventana total en q hago promedio corrido de los monits
    GPS2UTC     = 315964800     # where utc=gps+GPS2UTC
                                # OJO: escoji 30min xq es +/- el maximo hueco de tiempo en q un
                                # tanque del todo array tira un dato de AoP
    aop_min, aop_max = aop_lim
    n           = 0                # tamanio de la data final
    DATA        = sf.My2DArray((8,5), dtype=np.float64)
    OneDay      = timedelta(days=1)

    date_ini    = date_range[0] #datetime(YEAR_INI, 1, 1)
    date_end    = date_range[1] #datetime(YEAR_FIN, 12, 31)
    date        = date_ini
    yyyy,mm,dd  = date.year, date.month, date.day
    #--- gral data for output
    fname_out_tmp = fname_out+'__tmp__'
    fo = h5py.File(fname_out_tmp, 'w')
    fo['dir_scals'] = DIR_SCALS
    fo['dir_monit'] = DIR_MONITS
    fo['sd_array']  = sdarray
    fo['q_rejection'] = Q        # rejection factor
    fo['AoP_slope_correction'] = m

    #--- column numbers
    col = {
        'gps'       : 0,
        'ntanks'    : 1,
        'aop'       : 2,
        'sc_uncorr' : 3,
        'sc_corr'   : 4,
    }

    while date <= date_end:
        if yyyy!=date.year or mm!=date.month or date==date_ini:
            print " year/month: ", date.year, date.month
            if mm!=date.month or date==date_ini:
                # ---------------------------- leo scalers
                fname_scal  = '%s/scals_%04d_%02d.h5' % (DIR_SCALS, date.year, date.month) # estos scalers ya estan filtrados a un rango "razonable"
                fscl        = h5py.File(fname_scal, 'r')
                print '   scaler leido.   %s\n' % fname_scal
                #--- reset buffer
                n    = 0            # size of data for today
                DATA = sf.My2DArray((8,5), dtype=np.float64)

        yyyy, mm, dd = date.year, date.month, date.day
        #--- extraigo del file de scalers, solo el extracto para hoy
        tscl_   = fscl['time'].value # [gps sec]
        gpshoy  = date2gps(date)     # [gps sec]
        cc      = (tscl_>=gpshoy) & (tscl_<=(gpshoy+86400.))
        tscl    = fscl['time'][cc]   # [gps sec]
        scl     = {} # dict for all array (scalers for today!)
        for Id in range(2, 1999+1): # TODO: replace with 'sdarray'
            scl[Id] = fscl['scl_%04d'%Id][cc]

        nscl = tscl.size #nmbr of scalers for today (size of tscl[:] & scl[Id][:])
        print " datehoy: ", date
        if nscl<=1: # hay scalers para hoy?
            date += OneDay
            continue

        #---------------------- leo monits "scaler3"
        inp = {
        'date'      : date,
        'dir_mon'   : DIR_MONITS,
        'stations'  : sdarray,
        }
        fmh = fmonit_handler(inp, params=None, verbose=False)
        if not fmh.files_ok:
            date += OneDay
            continue
        print " ---> syncing monits w/ scalers... @", date
        for i in range(nscl):
            ts     = tscl[i] # [gps sec]
            ntanks = 0       # tank counter
            ind    = {}      # conditionals for each tank
            # NOTE: 'ind[Id]' filters-in data:
            # - within window (t-WNDW/.2, t+WNDW/2.)
            # - with 3 PMTs with Ok status in tank 'Id'
            # - with AoP values within (`aop_min`, `aop_max`)
            for Id in map(int, fmh.mon_t.keys()): #range(2, 1999+1):
                tmon    = fmh.mon_t['%04d'%Id]-GPS2UTC  # [gps sec]
                cc      = (tmon>=ts-WNDW/2.) & (tmon<=ts+WNDW/2.)
                fpmt    = fmh.mon_pmtStat['%04d'%Id]
                #cc     &= ((fpmt==7) | (fpmt==15)) | (fpmt==23)
                cc     &= (fpmt==7)
                aop     = fmh.mon_aop['%04d'%Id]
                cc     &= (aop>aop_min) & (aop<aop_max)
                if len(find(cc))>1: # tiene data este ID?
                    ind[Id] = cc
                    ntanks += 1

            #--- check before slicing data below
            if ntanks<MIN_TANKS:
                continue # not enough tanks!, so abort && next 'ts'

            sc_uncorr = np.empty(ntanks, dtype=np.float32)
            #--- grab scals from all array for today
            IDs = ind.keys() # IDs con data
            for j, Id in zip(range(len(IDs)), IDs): 
                sc_uncorr[j] = scl[Id][i] 

            aop = np.empty(ntanks, dtype=np.float32)
            for j, Id in zip(range(len(IDs)), IDs):
                aop[j] = fmh.mon_aop['%04d'%Id][ind[Id]].mean()

            sc_corr   = sc_uncorr - m*(aop - aop_avr) # scalers corregidos por tanque (*)
            #--- reject outliers
            cc        = data_rejection(Q, sc_corr) # is this necessary??
            sc_uncorr = sc_uncorr[cc]
            sc_corr   = sc_corr[cc]
            aop       = aop[cc]
            ncorr     = sc_corr.size # nmbr of tanks where I corrected for AoP (*)
            if ncorr<2:
                continue # correction is worth for more than 1 tank

            #(*): simultaneously cleaned by NaNs in AoP and scalers, and cleaned from scaler outliers.
            DATA[n,col['gps']]        = ts              # [gps sec]
            DATA[n,col['ntanks']]    = ncorr #ntanks   # nmbr of tanks
            DATA[n,col['aop']]       = mean(aop)       # <..>_{array} en esta ventana
            DATA[n,col['sc_uncorr']] = mean(sc_uncorr) # <..>_{array} "
            DATA[n,col['sc_corr']]   = mean(sc_corr)   # <..>_{array} "
            if isnan(DATA[n,3]):
                print locals().keys()
                raise SystemExit(\
                " ----> CORRUPTO!! en i:%d; date: %r" % (i, date) +\
                " ----> sc_uncorr: %r" % sc_uncorr +\
                " ----> sc_uncorr.shape: %r" % sc_uncorr.shape \
                )
            n += 1
        #we finished for today, next day
        date    += OneDay
        #--- save data
        if (date.month!=mm) or (date-OneDay==date_end):
            dpath = 'data/%04d/%02d' % (yyyy,mm)
            print " ---> saving: "+dpath
            fo[dpath] = DATA[:n,:]
            for name, _col in col.iteritems():
                fo[dpath].attrs[name] = _col

    fo.close()
    # if everything worked silently, we build the right file 'fname_out'
    os.system('mv %s %s'%(fname_out_tmp,fname_out))
    # (*) algunos pueden tener NaNs debido a q en 'data_scals' los hay.


def unify_all(fname_out, wsize):
    """ function to unify all the
        output .h5 of all processors.
    NOTE: This works w/ massive_AoP_correc_mpi.py
    """
    fout = h5py.File(fname_out, 'w')
    print " [*] Unifying temporary files into a single file...\n"
    for r in range(wsize): # iterate over ranks
        fnm_inp = fname_out[:-3]+'_rank.%02d.h5'%r
        print " [*] reading temporary file: "+fnm_inp
        finp = h5py.File(fnm_inp, 'r')

        dnames = [] # names of directories inside the .h5 file
        cont=[] # list of contents
        finp.visit(cont.append) # save the tree structure into 'cont'
        for c in cont:           # iterate over each group
            if len(c.split('/'))==3:
                fout[c] = finp[c].value # only those of depth=3
                # also copy the attributes (whether exist or not)
                fout[c].attrs.update(finp[c].attrs)
                dname = c.split('/')[0] # directory name
                if dname not in dnames: # collect directory names
                    dnames.append(dname)

        for nm in finp.keys():
            # we want to copy the keynames that are NOT
            # directories, and those that are NOT already
            # in the output file. WARNING: these keynames 'nm'
            # are ASSUMED to have the same value in all 
            # the partial-files!
            if nm not in dnames+fout.keys():
                fout[nm] = finp[nm].value

        #--- close partial file
        finp.close()
        print " [*] removing partial/temporary file."
        os.system('rm {fnm}'.format(fnm=fnm_inp))

    print " [+] We generated: "+fout.filename
    fout.close()
    print " [*] FINISHED UNIFYING OUTPUT :D"


class Correl_wAoP(object):
    def __init__(self, **kargs):
        """
        we take the necessary arguments, as we build more
        methods to 'self'
        """
        for name, value in kargs.iteritems():
            setattr(self,name,value)

    def build_hist2d(self, date_range, sdarray, aop_lim, scl_lim, bins=50, MIN_TANKS=300):
        """
        - Este codigo CONCATENA y SINCRONIZA la data de scalers y monitoreo, 
          con el fin de tener <AoP> y scalers en un solo archivo.
        - Este codigo depende de las salidas que genera 
          "recupera_scalers_at_hegea.m" en
          $HOME/ccin2p3/in2p3_data/data_auger/data_monit/codigos_procesamiento/.
        Inputs:
        - sdarray: array sobre el cual se hace la correccion (todo el SD 
                array por defecto).
        - date_range: tuple of two datetime.datetime objects, to delimit the
                range of time for the 2d-histogram build.
        - MIN_TANKS: minimum number of tanks that I ask for, involved at each
                time-window `(tmon>=ts-WNDW/2.) & (tmon<=ts+WNDW/2.)`, and 
                that satifies `fpmt==7` && with "AoPs in reasonable range".
        ** we need **:
        self.dir_scals, self.dir_monits
        """
        dir_scals   = self.dir_scals
        dir_monits  = self.dir_monits
        aop_lo,aop_hi = aop_lim
        # range for 2d-histogram building
        RANGE   = (aop_lim,scl_lim)

        MIN         = 60.      # [seg]
        WNDW        = 30.*MIN  # ventana total en q hago promedio corrido de los monits
        # NOTE: escoji 30min xq es +/- el maximo hueco de tiempo en q un
        # tanque del todo array tira un dato de AoP
        GPS2UTC     = 315964800 # where utc=gps+GPS2UTC
        OneDay      = timedelta(days=1)

        date_ini    = date_range[0] #datetime(YEAR_INI, 1, 1)
        date_end    = date_range[1] #datetime(YEAR_FIN, 12, 31)
        date        = date_ini
        yyyy,mm,dd  = date.year, date.month, date.day
        H2D = np.zeros((bins,bins)) # 2d-hist
        # to accumulate all the AoP from each station, in this
        # window date_ini-date_end
        buff_aop = {'aop_sum': 0.0, 'aop_n': 0}
        while date <= date_end:
            if yyyy!=date.year or mm!=date.month or date==date_ini:
                print " year/month: ", date.year, date.month
                if mm!=date.month or date==date_ini:
                    #-------------- read scalers (for this month)
                    fname_scal  = '%s/scals_%04d_%02d.h5' % (dir_scals, date.year, date.month) # estos scalers ya estan filtrados a un rango "razonable"
                    fscl        = h5py.File(fname_scal, 'r')
                    print '   scaler leido.   %s\n' % fname_scal

            yyyy, mm, dd = date.year, date.month, date.day
            #--- extraigo del file de scalers, solo el extracto para hoy
            tscl_   = fscl['time'].value # [gps sec]
            gpshoy  = date2gps(date)     # [gps sec]
            cc      = (tscl_>=gpshoy) & (tscl_<=(gpshoy+86400.))
            tscl    = fscl['time'][cc]   # [gps sec]
            scl     = {} # dict for all array (scalers for today!)
            for Id in range(2, 1999+1): # TODO: replace with 'sdarray'?
                scl[Id] = fscl['scl_%04d'%Id][cc]

            nscl = tscl.size #nmbr of scalers for today (size of tscl[:] & scl[Id][:])
            #print " ---> nscl, gpshoy, tscl_[0]: ", nscl, gpshoy, tscl_[0]
            print " datehoy: ", date
            if nscl<=1: # hay scalers para hoy?
                date += OneDay
                continue

            #----------------- read monits "scaler3"
            inp = {
            'date'      : date,
            'dir_mon'   : dir_monits,
            'stations'  : sdarray,
            }
            fmh = fmonit_handler(inp, params=None, verbose=False)
            if not fmh.files_ok:
                date += OneDay
                continue

            #--- check if we have cache for today
            if os.path.isfile(self.fname_cache):
                fc = h5py.File(self.fname_cache, 'r')
                ds_name = '%04d/%02d/%02d' % (yyyy,mm,dd)
                if ds_name in fc:
                    # contribution to the 2D-hist
                    assert fc[ds_name][:,:].sum() > 0.0, \
                    ' [-] error on %r\n'% date
                    H2D  += fc[ds_name][:,:]
                    fc.close()
                    date += OneDay
                    continue
                else:
                    # else, go ahead and calculate the 2D-hist 
                    # contribution for today
                    fc.close()

            # 2D-hist contribution from today's data
            H2D_today = np.zeros((bins,bins))
            print " ---> syncing monits w/ scalers... @", date
            # iterate over the timestamps for today
            for i in range(nscl):
                ts     = tscl[i] # [gps sec]
                ntanks = 0       # tank counter (for this time-stamp)
                ind    = {}      # conditionals for each tank
                # NOTE: 'ind[Id]' filters-in data:
                # - within window (t-WNDW/.2, t+WNDW/2.)
                # - with 3 PMTs with Ok status in tank 'Id'
                # - with reasonable AoP values
                for Id in map(int, fmh.mon_t.keys()): #range(2, 1999+1):
                    tmon    = fmh.mon_t['%04d'%Id]-GPS2UTC  # [gps sec]
                    cc      = (tmon>=ts-WNDW/2.) & (tmon<=ts+WNDW/2.)
                    fpmt    = fmh.mon_pmtStat['%04d'%Id]
                    #cc     &= ((fpmt==7) | (fpmt==15)) | (fpmt==23)
                    cc     &= (fpmt==7)
                    aop     = fmh.mon_aop['%04d'%Id]
                    cc     &= (aop>aop_lo) & (aop<aop_hi)
                    if len(find(cc))>1: # tiene data este ID?
                        ind[Id] = cc
                        ntanks += 1

                #--- check before slicing data below
                if ntanks < MIN_TANKS:
                    continue # not enough tanks!, so abort && next 'ts'

                sc_uncorr = np.empty(ntanks, dtype=np.float32)
                #--- grab scals from all array for today
                IDs = ind.keys() # IDs con data
                assert ntanks == len(IDs)
                for j, Id in zip(range(ntanks), IDs): 
                    sc_uncorr[j] = scl[Id][i] 

                aop = np.empty(ntanks, dtype=np.float32)
                for j, Id in zip(range(ntanks), IDs):
                    # average of all the data-point for this station with
                    # identification number 'Id', for the time-window given
                    # by the resolution of 'tscl[:]'
                    aop[j] = fmh.mon_aop['%04d'%Id][ind[Id]].mean()

                H2D_today += np.histogram2d(
                    aop, sc_uncorr,
                    bins=(bins,bins), 
                    range=RANGE, 
                    normed=False,
                    )[0]

                # accumulate, so we calculate the global average later
                buff_aop['aop_sum'] += aop.sum()
                buff_aop['aop_n']   += aop.size
                # we should know if we are accumulating garbage
                assert ~np.isnan(buff_aop['aop_sum'])

            # NOTE: if we reached this point, is because the daily H2D
            # doesn't exist in cache for this 'date', so save this 'H2D_today'
            # in cache.
            if self.fname_cache:
                fc = h5py.File(self.fname_cache, 'a')
                ds_name = '%04d/%02d/%02d' % (yyyy,mm,dd)
                fc.create_dataset(
                    ds_name,
                    dtype = 'f',
                    shape = (bins,bins),
                    fillvalue = 0.0,
                    compression = 'gzip',
                    compression_opts = 9,
                )
                # save daily contribution
                fc[ds_name][...] = H2D_today
                fc.close()

            H2D  += H2D_today
            date += OneDay

        out = {
        'H2D'       : H2D,
        'AoP_buff'  : buff_aop,
        }
        return out

def unify_h2d_cache(fcache_unif, fcaches, fname_out, wsize, AoP_buff, period, **meta):
    """ unify all the sub-caches in one single cache file.
    NOTE: this is a task for the root processor only.
    """
    fout = h5py.File(fcache_unif, 'w')
    for r in range(wsize):
        # open the sub-cache
        finp = h5py.File(fcaches[r], 'r')
        cont=[] # list of contents of the sub-cache
        finp.visit(cont.append) # save the tree structure into 'cont'
        for c in cont:           # iterate over each group
            try:
                if len(c.split('/'))==3:
                    print " [r:%d] copy '%s'" % (r, c)
                    fout[c] = finp[c].value # only those of depth=3
                    #finp.copy(c, fout)
            except ValueError:
                print "\n[-] ERROR: can't write key '%s' to %s\n"%(c,fout.filename)
                print "[*] current keys:\n - " + '\n - '.join(fout.keys())
                print ""
                sys.exit(1)

        finp.close()
        print(" [*] removing partial file: "+fcaches[r])
        os.system('rm {fname}'.format(fname=fcaches[r]))

    #--- metadata
    for nm in meta.keys():
        fout['meta/%s' % nm] = meta[nm]
    fout['meta/aop_mean'] = 1.*AoP_buff[0]/AoP_buff[1]
    fout['meta/aop_n']    = 1.*AoP_buff[1]
    fout['meta/period']   = [ _.strftime('%d-%m-%Y %H:%M') for _ in period ]

    print "\n [+] We generated: "+fout.filename
    fout.close()
    _right_now = datetime.now().strftime("%d %b %Y %H:%M:%S")
    print "\n ----> FINISHED UNIFYING OUTPUT @ " + _right_now +'\n'


def slice_h2d(fcache_unif, fname_cache, period, metakeys):
    """
    slice the all datasets from 'fcache_unif', that
    fall within the sub-period (ini-end), and copy
    them into the sub-cache 'fname_cache'.
    """
    fu   = h5py.File(fcache_unif, 'r')
    fc   = h5py.File(fname_cache, 'w')
    nke  = 0 # number of KeyError occurrences
    nall = 0 # number of all queried datasets

    # let's decide in which window (ini:end) should we query 'fu', so
    # that we avoid unfair exception counts in 'nke'
    _speriod = fu['meta/period'].value # 2-list of time borders
    FDATE = '%d-%m-%Y %H:%M'
    if datetime.strptime(_speriod[0], FDATE) >= period[0]:
        ini = datetime.strptime(_speriod[0], FDATE)
    else:
        ini = period[0]
    if datetime.strptime(_speriod[1], FDATE) <= period[1]:
        end = datetime.strptime(_speriod[1], FDATE)
    else:
        end = period[1]

    date = ini
    while date <= end:
        dd, mm, yyyy = date.day, date.month, date.year
        ds_path = '%04d/%02d/%02d'%(yyyy,mm,dd)
        nall += 1
        try:
            fc[ds_path] = fu[ds_path].value # only those of depth=3
        except KeyError as err:
            nke += 1
            # sometimes, the entry 'ds_path' doesn't exists
            # because either of these thing happened when building 
            # 'fu' in the routine 'build_hist2d()' for this day:
            # - there wasn't enough valid Scaler data,
            # - there wasn't monitoring data (check 'files_ok' flag),
            # so it's okey to just report/log this and go on.
            if err.message.startswith('Unable to open object'):
                print " [-] failed opening '%s' from '%s'\n" % (ds_path,
                    os.path.basename(fu.filename))
            else:
                raise err

        date += timedelta(days=1)

    # metadata too
    for mk in metakeys:
        fc['meta/'+mk] = fu['meta/'+mk].value

    fc.close()
    fu.close()
    return nke, nall


def split_h2d_cache(fcache_unif, fcaches, period, wsize, metakeys):
    """
    split the cache file into several cache-files; one
    for each processor.
    fcaches : filenames of the sub-caches (one cache file per processor)
    wsize   : total number of processors
    """
    # split the global time period
    date_bd  = sf.equi_dates(period[0], period[1] + timedelta(days=1), wsize)
    # list of cache file for each proc
    ini, end   = [], []
    for rank in range(wsize):
        # build sub-periods (one for each proc)
        ini += [ date_bd[rank] ]
        end += [ date_bd[rank+1] - timedelta(days=1) ]

    print "\n [*] splitting cache file into %d files...\n" % wsize
    # total number of KeyError (with a 'Unable to open...' message) exceptions
    nke  = 0 
    # total number of queried dataset to 'fcache_unif'
    nall = 0
    # slice the data for each sub-period
    for rank in range(wsize):
        print "   [*] building %s ...\n" % fcaches[rank]
        _e, _a = slice_h2d(
            fcache_unif, 
            fcaches[rank], 
            [ini[rank], end[rank]],
            metakeys,
        )
        nke  += _e
        nall += _a

    print "\n [+] finished building the sub-caches from %s\n" % fcache_unif
    # fraction of KeyError exception ocurrences
    fke = 1.*nke/nall
    print "\n [*] fraction of KeyError occurrences: %2.4g\n" % fke

    # return bad status if the KeyError fraction is too high
    if fke > 0.2:
        print "\n [!] this is weird! we have a lot to failed dataset queries.\n"
        print   " [*] queried input file:\n %s\n" % fcache_unif
        return 1

    return 0

#EOF
