<!--- ################################### -->
## Meta builds

Meta-scripts to calls other script that belong to different processing stages.
This meta-scripts [are supposed to] call them in a coherent sequence to build the corrected data.
If specified, also generates log files for debug/monitoring information, as well as auxiliares `.png` figures for qualitative "health" checks.




<!--- ################################### -->
## TODO

- [ ] Get the whole Anaconda installation environment into a Docker container.

- [ ] Once, these scripts are tested on runs in host, wrap whole thing to be run 
      inside containers.


<!--- EOF -->
