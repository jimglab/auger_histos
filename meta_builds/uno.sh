#!/bin/bash

#--- load env variables
source makefile.in

function report_pid(){
    # Print status as long as the process still exists.
    # Once every second.
    local pid=$1
    local freq=$2  # print every 'freq' seconds
    while [[ $(ps aux | awk '{print $2}' | grep $pid) == "$pid" ]]; do
        tail -20 $flog
        sleep $freq
    done
}

function exec_and_watch(){
    # we need arguments
    [[ $# -lt 2 ]] && {
        echo -e "\n [-] ERROR: we need 2 arguments!\n";
        return 1;
    }
    local flog="$1"
    local CMD="$2"
    
    mkdir -p $(dirname $flog)

    # execute the thing
    bash -c "${CMD}" &

    local pid=$! # grab the PID of the job

    # print the runlog path
    echo -e "\n [*] runlog (PID: $pid) :\n $flog\n"
    ## in the meantime, report status
    sleep 2 && tail -f $flog &
    local pid_tail=$!

    # when the process finishes:
    # 1) sucessfully, then touch the output.
    # 2) with and error exit code: print error msg && exit.
    wait $pid
    #echo -e "\n [**] the PID: $pid FINISHED!!!\n"
    exitcode=$?
    kill ${pid_tail} > /dev/null 2>&1

    # report runlog path again
    echo -e "\n [*] ${FUNCNAME[1]} :: runlog:\n $flog\n"

    # NOTE: the FUNCNAME points to the function that is called
    # this function. That info is more important than refering
    # to the current stack (because this is a generic function!).
    if [[ $exitcode -eq 0 ]]; then
        echo -e "\n [+] finished ok @ ${FUNCNAME[1]}\n"
    else
        echo -e "\n [-] sthing wen't wrong @ ${FUNCNAME[1]}\n"
        return 1
    fi
}

function docker__run_auger(){
    # runs container that:
    #   - compiles histos code
    
    #--- grab args
    [[ $# -eq 0 ]] && {
        # TODO: print help.
        echo -e "\n [-] we need arguments! XD\n";
        return 1;
    }
    while [[ $# -gt 0 ]]; do
        key="$1"
        case $key in
            -DirRep)
            DirRep="$2"
            shift; shift
            ;;
            -DirSrc)
            DirSrc="$2"     # read data from here (original .root)
            shift; shift
            ;;
            -DirDst)
            DirDst="$2"     # output dir
            shift; shift
            ;;
            -DirLog)
            DirLog="$2"     # to save run logs
            shift; shift
            ;;
            -cname)
            CNAME="$2"
            shift; shift
            ;;
            -CMD)
            CMD=${@:2}  # read all the remaining stuff
            echo -e "\n [*] run command:\n $CMD\n"
            for _i in $(seq 1 $#); do
                shift
            done
            ;;
            *)
            echo -e "\n [-][$0] ERROR: bad argument!\n"
            return 1
        esac
    done

    echo " DirRep   : $DirRep"
    echo " DirSrc   : $DirSrc"
    echo " DirDst   : $DirDst"
    echo " DirLog   : $DirLog"

    DockerImage="lamp/auger:hegea"
    #DockerImage=lamp/auger:hegea_and_conda
    
    docker run -d \
        -e DISPLAY=unix$DISPLAY \
        -v /tmp/.X11-unix:/tmp/.X11-unix \
        -v /dev/shm:/dev/shm \
        --cap-add=SYS_ADMIN \
        --security-opt seccomp=${AUGER_REPO}/the-docker-way/sec_config.json \
        --name=$CNAME \
        -v ${DirRep}:/auger_repo \
        -v ${DirSrc}:/DataInp \
        -v ${DirDst}:/DataOut \
        -v ${DirLog}:/runlogs \
        -u $(id -u):$(id -g) \
        $DockerImage \
        bash -c "$CMD"
        # NOTE: $CMD can be e.g.:
        #/auger_repo/build_Histos/massive.sh \
        #    -DateIni 01 01 2006 \
        #    -DateEnd 31 12 2007
}

#+++++++++++++++++++++++++++++++++++++++++++++++
# BEGIN: HISTOS ++++++++++++++++++++++++++++++++

# NOTE: this is a **heavy** work, but there's no parallel 
# processing.
function build_Histos(){
    # prefix for all the log files below
    local flog_pref="$DirLogs/${FUNCNAME[0]}/`date +%d%b%Y_%H:%M`_"
    local WID CNAME yyyy nf flog

    mkdir -p ${hdir_dst2}
    for yyyy in $(seq ${yyyy_ini} ${yyyy_end}); do
        # check if we already have files for this year && don't 
        # re-process && go ahead with the next year
        nf=$(ls -l ${hdir_dst2}/$yyyy/* | wc -l 2> /dev/null)
        [[ $nf -gt 0 ]] && {
            echo -e "\n [*] ${hdir_dst2}/$yyyy already have nonzero files: nothing to do here.\n";
            continue;
        }

        # container name
        CNAME="auger__${FUNCNAME[0]}_${yyyy}"
        # up the container
        echo -e "\n [*] starting container $CNAME ..."
        # NOTE: if there are files inside ${hdir_dst2}/$yyyy, this 
        # processing will *not* be executed.
        docker__run_auger \
            -DirRep ${AUGER_REPO} \
            -DirSrc ${hdir_src2} \
            -DirDst ${hdir_dst2} \
            -DirLog "${DirLogs}/${FUNCNAME[0]}" \
            -cname $CNAME \
            -CMD /auger_repo/build_Histos/massive.sh \
                -DateIni 01 01 ${yyyy} \
                -DateEnd 31 12 ${yyyy}

        # grab the stdout of the container
        flog="${flog_pref}_${yyyy}.log"
        docker logs -f $CNAME > $flog &
        echo -e "\n [*] runlog:\n $flog\n"
        # job id to keep track of the container status
        WID[${yyyy}]=$!

        # give the container a time for the compiling to finish, before
        # the next one cleans && compiles too.
        sleep 10
    done

    # keep track if any container has finished
    while [[ ${#WID[@]} -gt 0 ]]; do
        # wait a while before next check
        sleep 45
        for yyyy in $(seq ${yyyy_ini} ${yyyy_end}); do
            # if we deleted this PID from our list 
            # already, move on
            [[ ! -v "WID[$yyyy]" ]] && continue

            # Then this PID is still in our list.
            # Check if process is still in our system.
            if ! kill -0 ${WID[$yyyy]}; then
                echo -e "\n [*] job finished for year: $yyyy"
                # remove this PID from our list
                unset "WID[$yyyy]" 
                # remove the dangling container
                docker rm "auger__${FUNCNAME[0]}_$yyyy"
            fi
        done

        echo -e " [*] $(date "+%d/%b/%Y %H:%M:%S"), pending ${#WID[@]} running containers...\n"
    done

    # we are done!
    echo -e "\n [*] $(date +%d%b%Y-%H:%M:%S): ALL CONTAINERS EXITED.\n"
    echo -e " [*] logs are with prefix:\n ${flog_pref}\n"
}

# Do:
# * merge the charge-histograms and monit data (also perform 
#   the necessary time syncronization before merge).
# * make ``multiplet'' corrections.
# * filter those charge-histograms with bad PMT-flag (using 
# 	monit/callibration data values), and AoP outliers.
# * make partial integrals of the histogram counts, in a total 
# 	of 20 bins of deposited energy.
function build_ReducedHistos(){
    local flog="$DirLogs/build_ReducedHistos/${FUNCNAME[0]}__`date +%d%b%Y_%H:%M`.log"

    # NPROC: number of processors to run in parallel.
    local NPROC=${1:-4}
    # NOTE: the whole time range will be equi-partitioned giving
    # a partition to each processor.
    echo -e "\n [*] Running ${FUNCNAME[0]} in ${NPROC} processors.\n"

    exec_and_watch \
        $flog \
        "time mpirun -n ${NPROC} \
            ${AUGER_REPO}/build_ReducedHistos/gral_mpi.py -- \
            --InpHsts ${hdir_dst2} \
            --InpMon ${sdir_dst2} \
            --dir_dst ${hdir_dst3} \
            -ini ${dd_ini}/${mm_ini}/${yyyy_ini} \
            -end ${dd_end}/${mm_end}/${yyyy_end} \
            > $flog 2>&1"
}

# Calculate average values (and its associated errors and 
# medians) over the SD array. For this, it makes quality 
# cuts based on:
# * the status of the 3 PMTs on every SD detector.
# * the position of the 1st peak in the charge histogram of each 
#   detector, at each timestamp.
# * number of tanks entering in the array average.
# * AoP array-average values.
# * among other stuff (see ICRC-2015 proceeding)
function build_array_avrs(){
    local flog="$DirLogs/build_array_avrs/${FUNCNAME[0]}__`date +%d%b%Y_%H:%M`.log"

    # NPROC: number of processors to run in parallel.
    local NPROC=${1:-4}
    # NOTE: the whole time range will be equi-partitioned giving
    # a partition to each processor.
    echo -e "\n [*] Running ${FUNCNAME[0]} in ${NPROC} processors.\n"

    exec_and_watch \
        $flog \
        "time mpirun -n ${NPROC} \
            ${AUGER_REPO}/build_array.avrs/test_2006.py -- \
            -src ${hdir_dst3} \
            -dst ${hdir_dst4} \
            -m shape.ok_and_3pmt.ok \
            -ini ${dd_ini}/${mm_ini}/${yyyy_ini} \
            -end ${dd_end}/${mm_end}/${yyyy_end} \
            > $flog 2>&1"
}


# Build time-averaged charge-histograms (i.e. profiles of 
# count rates vs energy)
function build_avr_histos(){
    local flog="$DirLogs/build_avr_histos/${FUNCNAME[0]}__`date +%d%b%Y_%H:%M`.log"

    # NOTE: it's assumed that we are processing entires years.
    exec_and_watch \
        $flog \
        "time ${AUGER_REPO}/build_avr_histos/test_2006.py -- \
            -src ${hdir_src5} \
            -ini ${dd_ini}/$((${mm_ini}+2))/${yyyy_ini} \
            --nmonth 4 \
            --ntmonth $((${nyears}*12)) \
            --maskname shape.ok_and_3pmt.ok \
            -fo ${hfname_out5} \
            > ${flog} 2>&1"
}

# Build 2D-histograms of correlations between rates vs AoP
function code_hist2d_i(){
    local flog="$DirLogs/code_hist2d/${FUNCNAME[0]}__`date +%d%b%Y_%H:%M`.log"

    # --usecache: to keep track of the 2D-histogram contribution
    # of each day, in an auxiliary HDF5 file.
    exec_and_watch \
        $flog \
        "time ${AUGER_REPO}/code_hist2d/test_build.H2d.txt.py -- \
            -src ${hdir_dst4}/15min \
            -fi ${hfname_out5} \
            -dst ${hdir_dst6} \
            -m shape.ok_and_3pmt.ok \
            -bins 50 \
            -rx 2.6 4.6 \
            -ry 0.7 1.4 \
            -mintank 150 \
            -ini ${dd_ini}/${mm_ini}/${yyyy_ini} \
            -end ${dd_end}/${mm_end}/${yyyy_end} \
            --usecache \
            > $flog 2>&1"

    touch -c ${hdir_dst6}
}

# fits of correlations between rates vs AoP
function code_hist2d_ii(){
    local flog="$DirLogs/code_hist2d/${FUNCNAME[0]}__`date +%d%b%Y_%H:%M`.log"

    # dir for plots
    local DirPlots=${hdir_dst6}/fits
    mkdir -p $DirPlots

    exec_and_watch \
        $flog \
        "time ${AUGER_REPO}/code_hist2d/make_fits.py --pdb -- \
            -src ${hdir_dst6} \
            -fo ${hfname_out7} \
            -m shape.ok_and_3pmt.ok \
            -cbmax 6e6 \
            -plot $DirPlots \
            > $flog 2>&1"
}

function code_aop_correction(){
    local flog="$DirLogs/code_aop.correction/${FUNCNAME[0]}__`date +%d%b%Y_%H:%M`.txt"

    exec_and_watch \
        $flog \
        "time ${AUGER_REPO}/code_aop.correction/test.py -- \
            -src ${hdir_src5} \
            -fi1 ${hfname_out5} \
            -fi2 ${hfname_out7} \
            -dst ${hdir_dst8} \
            -ini ${dd_ini}/${mm_ini}/${yyyy_ini} \
            -end ${dd_end}/${mm_end}/${yyyy_end} \
            > $flog 2>&1"

    touch -c ${hdir_dst8}
}

# just builds the pressure data
function build_weather_i(){
    local flog="${DirLogs}/build_weather/${FUNCNAME[0]}__`date +%d%b%Y_%H:%M`.txt"

    # number of processor where to run all these years of data
    # NOTE: the run will be distributed as 1 year of data per processor.
    local NPROC=${1:-4}
    echo -e "\n [*] Running ${FUNCNAME[0]} in ${NPROC} processors.\n"

    exec_and_watch \
        $flog \
        "time mpirun -np ${NPROC} ${AUGER_REPO}/build_weather/tt_mpi.py -- \
            -fi ${hfname_inp9} \
            -fo ${wfname_out9} \
            -res 5 \
            -ini ${date_ini_press} \
            -end ${date_end_press} \
            > $flog 2>&1"
}

# Just take scaler and pressure data and merge them 
# into a single file.
function build_aop_corr_phys(){
    local flog="$DirLogs/build_aop.corr_phys/${FUNCNAME[0]}__`date +%d%b%Y_%H:%M`.log"

    exec_and_watch \
        $flog \
        "time ${AUGER_REPO}/build_aop.corr_phys/test.py -- \
            -src ${hdir_dst8} \
            -fi ${wfname_out9} \
            -dst ${hdir_dst10} \
            -ini ${dd_ini}/${mm_ini}/${yyyy_ini} \
            -end ${dd_end}/${mm_end}/${yyyy_end} \
            > $flog 2>&1"

    touch -c ${hdir_dst10}
}


# build 2D-histograms of correlations w/ pressure
function build_hist2d_correl_pressure_i(){
    local flog="$DirLogs/build_hist2d_correl.pressure/${FUNCNAME[0]}__`date +%d%b%Y_%H:%M`.log"

    exec_and_watch \
        $flog \
        "time ${AUGER_REPO}/build_hist2d_correl.pressure/test.py -- \
            -src ${hdir_dst10} \
            -fi ${hfname_out5} \
            -dst ${hdir_dst11} \
            -m shape.ok_and_3pmt.ok \
            -rx 845.0 880.0 \
            -ry 0.9 1.1 \
            -bins 50 \
            -ini ${dd_ini}/${mm_ini}/${yyyy_ini} \
            -end ${dd_end}/${mm_end}/${yyyy_end} \
            > $flog 2>&1"

    touch -c ${hdir_dst11}
}

# fit the correlations w/ pressure
function build_hist2d_correl_pressure_ii(){
    local flog="$DirLogs/build_hist2d_correl.pressure/${FUNCNAME[0]}__`date +%d%b%Y_%H:%M`.log"

    exec_and_watch \
        $flog \
        "time ${AUGER_REPO}/build_hist2d_correl.pressure/test_fits.py -- \
            -src ${hdir_dst11} \
            -m shape.ok_and_3pmt.ok \
            -fo ${hfname_out12} \
            -plot ${hdir_dst11}/fits \
            -cbmax 6e4 \
            > $flog 2>&1"
}

# build pressure-corrected rates, and
# fit the correlations w/ pressure
function build_pressure_corr(){
    local flog="$DirLogs/build_pressure.corr/runlog_`date +%d%b%Y_%H:%M`.log"

    exec_and_watch \
        $flog \
        "time ${AUGER_REPO}/build_pressure.corr/test.py -- \
            -src ${hdir_dst10} \
            -fi1 ${hfname_out5} \
            -fi2 ${hfname_out12} \
            -dst ${hdir_dst13} \
            -ini ${dd_ini}/${mm_ini}/${yyyy_ini} \
            -end ${dd_end}/${mm_end}/${yyyy_end} \
            > $flog 2>&1"

    touch -c ${hdir_dst13}
}

function _build_ncep(){
    # we need 2 arguments
    [[ $# -eq 0 ]] && {
        echo -e "\n [-] ERROR: we need 2 arguments!\n";
        return 1;
    }

    # grab the log file && year
    local flog="$1"
    mkdir -p $(dirname $flog)
    local yyyy=$2

    echo -e " [*] processing year $yyyy @ ${FUNCNAME[0]}\n"
    time ${AUGER_REPO}/build_weather/build_ncep_gh.py -- \
        -di ${wdir_src14} \
        -do ${wdir_dst14} \
        -y ${yyyy} ${yyyy} \
        > $flog 2>&1 &

    local pid=$! # grab the PID of the job

    # print the runlog path
    echo -e "\n [*] runlog:\n $flog\n"
    ## in the meantime, report status
    tail -f $flog &
    local pid_tail=$!

    # when the process finishes:
    # 1) sucessfully, then touch the output.
    # 2) with and error exit code: don't touch the output && print error msg && exit.
    wait $pid
    exitcode=$?
    kill ${pid_tail}
    if [[ $exitcode -eq 0 ]]; then
        touch -c ${wdir_dst14}
    else
        echo -e "\n [-] sthing wen't wrong @ ${FUNCNAME[0]}\n"
        return 1
    fi
}

# calculate an average (along the years; e.g. 2006-2013) histogram, so 
# that later we can use it to normalize individual histograms before 
# making the temperature corrections.
function build_temp_hist_fits_i(){
    local flog="$DirLogs/build_temp_hist.and.fits/${FUNCNAME[0]}_`date +%d%b%Y_%H:%M`.log"
    
    exec_and_watch \
        $flog \
        "time ${AUGER_REPO}/build_temp_hist.and.fits/build_avr_histos_press.txt.py -- \
            -src ${hdir_dst13} \
            --nmonth 4 \
            --ntmonth $((${nyears}*12)) \
            -ini ${dd_ini}/$((${mm_ini}+2))/${yyyy_ini} \
            --maskname shape.ok_and_3pmt.ok \
            -fo ${hfname_out15} \
            > $flog 2>&1"
}

function build_temp_hist_fits_ii(){
    local flog="$DirLogs/build_weather/${FUNCNAME[0]}__`date +%d%b%Y_%H:%M`.txt"

    exec_and_watch \
        $flog \
        "time ${AUGER_REPO}/build_temp_hist.and.fits/build_H2D_Ed.ascii.py --pdb -- \
            -src ${hdir_dst13} \
            -ncep ${wdir_dst14} \
            -fa ${hfname_out15} \
            -dst ${hdir_dst16} \
            -rx 16050 16650 \
            -ry 0.95 1.1 \
            -bins 200 \
            -mintank 150 \
            -ini ${dd_ini}/${mm_ini}/${yyyy_ini} \
            -end ${dd_end}/${mm_end}/${yyyy_end} \
            > $flog 2>&1"

    # if we got this far, all went ok above.
    touch -c ${hdir_dst16}
}

function build_temp_hist_fits_iii(){
    local flog="$DirLogs/build_weather/${FUNCNAME[0]}__`date +%d%b%Y_%H:%M`.txt"

    exec_and_watch \
        $flog \
        "time ${AUGER_REPO}/build_temp_hist.and.fits/fitplots.py --pdb -- \
            -src ${hdir_dst16} \
            -df ${hdir_dst17} \
            -fo ${hfname_out17} \
            -cbmax 1e2 \
            > $flog 2>&1"
}

function build_temp_corr(){
    # build pressure-corrected rates
    # fit the correlations w/ pressure
    local flog="$DirLogs/build_temp.corr/${FUNCNAME[0]}_`date +%d%b%Y_%H:%M`.txt"

    exec_and_watch \
        $flog \
        "time ${AUGER_REPO}/build_temp.corr/build_temp.corr.py -- \
            --dir_hst ${hdir_dst13} \
            --dir_ncep ${wdir_dst14} \
            --fname_avr ${hfname_out15} \
            --fname_slopes ${hfname_out17} \
            -o ${hfname_out18} \
            > $flog 2>&1"
}

# END: HISTOS ++++++++++++++++++++++++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++



#+++++++++++++++++++++++++++++++++++++++++++++++
# BEGIN: SCALERS +++++++++++++++++++++++++++++++

function scl_build_scals(){
    # build reduced versions of the *.bz2 files
    local flog="$DirLogs/build_mc/${FUNCNAME[0]}_`date +%d%b%Y_%H:%M`.log"
    mkdir -p $(dirname $flog)

    # number of processor where to run all these years of data
    # NOTE: the run will be distributed as 1 year of data per processor.
    local NPROC=${1:-4}
    echo -e "\n [*] Running scl.build_scals in ${NPROC} processors.\n"

    # TODO: FIRST, IMPLEMENT THE PYTHON SCRIPT WITH MPI.
    # ...
    exitcode=$?
    if [[ $exitcode -eq 0 ]]; then
        touch ${sflag_done1}
    else
        echo -e "\n [-] sthing wen't wrong @ ${FUNCNAME[0]}\n"
        return 1
    fi
}

function Scalers__build_mc(){
    local flog="$DirLogs/build_mc/${FUNCNAME[0]}_`date +%d%b%Y_%H:%M`.log"
    mkdir -p $(dirname $flog)
    #docker__run_auger \
    #    -DirRep ~/auger_repo \
    #    -DirSrc ~iauger/monit/sd \
    #    -DirDst ~oauger/build_mc \
    #    -DirLog ~oauger/logs/build_mc \
    #    -cname auger__build_mc__2017 \
    #    -CMD /auger_repo/build_mc/gral.sh \
    #    -DateIni 01 01 2017 \
    #    -DateEnd 31 12 2017 \
    #    -DirSrc /DataInp \
    #    -DirDst /DataOut \
    #    -DirLog /runlogs
    
    # TODO: FIRST, IMPLEMENT THE DOCKER SCRIPT WITH MPI.
    # ...
    exitcode=$?
    if [[ $exitcode -eq 0 ]]; then
        touch ${sflag_done2}
    else
        echo -e "\n [-] sthing wen't wrong @ ${FUNCNAME[0]}\n"
        return 1
    fi
}

# Build 2D-histograms of correlations w/ AoP
function scl_build_AoPcorr_i(){
    local flog="$DirLogs/scl.build_AoPcorr/${FUNCNAME[0]}_`date +%d%b%Y_%H:%M`.log"

    # number of processor where to run all these years of data
    # NOTE: the run will be distributed as 1 year of data per processor.
    local NPROC=${1:-4}
    echo -e "\n [*] Running ${FUNCNAME[0]} in ${NPROC} processors.\n"

    exec_and_watch \
        $flog \
        "time mpirun -n ${NPROC} \
        ${AUGER_REPO}/scl.build_AoPcorr/make_AoPHist2d.py -- \
            --inp_scals ${sdir_dst1} \
            --inp_monit ${sdir_dst2} \
            --bins 200 \
            --period ${dd_ini}/${mm_ini}/${yyyy_ini} ${dd_end}/${mm_end}/${yyyy_end} \
            -o ${sfname_out3} \
            --fcache ${sfname_out3a} \
            > $flog 2>&1"
}

# Fit the correlations of Scalers w/ AoP (&& append 
# fitted parameters into the input file).
function scl_build_AoPcorr_ii(){
    local flog="$DirLogs/scl.build_AoPcorr/${FUNCNAME[0]}_`date +%d%b%Y_%H:%M`.log"

    exec_and_watch \
        $flog \
        "time ${AUGER_REPO}/scl.build_AoPcorr/fit_hist2d.py -- \
            -fio ${sfname_out3} \
            -fig ${sfname_out3%.*}.png \
            > $flog 2>&1"
}

# correct Scalers by AoP effect
function scl_build_AoPcorr_iii(){
    local flog="$DirLogs/scl.build_AoPcorr/${FUNCNAME[0]}_`date +%d%b%Y_%H:%M`.log"

    # number of processor where to run all these years of data
    # NOTE: the run will be distributed as 1 year of data per processor.
    local NPROC=${1:-4}
    echo -e "\n [*] Running ${FUNCNAME[0]} in ${NPROC} processors.\n"

    exec_and_watch \
        $flog \
        "time mpirun -np ${NPROC} \
            ${AUGER_REPO}/scl.build_AoPcorr/massive_AoP_correc_mpi.py -- \
            --h5input ${sfname_out3}  \
            --dir_scl ${sdir_dst1} \
            --dir_mon ${sdir_dst2}  \
            --Qvalue 2.5 \
            -ini ${dd_ini}/${mm_ini}/${yyyy_ini} \
            -end ${dd_end}/${mm_end}/${yyyy_end} \
            -o ${sfname_out4} \
            > $flog 2>&1"
}


# get it to HDF5 format (for faster i/o later)
function Scalers_build_weather_ii(){
    local flog="$DirLogs/build_weather/${FUNCNAME[0]}_`date +%d%b%Y_%H:%M`.log"

    exec_and_watch \
        $flog \
        "time ${AUGER_REPO}/build_weather/build_h5_weather.py -- \
            -fi ${wfname_out9} \
            -fo ${sfname_out6} \
            -y ${yyyy_ini} ${yyyy_end} \
            > $flog 2>&1"
}

# just build the HDF5 versions for the years that are necessary
# The build_ncep() is supposed to have build them already.
function build_ncep(){
    # NOTE: sdir_ds7 <--> wdir_src14
    local flog="$DirLogs/build_weather/${FUNCNAME[0]}__`date +%d%b%Y_%H:%M`.log"
    mkdir -p $(dirname $flog)
    echo -e "\n [*] runlog:\n $flog\n"

    local fname_out
    for yyyy in $(seq ${yyyy_ini} 1 ${yyyy_end}); do
        # real target
        fname_out="${wdir_dst14}/test_${yyyy}.h5" 
        if [[ $(ls -lh ${fname_out}) ]]; then
            # if already exists, just touch it
            echo -e " [+] Already exists: ${fname_out}\n"
            touch -c ${fname_out}
        else
            # process this year only && append all in the runlog
            _build_ncep $flog $yyyy
        fi
    done

    # if we got this far, all went ok.
    #touch ${sflag_done7}
}

# data rejection && pressure correction
function Scalers_data_rejection(){
    local flog="$DirLogs/scl.build_final/${FUNCNAME[0]}_`date +%d%b%Y_%H:%M`.log"
    
    exec_and_watch \
        $flog \
        "time ${AUGER_REPO}/scl.build_final/rejection_and_PressCorr.py -- \
            --inp_h5 ${sfname_out4} \
            --inp_weather ${sfname_out6} \
            --date_lim ${dd_ini}/${mm_ini}/${yyyy_ini} ${dd_end}/${mm_end}/${yyyy_end} \
            -fo ${sfname_out8} \
            > $flog 2>&1"
}

# Build 2D-histograms of correlations w/ pressure (for Scalers)
function Scalers__hist2d_wPress(){
    local flog="$DirLogs/scl.build_final/${FUNCNAME[0]}_`date +%d%b%Y_%H:%M`.log"

    exec_and_watch \
        $flog \
        "time ${AUGER_REPO}/scl.build_final/build_hist2d.py --pdb -- \
        --cbmax 1e2  \
        --fname_inp ${sfname_out8} \
        --dir_ncep ${wdir_dst14} \
        --fname_out ${sfname_out9} \
        --fig_fit_press ${sfname_out9%.*}.png \
        --period ${dd_ini}/${mm_ini}/${yyyy_ini} ${dd_end}/${mm_end}/${yyyy_end} 
        > $flog 2>&1"

    # remove it, so that Make can re-do this
    if [[ $? -ne 0 ]]; then
        echo -e "\n [-] removing:\n ${sfname_out9}\n"
        rm ${sfname_out9}
        return 1
    fi
}

# Corrections for pressure and temperature (i.e. geop-height)
function Scalers__PressCorr(){
    local flog="$DirLogs/scl.build_final/${FUNCNAME[0]}_`date +%d%b%Y_%H:%M`.log"

    exec_and_watch \
        $flog \
        "time ${AUGER_REPO}/scl.build_final/build_corr.py -- \
            -idata ${sfname_out8} \
            -ifit ${sfname_out9} \
            -ncep ${wdir_dst14} \
            --period ${dd_ini}/${mm_ini}/${yyyy_ini} ${dd_end}/${mm_end}/${yyyy_end} \
            -o ${sfname_out10} \
            > $flog 2>&1"
}



# END: SCALERS +++++++++++++++++++++++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++


#EOF
