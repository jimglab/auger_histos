#!/usr/bin/env ipython
"""
 author: j.j. masias meza

 runtime: ~100min
"""
from funcs import *
import os, sys
import numpy as np
import shared_funcs.funcs as sf
import argparse

#--- parse args
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter,
description="""
Make AoP corrections.
""",
)
parser.add_argument(
'-src', '--dir_src',
type=str,
help='input directory. Should be the output dir of "build_array.avrs"',
)
parser.add_argument(
'-fi1', '--fname_inp1',
type=str,
help="""
input ASCII file. Should be the output of 'build_avr_histos'.
""",
)
parser.add_argument(
'-fi2', '--fname_inp2',
type=str,
help="""
Input ASCII file, containing the fit parameters of the linear relation 
between AoP and charge-histogram rates.
Should be the output of 'code_hist2d'.
""",
)
parser.add_argument(
'-dst', '--dir_dst',
type=str,
help='directory for output ASCII files, containing the 2D histograms. \
One file per energy channel.',
)
parser.add_argument(
'-ini', '--ini_date', 
type=str, 
action=sf.arg_to_datetime,
default='01/01/2006',
help='initial date',
)
parser.add_argument(
'-end', '--end_date', 
type=str, 
action=sf.arg_to_datetime,
default='31/12/2015',
help='end date',
)
pa = parser.parse_args()


os.system('mkdir -p %s' % pa.dir_dst)
#--------------------- agarro valores tipicos para construir rangos
print " ---> reading: %s\n" % pa.fname_inp1
prom       = np.loadtxt(pa.fname_inp1)
typic      = np.mean(prom[:,1:], axis=0)   # agarro el promedio del array de los 8 anios!
print " ---> reading: %s\n" % pa.fname_inp2
maop       = np.loadtxt(pa.fname_inp2, usecols=(1,))
#-------------------------------------------------------------------

bins            = 50
n               = 96
ntot            = 0                     # monitoring

#---- average value of AoP (in the whole time period)
#AOP_AVR = calc_aop_prom(pa.dir_src, pa.ini_date, pa.end_date)
AOP_AVR = float(sf.ReadParam_from_2dHist(pa.fname_inp2, 'AoP_mean')[0])
# if any is NaN, everything fails silently (output will be all NaN)
if (np.isnan(AOP_AVR) or np.isinf(AOP_AVR)):
    raise SystemExit("\n [-] GARBAGE VALUE IN AOP_AVR=%g !!\n" % AOP_AVR)
#---------------------------

fnull = 0  # number of not-existent files
date  = pa.ini_date
while date <= pa.end_date:
    yyyy    = date.year
    mm      = date.month
    dd      = date.day

    fname_inp   = '%s/%04d/%04d_%02d_%02d.nc' % (pa.dir_src, yyyy, yyyy, mm, dd)
    if os.path.isfile(fname_inp): # if the input file doesn't exist, don't generate output
        try:
            f       = netcdf_file(fname_inp, 'r')
            print ccl.R + " [*] Reading: %s" % fname_inp + ccl.W
            m       = f.variables['cnts_intgr_id'].data     # cnts_intgr_id(ntime, nstations, nbins_mev)
            aop     = f.variables['AoP'].data               # A/P(ntime, nstations) from mc
            aop_sd  = f.variables['AoP_sd'].data            # A/P(ntime, nstations) from sd
            #xx     = f.variables['ntanks'].data > 150.     # solos estos aportan al histograma2d
            rate_corr   = np.empty((n,2000,50))
            rate_uncorr = np.empty((n,2000,50))
            for i in range(50):                 # corregimos para c/canal de Ed
                rate    = m[:,:,i] / typic[i]   # [1] todas las cuentas para el canal 'i'
                slope   = maop[i]               # [1] pendiente para la i-esima energia Ed
                rate_corr[:,:,i] = rate - slope*(aop/AOP_AVR - 1.)   # [1] corregidos por efecto AoP :-)
                rate_uncorr[:,:,i] = rate       # guardamos sin corregir tmb

            #********************************
            fo              = file_mgr(pa.dir_dst, n, yyyy, mm, dd)
            fo.time         = f.variables['t_utc'].data
            fo.ntanks       = f.variables['ntanks'].data
            fo.aop_avr      = np.nanmean(aop, axis=1)    # AoP from mc, averaged over the array
            fo.aop_avr_sd   = np.nanmean(aop_sd, axis=1) # AoP from sd, averaged over the array
            fo.cts_corr     = np.nanmean(rate_corr, axis=1)*typic   # cts_corr(ntime, nbins_mev): corrected rates averaged over the array
            fo.cts_uncorr   = np.nanmean(rate_uncorr,1)*typic # rates *without* correction
            fo.cts_corr_std = np.nanstd(rate_corr,1)*typic    # standard deviat of the corrected rates
            fo.save()                                         # save the corrected rates in the .nc
            ntot    += 1
            f.close()
            del fo

        except KeyboardInterrupt:
            raise SystemExit("\n ----> KEYBOARD-INTERRUPT! @%s\n"%sys.argv[0])
        
        except:
            print " ----> CORRUPT!: %s" % fname_inp
            pass
    else:
        fnull += 1

    date    += timedelta(days=1)
    if date.month!=mm:
        print ccl.Gn + " ---> month: %02d/%04d" % (mm,yyyy) + ccl.W

# report how many files we found
print "\n [*] There were %d absent files out of %d in total.\n" % (fnull, (pa.end_date-pa.ini_date).days+1)

print "\n ----> output: %s\n" % pa.dir_dst
#EOF
