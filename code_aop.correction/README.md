## Build AoP-corrected scalers:

Build the AoP-corrected data.
To run: 
```bash
# runtime ~100min
./test.py -- \
    -src /media/scratch1/auger/build_array.avrs/15min \
    -fi1 /media/scratch1/auger/build_avr_histos/avr_histos__Jan2006-Dec2014.txt \
    -fi2 /media/scratch1/auger/code_hist2d/fit_params.txt \
    -dst /media/scratch1/auger/code_aop.correction \
    -ini 01/01/2006 \
    -end 31/12/2014
```

Full help:

	usage: test.py [-h] [-src DIR_SRC] [-fi1 FNAME_INP1] [-fi2 FNAME_INP2]
				   [-dst DIR_DST] [-ini INI_DATE] [-end END_DATE]

	Make AoP corrections.

	optional arguments:
	  -h, --help            show this help message and exit
	  -src DIR_SRC, --dir_src DIR_SRC
							input directory. Should be the output dir of
							"build_array.avrs" (default: None)
	  -fi1 FNAME_INP1, --fname_inp1 FNAME_INP1
							input ASCII file. Should be the output of
							'build_avr_histos'. (default: None)
	  -fi2 FNAME_INP2, --fname_inp2 FNAME_INP2
							Input ASCII file, containing the fit parameters of the
							linear relation between AoP and charge-histogram
							rates. Should be the output of 'code_hist2d'.
							(default: None)
	  -dst DIR_DST, --dir_dst DIR_DST
							directory for output ASCII files, containing the 2D
							histograms. One file per energy channel. (default:
							None)
	  -ini INI_DATE, --ini_date INI_DATE
							initial date (default: 01/01/2006)
	  -end END_DATE, --end_date END_DATE
							end date (default: 31/12/2015)


---
## Output:

The `yyyy_mm_dd.nc` files have these key-named contents:
* `time`: `(ntime,)` UTC-seconds in a uniform sequence of 15min for this day `dd/mm/yyyy`.
* `aop_avrg_over_array`: `(ntime,)` averages of the AoP values over the SD array, from the `mc..root` files.
* `aop_avrg_over_array_from.sd`: `(ntime,)` averages of the AoP values over the SD array, from the `sd..root` files.
* `ntanks`: `(ntime,)` number of tanks contributing with non-gap data values to the averages.
* `aop.uncorrected_cnts`: `('ntime', 'nbins_mev')` scaler counts without AoP correction.
* `aop.corrected_cnts`: `('ntime', 'nbins_mev')` AoP-corrected charge-histogram counts.
* `aop.corrected_cnts_std`: `('ntime', 'nbins_mev')` associated standard deviations.

where:
* `nbins_mev`: number of deposited-energy uniform subintervals (50 by default), in the range (0-1000) MeV.
* `ntime`: number of uniformly spaced UTC-seconds along the day `dd/mm/yyyy`.

<!--- EOF -->
