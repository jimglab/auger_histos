"""
 author: j.j. masias meza
"""
from pylab import *
from scipy.io.netcdf import netcdf_file
from datetime import datetime, timedelta
import numpy as np
import os
import console_colors as ccl



class file_mgr:
    def __init__(self, dir_dst, ntime, yyyy, mm, dd):
        self.dir_dst    = '%s/%04d' % (dir_dst, yyyy)
        self.ntime      = ntime
        self.fname_out  = '%s/%04d_%02d_%02d.nc' % (self.dir_dst, yyyy, mm, dd)
        os.system('mkdir -p %s' % self.dir_dst)


    def save(self):
        self.fout = netcdf_file(self.fname_out, 'w')
        self.fout.createDimension('ntime', self.ntime)
        self.fout.createDimension('nbins_mev', 50)

        # guardamos cuentas cormalizadas corregidas por AoP
        dims    = ('ntime', 'nbins_mev')
        self.write_variable('aop.corrected_cnts'  , dims, self.cts_corr  , 'd', '[cts/m2/s]')
        self.write_variable('aop.uncorrected_cnts', dims, self.cts_uncorr, 'd', '[cts/m2/s]')
        self.write_variable('aop.corrected_cnts_std', dims, self.cts_corr_std, 'd', '[cts/m2/s]')

        # guardamos tiempo
        dims    = ('ntime',)
        self.write_variable('time', dims, self.time, 'd', '[sec-utc]')

        # AoP promediado sobre el array
        self.write_variable('aop_avrg_over_array', dims, self.aop_avr, 'd', '[1] AoP promediado sobre array (data from mc..root)')
        self.write_variable('aop_avrg_over_array_from.sd', dims, self.aop_avr_sd, 'd', '[1] AoP promediado sobre array (data from sd..root)')

        # nro de tanque q aportan al promedio
        self.write_variable('ntanks', dims, self.ntanks, 'd', '[1] nro de tanques q aportan')

        self.fout.close()


    def write_variable(self, varname, dims, var, datatype, comments):
        dummy           = self.fout.createVariable(varname, datatype, dims)
        dummy[:]        = var
        dummy.units     = comments



def calc_aop_prom(dir_src, ini_date, end_date):
    ntot            = 0
    aop_avr         = 0.0

    date = ini_date
    while date < end_date:
        yyyy    = date.year
        mm      = date.month
        dd      = date.day

        fname_inp   = '%s/%04d/%04d_%02d_%02d.nc' % (dir_src, yyyy, yyyy, mm, dd)

        if os.path.isfile(fname_inp):
            try:
                f       = netcdf_file(fname_inp, 'r')
                print ccl.B + "    [*] Reading: %s" % fname_inp + ccl.W
                aop     = f.variables['AoP'].data
                xx      = np.isinf(aop)  # True para valores infinitos
                if not np.isnan(np.nanmean(aop)):
                    aop_avr += np.nanmean(aop[~xx])
                    ntot    += 1

                if (np.isnan(aop_avr) or np.isinf(aop_avr)):
                    print " ---> @calc_aop_prom(): acumulamos basura en:", date
                    return np.nan

                f.close()

            except KeyboardInterrupt:
                print "\n KEYBOARD-INTERRUPT! @calc_aop_prom()"
                raise SystemExit

            except:
                print ccl.B + " CORRUPTO: %s"%fname_inp + ccl.W

        date    += timedelta(days=1)

    if ntot>1:
        aop_avr /= 1.*ntot
        print " nro de archivos  q aportan: %d" % ntot
        print " <AoP> = %3.2g" % aop_avr
    else:
        print " FUCK!, ntot: %d" % ntot
        raise SystemExit

    return aop_avr

#EOF
