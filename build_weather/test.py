#!/usr/bin/env ipython
"""
 author: j.j. masias meza

 runtime ~ 8hs
"""
import pylab as pl
import numpy as np
from datetime import datetime, timedelta
import os, argparse
import shared_funcs.funcs as sf


#--- retrieve args
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
'-fi', '--fname_inp',
type=str,
default='./data_original/wclf_l1.dat',
help='input .dat file that was downloaded from Auger weather page (CLF-weather data; LL data not recommended).',
)
parser.add_argument(
'-fo', '--fname_out',
type=str,
default='../out/out.build_weather/weather_test.dat',
help='output .dat file that will be generated.'
)
parser.add_argument(
'-ini', '--ini_date', 
type=str, 
action=sf.arg_to_datetime,
metavar='IniDateCenter',
default='01/03/2006',
help='Start time from which we process weather data.',
)
parser.add_argument(
'-end', '--end_date', 
type=str, 
action=sf.arg_to_datetime,
metavar='IniDateCenter',
default='31/12/2006',
help='End time from which we process weather data.',
)
parser.add_argument(
'-res', '--resol',
type=int,   # integer number (necessary?)
default=5,  # [min]
help='output resolution in minutes.'
)
pa = parser.parse_args()


this_dir    = os.environ['AUGER_REPO'] # root path of this repo!
#this_dir    = '%s/auger.histos_aop' % home
#dir_src     = '%s/build_weather/data_original' % this_dir
#dir_dst     = '%s/out/out.build_weather' % this_dir

RES         = pa.resol      # [min] time resolution I want to build
fname_out   = pa.fname_out 

# read data from original files (which has 5min resolution)
data        = np.loadtxt(pa.fname_inp, unpack=True)
data        = data[:,1:]
t           = data[0]
press_orig  = data[7]

min     = 60.
n       = 0
OneDay  = timedelta(days=1)
nlong   = (86400/(RES*min))*((pa.end_date-pa.ini_date).days+1) # nro de tiempo espaciados en 5min, en todos estos anios
# alloc memory space
time    = np.empty(nlong)
press   = np.empty(nlong)
nok     = 0
nread   = 0
ntries  = 0

date    = pa.ini_date
while date <= pa.end_date:
    yyyy    = date.year
    mm  = date.month
    dd  = date.day
    if date.month!=(date-OneDay).month:
        print " yyyy/mm: %d/%d" % (date.year, date.month)

    for HH in range(24):
        for MM in range(0, 60, RES):
            try:
                fecha   = datetime(yyyy, mm, dd, HH, MM)
                t_utc   = sf.date2utc(fecha) # (*1)
                tini    = sf.date2utc(fecha - timedelta(0, seconds=RES*min/2))
                tfin    = sf.date2utc(fecha + timedelta(0, seconds=RES*min/2))
                cc  = (t>=tini) & (t<=tfin)
                if len(pl.find(cc))>0: # at least one data point in `RES` window
                    time[n]     = t_utc
                    press[n]    = np.mean(press_orig[cc])
                    n           += 1
                    #print " from file..."

                else: # we flag this data point
                    time[n]  = t_utc
                    press[n] = np.nan
                    print " [*] flagging: %d / %d" %(data[0][nread], t_utc)
                    n        += 1

            except IndexError as err:
                print " [*] %s --> n/nlong: %d/%d" % (err, n, nlong)

            except KeyboardInterrupt:
                raise SystemExit("\n [-] Interrupted when processing data for %s\n"%date.strftime('%Y %b %d, %H:%M'))

            except:
                print("\n [-] Unknown error for this date: %s\n"%date.strftime('%Y %b %d, %H:%M'))
                pass

            ntries  += 1

    date    += OneDay
    #print " t, press: %12d, %g" % (time[n-1], press[n-1])

#---------------------------------------------------------------------------
# get rid of garbage np.empty() :-)
time    = time[:n]
press   = press[:n]
cc      = np.isnan(press)

data_out    = np.array([time, press]).T
print " ----> saving: " + fname_out
np.savetxt(fname_out, data_out, fmt='%12.4f')
##
