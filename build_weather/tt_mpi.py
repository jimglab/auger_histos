#!/usr/bin/env ipython
"""
 author: j.j. masias meza

 runtime ~ 8hs
"""
import numpy as np
from datetime import datetime, timedelta
import os, argparse
from time import sleep
import shared_funcs.funcs as sf
from mpi4py import MPI

#--- retrieve args
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
'-fi', '--fname_inp',
type=str,
default='./data_original/wclf_l1.dat',
help='input .dat file that was downloaded from Auger weather page (CLF-weather data; LL data not recommended).',
)
parser.add_argument(
'-fo', '--fname_out',
type=str,
default='../out/out.build_weather/weather_test.dat',
help='output .dat file that will be generated.'
)
parser.add_argument(
'-ini', '--ini_date', 
type=str, 
action=sf.arg_to_datetime,
metavar='IniDateCenter',
default='01/03/2006',
help='Start time from which we process weather data.',
)
parser.add_argument(
'-end', '--end_date', 
type=str, 
action=sf.arg_to_datetime,
metavar='IniDateCenter',
default='31/12/2006',
help='End time from which we process weather data.',
)
parser.add_argument(
'-res', '--resol',
type=int,   # integer number (necessary?)
default=5,  # [min]
help='output resolution in minutes.'
)
pa = parser.parse_args()

RES         = pa.resol      # [min] time resolution I want to build

OneDay  = timedelta(days=1)
min     = 60.

#--- parallelize this stupid thing
comm  = MPI.COMM_WORLD
rank  = comm.Get_rank()
wsize = comm.Get_size()
date_bd  = sf.equi_dates(pa.ini_date, pa.end_date + timedelta(days=1), wsize)
date_ini = date_bd[rank]
date_end = date_bd[rank+1] - timedelta(days=1)
# report the repartition of jobs
print("")
print(" [*][rank:{rank}/{maxrank}] period for processing: {ini} - {end}".format(
    rank=rank, 
    maxrank=wsize-1,
    ini=date_ini.strftime("%d/%b/%Y %H:%M:%S"),
    end=date_end.strftime("%d/%b/%Y %H:%M:%S")
    ))
print("")


#------ read data && send "pieces" to the other processors
if rank == 0: 
    # read data from original files (which has 5min resolution)
    print " [*] reading: " + pa.fname_inp
    data        = np.loadtxt(pa.fname_inp)
    print " [*] finished reading: " + pa.fname_inp
    _t      = data[:,0]  # [utc-sec] UTC seconds
    _press  = data[:,7]  # [hPa] atmospheric pressure
    print " t0 ", _t[0]
    print " tf ", _t[-1]
    # cut the data into pieces
    dt = 1.*(sf.date2utc(pa.end_date) - sf.date2utc(pa.ini_date))/wsize
    # here, we save the size (in time dimenstions) of the "pieces" of
    # of data that the root sends to the other processors.
    _size = np.empty((wsize,1), dtype='i')
    for ir in range(wsize):
        t_ini   = sf.date2utc(pa.ini_date) + (ir-0.5)*dt #- 30*86400.
        t_end   = t_ini + 2.*dt #+ 2*30*86400.
        # select this piece to send to processor 'ir'
        cc      = (_t>=t_ini) & (_t<=t_end)
        # NOTE: each piece must have a size >0; otherwise, the time
        # boundaries 'pa.ini_date' and 'pa.end_date' are not
        # suited for this parallel run. So check it with assert:
        assert cc.nonzero()[0].size > 0
        _buffer = np.empty((_t[cc].size,2), dtype=np.float64)
        _buffer[:,0] = _t[cc]
        _buffer[:,1] = _press[cc]
        # send data to the other proc
        if ir != 0:
            print " [r:%d] ---> sending data to proc %d" % (rank, ir)
            _size[ir,0] = _buffer.shape[0] # size in the time dimension
            comm.Send([_size[ir], MPI.INT],   dest=ir, tag=666)
            comm.Send([_buffer, MPI.DOUBLE], dest=ir, tag=777)
        else:
            t          = _buffer[:,0]
            press_orig = _buffer[:,1]

#--- recieve data from root
if rank != 0:
    print " [r:%d] <--- recieving data from proc 0" % rank
    _size = np.empty(1, dtype='i')
    status = MPI.Status()
    comm.Recv([_size, MPI.INT], source=0, tag=666, status=status)
    print " [r:%d] _size = "%rank, _size[0]
    _buffer = np.empty((_size[0],2), dtype=np.float64) # buffer to recieve next data
    comm.Recv([_buffer, MPI.DOUBLE], source=0, tag=777)
    print _buffer.shape, _buffer[:4,1]
    t          = _buffer[:,0]
    press_orig = _buffer[:,1]

#raise SystemExit()
n       = 0
nlong   = (86400/(RES*min))*((date_end-date_ini).days+1) # nro de tiempo espaciados en 5min, en todos estos anios
# alloc memory space
time    = np.empty(nlong, dtype=np.float64)
press   = np.empty(nlong, dtype=np.float64)
nok     = 0
nread   = 0
ntries  = 0

date    = date_ini
while date <= date_end:
    yyyy    = date.year
    mm  = date.month
    dd  = date.day
    if date.month!=(date-OneDay).month:
        print " yyyy/mm: %d/%d" % (date.year, date.month)

    for HH in range(24):
        for MM in range(0, 60, RES):
            try:
                fecha   = datetime(yyyy, mm, dd, HH, MM)
                t_utc   = sf.date2utc(fecha) # (*1)
                tini    = sf.date2utc(fecha - timedelta(0, seconds=RES*min/2))
                tfin    = sf.date2utc(fecha + timedelta(0, seconds=RES*min/2))
                cc  = (t>=tini) & (t<=tfin)
                time[n]     = t_utc
                if cc.nonzero()[0].size > 0: # at least one data point in `RES` window
                    press[n] = np.nanmean(press_orig[cc])
                else: # we flag this data point
                    press[n] = np.nan
                    #print " [*] flagging: %d (%02d/%04d)" % (t_utc,mm,yyyy)

                n        += 1

            except IndexError as err:
                print " [*] %s --> n/nlong: %d/%d" % (err, n, nlong)

            except KeyboardInterrupt:
                raise SystemExit("\n [-] Interrupted when processing data for %s\n"%date.strftime('%Y %b %d, %H:%M'))

            except:
                print("\n [-] Unknown error for this date: %s\n"%date.strftime('%Y %b %d, %H:%M'))
                pass

            ntries  += 1

    date    += OneDay

print " [r:%d] <press>: %g" % (rank, np.nanmean(press[:n]))

#--------------------------------------------------------------

#--- recieve data from root
if rank != 0:
    # send the size of the data
    _size    = np.empty(1, dtype='i')
    _size[0] = n # the "effective" size (in time dimension)
    print " [r:%d] ---> sending %d rows to root" % (rank, _size[0])
    comm.Send([_size[0], MPI.INT],  dest=0, tag=669)
    # send data itself
    _buffer      = np.empty((n,2), dtype=np.float64)
    _buffer[:,0] = time[:n]
    _buffer[:,1] = press[:n]
    print " [r:%d] sending %r" % (rank, [time[-1], press[-1]])
    comm.Send([_buffer, MPI.DOUBLE], dest=0, tag=778)

if rank == 0:
    data_out = np.array([time[:n], press[:n]], dtype=np.float64).T
    for ir in range(1,wsize):
        _size = np.empty(1, dtype='i')
        status = MPI.Status()
        comm.Recv([_size, MPI.INT], source=ir, tag=669, status=status)
        print " [r:%d] <--- recv %d rows from proc %d" % (rank, _size[0],ir)
        _buffer = np.empty((_size[0],2), dtype=np.float64)
        comm.Recv([_buffer, MPI.DOUBLE], source=ir, tag=778, status=status)
        print " [*] buff from %d: %r" % (ir, _buffer[-1,:])
        print " [r:%d] concatenating with proc %d" % (rank, ir)
        data_out = np.concatenate([data_out, _buffer], axis=0)

    print " data_out.shape: ", data_out.shape
    print " ----> saving: " + pa.fname_out
    np.savetxt(pa.fname_out, data_out, fmt='%12.4f')

#EOF
