#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
""" convert ASCII pressure-data to a hdf5 version, so
that later we can slice for processing 
NOTE: new weather files, I get them from:
http://auger.uis.edu.co/data/privatev2.html#files
Specifically, from this file:
http://auger.uis.edu.co/data/files/v2r0/wclf_l1.dat.bz2
"""
import numpy as np
from numpy import (
    nanmean, mean, nanstd, nanstd, isnan, isinf
)
from pylab import pause, find
from h5py import File as h5
from datetime import datetime, timedelta
import shared_funcs.funcs as sf
import argparse

#--- retrieve args
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
'-fi', '--fname_inp',
type=str,
default='./data_original/wclf_l1.dat',
help='input .dat file that was downloaded from Auger weather page (CLF-weather data; LL data not recommended).',
)
parser.add_argument(
'-fo', '--fname_out',
type=str,
default='../out/out.build_weather/weather_test.dat',
help='output .dat file that will be generated.'
)
parser.add_argument(
'-y', '--years',
type=int,
nargs=2,
default=[2006,2015],
metavar=('YearIni','YearEnd'),
help='years between which the data will be processed.',
)
pa = parser.parse_args()

#--- period of data to build (this is assumed to be
#    consistent with the input/output filenames (perhaps 
#    this script should be merged with test.py).
year_ini,year_end = pa.years#2006,2015# Jan/year_ini:Dec/year_end
nyear = year_end-year_ini+1

data_inp = np.loadtxt(pa.fname_inp)
tw     = data_inp[:,0] #+ 315964800 # gps-->utc
press  = data_inp[:,1]
print " ---> archivo leido: "+pa.fname_inp

#--- build years & months
ly, lm = [], []
for iy in range(nyear):
    for im in range(1,12+1):
        ly += [ year_ini+iy ]
        lm += [ im ]

fo = h5(pa.fname_out, 'w')
for year, month in zip(ly,lm):
    tini = sf.date2utc(datetime(year,month,1))
    tend = sf.date2utc(datetime(year+month/12,month%12+1,1))
    # filter-out garbage
    cc   = (tw>=tini) & (tw<tend)
    cc   &= (~isnan(press)) & (~isinf(press))   # bad data points
    cc   &= press!=0.0                          # (bis)
    dpath = '%04d/%02d/pressure'%(year,month)
    fo[dpath] = press[cc]
    dpath = '%04d/%02d/utc_sec'%(year,month)
    fo[dpath] = tw[cc]

print " ---> generamos: "+pa.fname_out+'\n'
fo.close()
#EOF
