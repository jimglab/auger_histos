## weather data

---
### data sources

Grab the original downloaded data `*.dat` from [auger.uis.edu.co](http://auger.uis.edu.co/data/privatev2.html#files). 
Specifically:
* for CLF station:          [wclf_l1.dat.bz2](http://auger.uis.edu.co/data/files/v2r0/wclf_l1.dat.bz2).
* for Los Leones station:   [wll_l2.dat.bz2] (http://auger.uis.edu.co/data/files/v2r0/wll_l2.dat.bz2)
The one from Los Leones is usually more complete.
Sometimes, CLF stations has data where Los Leones has long data gaps (e.g. for the Forbush event of 14/Dec/2016).


---
### generate a unform-reosolution version of the data

This takes the original weather data from Auger, and generates an output with 
uniform resolution of 5 minutes (see `-res` option):
```bash
./test.py -- \
    -fi $AUGER_REPO/build_weather/data_original/wll_l2.dat \
    -fo /media/scratch1/auger/build_weather/weather_ll__5min_2006-2015.dat \
    -res 5 \
    -y 2006 2015
```
where, for example, `-fi` can be:
    $AUGER_REPO/build_weather/data_original/wclf_l1.dat  # CLF station
    $AUGER_REPO/build_weather/data_original/wll_l2.dat   # Los Leones station


---
### generate an HDF5 version (for recurrent slicings later)
Take the ASCII file (`.dat`) generated above and generated a HDF5 version with groups the data
in month periods:
```bash
./build_h5_weather.py -- \
    -fi ./data_original/wclf_CLF.dat \
    -fo /media/scratch1/auger/build_weather/wclf_CLF__5min_2006-2015.h5 \
    -y 2006 2015
```

---
This extracts geopotential height from NCEP re-analysis data (see 
general description inside).
Runtime: ~1h for 11 years of data.
```bash
./build_ncep_gh.py -- \
    -di /media/scratch2/auger/weather/ncep_original \
    -do /media/scratch1/auger/build_weather/build_ncep_gh \
    -y 2006 2015
```

Full help:

    $ ./build_ncep_gh.py -- -husage: build_ncep_gh.py [-h] [-y YEARS YEARS] [-di DIR_SRC] [-do DIR_OUT]

    optional arguments:
      -h, --help            show this help message and exit
      -y YEARS YEARS, --years YEARS YEARS
                            years for period of data-processing
                            (yearini/Jan/01-yearend/Dec/31) (default: [2006,
                            2013])
      -di DIR_SRC, --dir_src DIR_SRC
                            input directory, containing *.pgb.f00 NCEP original
                            files (default: /media/Elements/data_gdas)
      -do DIR_OUT, --dir_out DIR_OUT
                            output directory for HDF5 files (default: .)


<!--- EOF -->
