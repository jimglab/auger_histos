## Time-averaged charge histograms

Build average charge-histograms (_i.e._ profiles of count rates vs energy).
Averages are built in running windows (runtime: ~1min):
```bash
./test_2006.py -- \
    -src /media/scratch1/auger/build_array.avrs/15min \
    -fo /media/scratch1/auger/build_avr_histos/avr_histos__Jan2006-Dec2013.txt \
    -ini 01/03/2006 \
    --nmonth 4 \
    --ntmonth $((8*12))
```

---
Full description:

	usage: test_2006.py [-h] [-src DIR_SRC] [-fo FNAME_OUT] [-ini IniDateCenter]
						[-nm NW_MONTH] [-ntm NTOTAL_MONTH]

	Build running time-averages (of the counts in charge histogram, for every
	energy channel) over the whole time-period. This values are needed as
	normalization factors in the subsequent stages of processing.

	optional arguments:
	  -h, --help            show this help message and exit
	  -src DIR_SRC, --dir_src DIR_SRC
							input directory. Should be the output dir of
							"build_ReduceHistos" (default: None)
	  -fo FNAME_OUT, --fname_out FNAME_OUT
							output directory, recommended in the form
							path/<maskname> (default: None)
	  -ini IniDateCenter, --ini_date IniDateCenter
							initial date, which will be taken as the center of the
							1st time-window for the calculation of running
							averages. It should be IniDateCenter=<IniDate> +
							0.5*NW_MONTH, where <IniDate> is the intended initial
							date to take into account in the whole time period of
							processing. So for ex., if we want to analyze since
							01/Jan/2006, and NW_MONTH=4, we should specify here
							01/Mar/2006 (i.e. 60 days after 01/Jan/2006).
							(default: 01/03/2006)
	  -nm NW_MONTH, --nmonth NW_MONTH
							width of window (in months) of the running averages.
							(default: 4)
	  -ntm NTOTAL_MONTH, --ntmonth NTOTAL_MONTH
							total number of months of the whole time period to be
							processed (in months). It **has** to be a multiple of
							NW_MONTH, so that the running windows fit exactly on
							the whole time period of analysis. (default: 96)


<!--- EOF -->
