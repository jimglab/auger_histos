#!/usr/bin/env ipython
"""
 author: j.j. masias meza

 decription:
 this generates an ascii file containing the time average 
 rates in each energy channel Ed, in running time windows 
 of 4 months (see 'nmonth').

 NOTE:
 - you can safely ignore these runtime messages:
   * RuntimeWarning: Mean of empty slice
   * RuntimeWarning: Cannot close a netcdf_file opened with mmap=True, ...

 runtime:
    real    0m52.411s
    user    0m3.384s
    sys     0m0.876s
"""
import numpy as np
from pylab import *
from scipy.io.netcdf import netcdf_file
import os
from os.path import isfile, isdir
from os import environ as env
from datetime import datetime, timedelta
import console_colors as ccl
from numpy import (
    nanmean, nanmedian, array, zeros, ones, nan,
    concatenate, savetxt
)
import argparse
import shared_funcs.funcs as sf

#--- parse args
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
	description="""
	Build running time-averages (of the counts in charge histogram, for
	every energy channel) over the whole time-period. This values
	are needed as normalization factors in the subsequent stages of 
	processing.
	""",
)
parser.add_argument(
'-src', '--dir_src',
type=str,
help='input directory. Should be the output dir of "build_ReduceHistos"',
)
parser.add_argument(
'-fo', '--fname_out',
type=str,
help='output directory, recommended in the form path/<maskname>',
)
parser.add_argument(
'-ini', '--ini_date', 
type=str, 
action=sf.arg_to_datetime,
metavar='IniDateCenter',
default='01/03/2006',
help='initial date, which will be taken as the center of the 1st \
time-window for the calculation of running averages. It should be \
IniDateCenter=<IniDate> + 0.5*NW_MONTH, where <IniDate> is the \
intended initial date to take into account in the whole time period \
of processing. So for ex., if we want to analyze since 01/Jan/2006, and \
NW_MONTH=4, we should specify here 01/Mar/2006 (i.e. 60 days after 01/Jan/2006).',
)
parser.add_argument(
'-nm', '--nmonth',
type=int,
default=4,
metavar='NW_MONTH',
help='width of window (in months) of the running averages.',
)
parser.add_argument(
'-ntm', '--ntmonth',
type=int,
default=8*12, # 9 years in units of months
metavar='NTOTAL_MONTH',
help='total number of months of the whole time period to be \
processed (in months). It **has** to be a multiple of NW_MONTH, so \
that the running windows fit exactly on the whole time period of analysis.',
)
parser.add_argument(
'-m', '--maskname',
type=str,
default='shape.ok_and_3pmt.ok',
help='Quality cut options: \'shape.ok_and_3pmt.ok\', \'3pmt.ok\', or \'all\'. We \
dont make quality cuts at this point. This is just to use the label of the \
previous stages of processing; it\'s healthy to keep track of it.'
)
pa = parser.parse_args()


def date_to_utc(fecha):
    utc = datetime(1970, 1, 1, 0, 0, 0, 0)
    sec_utc = (fecha - utc).total_seconds()
    return sec_utc


this_dir    = '{HOME}/auger.histos_aop'.format(**env)

bins    = 50
H       = zeros(bins)
RANGE   = [800., 1040.] # eye estimation
nshifts = pa.ntmonth/pa.nmonth
avr_tot = zeros((nshifts, 50), dtype=np.float64)   # inicilizar en cero!
time    = zeros((nshifts,1), dtype=np.float64)
nothing = nan*ones(50)

for i in range(nshifts):
    center_shift = i*30*pa.nmonth   # [days] cada cuanto muevo el centro de mi promedio
    center      = pa.ini_date + timedelta(days=center_shift)
    time[i]     = date_to_utc(center)       # [seg utc] tiempo del centro
    print " ---> center: ", center
    ntot        = 0         # para dsps hallar el promedio de todos loq  aportaron

    for shift in range(-30*pa.nmonth/2, 30*pa.nmonth/2):#ventana de promedio centrado
        date  = center + timedelta(days=shift)
        fname = '{src}/{year}/{year}_{mm:02d}_{dd:02d}.nc'.format(
            src=pa.dir_src, year=date.year, mm=date.month, dd=date.day
            )

        if not(isfile(fname)):
            continue  # next date

        try:
            f       = netcdf_file(fname, 'r')
            print ccl.B + "    [*] Opening: %s " % fname + ccl.W
            m       = f.variables['cnts_intgr'].data  # cnts_intgr_id(ntime, nbins_mev)
            avr_aux = nanmean(m, axis=0) # promedio temporal de todos los canales
            if np.isnan(avr_aux[1]): # (*)
                continue
            else:
                avr_tot[i,:] += avr_aux
                ntot         += 1
            f.close()
        
        except KeyboardInterrupt:
            raise SystemExit(
            "\n [!] KEYBOARD-INTERRUPT while processing %s\n"%date.strftime("%d/%b/%Y")
            )

        except:
            print ccl.R + " [*] CORRUPT!: %s " % fname + ccl.W
            pass

    if ntot!=0:
        avr_tot[i,:] /= 1.*ntot
    else:
        avr_tot[i,:] = nan*ones(size(avr_tot, 1))
                
#-----------------------------------------------------------------------
data    = concatenate([time, avr_tot], axis=1)
HEADER	= 'maskname: %s\n' % pa.maskname + \
	'ntmonth: %d\n' % pa.ntmonth + \
	'nmonth: %d\n' % pa.nmonth + \
	'IniDateCenter: %s' % pa.ini_date.strftime('%d/%b/%Y %H:%M')
savetxt(pa.fname_out, data, fmt='%10.9g', header=HEADER)
print ccl.G + "\n [+] generated: %s\n" % pa.fname_out + ccl.W
#
# (*) it's acceptable for 'avr_aux[0]' to be 'nan' but not 
#     the other Ed channels!
#
#EOF
