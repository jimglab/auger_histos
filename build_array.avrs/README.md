## Averages over the SD array:

To build observables averaged in a given array of SD tanks:
```bash
# charge histograms with less time-resolution than in .root
InpHist=/media/scratch1/auger/build_ReducedHistos
OutDir=/media/scratch1/auger/build_array.avrs
# runtime ~1h25m in 8 proc
mpirun -n 8 ./test_2006.py -- \
    -src $InpHist \
    -dst $OutDir \
    -m shape.ok_and_3pmt.ok \
    -ini 01/01/2006 \
    -end 31/12/2013
```

Full description is:

    usage: test_2006.py [-h] [-src DIR_SRC] [-dst DIR_DST] [-ini INI_DATE]
                        [-m MASKNAME] [-end END_DATE]

    optional arguments:
      -h, --help            show this help message and exit
      -src DIR_SRC, --dir_src DIR_SRC
                            input directory. Should be the output dir of
                            "build_ReduceHistos" (default: None)
      -dst DIR_DST, --dir_dst DIR_DST
                            output directory, recommended in the form
                            path/<maskname> (default: None)
      -ini INI_DATE, --ini_date INI_DATE
                            initial date (default: 01/01/2006)
      -m MASKNAME, --maskname MASKNAME
                            Quality cut options: 'shape.ok_and_3pmt.ok',
                            '3pmt.ok', or 'all'. See "cc" variable in
                            calc_array_avrs(). (default: shape.ok_and_3pmt.ok)
      -end END_DATE, --end_date END_DATE
                            end date (default: 31/12/2015)


---
# Output:

The contents of the `yyyy_mm_dd.nc` output files are:
* `t_utc`: `(ntime)` UTC-seconds for this day `dd/mm/yyyy`, in a uniform sequence of 15min.
* `ntanks`: `(ntime)` the number of tanks contributing with non-gap data.
* `cnts_intgr`: `('ntime', 'nbins_mev')` charge-histogram counts (mean values over the SD-array in time-windows of 60min wide; see `ArrayAvrs.calc_array_avrs()` method) for each of the `nbins_mev` deposited-energy channels. These are partial integral over sub-intervals of the deposited-energy Ed domain, and this domain is uniformly partitioned in `nbins_mev` sub-intervals.
* `cnts_std`: `('ntime', 'nbins_mev')` associated standard deviations of the former means.
* `cnts_median`: `('ntime', 'nbins_mev')` associated median values
* `AoP`: `('ntime', 'nstations')` AoP values from the `mc..root` files.
* `AoP_sd`: `('ntime', 'nstations')` AoP values from the `sd..root` files.
* `cnts_intgr_id`: `('ntime', 'nstations', 'nbins_mev')` partial integral of the charge-histogram counts, for each of the `nstations` SD-stations.

See the `ArrayAvrs.save2file()` method for more details.

<!--- EOF -->
