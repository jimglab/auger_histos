#!/usr/bin/env ipython
"""
 NOTE: ignore this type of messages during run:
 Exception AttributeError: "'netcdf_file' object has no attribute 'fp'" in <bound method netcdf_file.close of <scipy.io.netcdf.netcdf_file object at 0x7f68ce956dd0>> ignored

 IMPORTANT: flag 'maskname' refers to charge histogram quality cuts. See 
 calc_array_avrs() routine in funcs.py.
"""
import sys, os		# para pasarle argumento a este script
from funcs import gral, ArrayAvrs
import resource		# to change the max number of open files (ulimit -a)
from datetime import datetime, timedelta
from os import environ as env
import argparse
from mpi4py import MPI
from shared_funcs.funcs import arg_to_datetime, equi_dates

#--- parse args
parser = argparse.ArgumentParser(
    description="""
    Calculate average values (and its associated errors and medians) over the SD array.
    For this, it makes quality cuts based on:\n
    * the status of the 3 PMTs on every SD detector.
    * the position of the 1st peak in the charge histogram of each detector, at each time.
    * number of tanks entering in the array average.
    * AoP array-average values.
    * among other stuff (see ICRC-2015 proceeding)
    """,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
'-src', '--dir_src',
type=str,
help='input directory. Should be the output dir of "build_ReduceHistos"',
)
parser.add_argument(
'-dst', '--dir_dst',
type=str,
help='output directory, recommended in the form path/<maskname>',
)
parser.add_argument(
'-m', '--maskname',
type=str,
default='shape.ok_and_3pmt.ok',
help='Quality cut options: \'shape.ok_and_3pmt.ok\', \'3pmt.ok\', or \'all\'. See "cc" variable in calc_array_avrs().',
)
parser.add_argument(
'-ini', '--ini_date', 
type=str, 
action=arg_to_datetime,
default='01/01/2006',
help='initial date',
)
parser.add_argument(
'-end', '--end_date', 
type=str, 
action=arg_to_datetime,
default='31/12/2015',
help='end date',
)
pa = parser.parse_args()

# cambio el limite de archivos abiertos.
# el 1er argumento es 4096 xq estoy abriendo dos
# archivos por cada vez q llamo a "ArrayAvrs()"
resource.setrlimit(resource.RLIMIT_NOFILE, (4096, 4096))

this_dir    = sys.argv[1] #home+'/ccin2p3/in2p3_data/data_auger/procesamiento/histos.aop_all'
inp	    = gral()
stations    = range(1, 2000+1)      # array q quiero procesar
nstations	= len(stations)

minute      = 60.
inp.wndw    = 60.*minute
inp.sst	    = 15.*minute
mintanks    = 10	# nro minimo de tanques q deben aportar al promedio-sobre-array

comm  = MPI.COMM_WORLD
rank  = comm.Get_rank()
wsize = comm.Get_size()

# define the partition of jobs for each processor
date_bd = equi_dates(pa.ini_date, pa.end_date, wsize)
inp.date_ini = date_bd[rank]
inp.date_end = date_bd[rank+1] - timedelta(days=1)

# report the repartition of jobs
print("")
for _r in range(wsize):
    print(" [*][rank:{rank}/{maxrank}] period for processing: {ini} - {end}".format(
        rank=_r, 
        maxrank=wsize-1,
        ini=inp.date_ini.strftime("%d/%b/%Y %H:%M:%S"),
        end=inp.date_end.strftime("%d/%b/%Y %H:%M:%S")
    ))
print("")

# each proc works on its time period
date = inp.date_ini
while date <= inp.date_end:
    inp.yyyy, inp.mm, inp.dd = date.year, date.month, date.day
    if (date-timedelta(days=1)).day == (date.day-1):
        print(date.strftime(" ---> %b/%Y"))

    aa	= ArrayAvrs(pa.dir_src, pa.dir_dst, 
            mintanks, stations, inp, pa.maskname)
    if aa.file_ok:
        aa.process()
        os.system('ls -lhtr %s' % aa.fname_out)
    del aa
    date += timedelta(days=1)

# (*1): No uso esto xq el np.mean() acumula memoria :-(
# (*2): Para esto, comentar el filtro 'cc &=' adicional en "calc_array_avrs()".
#EOF
