#!/usr/bin/env ipython
"""
 author: j.j. masias meza
"""
from scipy.io.netcdf import netcdf_file
import os
from pylab import *
from numpy import *
import numpy as np
from numpy import (
    zeros, ones, mean, median, nanmedian, nanmean,
    sum, array, empty, nan, arange, std
)
from datetime import datetime, time, timedelta


minute  = 60        # [seg]
hour    = 60*minute # [seg]
day     = 24*hour   # [seg]


class generate_id_histos:
    def __init__(self, fname, dir_dst, stations, inp):
        self.fname  = fname
        if os.path.isfile(fname):           # checko si existe archivo
            self.f      = netcdf_file(fname, 'r')
            self.file_ok    = True
        else:
            print " NO EXISTE: %s" % self.fname
            print " ABORTANDO..."
            self.file_ok    = False

        if self.file_ok==True:
            self.yyyy   = inp.yyyy
            self.mm     = inp.mm
            self.dd     = inp.dd
            self.dir_dst    = dir_dst
            self.stations   = stations
            self.nstations  = len(stations)
            self.dim_cols   = 'ncols'
            self.narrays    = 0
            self.ndummy = 2             # nro inic de filas "dummies" solo para inicializar
            self.add_cols   = 1             # nro de cols adicionales a de los conteos
            self.nbins_mev  = 50                # nro de canales de energia para el output file
            self.ncols  = self.nbins_mev+self.add_cols  # nro total de columnas
            self.hist_corr  = zeros(self.ndummy*self.ncols).reshape(self.ndummy, self.ncols)
            self.fname_out  = '%s/red_%04d_%02d_%02d.nc' % (self.dir_dst, inp.yyyy, inp.mm, inp.dd)
            self.fout   = netcdf_file(self.fname_out, 'w')
            self.fout.createDimension(self.dim_cols, self.ncols)
            self.ch_0   = 19                # columna del canal 0 MeV, en el archivo input

            #------------------------------------------------
        self.nsave  = 0

    def __del__(self):
        if self.file_ok:
            print " ----> cerrando: %s" % self.f.filename
            self.f.close()
            #---- guardamos otros parametros:
            self.fout.createDimension('1', 1)
            self.write_variable('nbins_mev', '1', self.nbins_mev, 'i', '[1]')
            #---- cerramos archivo de salida
            print " ----> cerrando: %s" % self.fout.filename
            self.fout.close()
            del self.f
            del self.fout
            
        print " Eliminando objeto: generate_id_histos()"
        print " tanques guardados: %d" % self.nsave

    def get_histos(self):
        self.m  = array(self.f.variables['matrix'].data)
        self.nx = size(self.m, 0)
        self.ny = size(self.m, 1)

        ch_0000     = self.ch_0
        ch_1000     = self.ch_0 + 1000
        nch         = ch_1000 - ch_0000
        nothing     = zeros(self.nx, dtype=bool)
        #------ iteramos sobre el array
        nstart          = 800           # inicializo
        self.hid_phys   = zeros(nstart*self.ny).reshape(nstart, self.ny)
        self.ntot       = 0
        for i in range(self.nstations):
            self.id = self.stations[i]
            ss      = (self.m[:,2] == self.id)      # selecciono data de este 'id'
            if prod(ss==nothing, dtype=bool):
                print " id: %04d no aporta data!" % self.id

            else:
                self.hid     = self.m[ss, :]        # histos de este 'id'
                self.nhistos = size(self.hid, 0)    # nro de histos de este 'id'
                self.calc_multiplet_order()
                print " id: %04d, singletes/histos: %d/%d" % (self.id, self.N_1er, self.nhistos)
                hid_cnts    = divide(self.hid[:, ch_0000:ch_1000+1].T, self.orden_int).T # conteos corregidos
                #self.save_id_data()
                stuff       = self.hid[:, 0:ch_0000]
                hid_corr    = concatenate([stuff, hid_cnts], axis=1) # concatena en direcc horizontal
                self.ncorr  = size(hid_corr, 0)

                self.hid_phys_check()
                i=self.ntot; ii=i+self.ncorr
                #print " hid_phys: ", size(self.hid_phys[i:ii],0), ", ", size(self.hid_phys[i:ii], 1)
                #print " hid_corr: ", size(hid_corr, 0), ", ", size(hid_corr, 1)
                self.hid_phys[i:ii] = hid_corr

                self.ntot   += self.ncorr

        # lo mismo q tenia en el archivo input, pero ahora los conteos estan
        # corregidos por efecto de "multipletes"
        self.hid_phys   = self.hid_phys[:self.ntot]
        print ' ----> shape    :', shape(self.hid_phys)
        #print ' ----> val      :', self.hid_phys[0][25]
        self.build_file_scaler_sdid()


    def hid_phys_check(self):
        nphys   = size(self.hid_phys, 0)
        if self.ntot+self.ncorr >= nphys:
            aux         = array(self.hid_phys[:self.ntot])
            self.hid_phys   = zeros(2*nphys*self.ny).reshape(2*nphys, self.ny)
            self.hid_phys[:self.ntot] = aux
            del aux
            self.hid_phys_check()


    def build_file_scaler_sdid(self):
        dE  = 20
        che = self.ch_0 + array(range(0,1000+dE,dE)) +1 # rejilla de energias final
        nE  = len(che)
        avr_cnts = zeros(self.ntot*(nE-1)).reshape(self.ntot, nE-1)
        for i in range(nE-1):
            avr_cnts[:,i] = sum(self.hid_phys[:,che[i]:che[i+1]], axis=1)   # integral por bandas de ancho 'dE' MeV
            
        # es una solucion medio chancha pero hace lo q quiero:
        all = array([self.hid_phys[:,0], avr_cnts[:,0]])        # columna tiempo y la 1er cuenta integral
        all = concatenate([all.T, avr_cnts[:,1:nE-1]], axis=1)  # tiempo a la izq de las cuentas integrales
        print " all.shape: ", shape(all)
        
        # creamos 1 matriz por cada tanque:
        nothing = zeros(size(self.ntot), dtype=bool)
        units   = '[cnts/m2/s/%3.1fMeV]' % (1.*dE)
        self.nsave = 0          # quiero contar el nro de array q guardo
        for i in range(self.nstations):
            id  = self.stations[i]
            ss  = self.hid_phys[:,2]==id
            if not(prod(ss==nothing, dtype=bool)):
                var = all[ss,:]
                varname = 'hist_id.%04d' % (id)
                dimname = 'nhist_id.%04d' % (id)
                ndim    = size(var, 0)
                self.fout.createDimension(dimname, ndim)
                dims    = (dimname, self.dim_cols)
                self.write_variable(varname, dims, var, 'd', units)
                self.nsave += 1


    #---- escribe en 'self.fout'
    def write_variable(self, varname, dims, var, datatype, comments):
        dummy           = self.fout.createVariable(varname, datatype, dims)
        dummy[:]        = var
        dummy.units     = comments


    def calc_multiplet_order(self):
        ch_240  = self.ch_0 + 240
        ch_200  = ch_240 - 40
        ch_300  = ch_240 + 60
        avr_240 =  mean(self.hid[:, ch_200:ch_300+1], 1)    # tasas medias entre 200:300 MeV
        min_rate_240 = min(avr_240)

        self.N_1er  = 0
        mean_rate_1er   = 0.0
        """
        for i in range(self.nhistos):
            if round(avr_240[i]/min_rate_240) == 1.0:
                mean_rate_1er   += avr_240[i]
                N_1er       += 1
        """
        cc              = round_(avr_240/min_rate_240) == 1.0
        mean_rate_1er   = mean(avr_240[cc])
        self.N_1er      = sum(cc)

        if self.N_1er==0:
            print " ---> NO PRESENTA DATA!\n Exiting..."
            raise SystemExit
            #sleep(1e6)
        else: 
            mean_rate_1er /= 1.0*self.N_1er     # tasa promedio para los histogramas
                                # singletes en el rango 200-300 MeV
        self.orden  = avr_240/mean_rate_1er
        self.orden_int  = round_(self.orden)        # orden del multiplete (a cada tiempo)


#***************************************************************************
#***************************************************************************
class fechas:
    def __init__(self, yyyy, mm, dd):
        self.yyyy   = yyyy
        self.mm     = mm
        self.dd     = dd
    def __init__(self):
        self.yyyy = -1


#***************************************************************************
#***************************************************************************
class gral:
    pass


#***************************************************************************
#***************************************************************************
def date_to_utc(fecha):
    utc = datetime(1970, 1, 1, 0, 0, 0, 0)
    time = (fecha - utc).total_seconds()
    return time


#***************************************************************************
#***************************************************************************
class massive_reduce:
    def __init__(self, dir_src, dir_dst, stations, YYYY, MM, DD):
        self.dir_src    = dir_src
        self.dir_dst    = dir_dst
        self.stations   = stations
        self.YYYY   = YYYY
        self.MM     = MM
        self.DD     = DD
        #----------------

    def process(self):
        inp = fechas()
        for yyyy in self.YYYY:
            dir_dst2    = '%s/%04d' % (self.dir_dst, yyyy)
            os.system('mkdir -p %s' % dir_dst2)
            for mm in self.MM:
                for dd in self.DD:
                    fname_inp   = '%s/%04d/%04d_%02d_%02d.nc' % (self.dir_src, yyyy, yyyy, mm, dd)
                    inp.yyyy    = yyyy
                    inp.mm      = mm
                    inp.dd      = dd
                    m = generate_id_histos(fname_inp, dir_dst2, self.stations, inp)
                    if m.file_ok:
                        m.get_histos()
                    del m           # para q cierre los archivos y etc.
                    print " ************* OBJETO ELIMINADO ****************"


#***************************************************************************
#***************************************************************************
class ArrayAvrs(object):
    #@profile
    def __init__(self, dir_inp, dir_dst, mintanks, stations, inp, maskname=None):
        self.inp        = inp
        self.dir_inp    = dir_inp
        self.check_files() # determina 'self.file_ok'
        self.maskname   = maskname
        print " file_ok: %d" % int(self.file_ok)
        if self.file_ok:  # significa q el archivo de hoy y/o de ayer existe(n)
            try:
                print "leyendo:\n"
                os.system('ls -lhtr %s %s' % (self.fname_ayer, self.fname_hoy))
                self.finp_hoy   = netcdf_file(self.fname_hoy , 'r')             # READ ONLY
                self.finp_ayer  = netcdf_file(self.fname_ayer, 'r')             # READ ONLY
            except:     # sale error por parte de "hoy"?
                try:    # intentemos con "ayer" solamente
                    self.finp_ayer  = netcdf_file(self.fname_ayer, 'r')     # READ ONLY
                except:                 # el error de lectura tmb se da en "ayer" :(
                    self.file_ok    = False     # entonces los archivos estan corruptos!
                    print " ---> CORRUPTOS :("

        if self.file_ok:                # significa q el archivo de hoy y/o de ayer existe(n) y estan bien
            try:
                self.nbins_mev  =  self.finp_hoy.variables['nbins_mev'].getValue()  # nro de bines MeV
            except:
                self.nbins_mev  = self.finp_ayer.variables['nbins_mev'].getValue()  # nro de bines MeV
            self.dir_dst    = dir_dst
            self.stations   = stations
            self.mintanks   = mintanks
            self.nstations  = len(stations)
            self.ch0    = 8         # el 1er canal de energia del file input
            self.col_aop_mc = 7         # columna del AoP (de los mc..root) (*1)
            self.col_aop_sd = 1         # columna del AoP (de los sd..root) (*2)
            self.col_fpmt   = 2         # columna del flag para los PMTs (debe ser 7)
            self.col_p1max  = 3         # pos of 1st max
            self.col_p1min  = 4         # pos of 1st min
            self.col_w1max  = 5         # width of 1st max #en realidad los limites
            self.col_w1min  = 6         # width of 1st min #en realidad los limites

        print " file_ok: %d" % int(self.file_ok)
        """
        (*1):   En la columna 1, estan los AoPs de los sd..root, pero 
                no los uso para la correcion de A/P
        """


    #@profile
    def __del__(self): #ArrayAvrs
        if self.file_ok:
            try:
                self.finp_hoy.close()
                del self.finp_hoy
            except:
                self.finp_ayer.close()
                del self.finp_ayer

            self.fout.close()
            del self.c
            del self.t
            del self.ntanques
            del self.cts_intgr


    #@profile
    def grab_data(self): #ArrayAvrs
        nstations = len(self.stations)
        # inicializo diccionarios
        self.t      = {}
        self.c      = {}
        self.aop    = {}
        self.aop_sd = {}
        self.fpmt   = {}
        self.p1max  = {}
        for id in self.stations:
            ok  = False
            ok_ayer = False
            ok_hoy  = False
            try:
                if self.file_ayer:          # el archivo existe
                    try:
                        dataid_ayer = array(self.finp_ayer.variables['hist_id.%04d'%id].data)
                        ok_ayer     = True
                    except:
                        ok_ayer     = False

                if self.file_hoy:           # el archivo existe
                    try:
                        dataid_hoy  = array(self.finp_hoy.variables['hist_id.%04d'%id].data)
                        ok_hoy      = True
                    except:
                        ok_hoy      = False

                if (ok_ayer & ok_hoy):      # si este tanque tiene data para ambos dias
                    dataid      = concatenate([dataid_ayer, dataid_hoy], axis=0)
                    del dataid_ayer
                    del dataid_hoy
                elif ok_ayer:               # adopto la data de uno de los 2 dias
                    dataid      = dataid_ayer
                    del dataid_ayer
                elif ok_hoy:                    # o bien solo hay data de hoy o ninguno de ambos dias.
                    dataid      = dataid_hoy    # esto falla si no hay data hoy tampoco, y entonces
                    del dataid_hoy          # dire q " no data for id".
                if ok_ayer or ok_hoy:
                    ok      = True          # llego aqui si adopte data de por lo menos uno de los 2 dias
                    print " [id:%04d] ayer/hoy: %d/%d" % (id, int(ok_ayer), int(ok_hoy))
            except:
                print " [id:%04d] no data :*(" % id
                #continue

            if ok:
                if len(dataid)>1:
                    nid       = size(dataid, 0)
                    name          = '%04d' % id
                    self.t[name]      = dataid[:,0]         # columna tiempo
                    self.c[name]      = dataid[:,self.ch0:]     # las demas columnas (cuentas integrales)
                    self.aop[name]    = dataid[:,self.col_aop_mc]   # AoP de c/tanques (de los mc..root)
                    self.aop_sd[name] = dataid[:,self.col_aop_sd]   # AoP de c/tanques (de los sd..root)
                    self.fpmt[name]   = dataid[:,self.col_fpmt] # flag de estado de los PMTs (debe ser 7)
                    self.p1max[name]  = dataid[:,self.col_p1max]    # pos of 1st max
            else:
                print " [id:%04d] no data" % id


    def check_files(self): #ArrayAvrs
        inp = self.inp
        try:    # puede q esta fecha ni si quiera existe
            #print " ayer: ", (inp.yyyy, inp.mm, inp.dd)
            ayer    = datetime(inp.yyyy, inp.mm, inp.dd, 0,0) - timedelta(1)
            #print " ayer: ", ayer
            yyyy    = ayer.year
            mm      = ayer.month
            dd      = ayer.day
            self.fname_ayer = '%s/%04d/red_%04d_%02d_%02d.nc' % (self.dir_inp, yyyy, yyyy, mm, dd)
            self.file_ayer  = os.path.isfile(self.fname_ayer)       # me dice si el archivo de ayer existe
            #print " ayer: %d" % int(self.file_ayer)
            #os.system('ls -lhtr %s' % self.fname_ayer)
        except:
            self.file_ayer  = False

        self.fname_hoy  = '%s/%04d/red_%04d_%02d_%02d.nc' % (self.dir_inp, inp.yyyy, inp.yyyy, inp.mm, inp.dd)
        self.file_hoy   = os.path.isfile(self.fname_hoy)
        self.file_ok    = (self.file_ayer or self.file_hoy)


    #@profile
    def calc_array_avrs(self): #ArrayAvrs
        wndw    = self.inp.wndw # [seg]
        sst     = self.inp.sst  # [seg]
        yyyy    = self.inp.yyyy
        mm      = self.inp.mm
        dd      = self.inp.dd
        to      = date_to_utc(datetime(yyyy, mm, dd, 0, 0)) # [sec-utc] tiempo 00:00hs de este dia
        Dt      = 1.*day            # [sec] tiempo total q abarca el procesamiento de datos
        #ntime  = int(dt/sst)
        time    = arange(to, to+Dt, sst) # rejilla temporal --> 47 elementos para sst=30min, dt=(1dia-30min)
        self.ntime  = ntime   = len(time);    #self.ntime = ntime

        nstations   = len(self.stations)
        nbins_mev   = self.nbins_mev
        basura      = nan*ones(nbins_mev) # para rellenar cuando no haya suficientes tanques a un tiempo dado
        self.cts_intgr  = zeros((ntime, nbins_mev), dtype=np.float64)
        self.std_cts    = empty((ntime, nbins_mev))
        self.median_cts = empty((ntime, nbins_mev))
        self.ntanques   = zeros(ntime)       # nro de tanques q aportan en c/tiempo (0 por defecto)
        self.AoP        = nan*ones((ntime, nstations), dtype=np.float64)
        self.AoP_sd     = nan*ones((ntime, nstations), dtype=np.float64)
        self.cts_id     = nan*ones((ntime, nstations, nbins_mev), dtype=np.float64)
        #cts_intgr_id   = zeros(self.nstations*nbins_mev).reshape(self.nstations, nbins_mev)
        #self.cts_std   = -1.*ones(ntime)   # desv standard, -1 por defecto

        for j in range(ntime):
            nb      = 0         # nro de tanques q aportan data
            cts_now = empty((self.nstations, nbins_mev)) # (**)

            for id in self.stations:
                name    = '%04d' % id
                if name in self.t.keys():   # ver si este tanque aporta data en el archivo 'finp_hoy/ayer'
                    ti  = time[j] - wndw/2.
                    tf  = time[j] + wndw/2.
                    cc  = (self.t[name]>ti) & (self.t[name]<tf) # defino ventana temporal
                    #-------------- filtros
                    # for ICRC 2015 we use 'shape.ok_and_3pmt.ok'
                    if self.maskname=='shape.ok_and_3pmt.ok':
                        cc  &= self.fpmt[name]==7     # (*3) 
                        cc  &= self.p1max[name]>20.0  # (*4) 

                    elif self.maskname=='3pmt.ok':
                        cc  &= self.fpmt[name]==7     # (*3) 

                    elif self.maskname=='all':        # no quality cut
                        pass    # get out of if-statement and continue below

                    else:
                        print " ##### ERROR #####: need to choose correct maskname!"
                        print " Exiting...\n"
                        raise SystemExit

                    #----------------------
                    if len(find(cc))>0:     # hay datos en esta ventana de tiempo?
                        cts_now[nb, :]      = mean(self.c[name][cc,:], axis=0)   # (*1)
                        self.cts_id[j,id-1,:]   = cts_now[nb, :]
                        self.cts_intgr[j,:] += cts_now[nb, :]         #uso menos memoria asi q usando mean()
                        self.AoP[j,id-1]    = mean(self.aop[name][cc], axis=0)
                        self.AoP_sd[j,id-1] = mean(self.aop_sd[name][cc], axis=0)
                        nb          += 1

            if nb>=self.mintanks:
                self.cts_intgr[j,:]     /= 1.*nb                # normalizo x el nro de tanques q aportaron
                self.ntanques[j]        = nb
                self.std_cts[j,:]       = std(cts_now[:nb, :], axis=0)   # (*2) desv standard a lo largo del array
                self.median_cts[j,:]    = median(cts_now[:nb, :], axis=0)
            else:
                self.cts_intgr[j,:]     = basura                # NaNs si no hay suficientes tanques
                self.ntanques[j]        = 0
                self.std_cts[j,:]       = basura                # ""
                self.median_cts[j,:]    = basura                # ""
        #
        self.time   = time
        del time
        #del cts_intgr_id
        del basura
        del cc
        """
         (**):  aqui pongo los conteos de todos los tanques para 
                este tiempo 'time[j]', contenido en este 
                archivo 'finp_ayer/hoy'. Puede haber menos de 
                'self.nstations' en este tiempo. El total de tanques 
                q aportan en este tiempo lo dice 'nb'.
         (*2):  notar q el ultimo indice con el q se escribio a 
                "cts_now[]" es nb-1.
         (*3):  escojemos tanques q tienen los 3PMTs ok!
         (*4):  position of 1st max of charge histog
        """


    #*******************************************
    #@profile
    def save2file(self):
        try:
            dir_dst2    = '%s/%02dmin/%04d' % (self.dir_dst, self.inp.sst/minute,self.inp.yyyy)
            os.system('mkdir -p %s' % dir_dst2)
            self.fname_out  = '%s/%04d_%02d_%02d.nc' % (dir_dst2, self.inp.yyyy, self.inp.mm, self.inp.dd)
            self.fout   = netcdf_file(self.fname_out, 'w')
            print " ----> guardando: %s" % self.fname_out

            #------- dimensiones
            self.fout.createDimension('nstations', 2000)
            self.fout.createDimension('nbins_mev', self.nbins_mev)
            self.fout.createDimension('ntime', self.ntime)
            #------------

            #-------------------- data
            dims    = ('ntime',)
            self.write_variable('t_utc', dims, self.time, 'i', '[sec-utc]')
            self.write_variable('ntanks', dims, self.ntanques, 'i', '[1]')

            dims    = ('ntime', 'nbins_mev')
            self.write_variable('cnts_intgr', dims, self.cts_intgr, 'd', '[cts/s/m2/20MeV]')
            self.write_variable('cnts_stds', dims, self.std_cts, 'd', '[cts/s/m2/20MeV]')
            self.write_variable('cnts_median', dims, self.median_cts, 'd', '[cts/s/m2/20MeV]')

            dims    = ('ntime', 'nstations')
            self.write_variable('AoP'   , dims, self.AoP   , 'd', '[1]')
            self.write_variable('AoP_sd', dims, self.AoP_sd, 'd', '[1]')

            dims    = ('ntime', 'nstations', 'nbins_mev')
            self.write_variable('cnts_intgr_id', dims, self.cts_id, 'd', '[cts/s/m2/20MeV]')
            #--------------------------

        except KeyboardInterrupt:
            print " KEYBOARD-INTERRUPT @save2file()....... exit gracefully :)"
            raise SystemExit
    #
    # (*2)  la 1ra y 2da columna es tiempo y nro de tanques respectivamente
    #
    # (*1)  promedio todos los datos q tenga este tanque para c/canal de
    #       energia. El promedio se hace sobre el tiempo; escoge todos los
    #   tiempos q caigan en la ventana (ti, tf)


    #@profile
    def process(self):
        self.grab_data()        # leo la data hacia las variables 'c' y 't'
        print " ----> calculando promedio sobre el array..."
        self.calc_array_avrs()      # calculo averages hacia 'self.cts_intgr' (tiempo x energia)
        self.save2file()        # guardo 'self.cts_intgr' en ascii


    def write_variable(self, varname, dims, var, datatype, comments):
        dummy           = self.fout.createVariable(varname, datatype, dims)
        dummy[:]        = var
        dummy.units     = comments


#EOF
