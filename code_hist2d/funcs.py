#------------------------------------------------
from pylab import figure, show, close, savefig
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt 
import numpy as np
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib.colors import LogNorm
#------------------------------------------------
from scipy.io.netcdf import netcdf_file
import os, h5py
from datetime import datetime, timedelta
import console_colors as ccl


def build_h2d(fname, cfnm, ddmmyyyy, typic, nstations_max, pa, nE, RANGE):
    dd, mm, yyyy = ddmmyyyy # month and day
    f      = netcdf_file(fname, 'r')
    print ccl.R + " [*] Opening: %s" % fname + ccl.W
    m      = f.variables['cnts_intgr_id'].data.copy() # cnts_intgr_id(ntime, nstations, nbins_mev)
    AoP    = f.variables['AoP'].data.copy()
    ntanks = f.variables['ntanks'].data.copy()
    cc     = ntanks > pa.ntanks_min
    n      = cc.nonzero()[0].size
    aop    = AoP[cc,:].reshape(n*nstations_max)

    # <AoP> stuff
    cc_aop   = ~(np.isnan(aop) | np.isinf(aop))
    mean_aop = aop[cc_aop].sum()
    n_aop    = cc_aop.nonzero()[0].size
    assert not(np.isinf(mean_aop) or np.isnan(mean_aop)),\
    '\n [-] we found non-numeric values for AoP on "%s"\n'%fname

    H_today = np.empty((nE,pa.bins,pa.bins)) # buffer for today only
    #cc    = np.isnan(rate1)
    for i in range(nE):
        # grab all the count rates for all the array, but for
        # this energy channel only
        rate       = m[cc,:,i].reshape(n*nstations_max) / typic[i]
        H_today[i] = np.histogram2d(aop, rate, 
            bins=[pa.bins, pa.bins], 
            range=RANGE, 
            normed=False)[0]

    if pa.usecache: # build/contribute to cache
        # open in append mode (Read/write if exists, create otherwise)
        cf      = h5py.File(cfnm,'a')
        # dataset name
        ds_name = '%04d/%02d/%02d'%(yyyy,mm,dd)
        if ds_name not in cf:
            cf.create_dataset(
                ds_name,
                dtype = 'f',
                shape = (nE,pa.bins,pa.bins),
                fillvalue = 0.0,
                compression = 'gzip',
                compression_opts = 9,
            )

        cf[ds_name][...] = H_today[:,:,:] # (nE,bins,bins)

        # save metadata if not present
        if 'meta' not in cf.keys():
            cf['meta/bins']     = pa.bins
            cf['meta/range_x']  = pa.range_x
            cf['meta/range_y']  = pa.range_y
            cf['meta/mintank']  = pa.ntanks_min
            cf['meta/ini_date'] = pa.ini_date.strftime('%d/%m/%Y')
            cf['meta/end_date'] = pa.end_date.strftime('%d/%m/%Y')

        cf.close()

    f.close()

    return H_today, mean_aop, n_aop

def equi_days(dini, dend, n):
    """
    returns most equi-partitioned tuple of number 
    of days between dates 'dini' and 'dend'
    """
    days = (dend - dini).days
    days_part = np.zeros(n, dtype=np.int)
    resid = np.mod(days, n)
    for i in range(n-resid):
        days_part[i] = days/n

    # last positions where I put residuals
    last = np.arange(start=-1,stop=-resid-1,step=-1)
    for i in last:
        days_part[i] = days/n+1

    assert np.sum(days_part)==days, \
        " --> somethng went wrong!  :/ "

    return days_part


def equi_dates(dini, dend, n):
    """
    returns:
    - inis[:-1]: 'n' start-dates of most equi-partitioned 
                 contiguous blocks of dates between 'dini' 
                 and 'end'
    - inis[-1]: 'dend' (max date of whole block)
    """
    days_part = equi_days(dini, dend, n)
    inis = np.empty(n+1, dtype=datetime)

    inis[0]  = dini
    inis[-1] = dend
    for i in range(1, inis.size-1):
        inis[i] = inis[i-1] + timedelta(days=days_part[i-1])

    return inis


def make_hist2d(fname, lim, cbmax, title, i, dir_fig):
    H2D     = np.loadtxt(fname, unpack=True) + 1. # le sumo uno para q sea >0.
    bins    = H2D.shape[0]

    aop     = linspace(2., 5., bins)
    scal    = linspace(lim[0], lim[1], bins)

    fig     = figure(1, figsize=(6, 4))
    ax      = fig.add_subplot(111)

    CBMIN   = 1.0
    CBMAX   = cbmax #6e6
    surf    = ax.contourf(aop, scal, H2D, facecolors=cm.jet(H2D), linewidth=0, 
            cmap=cm.gray_r, alpha=0.9, vmin=CBMIN, vmax=CBMAX, norm=LogNorm())

    m = cm.ScalarMappable(cmap=surf.cmap, norm=surf.norm)
    m.set_array(H2D)

    axcb = plt.colorbar(m)
    LABEL_COLOR_BAR = 'points per bin square'
    axcb.set_label(LABEL_COLOR_BAR, fontsize=20)
    ax.set_ylabel('partial-integrated rate [cts/s/m2]')
    ax.set_xlabel('AoP')
    ax.set_title(title)

    m.set_clim(vmin=CBMIN, vmax=CBMAX)
    ax.grid()
    plt.tight_layout()

    fname_fig = '%s/hist2d_%02d.png' % (dir_fig, i)
    savefig(fname_fig, dpi=135, bbox_inches='tight')
    close()


def make_fitplot_muon(fname, stuff, cbmax, title, i):
    xlim    = stuff[0][0]
    ylim    = stuff[0][1]
    m   = stuff[1][0]
    b   = stuff[1][1]
    xvec    = stuff[2][0]
    yvec    = stuff[2][1]

    H2D = np.loadtxt(fname, unpack=True) + 1. # le sumo uno para q sea >0.
    bins    = H2D.shape[0]

    aop = linspace(xlim[0], xlim[1], bins)
    scal    = linspace(ylim[0], ylim[1], bins)

    fig     = figure(1, figsize=(6, 4))
    ax      = fig.add_subplot(111)

    CBMIN   = 1.0
    CBMAX   = cbmax #6e6
    surf    = ax.contourf(aop, scal, H2D, facecolors=cm.jet(H2D), linewidth=0, 
            cmap=cm.gray_r, alpha=0.9, vmin=CBMIN, vmax=CBMAX, norm=LogNorm())
    LABEL = '8yr fit:\n m=%3.2f\n b=%3.1f' % (m, b)
    ax.plot(xvec, m*xvec+b, '--', lw=3., c='red', alpha=0.6, label=LABEL)

    m = cm.ScalarMappable(cmap=surf.cmap, norm=surf.norm)
    m.set_array(H2D)

    axcb = plt.colorbar(m)
    LABEL_COLOR_BAR = 'points per bin square'
    axcb.set_label(LABEL_COLOR_BAR, fontsize=12)
    ax.set_ylabel('$H_{id,e}$')
    ax.set_xlabel('$AoP_{id}$')
    ax.set_title(title)
    #ax.set_ylim(ylim[0], ylim[1])  # no deberia necesitarlo
    #ax.set_xlim(xlim[0], xlim[1])  # no deberia necesitarlo

    m.set_clim(vmin=CBMIN, vmax=CBMAX)
    ax.grid()
    plt.tight_layout()
    ax.legend(loc='best', fontsize=12)

    fname_fig = '../../figs/hist2d_fit/hist2d_muon_%02d'%i
    savefig('%s.png'%fname_fig, format='png', dpi=135, bbox_inches='tight')
    savefig('%s.eps'%fname_fig, format='eps', dpi=135, bbox_inches='tight')
    close()


def make_fitplot_muon_poster(fname, stuff, cbmax, title, i):
    xlim    = stuff[0][0]
    ylim    = stuff[0][1]
    m   = stuff[1][0]
    b   = stuff[1][1]
    xvec    = stuff[2][0]
    yvec    = stuff[2][1]

    H2D = np.loadtxt(fname, unpack=True) + 1. # le sumo uno para q sea >0.
    bins    = H2D.shape[0]

    aop = linspace(xlim[0], xlim[1], bins)
    scal    = linspace(ylim[0], ylim[1], bins)

    fig     = figure(1, figsize=(6, 4))
    ax  = fig.add_subplot(111)

    CBMIN   = 1.0
    CBMAX   = cbmax #6e6
    surf    = ax.contourf(aop, scal, H2D, facecolors=cm.jet(H2D), linewidth=0, 
            cmap=cm.gray_r, alpha=0.9, vmin=CBMIN, vmax=CBMAX, norm=LogNorm())
    LABEL = '8yr fit:\n m=%3.2f' % (m)
    ax.plot(xvec, m*xvec+b, '--', lw=3., c='red', alpha=0.6, label=LABEL)

    m = cm.ScalarMappable(cmap=surf.cmap, norm=surf.norm)
    m.set_array(H2D)

    axcb = plt.colorbar(m)
    LABEL_COLOR_BAR = 'points per bin square'
    axcb.set_label(LABEL_COLOR_BAR, fontsize=14)
    ax.set_ylabel('$H_{id,e}$', fontsize=17)
    ax.set_xlabel('$AoP_{id}$', fontsize=19)
    ax.set_title(title, fontsize=18)
    #ax.set_ylim(ylim[0], ylim[1])  # no deberia necesitarlo
    #ax.set_xlim(xlim[0], xlim[1])  # no deberia necesitarlo

    m.set_clim(vmin=CBMIN, vmax=CBMAX)
    ax.grid()
    plt.tight_layout()
    ax.legend(loc='best', fontsize=15)

    fname_fig = '../../figs/hist2d_fit/hist2d_muon.poster_%02d'%i
    savefig('%s.png'%fname_fig, format='png', dpi=135, bbox_inches='tight')
    savefig('%s.eps'%fname_fig, format='eps', dpi=135, bbox_inches='tight')
    close()


#EOF
