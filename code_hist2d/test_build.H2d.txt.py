#!/usr/bin/env ipython
"""
 author: j.j. masias meza

 runtime:
real    160m50.079s
user    112m59.592s
sys     8m39.348s
"""
import numpy as np
from scipy.io.netcdf import netcdf_file
import console_colors as ccl
from os import environ as env
from datetime import datetime, timedelta
import argparse, os, sys
import shared_funcs.funcs as sf
import funcs as ff
import h5py

#--- parse args
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    description="""
    Build 2D histograms of the relation between AoP and 
    charge-histogram rates.
    It uses data of individual SD tanks.
    """,
)
parser.add_argument(
'-src', '--dir_src',
type=str,
help='input directory. Should be the output dir of "build_array.avrs"',
)
parser.add_argument(
'-fi', '--fname_inp',
type=str,
help='input ASCII file. Should be the output dir of "build_avr_histos"',
)
parser.add_argument(
'-dst', '--dir_dst',
type=str,
help='directory for output ASCII files, containing the 2D histograms. \
One file per energy channel.',
)
parser.add_argument(
'-ini', '--ini_date', 
type=str, 
action=sf.arg_to_datetime,
metavar='IniDateCenter',
default='01/03/2006',
help='Start time from which we build the accumulated 2D histograms.',
)
parser.add_argument(
'-end', '--end_date', 
type=str, 
action=sf.arg_to_datetime,
metavar='IniDateCenter',
default='01/03/2006',
help='Stop time until which we build the accumulated 2D histograms.',
)
parser.add_argument(
'-m', '--maskname',
type=str,
default='shape.ok_and_3pmt.ok',
help='Quality cut options: \'shape.ok_and_3pmt.ok\', \'3pmt.ok\', or \'all\'. We \
dont make quality cuts at this point. This is just to use the label of the \
previous stages of processing; it\'s healthy to keep track of it.'
)
parser.add_argument(
'-bins', '--bins',
type=int,
default=50,
help='number of bins by which the input 2D histograms are discretized. Must \
be the same number as the bins in the input files in DIR_DST directory.',
)
parser.add_argument(
'-rx', '--range_x',
type=np.float64,
nargs=2,
default=[2.6, 4.6],
metavar=('XMIN','XMAX'),
help='x-range (AoP limits) where to build the 2D histograms',
)
parser.add_argument(
'-ry', '--range_y',
type=np.float64,
nargs=2,
default=[0.7, 1.4],
metavar=('YMIN','YMAX'),
help='y-range (normalized rate limits) where to build the 2D histograms',
)
parser.add_argument(
'-mintank', '--ntanks_min',
type=int,
default=150,
help="""
For each data-point in the input files, there is an associated 
number-of-tanks field. Such value is the number of SD tanks that 
were filtered-in in the time window of each timestamp, in the energy interval 
of that data-point.
So such value gives us the 'statistics' at each timestamp.
With this argument, we can choose a lower threshold to filter the
data-points by its 'statistics' weight.
""",
)
parser.add_argument(
'-c', '--usecache',
action='store_true',
default=False,
help='use if it\'s intended to use the "cache system" to store daily 2D-histograms.',
)
pa = parser.parse_args()


#---------------------------------------- grab typical values
prom    = np.loadtxt(pa.fname_inp)
typic   = np.mean(prom[:,1:], axis=0) # the averages of the entire array for this whole time period

#-------------------------------------------------------------------------------------
nE      = 50    	# number of energy channels
H       = np.zeros((nE, pa.bins, pa.bins))
ntot    = 0     	# monitoring
RANGE   = [pa.range_x, pa.range_y]

nstations_max   = 2000

# buffer to calculate the global average <AoP>
buff_aop = {'mean_aop': 0.0, 'n_aop': 0}

os.system('mkdir -p ' + pa.dir_dst)
if pa.usecache:
    cfnm     = pa.dir_dst + '/h2d_cache.h5'

date        = pa.ini_date
yyyy_, mm_  = date.year-1, date.month-1 # buffer of "yesterday"
while date <= pa.end_date:
    yyyy, mm, dd = date.year, date.month, date.day
    if yyyy != yyyy_:
        print ccl.Gn + " ----> YEAR: %d" % yyyy + ccl.W
    if mm != mm_:
        print ccl.Gn + " ---> month: %02d" % mm + ccl.W
    yyyy_, mm_ = yyyy, mm

    fname = '%s/%04d/%04d_%02d_%02d.nc' % (pa.dir_src, yyyy, yyyy, mm, dd)
    #print " fname: ", fname; raw_input()
    date += timedelta(days=1)  # next day
    if not(os.path.isfile(fname)):
        continue  # next date

    # TODO: check if there already exists a cache file for the 2D-histogram. 
    # Else build the bloody 2d-hist.
    # NOTE: one cache file per year
    if pa.usecache and os.path.isfile(cfnm):
        cf = h5py.File(cfnm,'r')

        # TODO: check if this existing 2D-hist has has the same
        # bins, limits, mintank, date-borders, etc that we need || build it below.
        assert not(pa.bins==cf['meta/bins'].value \
            and all([_1==_2 for _1,_2 in zip(pa.range_x,cf['meta/range_x'].value)]) \
            and all([_1==_2 for _1,_2 in zip(pa.range_y,cf['meta/range_y'].value)]) \
            and pa.ntanks_min==cf['meta/mintank']), \
            '\n [-] the existing cache is not consistent with some of these parameters: bins, range_x, range_y.\n'
            # go build the bloody 2d-hist below
            #break

        ds_name = '%04d/%02d/%02d'%(yyyy,mm,dd)
        if ds_name in cf:
            # Grab the 2D-histogram (likely)
            print ccl.R+" [*] Grabbing daily 2D-hist from cache '%s'"%ds_name+ccl.W
            for i in range(nE):
                H[i] += cf[ds_name][...][i]

            cf.close()
            # no need to calculate the 2D-hist
            continue # next date

        cf.close()

    # build the 2D-hist (and the cache of it)
    try:
        H_today, mean_aop, n_aop = ff.build_h2d(
            fname, 
            cfnm if pa.usecache else None, 
            [dd,mm,yyyy], 
            typic,
            nstations_max, 
            pa, 
            nE, 
            RANGE
        )
        for i in range(nE):
            H[i] += H_today[i]

        buff_aop['mean_aop'] += mean_aop
        buff_aop['n_aop']    += n_aop

        ntot += 1

    except KeyboardInterrupt:
        print "\n [!] KEYBOARD-INTERRUPT while processing %s\n"%date.strftime("%d/%b/%Y")
        # clean cache if necessary
        if pa.usecache:
            cf = h5py.File(cfnm,'a')
            # pop the cache for today, because most likely 
            # it wasn't saved correctly
            print "\n [*] removing last cache entry...\n"
            if ds_name in cf: cf.pop(ds_name)
            cf.close()

        sys.exit(0)

    #except:
    #    print " CORRUPT!: %s" % fname
    #    pass


# now the actual average <AoP>
AOP_AVR = buff_aop['mean_aop'] / buff_aop['n_aop']

#--------------------------------------------------------------------
# same header for all files
HEADER='lim-AoP    : %g:%g\n' % (pa.range_x[0],pa.range_x[1]) +\
       'lim-rate   : %g:%g\n' % (pa.range_y[0],pa.range_y[1]) +\
       'AoP_mean   : %9.8g\n' % AOP_AVR +\
       'bins       : %d\n'    % pa.bins +\
       'ntanks_min : %d\n'    % pa.ntanks_min +\
       'maskname   : %s\n'    % pa.maskname +\
       'ini_date   : %s\n'    % pa.ini_date.strftime('%d/%b/%Y %H:%M') +\
       'end_date   : %s'      % pa.end_date.strftime('%d/%b/%Y %H:%M')

# save the ascii files
os.system('mkdir -p '+pa.dir_dst)
for i in range(nE):
    fname_out = '%s/H_%02d_%s.txt' % (pa.dir_dst, i, pa.maskname)
    np.savetxt(fname_out, H[i], fmt='%9.9g', header=HEADER)

print " [*] maskname: %s" % pa.maskname
print " [*] histos2D in " + pa.dir_dst
#--------------------------------------------------------------------

#EOF
