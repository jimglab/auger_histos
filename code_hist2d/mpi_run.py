#!/usr/bin/env ipython
"""
 author: j.j. masias meza

 runtime:
real    160m50.079s
user    112m59.592s
sys     8m39.348s
"""
from pylab import *
import numpy as np
from scipy.io.netcdf import netcdf_file
import os
import console_colors as ccl
from os import environ as env
from datetime import datetime, timedelta
from funcs import equi_dates
from mpi4py import MPI

home        = os.environ['HOME']
#this_dir    = '%s/auger/histos.aop_all' % home
this_dir    = '{HOME}/auger.histos_aop'.format(**env)
maskname    = 'shape.ok_and_3pmt.ok' #'shape.ok_and_3pmt.ok' #'3pmt.ok' # 'all'
dir_src     = '{HOME}/auger/histogramas/netcdf/all.array.avrs/{msk}/15min'.format(msk=maskname, **env)
#dir_src2    = '%s/auger/histogramas/scripts/build_avr_histos' % home
dir_src2    = '%s/out/out.build_avr_histos/%s/15min' % (this_dir, maskname)
dir_dst     = '%s/out/out.code_hist2d/ascii' % this_dir

#---------------------------------------- agarro valores tipicos para construir rangos
fname_avr   = '%s/avr_histos_%s.txt' % (dir_src2, maskname)
prom        = np.loadtxt(fname_avr)
typic       = np.mean(prom[:,1:], axis=0)   # agarro el promedio del array de los 8 anios!

#------------------------------------------------------------MPI
comm    = MPI.COMM_WORLD
rank    = comm.Get_rank()
wsize   = comm.Get_size()
#---------------------------------------------------------------

bins    = 50
nEd     = 50
H        = np.zeros((nEd,bins,bins), dtype=np.float64)
ntot     = 0         # monitoring
RANGE    = [[2.6, 4.6], [0.1, 2.5]]

#--- settings
year_ini        = 2006      # inclusive
year_fin        = 2013      # inclusive
nyears          = year_fin-year_ini+1
ntanks_min      = 150       # minimum nmbr of stations that I want
nstations_max   = 2000

#--- equi-partition of dates
DateIni = datetime(year_ini, 1, 1)
DateEnd = datetime(year_fin, 12, 31)
date_bd = equi_dates(DateIni, DateEnd, wsize)
date_ini = date_bd[rank]
date_end = date_bd[rank+1] - timedelta(days=1)

#--- info to screen
date = date_ini #datetime(year_ini, 1, 1)
print " [r:{rank}] ---> MY PERIOD: {dini} TO {dend}".format(rank=rank, dini=date, dend=date_end)

#--- build histogram 2d-arrays
yyyy_, mm_ = date.year-1, date.month-1
while date <= date_end: #datetime(year_fin, 12, 31):
    yyyy, mm, dd = date.year, date.month, date.day
    if yyyy != yyyy_:
        print ccl.Gn + " ----> YEAR: %d" % yyyy + ccl.W
    if mm != mm_:
        print ccl.Gn + " ---> mes: %02d" % mm + ccl.W
    yyyy_, mm_ = yyyy, mm

    fname = '%s/%04d/%04d_%02d_%02d.nc' % (dir_src, yyyy, yyyy, mm, dd)
    date += timedelta(days=1)  # next day
    #print " fname: ", fname; raw_input()
    if not(os.path.isfile(fname)):
        continue  # next date

    try:
        f       = netcdf_file(fname, 'r')
        print ccl.R + "    --> Abriendo: %s" % fname + ccl.W
        m       = f.variables['cnts_intgr_id'].data.copy() # cnts_intgr_id(ntime, nstations, nbins_mev)
        AoP     = f.variables['AoP'].data.copy()
        ntanks  = f.variables['ntanks'].data.copy()
        cc      = ntanks>ntanks_min
        n       = len(find(cc))
        aop     = AoP[cc,:].reshape(n*nstations_max)
        #cc     = np.isnan(rate1)
        #for i in range(nEd_rank*rank, nEd_rank*(rank+1)):
        for i in range(nEd):
            rate    = m[cc,:,i].reshape(n*nstations_max) / typic[i]
            H[i]    += np.histogram2d(aop, rate, 
                        bins=[bins, bins], 
                        range=RANGE, 
                        normed=False)[0]

        ntot    += 1
        f.close()
        #if ntot==90:
        #   savetxt('./test.txt', H[12], fmt='%9.9g')

    except KeyboardInterrupt:
        print " KEYBOARD-INTERRUPT!"
        raise SystemExit

    except:
        print " CORRUPTO!: %s" % fname
        pass

#--- now let's gather data from all processors
#------ enviamos la data al "root"
if rank!=0: 
    print " [r:%d] ---> sending data to root :)" % rank
    comm.Send([H, MPI.FLOAT], dest=0, tag=777)

#----- unificar la data de *todos* los procesadores!
if rank==0:
    print " [r:0] ---> now gathering all data :)"
    data = np.empty(H.shape, dtype=np.float64) # buffer pa recibir data
    for sender in range(1, wsize):
        comm.Recv([data, MPI.FLOAT], source=sender, tag=777)
        H += data

    #--- now save to file
    #for i in range(nEd_rank*rank, nEd_rank*(rank+1)):
    for i in range(nEd):
        fname_out = '%s/H_%02d_%s.txt' % (dir_dst, i, maskname)
        np.savetxt(fname_out, H[i], fmt='%9.9g')

    print " ---> maskname: %s" % maskname
    print " ---> histos2D en " + dir_dst

#EOF
