#!/usr/bin/env ipython
"""
 author: j.j. masias meza

 note: if running on a remote server, must set 
 a DISPLAY environment variable, and redirection it to 
 virtual screen server (i.e. xpra works ok)
"""
from funcs import *

home        = os.environ['HOME']
this_dir    = '%s/auger/histos.aop_all' % home
dir_src     = '%s/out/out.code_hist2d/ascii' % this_dir
dir_fig     = '%s/out/out.code_hist2d/figs/hist2d' % this_dir
maskname    = 'shape.ok_and_3pmt.ok' #'shape.ok_and_3pmt.ok' #'3pmt.ok' # 'all'

lims	= [0.1, 2.5]
cbmax	= 6e6
for i in range(50):
	fname	= '%s/H_%02d_%s.txt' % (dir_src, i, maskname)
	title	= "Ed_%02d [20*MeV]"  % i
	make_hist2d(fname, lims, cbmax, title, i, dir_fig)
#
##
