## Build the 2D histograms 

Build a 2D histogram of the relation between the AoP values and the charge-histogram 
rates, at individual tank scale.
```bash
./test_build.H2d.txt.py --pdb -- \
    -src ~oauger/build_array.avrs/15min \
    -fi ~oauger/build_avr_histos/avr_histos__Jan2006-Dec2014.txt \
    -dst ~oauger/code_hist2d \
    -m shape.ok_and_3pmt.ok \
    -bins 50 \
    -rx 2.6 4.6 \
    -ry 0.1 2.5 \
    -mintank 150 \
    -ini 01/01/2006 \
    -end 31/12/2014
```

See full description:

	$ ./test_build.H2d.txt.py -- -h
	usage: test_build.H2d.txt.py [-h] [-src DIR_SRC] [-fi FNAME_INP]
								 [-dst DIR_DST] [-ini IniDateCenter]
								 [-end IniDateCenter] [-m MASKNAME] [-bins BINS]
								 [-rx XMIN XMAX] [-ry YMIN YMAX]
								 [-mintank NTANKS_MIN]

	Build 2D histograms of the relation between AoP and charge-histogram rates. It
	uses data of individual SD tanks.

	optional arguments:
	  -h, --help            show this help message and exit
	  -src DIR_SRC, --dir_src DIR_SRC
							input directory. Should be the output dir of
							"build_array.avrs" (default: None)
	  -fi FNAME_INP, --fname_inp FNAME_INP
							input ASCII file. Should be the output dir of
							"build_avr_histos" (default: None)
	  -dst DIR_DST, --dir_dst DIR_DST
							directory for output ASCII files, containing the 2D
							histograms. One file per energy channel. (default:
							None)
	  -ini IniDateCenter, --ini_date IniDateCenter
							Start time from which we build the accumulated 2D
							histograms. (default: 01/03/2006)
	  -end IniDateCenter, --end_date IniDateCenter
							Stop time until which we build the accumulated 2D
							histograms. (default: 01/03/2006)
	  -m MASKNAME, --maskname MASKNAME
							Quality cut options: 'shape.ok_and_3pmt.ok',
							'3pmt.ok', or 'all'. We dont make quality cuts at this
							point. This is just to use the label of the previous
							stages of processing; it's healthy to keep track of
							it. (default: shape.ok_and_3pmt.ok)
	  -bins BINS, --bins BINS
							number of bins by which the input 2D histograms are
							discretized. Must be the same number as the bins in
							the input files in DIR_DST directory. (default: 50)
	  -rx XMIN XMAX, --range_x XMIN XMAX
							x-range (AoP limits) where to build the 2D histograms
							(default: [2.6, 4.6])
	  -ry YMIN YMAX, --range_y YMIN YMAX
							y-range (normalized rate limits) where to build the 2D
							histograms (default: [0.1, 2.5])
	  -mintank NTANKS_MIN, --ntanks_min NTANKS_MIN
							For each data-point in the input files, there is an
							associated number-of-tanks field. Such value is the
							number of SD tanks that were filtered-in in the time
							window of each timestamp, in the energy interval of
							that data-point. So such value gives us the
							'statistics' at each timestamp. With this argument, we
							can choose a lower threshold to filter the data-points
							by its 'statistics' weight. (default: 150)


---
## Fit the 2D histograms (&& plot optionally)

Fit the correlations && optionally make a figure of such a fit for every energy channel.
In case you are working on a remote server, set the `$DISPLAY` environment to `:200` for 
example, so that you can generate the figures.

```bash
# if working on a remote server, you might need to:
export DISPLAY=:200  # or any other number

# run
./make_fits.py --pdb -- \
    -src /media/scratch1/auger/code_hist2d \
    -fo /media/scratch1/auger/code_hist2d/fit_params.txt \
    -m shape.ok_and_3pmt.ok \
    -cbmax 6e6 \
    -plot /media/scratch1/auger/code_hist2d/fits
```

Full description:

	$ ./make_fits.py -- -h
	usage: make_fits.py [-h] [-src DIR_SRC] [-fo FNAME_OUT] [-plot DIR_FIG]
						[-m MASKNAME] [-cbmax CBMAX]

	Build 2D histograms of the relation between AoP and charge-histogram rates. It
	uses data of individual SD tanks.

	optional arguments:
	  -h, --help            show this help message and exit
	  -src DIR_SRC, --dir_src DIR_SRC
							input directory. Should be the output dir of
							"code_hist2d/est_build.H2d.txt.py" (default: None)
	  -fo FNAME_OUT, --fname_out FNAME_OUT
							output ASCII file, containing the fit parameters (as a
							function of the energy channel) (default: None)
	  -plot DIR_FIG, --plot DIR_FIG
							Use it if you want to generate figure files on DIR_FIG
							directory. If --plot is also used, the .png files will
							also be placed in DIR_FIG. (default: None)
	  -m MASKNAME, --maskname MASKNAME
							name of the quality cut option used in the previous
							processing stage: 'shape.ok_and_3pmt.ok', '3pmt.ok',
							or 'all'. We dont make quality cuts at this point.
							This is just to use the label of the previous stages
							of processing; it's healthy to keep track of it.
							(default: shape.ok_and_3pmt.ok)
	  -cbmax CBMAX, --cbmax CBMAX
							max value of color scale in the contour plots of the
							2D histograms (default: 6000000.0)



<!--- EOF -->
