#!/usr/bin/env ipython
"""
 author: j.j. masias meza
"""
from funcs import *
from datetime import datetime, timedelta
import os
from os import environ as env
import argparse
import shared_funcs.funcs as sf

#--- parse args
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter,
description="""
Fit the 2D-histograms of the correlations of Histograms with AoP.
""",
)
parser.add_argument(
'-src', '--dir_src',
type=str,
help='input directory. Should be the output dir of "code_hist2d/test_build.H2d.txt.py"',
)
parser.add_argument(
'-fo', '--fname_out',
type=str,
help='output ASCII file, containing the fit parameters (as a function \
of the energy channel)',
)
parser.add_argument(
'-plot', '--plot',
type=str,
metavar='DIR_FIG',
default='',
help='Use it if you want to generate figure files on DIR_FIG directory. \
If this argument is used, the .png files will also be placed in DIR_FIG.',
)
parser.add_argument(
'-m', '--maskname',
type=str,
default='shape.ok_and_3pmt.ok',
help='name of the quality cut option used in the previous processing stage: \
\'shape.ok_and_3pmt.ok\', \'3pmt.ok\', or \'all\'. We \
dont make quality cuts at this point. This is just to use the label of the \
previous stages of processing; it\'s healthy to keep track of it.'
)
parser.add_argument(
'-cbmax', '--cbmax',
type=np.float64,
default=6e6,
help='max value of color scale in the contour plots of the 2D histograms',
)
parser.add_argument(
'-fm', '--figmode',
type=str,
default='verb',
help='figure mode: "verb" (verbose info in figure) or "paper" (pretty stuff to look good)',
)
pa = parser.parse_args()

#-------------------------------------------
# open a sample input file to grab some parameters
fsample = '%s/H_00_%s.txt' % (pa.dir_src,pa.maskname)
AOP_AVR = float(sf.ReadParam_from_2dHist(fsample,'AoP_mean')[0])
xlim = [ (_/AOP_AVR - 1.)*100. for _ in map(float, sf.ReadParam_from_2dHist(fsample,'lim-AoP' )) ]
ylim = [ (_ - 1.)*100. for _ in map(float, sf.ReadParam_from_2dHist(fsample,'lim-rate')) ]
bins = int(sf.ReadParam_from_2dHist(fsample,'bins')[0])
date_ini_str = (sf.ReadParam_from_2dHist(fsample,'ini_date'))[:-1]
date_end_str = (sf.ReadParam_from_2dHist(fsample,'end_date'))[:-1]
ntanks_min   = int(sf.ReadParam_from_2dHist(fsample,'ntanks_min')[0])
assert pa.maskname == sf.ReadParam_from_2dHist(fsample, 'maskname')[0][1:-1]
#-------------------------------------------

# get datetimes of the borders
date_ini = datetime.strptime(date_ini_str, '%d/%b/%Y %H:%M')
date_end = datetime.strptime(date_end_str, '%d/%b/%Y %H:%M')
# estimate of the number of years
nyr      = (date_end - date_ini).days/365.

# energy discretization
nEd     = 50		# number of [deposited] energy channels
dE      = 1000./nEd	# [MeV] energy resolution

#--- build grid for the input 2D histograms
yval    = np.linspace(ylim[0], ylim[1], bins)
xval    = np.linspace(xlim[0], xlim[1], bins)
xvec    = np.empty(bins*bins)
yvec    = np.empty(bins*bins)
for i in range(bins):
    ini = i*bins
    fin = (i+1)*bins
    yvec[ini:fin] = yval
    xvec[ini:fin] = xval[i]*np.ones(bins)

#******************************************************
m       = np.empty(nEd, dtype=np.float64)
b       = np.empty(nEd, dtype=np.float64)

os.system('mkdir -p ' + pa.plot)
print " ---> reading from: %s\n" % pa.dir_src
for i_h in range(nEd):
    fname   = '%s/H_%02d_%s.txt' % (pa.dir_src, i_h, pa.maskname)
    # add 1.0 so that we can later make a plot in logarithmic scale
    # NOTE: log-scale is not possible if any number is <=0.0
    H       = np.loadtxt(fname) + 1.0

    H   = H.reshape(bins*bins)
    wei = np.sqrt(H)        # weight factors ~ 1/(number of data-points)
    p   = np.polyfit(xvec, yvec, 1, w=wei, cov=True)

    m[i_h]  = p[0][0]       # slope
    b[i_h]  = p[0][1]
    # in case we need the standard deviations of the
    # fit parameters, uncomment this:
    #cov    = p[1] # covariance matrix (2x2 shape)
    #err_m  = np.sqrt(cov[0,0])
    #err_b  = np.sqrt(cov[1,1])

    print " [%s] i: %02d/%02d" % (pa.maskname, i_h, nEd)
    stuff   = []
    title   = "Ed: %02d-%02d MeV"  % (i_h*dE, i_h*dE+dE)
    stuff   += [[m[i_h], b[i_h]]]
    stuff   += [[xvec, yvec]] # percentage units
    if pa.plot!='':
        assert os.path.isdir(pa.plot)
        sf.make_fitplot(
            fname_fig = pa.plot+'/hist2d_%02d.png' % i_h,
            fname_inp = fname, 
            stuff = stuff, 
            cblim = [1., pa.cbmax], 
            xylim = [xlim, ylim],
            title = title, 
            figsize = (6,3.5) if pa.figmode=='paper' else (6,4),
            xlabel = '$\Delta AoP$ [%]',
            ylabel = '$\Delta H_{id,e}$ [%]' if pa.figmode=='paper' else \
            'partial-integrated normalized rate [%]',
            xlabsize = 17,
            ylabsize = 17,
            flabel = '%d yr fit:\nm:%2.2f' % (int(nyr), m[i_h]),
            cblabel = 'points per bin square', 
        )

print "\n DONE! :-)"

#******************************************************
Ed          = np.arange(0.5*dE, 1000.+0.5*dE, dE)
data_out    = np.array([Ed, m, b]).T

HEADER = 'ini_date : %s\n'    % date_ini_str +\
         'end_date : %s\n'    % date_end_str +\
         'lim-AoP  : %g:%g\n' % (xlim[0], xlim[1]) +\
         'lim-rate : %g:%g\n' % (ylim[0], ylim[1]) +\
         'AoP_mean : %9.8g'   % AOP_AVR

np.savetxt(pa.fname_out, data_out, fmt='%8.7g', header=HEADER)
print "\n ---> generated: %s\n" % pa.fname_out

#EOF
