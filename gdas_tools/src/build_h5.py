#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
"""
 - Extract geopotencial-height data from GDAS (original) grib files
 - Save data of interest in a new .h5 file (one per year)

 NOTE:  grib files (one file per month) must be inside 'dir_dst', all 
        inside one subdirectory per year.

 author: jj masias m.
"""
#from pylab import *
import numpy as np
import pygrib
from datetime import datetime, timedelta
from glob import glob
from h5py import File as h5

#+++ original GDAS grib files directory
dir_inp_root = '/media/Elements/data_gdas'

#+++ output directory
dir_dst = '../out'

#+++ location of interest
Mlg_lon = 360. - 69.3 # longitude in domain (0, 360)
Mlg_lat = -35.3 # latitude in domain (-90, 90)
dlon, dlat = 5.1, 5.1 # dimension of square region of interest

#+++ period of interest
year_ini, year_end = 2006, 2013 #2006 #2013
#time_ini = datetime(year_ini, 1, 1)

time_ini = datetime(1970, 1, 1, 0, 0) # zero Unix time
LEVELS = (50, 100, 200, 300, 700, 850) #100 #700 # [mbar] isobaric surfaces we are interested in

h, t = {}, {}
for year in range(year_ini, year_end+1):
    dir_inp = '%s/%04d' % (dir_inp_root, year)
    # initialize dictionaries for each year to avoid memory accumulation
    for level in LEVELS:
        name = 'level_%04d' % level
        t[name], h[name] = {}, {}

    for month in range(1, 12+1):
        fname_inp = '%s/A*-%04d%02d.pgb.f00' % (dir_inp, year, month)
        fname_inp = glob(fname_inp)[0] # there's only one, take the first
        print "\n ---> NEXT: " + fname_inp
        g = pygrib.open(fname_inp)
        dname = '%04d-%02d' % (year, month)
        # initialize lists inside dictionaries
        for level in LEVELS:
            name = 'level_%04d' % level
            t[name][dname], h[name][dname] = [], []

        gg = g.readline()       # read 1st line/message
        while gg!=None:  # iterate over levels and days
            pname = gg['parameterName']
            sname = gg['shortName']
            level = gg['level']

            if sname=='gh' and (level in LEVELS): # only read geopotential height for these pressure 'LEVELS'
                yyyy, mm, dd = gg['year'], gg['month'], gg['day']
                HH, MM = gg['hour'], gg['minute']
                if level==LEVELS[0]:
                    print " --> date: %04d/%02d/%02d %02d:%02d" % (yyyy, mm, dd, HH, MM), "; t/utc_sec: %d" % rtime

                time = datetime(yyyy, mm, dd, HH, MM)
                rtime = (time - time_ini).total_seconds() # [UTC sec]
                lat, lon = gg.latlons() # latitude & longitude of data
                # filter the geographic [square] region 
                cc  =  (lat<(Mlg_lat+dlat/2.)) & (lat>(Mlg_lat-dlat/2.))
                cc  &= (lon<(Mlg_lon+dlon/2.)) & (lon>(Mlg_lon-dlon/2.))
                val = gg['values'] # [meters]
                name = 'level_%04d' % level
                h[name][dname] += [ val[cc].mean() ] # average in this square
                t[name][dname] += [ rtime ] # [UTC sec]

            gg = g.readline() # read next line/offset/message

        g.close() # to avoid troubles

    fname_out = '%s/test_%04d.h5' % (dir_dst, yyyy)
    print " -----> saving: " + fname_out
    #+++ write on file
    f5 = h5(fname_out, 'w')
    for level in LEVELS:
        lname = 'level_%04d' % level
        for dname in h[lname].keys():
            h[lname][dname] = np.array(h[lname][dname])
            t[lname][dname] = np.array(t[lname][dname])
            path = '%s/%s' % (lname, dname)
            f5['%s/t'%path] = t[lname][dname]
            f5['%s/h'%path] = h[lname][dname]

    f5.close()

print "\n ---> output: %s\n" % dir_dst
#EOF
