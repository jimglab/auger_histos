#!/usr/bin/env ipython
import funcs as ff
import os
from os import environ as env
from datetime import datetime, timedelta
from mpi4py import MPI
import argparse
from shared_funcs.funcs import arg_to_datetime


parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
'-ih', '--InpHsts',
type=str,
default='/media/hdd_extern_hegea/data_auger/_tmp_/intermediate/histos',
help='directory where we have the charge-histograms at\
 individual-SD level; and whichare a reduced version\
 of the `sd...root`.',
)
parser.add_argument(
'-im', '--InpMon',
type=str,
default='{HOME}/_tmp_/intermediate/monit'.format(**env),
help='directory of the `yyyy_mm_dd.nc` monit-files (more\
 friendly version of the `mc...root`).'
)
parser.add_argument(
'-o', '--dir_dst',
type=str,
default='{HOME}/auger/histogramas/reduced'.format(**env),
help='output directory, where to store the `red_yyyy_mm_dd.nc`',
)
parser.add_argument(
'-ini', '--ini_date', 
type=str, 
action=arg_to_datetime,
default='01/01/2006',
help='initial date',
)
parser.add_argument(
'-end', '--end_date', 
type=str, 
action=arg_to_datetime,
default='31/12/2015',
help='end date',
)
pa = parser.parse_args()


comm  = MPI.COMM_WORLD
rank  = comm.Get_rank()
wsize = comm.Get_size()

inp         = ff.gral()
inp.dir_src = pa.InpHsts
inp.dir_mon = pa.InpMon
inp.dir_dst = pa.dir_dst

inp.stations	= range(1, 2000+1)	 # array q quiero procesar
date_ini    = pa.ini_date #datetime(2014, 9,  26)   # start date
date_end    = pa.end_date #datetime(2015, 12, 31) # end date
date_bd = ff.equi_dates(date_ini, date_end, wsize)
inp.date_ini = date_bd[rank]
inp.date_end = date_bd[rank+1] - timedelta(days=1)

# report the repartition of jobs
print("")
for _r in range(wsize):
    print(" [*][rank:{rank}/{maxrank}] period for processing: {ini} - {end}".format(
        rank=_r, 
        maxrank=wsize-1,
        ini=inp.date_ini.strftime("%d/%b/%Y %H:%M:%S"),
        end=inp.date_end.strftime("%d/%b/%Y %H:%M:%S")
        ))
print("")

params = {}                   # optional
params['dt_in_min']     = 15  # [min] time window for sync with monit data
params['bins_out']      = 20  # [1] number of Ed bins for output
params['nbins_reduced'] = 10  # [MeV] width of the Ed bins in the input files "yyyy_mm_dd.nc" (*1)
mm = ff.massive_reduce(inp, params)
mm.process()

"""
(*1) if you modify this, you have to re-generate the yyyy_mm_dd.nc
     files (and previously, recompile with another 'nEd' in
     ../histos/src/clases.h)
"""
#EOF
