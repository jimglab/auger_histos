<!--- BOF -->

Script to:
* merge the charge-histograms and monit data (also perform the necessary time syncronization before merge).
* make ``multiplet'' corrections.
* filter those charge-histograms with bad PMT-flag (using monit/callibration data values), and AoP outliers.
* make partial integrals of the histogram counts, in a total of 20 bins of deposited energy.

For instance, to reproduce results in ICRC-2015 proceeding:
```bash
# i/o paths
Inp_hsts=/media/scratch1/auger/build_Histos         # <dir_dst1>, of build_Histos
Inp_mc=/media/scratch1/auger/build_mc               # <dir_dst2>, of build_mc
DataOut=/media/scratch1/auger/build_ReducedHistos   # <dir_dst3>

# this takes ~40min
mpirun -np 16 ./gral_mpi.py -- \
    --InpHsts $Inp_hsts \
    --InpMon $Inp_mc \
    --dir_dst $DataOut \
    -ini 01/01/2006 \
    -end 31/12/2013
```


The block inside the `red_yyyy_mm_dd.nc` output-files are key-named as `hist_id.{id:04d}` (data for each SD-tank `id`), whose contents are:
* `[:,0]`: time (gps or utc?)
* `[:,1]`: AoP values from the `sd..root` files.
* `[:,2]`: flag TubeMask (debemos escoger 7, 15, 23, 31, etc para q 3PMTs=ok).
* `[:,3]`: Pos of 1st max.
* `[:,4]`: Pos of 1st min.
* `[:,5]`: Width 1st max.
* `[:,6]`: Width 1st min.
* `[:,7]`: AoP values from the `mc..root` files.

For more details, see the method `generate_id_histos.build_file_scaler_sdid()` in `./funcs.py`.

<!--- EOF -->
