#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
"""
 author: j.j. masias meza

Las rutinas de la clase "generate_id_histos":
- leen la raw data (versiones .nc de los .root)
- corrigen los efectos de multiplicidad debido a errores
  del LS (ver gap de Bertou)
- genera nuevos .nc donde hay una matriz (tiempo, nbins_mev)
  por c/tanque q aporta data. Y solamente para los tanques q
  aportan data.
- tambien guarda las dimensiones correspondientes a las
  matrices de c/tanque
- los archivos se generan en los mismos tiempo q en los .nc
  originales! (NO HAY PROMEDIOS TEMPORALES)

"""
from pylab import *
from numpy import *
from scipy.io.netcdf import netcdf_file
import os
from datetime import datetime, time, timedelta
import numpy as np
from numpy import (
    sum, mean, median, zeros, ones,
    concatenate, nanmean, nansum, nanmedian,
    nanmin, nanmax, array, size
)
#import console_colors as ccl
import resource
resource.setrlimit(resource.RLIMIT_NOFILE, (4096, 4096)) # (*1)
# (*1): modifica el "ulimit" para poder guardar data de mas de 1024 detectores/variables en los .nc

class dirs:
    def __init__(self):
        comments = 'directorios'


class gral:
    pass


class generate_id_histos(object):
    def __init__(self, fname, inp, params=None):
        """
        OJO: hay parametros aqui q deben ser cambiados a
             a mano si algunas de las dimensiones de los 
             archivos input cambian (e.g. 'self.nreduced', 
             'self.ch_0').
        """
        MIN     = 60.               # [seg] 1min
        self.fname  = fname
        if os.path.isfile(fname):   # checko si existe archivo
            try:
                self.f      = netcdf_file(fname, 'r')
                self.file_ok    = True
            except:                 # fuck! (*2)
                print " ----> CORRUPTO: %s" % self.fname
                print " ABORTANDO..."
                self.file_ok    = False
        else:
            print " NO EXISTE: %s" % self.fname
            print " ABORTANDO..."
            self.file_ok    = False

        if self.file_ok:
            if params is None:  # default for icrc 2015
                self.dt_sync    = 15.*MIN   # [seg] ventana para sincronizacion con data de monitoreo
            else:
                self.dt_sync    = params['dt_in_min']*MIN

            self.yyyy       = inp.yyyy
            self.mm         = inp.mm
            self.dd         = inp.dd
            self.dir_dst    = inp.dir_dst
            self.dir_mon    = inp.dir_mon
            self.stations   = inp.stations
            self.nstations  = len(inp.stations)
            #------ agarremos la data de monitoreo
            if self.check_monit_files():        # chequeo si existen
                self.get_monit_files()      # declaro punteros a esos files
                self.grab_monit_data()      # leo la data de monitoreo
                #print " TERMINAMOS MONIT!"; raw_input()
            else:
                self.file_ok = False

        if self.file_ok: # all went ok above?
            self.dim_cols   = 'ncols'
            self.narrays    = 0
            self.ndummy     = 2         # nro inic de filas "dummies" solo para inicializar
            self.add_cols   = 7+1       # nro de cols adicionales a de los conteos
            if params==None: # default for icrc 2015
                self.bins_out   = 20    # en los scripts del directorio "../" es 20, no 10.
                self.nreduced   = 10    # nro de bines reducidos en el archivo input (*3)
            else:
                self.bins_out = params['bins_out']
                self.nreduced = params['nbins_reduced']

            self.nchE       = 1000/self.nreduced # nro canales en el file input
            self.nbins_mev  = 1000/self.bins_out # nro de canales de energia para el output file
            self.ncols      = self.nbins_mev + self.add_cols  # nro total de columnas
            self.fname_out  = '%s/red_%04d_%02d_%02d.nc' % (self.dir_dst, inp.yyyy, inp.mm, inp.dd)
            self.fout       = netcdf_file(self.fname_out, 'w')
            self.fout.createDimension(self.dim_cols, self.ncols)
            self.ch_0       = 13 # columna del 1er canal de energia, en el archivo input

        #------------------------------------------------
        self.nsave  = 0
    """
    (*2)    A veces, histos.cpp intenta recuperar data de un .root
            mal-cerrado.
            Por ej, en el caso del archivo "2012_06_21.root", despues
            de recuperar data, el codigo crashea sin llegar a cerrar
            el .nc que estuvo generando.
    (*3)    Lo que grabe en el archivo input "yyyy_mm_dd.nc" es una
            version reducida de los histogramas originales. Hice
            integrales parciales en anchos de 'self.nreduced'. Por
            ej, para self.nreduced=10, el archivo input tiene
            100 canales, de los 1000 originales en los .root
    """


    # chekeo si existen los archivos de monitoreo para hoy y ayer.
    def check_monit_files(self):
        """ checkea si *existen* los inputs """
        try:
            hoy = datetime(self.yyyy, self.mm, self.dd, 0,0) + timedelta(days=1)
            ayer    = hoy - timedelta(days=1)
            yyyy    = ayer.year
            mm  = ayer.month
            dd  = ayer.day
            self.fname_monit_ayer = '%s/%04d/%04d_%02d_%02d.nc' % (self.dir_mon, yyyy, yyyy, mm, dd)
            self.ok_monit_ayer  = os.path.isfile(self.fname_monit_ayer)
        except:
            self.ok_monit_ayer  = False

        self.fname_monit_hoy    = '%s/%04d/%04d_%02d_%02d.nc' % (self.dir_mon, hoy.year, hoy.year, hoy.month, hoy.day)
        self.ok_monit_hoy       = os.path.isfile(self.fname_monit_hoy)
        self.ok_monit       = (self.ok_monit_ayer or self.ok_monit_hoy)

        return self.ok_monit    # True si al menos uno de los 2 archivos existe; False otherwise.


    def get_monit_files(self):
        """ 
        declare pointers to files (works OK together with
        self::grab_monit_data() method) 
        """
        self.ok_both = self.ok_one = False
        try:
            print " leyendo:\n", self.fname_monit_ayer, self.fname_monit_hoy
            os.system('ls -lhtr %s %s' % (self.fname_monit_ayer, self.fname_monit_hoy))
            self.finp_monit_hoy  = netcdf_file(self.fname_monit_hoy , 'r')
            self.finp_monit_ayer = netcdf_file(self.fname_monit_ayer, 'r')
            self.ok_both = True
        except:
            try:
                self.finp_monit_ayer = netcdf_file(self.fname_monit_ayer, 'r')
                self.ok_one = True
            except:
                self.ok_monit = False # existen, pero estan corruptos
                print " files de monitoreo CORRUPTOS! :("
        print " --> ok_monit: ", self.ok_monit
        print " --> ok_both/ok_one : ", self.ok_both, "/", self.ok_one


    def grab_monit_data(self):
        """ 
        declare pointers to data && works OK whether 'self.finp_monit_hoy' ||
        'self.finp_monit_ayer' points to somewhere or not.
        """
        # inicializo diccionarios:
        self.mon_t       = {}
        self.mon_aop     = {}
        self.mon_pmtStat = {} # status of PMTs
        print " self.ok_monit_ayer:", self.ok_monit_ayer
        print " self.ok_monit_hoy :", self.ok_monit_hoy
        # leo data de c/tanque
        for id in self.stations:
            ok      = False
            ok_ayer = False
            ok_hoy  = False
            #----------------- unificamos la data de ayer y hoy
            try:
                if self.ok_monit_ayer:                  # el archivo existe
                    try:
                        dataid_ayer = self.finp_monit_ayer.variables['m_id.%04d'%id].data
                        ok_ayer     = True
                    except:
                        ok_ayer     = False

                if self.ok_monit_hoy:                       # el archivo existe
                    try:
                        dataid_hoy = self.finp_monit_hoy.variables['m_id.%04d'%id].data
                        ok_hoy     = True
                    except:
                        ok_hoy     = False

                if (ok_ayer & ok_hoy):  # si este tanque tiene data para ambos dias
                    dataid          = concatenate([dataid_ayer, dataid_hoy], axis=0)
                    del dataid_ayer
                    del dataid_hoy
                elif ok_ayer:           # adopto la data de uno de los 2 dias
                    dataid          = dataid_ayer
                    del dataid_ayer
                elif ok_hoy:            # o bien solo hay data de hoy o ninguno de ambos dias.
                    dataid          = dataid_hoy    # esto falla si no hay data hoy tampoco, y entonces
                    del dataid_hoy                  # dire q " no data for id".
                if ok_ayer or ok_hoy:
                    ok              = True          # llego aqui si adopte data de por lo menos uno de los 2 dias
                    print " [id:%04d] ayer/hoy: %d/%d" % (id, int(ok_ayer), int(ok_hoy))
            except:
                print " [id:%04d] no data :*(" % id
                #continue

            #----------------------- ahora si leo la data unificada 'dataid'
            if ok:
                #print " --> grabbing data.... "; raw_input()
                if len(dataid)>1:
                    #print " IM HERE!"
                    nid                    = size(dataid, axis=0)
                    name                   = '%04d' % id
                    self.mon_t[name]       = array(dataid[:,0])  # (*1)
                    self.mon_pmtStat[name] = array(dataid[:,1])  # (*1)
                    self.mon_aop[name]     = array(dataid[:,2])  # (*1)
                else:
                    print " [id:%04d] no data" % id

        #--- cerramos archivos de monitoreo
        if self.ok_both:
            self.finp_monit_hoy.close()
            self.finp_monit_ayer.close()
        if self.ok_one:
            self.finp_monit_ayer.close()
        """
        (*1) columnas en la matriz 'm_%04d' de los archivos de monitoreo:
            col0: utc seg
            col1: flag del tanque (estado de los pmts; 7, 15, 23, 31, etc for 3PMTs=ok)
            col2: <AoP>_{3pmts}
        """


    def __del__(self):
        if self.file_ok:
            print " ----> cerrando: %s" % self.f.filename
            self.f.close()
            #---- guardamos otros parametros:
            self.fout.createDimension('1', 1)
            self.write_variable('nbins_mev', '1', self.nbins_mev, 'i', '[1]')
            #---- cerramos archivo de salida
            print " ----> cerrando: %s" % self.fout.filename
            self.fout.close()
            del self.f
            del self.fout

        print " Eliminando objeto: generate_id_histos()"
        print " tanques guardados: %d" % self.nsave


    def get_histos(self):
        self.m  = np.array(self.f.variables['matrix'].data)
        self.nx = size(self.m, axis=0)
        self.ny = size(self.m, axis=1) + 1

        ch_0000     = self.ch_0
        ch_1000     = self.ch_0 + self.nchE - 1
        nothing     = np.zeros(self.nx, dtype=bool)
        #------ iteramos sobre el array
        nstart      = 800           # inicializo
        self.hid_phys   = np.zeros((nstart, self.ny))
        self.ntot   = 0
        for i in range(self.nstations):
            self.id = self.stations[i]
            ss  = (self.m[:,1] == self.id)  # selecciono data de este 'id'

            if prod(ss==nothing, dtype=bool):
                print " id: %04d no aporta data!" % self.id

            else:
                self.hid    = self.m[ss, :]         # histos de este 'id'
                self.nhistos    = size(self.hid, 0)     # nro de histos de este 'id'
                MpletOk = self.calc_multiplet_order()
                if not(MpletOk):
                    continue

                print " id: %04d, singletes/histos: %d/%d" % (self.id, self.N_1er, self.nhistos)
                hid_cnts    = divide(self.hid[:, ch_0000:ch_1000+1].T, self.orden_int).T # conteos corregidos
                self.sync_w_monit()             # construye 'self.aop' (*1)
                stuff       = self.hid[:, 0:ch_0000]
                hid_corr    = concatenate([stuff, self.aop, hid_cnts], axis=1) # concatena en direcc horizontal
                self.ncorr  = size(hid_corr, 0)
                self.hid_phys_check()
                ini = self.ntot
                fin = ini + self.ncorr
                self.hid_phys[ini:fin] = hid_corr
                self.ntot += self.ncorr

        # lo mismo q tenia en el archivo input, pero ahora los conteos estan
        # corregidos por efecto de "multipletes"
        self.hid_phys   = self.hid_phys[:self.ntot]
        print ' ----> shape    :', shape(self.hid_phys)
        self.build_file_scaler_sdid()
        """
        (*1)    sincronizado con los tiempos de los histogramas
                de este tanque 'self.id'. Los tiempos de los
                histogramas de este tanque estan en 'self.hid'
        """


    def sync_w_monit(self):
        """
        sincronizamos los tiempos de monitoreo con los histogramas 
        del tanque 'self.id'
        """
        t_hid    = self.hid[:,0]    # tiempos de los histos de este tanque
        self.aop = np.nan*np.ones(t_hid.size)   # flags NaN por defecto
        name     = '%04d'%self.id
        if name in self.mon_t.keys():   # solo puedo sincronizar este tanque si esta presente en los monit
            t_mon    = self.mon_t[name]
            for i in range(t_hid.size): # recorro los tiempos de los histogramas
                t    = t_hid[i]
                cc   = (t_mon>(t-0.5*self.dt_sync)) & (t_mon<(t+0.5*self.dt_sync))
                if len(find(cc))>0: # para evitar q promedie un array vacio
                    aop  = self.mon_aop[name][cc]
                    self.aop[i] = np.nanmean(aop) # promedio los datos validos en esta ventana 'self.dt_sync'

        self.aop = self.aop.reshape(t_hid.size,1) # para poder concatenarlo despues con 'stuff'


    def hid_phys_check(self):
        nphys   = size(self.hid_phys, 0)
        if self.ntot+self.ncorr >= nphys:
            aux         = np.array(self.hid_phys[:self.ntot])
            self.hid_phys   = np.zeros((2*nphys, self.ny))
            self.hid_phys[:self.ntot] = aux
            del aux
            self.hid_phys_check()


    def build_file_scaler_sdid(self):
        dche    = self.bins_out/self.nreduced
        che     = self.ch_0 + np.array(range(0,self.nchE+dche,dche)) # rejilla de energias final
        nE      = len(che)
        avr_cnts= np.zeros(self.ntot*(nE-1)).reshape(self.ntot, nE-1)
        for i in range(nE-1):
            avr_cnts[:,i] = np.sum(self.hid_phys[:,che[i]:che[i+1]], axis=1)    # integral por bandas de ancho 'dE' MeV
        """
        cols:
        $0 : tiempo
        $3 : (VEM charge)/(VEM peak) (values from the 'sd..root')
        $4 : flag TubeMask (debemos escoger 7, 15, 23, 31, etc para q 3PMTs=ok)
        $9 : Pos of 1st max
        $10: Pos of 1st min  (**)
        $11: Width 1st max
        $12: Width 1st min   (**)
        (**): estos ayudan a seleccionar "categorias" de histogramas!
        """
        cols = (0,3,4,9,10,11,12,13) # columnas diagnostico q me interesan (*)
        # es una solucion medio chancha pero hace lo q quiero:
        all = np.concatenate([self.hid_phys[:,cols], avr_cnts], axis=1) # columna tiempo y la 1er cuenta integral
        print " all.shape: ", shape(all)
        #(*): El tamanio de esto debe ser igual a 'self.add_cols' en 
        #     self.__init__(). Notar que en `cols = (...,13)` agregue
        #     el `13` debido a q estoy adjuntando la data de AoP (valores
        #     de los 'mc..root') en la linea 294, en la rutina 
        #     self.get_histos().

        # creamos 1 matriz por cada tanque:
        nothing = np.zeros(size(self.ntot), dtype=bool)
        units   = '[cnts/m2/s/%3.1fMeV]' % (1.*dche*self.nreduced)
        self.nsave = 0          # quiero contar el nro de array q guardo
        for i in range(self.nstations):
            id  = self.stations[i]
            ss  = self.hid_phys[:,1]==id
            if not(prod(ss==nothing, dtype=bool)):
                var = all[ss,:]
                varname = 'hist_id.%04d' % (id)
                dimname = 'nhist_id.%04d' % (id)
                ndim    = size(var, 0)
                self.fout.createDimension(dimname, ndim)
                dims    = (dimname, self.dim_cols)
                self.write_variable(varname, dims, var, 'd', units)
                self.nsave += 1
        # (*)   $10: Pos of 1st min
        #       $12: Width 1st min
        #       $13: AoP de los archivos "mc..root"


    #---- escribe en 'self.fout'
    def write_variable(self, varname, dims, var, datatype, comments):
        dummy           = self.fout.createVariable(varname, datatype, dims)
        dummy[:]        = var
        dummy.units     = comments


    def calc_multiplet_order(self):
        ch_240  = self.ch_0 + 240/self.nreduced
        ch_200  = ch_240 - 40/self.nreduced
        ch_300  = ch_240 + 60/self.nreduced
        avr_240 = mean(self.hid[:, ch_200:ch_300+1], axis=1) # tasas medias entre 200:300 MeV
        min_rate_240    = min(avr_240)
        self.N_1er      = 0
        mean_rate_1er   = 0.0

        cc              = round_(avr_240/min_rate_240) == 1.0
        mean_rate_1er   = np.mean(avr_240[cc])
        self.N_1er      = sum(cc)

        if self.N_1er==0:
            print " ---> NO PRESENTA DATA!"
            return False
                                # singletes en el rango 200-300 MeV
        self.orden      = avr_240/mean_rate_1er
        self.orden_int  = np.round_(self.orden)     # orden del multiplete (a cada tiempo)
        return True


#***************************************************************************
class fechas:
    def __init__(self, yyyy, mm, dd):
        self.yyyy   = yyyy
        self.mm     = mm
        self.dd     = dd
    def __init__(self):
        self.yyyy = -1


#***************************************************************************
def date_to_utc(fecha):
    utc = datetime(1970, 1, 1, 0, 0, 0, 0)
    time = (fecha - utc).total_seconds()

    return time



#***************************************************************************
#***************************************************************************
class massive_reduce(object):
    def __init__(self, inp, params=None):
        """
        self.dir_src    = inp.dir_src
        self.dir_mon    = inp.dir_mon
        self.dst        = inp.dir_dst
        self.stations   = inp.stations
        """
        for name in dir(inp):
            setattr(self, name, getattr(inp, name))

        #----------------
        self.params = params
        if self.params==None:
            print "\n ###### running with defaults settings (ICRC 2015 proceeding) ###### \n"
        else:
            print "\n ###### running with non-default settings ######\n"
            print self.params


    def process(self):
        inp = gral() #fechas()
        date = self.date_ini
        while date < self.date_end:
            yyyy, mm, dd = date.year, date.month, date.day
            dir_dst2     = '{dst}/{year}'.format(dst=self.dir_dst, 
                          year=yyyy)
            #os.makedirs(dir_dst2) # mkdir -p ..
            os.system('mkdir -p %s' % dir_dst2)

            fname_inp    = '{src}/{year}/{year}_{mm:02d}_{dd:02d}.nc' .format(src=self.dir_src, year=yyyy, mm=mm, dd=dd)
            inp.yyyy     = yyyy
            inp.mm       = mm
            inp.dd       = dd
            inp.dir_mon  = self.dir_mon
            inp.dir_dst  = dir_dst2
            inp.stations = self.stations
            m = generate_id_histos(fname_inp, inp, self.params)
            if m.file_ok:
                m.get_histos()
            del m           # para q cierre los archivos y etc.
            print " ************* OBJETO ELIMINADO ****************"

            date += timedelta(days=1)   # next day

    """
    def process(self):
        inp = fechas()
        for yyyy in self.YYYY:
            dir_dst2    = '%s/%04d' % (self.dir_dst, yyyy)
            os.system('mkdir -p %s' % dir_dst2)
            for mm in self.MM:
                for dd in self.DD:
                    fname_inp   = '%s/%04d/%04d_%02d_%02d.nc' % (self.dir_src, yyyy, yyyy, mm, dd)
                    inp.yyyy    = yyyy
                    inp.mm      = mm
                    inp.dd      = dd
                    m = generate_id_histos(fname_inp, self.dir_mon, dir_dst2, self.stations, inp, self.params)
                    if m.file_ok:
                        m.get_histos()
                    del m           # para q cierre los archivos y etc.
                    print " ************* OBJETO ELIMINADO ****************"
    """

def equi_days(dini, dend, n):
    """
    returns most equi-partitioned tuple of number 
    of days between dates 'dini' and 'dend'
    """
    days = (dend - dini).days
    days_part = np.zeros(n, dtype=np.int)
    resid = np.mod(days, n)
    for i in range(n-resid):
        days_part[i] = days/n

    # last positions where I put residuals
    last = np.arange(start=-1,stop=-resid-1,step=-1)
    for i in last:
        days_part[i] = days/n+1

    assert np.sum(days_part)==days, \
        " --> somethng went wrong!  :/ "

    return days_part


def equi_dates(dini, dend, n):
    """
    returns:
    - inis[:-1]: 'n' start-dates of most equi-partitioned 
                 contiguous blocks of dates between 'dini' 
                 and 'end'
    - inis[-1]: 'dend' (max date of whole block)
    """
    days_part = equi_days(dini, dend, n)
    inis = np.empty(n+1, dtype=datetime)

    inis[0]  = dini
    inis[-1] = dend
    for i in range(1, inis.size-1):
        inis[i] = inis[i-1] + timedelta(days=days_part[i-1])

    return inis

#***************************************************************************
#***************************************************************************
#
