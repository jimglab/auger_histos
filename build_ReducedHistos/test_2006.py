#!/usr/bin/env ipython
#***************************************************************************
import funcs as ff
import os
#***************************************************************************
home = os.environ['HOME']
this_dir = home+'/ccin2p3/in2p3_data/data_auger/procesamiento/histos.aop_all'
inp	= ff.fechas()
#----------------
dirs_all	= ff.dirs()
dirs_all.dir_src = '%s/out/out.build_Histos' % this_dir
dirs_all.dir_mon = '%s/out/out.build_mc.nc' % this_dir
dirs_all.dir_dst = '%s/out/out.build_ReducedHistos' % this_dir

stations	= range(1, 2000+1)	# array q quiero procesar
DD		    = range(1, 31+1)
MM		    = range(1, 12+1)
YYYY		= range(2006, 2006+1)
#
params = {}                     # optional
params['dt_in_min']     = 15    # [min] time window for sync with monit data
params['bins_out']      = 20    # [1] number of Ed bins for output
params['nbins_reduced'] = 10    # [MeV] width of the Ed bins in the input files "yyyy_mm_dd.nc" (*1)
mm = ff.massive_reduce(dirs_all, stations, YYYY, MM, DD, params)
mm.process()

"""
(*1) if you modify this, you have to regenerate the yyyy_mm_dd.nc 
     files (and previously, recompile with another 'nEd' in 
     ../histos/src/clases.h)
"""
#EOF
