#!/usr/bin/env ipython
#***************************************************************************
import funcs as ff
import os
from os import environ as env
from datetime import datetime
#***************************************************************************
inp         = ff.gral()
inp.dir_src = '/media/hdd_extern_hegea/data_auger/_tmp_/intermediate/histos'
inp.dir_mon = '{HOME}/_tmp_/intermediate/monit' .format(**env)
inp.dir_dst = '{HOME}/auger/histogramas/reduced'.format(**env)

inp.stations	= range(1, 2000+1)	        # array q quiero procesar
inp.date_ini    = datetime(2006, 1, 1)      # start date
inp.date_end    = datetime(2006, 12, 31)    # end date

params = {}                     # optional
params['dt_in_min']     = 15    # [min] time window for sync with monit data
params['bins_out']      = 20    # [1] number of Ed bins for output
params['nbins_reduced'] = 10    # [MeV] width of the Ed bins in the input files "yyyy_mm_dd.nc" (*1)
mm = ff.massive_reduce(inp, params)
mm.process()

"""
(*1) if you modify this, you have to regenerate the yyyy_mm_dd.nc
     files (and previously, recompile with another 'nEd' in
     ../histos/src/clases.h)
"""
#EOF
