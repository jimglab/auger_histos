# misc

Scripts for misc figures, and other purposes.

---
typical charge histogram:

```bash
./typical_histogram.py
```

---
Smoothed long-term profiles of:
    * scalers from histograms
    * muons from histograms
    * scalers, corrected by AoP and pressure
    * scalers, uncorrected (raw-scalers)

NOTE: we are using outputs of [this script](build_temp.corr/ch_Eds_smoo2.py) for histograms data.

TODO: path for scaler files are not official (we need to build the final scalers; bug pending), 
      so not included in argparse yet.

```bash
export HSCLS=$AUGER_REPO/out/out.build_temp.corr/ascii/smoo2_shape.ok_and_3pmt.ok_60-120.MeV.txt
export HMUON=$AUGER_REPO/out/out.build_temp.corr/ascii/smoo2_shape.ok_and_3pmt.ok_200-280.MeV.txt
./bands_all_smoo.py -- -hscls $HSCLS -hmuon $HMUON -fig ../figs/bands_all_smoo.png
```

<!--- EOF -->
