#!/usr/bin/env ipython
from pylab import *
import numpy as np
import matplotlib.patches as patches
import matplotlib.transforms as transforms
from matplotlib.patches import Ellipse
import os

maskname  = 'shape.ok_and_3pmt.ok'
#fname_avr = '../../code_figs/avr_histos_%s.txt' % maskname
fname_avr = '{AUGER_REPO}/out/out.build_avr_histos/{maskname}/15min/avr_histos_{maskname}.txt'.format(maskname=maskname, **os.environ)

prom	  = np.loadtxt(fname_avr)
typic	  = np.mean(prom[:,1:], axis=0)

nE	= len(typic)
Ed	= arange(10., 1000., 1000./nE)

fig	= figure(1, figsize=(6,4))
ax	= fig.add_subplot(111)

muon_rate = typic[11] 
ax.plot(Ed, typic/muon_rate, '-o', c='black', ms=3.)
#el = Ellipse((.55, -5.5), 0.5, 0.5)     # figura dummy para referencia una flecha
#-------- baja energia
trans   = transforms.blended_transform_factory( 
		ax.transData, ax.transAxes)
rect1   = patches.Rectangle((60.0, 0.0), width=60.0, height=1,
		transform=trans, color='red', alpha=0.3)
ax.add_patch(rect1)
#-------- alta energia
trans   = transforms.blended_transform_factory( 
		ax.transData, ax.transAxes)
rect1   = patches.Rectangle((200.0, 0.0), width=80.0, height=1,
		transform=trans, color='blue', alpha=0.3)
ax.add_patch(rect1)

#-------- limites <-> baja energia
ax.annotate("low energy\n  band $H_{sc}$\n(60-120)MeV", xy=(45., 0.06), fontsize=20) 
ax.annotate("", xy=(120., 0.03), xytext=(60., 0.03),
		arrowprops=dict(arrowstyle="<->", lw=2))
"""ax.annotate('low energy\nband', xy=(100., 0.3),  xycoords='data', 
		xytext=(60, 50), textcoords='offset points',
		size=25,
		arrowprops=dict(arrowstyle="fancy",
			fc="0.3", ec="none", 
			patchB=el, alpha=.9,
			connectionstyle="angle3,angleA=0,angleB=-90"),
		)"""

#-------- limites <-> alta energia
ax.annotate("high energy\n   band $H_\mu$\n(200-280)MeV", xy=(160., 0.0015), fontsize=20) 
ax.annotate("", xy=(280., 0.03), xytext=(200, 0.03),
		arrowprops=dict(arrowstyle="<->", lw=2))

#---------------------
LABELSIZE=17
ax.grid()
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_ylabel('normalized counts', fontsize=LABELSIZE)
ax.set_xlabel('deposited energy $E_d$ [MeV]', fontsize=LABELSIZE)

#XLABELS = ["60", "120.", "200."]
XLABELS = [15, 30, 60, 120, 240, 480, 960]
ax.set_xticks(XLABELS)
ax.set_xticklabels(XLABELS)
ax.set_xlim(30., 1000.)

fname_fig = './histos_two.bands.limits.png'
savefig(fname_fig, dpi=135, bbox_inches='tight')
#savefig('%s.eps'%fname_fig, format='eps', dpi=135, bbox_inches='tight')
close()
#EOF
