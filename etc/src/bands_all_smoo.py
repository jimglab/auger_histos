#!/usr/bin/env ipython
from pylab import find, show, close, figure, plot
import numpy as np
import os, argparse

#--- retrieve args
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
'-hscls', '--fname_hsts_scals',
type=str,
default='{PAO}/out/out.build_temp.corr/ascii/'.format(**os.environ)+\
        'smoo2_shape.ok_and_3pmt.ok_60-120.MeV.txt',
help='input for scalers-from-histograms',
)
parser.add_argument(
'-hmuon', '--fname_hsts_muon',
type=str,
default='{PAO}/out/out.build_temp.corr/ascii/'.format(**os.environ)+\
        'smoo2_shape.ok_and_3pmt.ok_200-280.MeV.txt',
help='input for muons-from-histograms',
)
parser.add_argument(
'-fig', '--fname_fig',
type=str,
default='../figs/bands_all_smoo.png',
)
pa = parser.parse_args()


PAO    = os.environ['PAO']
fname_hscls     = pa.fname_hsts_scals
fname_hmuon     = pa.fname_hsts_muon
#TODO: correct this scaler-path for the official scalers (bug related)
fname_scals 	= '%s/actividad_solar/neutron_monitors/figuras/NMs_res_15days.dat' % os.environ['HOME']
fname_scls      = '%s/data_auger/estudios_AoP/data/unir_con_presion/data_final_2006-2013.dat' % PAO
t_sc,   r_sc    = np.loadtxt(fname_hscls).T
t_mu,   r_mu    = np.loadtxt(fname_hmuon).T
t_scls, r_scls  = np.loadtxt(fname_scals, usecols=(0,5)).T
t_scls 		= (t_scls - 1136073600)/(86400.*365.)
r_scls		/= 1.*np.nanmean(r_scls)
utc_jan06       = 1136073600.

#-------- scalers
#data	= loadtxt(fname_all, unpack=True)
tsc, sc_woaop_wpres, sc_waop_wpres = np.loadtxt(fname_scls, usecols=(0,4,5), unpack=True)  # only scalers
tsc 	= (tsc - utc_jan06) / (86400.*365.)		# [yrs since jan.2006]
sc_woaop_wpres /= 1.0*np.nanmean(sc_woaop_wpres)
TMAX 	= tsc[-1]
dt	= 0.0666
sc_wo   = []
tsc_wo	= []
tt      = 0.5*dt
while tt<TMAX:
	cc      = (tsc>tt-0.5*dt) & (tsc<tt+0.5*dt)
	sc_wo   += [ np.nanmean(sc_woaop_wpres[cc]) ]
	tt	+= dt
	tsc_wo	+= [tt]
sc_wo 	= np.array(sc_wo)
tsc_wo	= np.array(tsc_wo)
#----------------



fig	= figure(1, figsize=(6, 4))
ax	= fig.add_subplot(111)
ax2 = ax.twinx()

LW=4
ax.plot(t_scls, r_scls, '-', color='black',  label='scalers',  lw=3., alpha=.8, mec='None')
ax.plot(t_sc, r_sc, '-', color='red',  label='$H_{sc}$: (60-120)MeV',  lw=LW, alpha=.7, mec='None')
ax.plot(t_mu, r_mu, '-', color='blue',   label='$H_{\mu}$: (200-280)MeV', lw=LW, alpha=.5, mec='None')
ax2.plot(tsc_wo, sc_wo, '-', alpha=0.6, mec='None', color='gray', lw=3.,	label='uncorrected Scalers')

LABELSIZE=15
ax.legend(loc='best')
ax.set_xlabel('years since Jan/2006', fontsize=LABELSIZE)
ax.set_ylabel('normalized rate', fontsize=LABELSIZE)
ax2.set_ylabel('uncorrected Scalers', fontsize=LABELSIZE)
ax.set_ylim(0.96, 1.03)
ax.set_xlim(0., 8.)
ax.grid()

#fname_fig = '../figs/bands_solphys'

fig.savefig(pa.fname_fig, format='png', dpi=135, bbox_inches='tight')
#savefig('%s.eps'%fname_fig, format='eps', dpi=135, bbox_inches='tight')
print " --> output:\n "+pa.fname_fig

close()
#EOF
